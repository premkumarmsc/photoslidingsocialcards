//
//  Config.m
//  PhotoSliding
//
//  Created by ephronsystems on 8/21/13.
//
//

#import "Config.h"

@implementation Config
+(NSString *)get_token
{
    
    // NSString *str = @"E,PH,RO,N1,3S,Y,S5,4,321";//
    
    NSString *str1 = SNG_API_KEY; // 16 Digit Code
    
    NSMutableString *mu = [NSMutableString stringWithString:str1];
    [mu insertString:@"," atIndex:1];
    [mu insertString:@"," atIndex:4];
    [mu insertString:@"," atIndex:7];
    [mu insertString:@"," atIndex:10];
    [mu insertString:@"," atIndex:13];
    [mu insertString:@"," atIndex:15];
    [mu insertString:@"," atIndex:18];
    [mu insertString:@"," atIndex:20];
    
  //  ////(@"MUT:%@",mu);
    
    
    NSArray  *testArray2 = [mu componentsSeparatedByString:@","];
    NSArray  *algorithm_arr=[NSArray arrayWithObjects:@"7",@"1",@"3",@"5",@"4",@"1",@"6",@"9",@"4", nil];
    
   // ////(@"TEST Array 2:%@",testArray2);
    
    
    NSMutableString* message = [NSMutableString stringWithCapacity:100];
    
    
   // ////(@"MESSAGEGHGH:%@",message);
    for (int j=0; j<[algorithm_arr count]; j++)
    {
        NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        NSMutableString *randomString = [NSMutableString stringWithCapacity: [algorithm_arr[j]intValue]];
        
        for (int i=0; i<[algorithm_arr[j]intValue]; i++)
        {
            [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random() % [letters length]]];
        }
        
       // ////(@"RANDOM:%@",randomString);
        
        
        NSString *arr_str=testArray2[j];
        
        [message appendString:[NSString stringWithFormat:@"%@%@",testArray2[j],randomString]];
        
        // [message deleteCharactersInRange:NSMakeRange([message length] - 1, 1)];
        
        
        
    }
    
    
   // ////(@"MESSAGE:%@",message);
    
    
    
    NSData *plainTextData = [message dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64String = [plainTextData base64EncodedString];
    
   // ////(@"RET:%@",base64String);
    
    //////(@"MD5:%@", [message MD5]);
    
    return base64String;

}
@end
