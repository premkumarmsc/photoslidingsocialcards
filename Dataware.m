//
//  Dataware.m
//  CoreData
//
//  Created by iMedDoc on 8-2-10.
//  Copyright 2010 Adya Solutiona. All rights reserved.
//

#import "Dataware.h"

@implementation Dataware
@synthesize databaseName, databasePath;

- (id) initDataware
{
	databaseName = @"SNG.sqlite";
	NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDir = [documentPaths objectAtIndex:0];
	databasePath = [documentsDir stringByAppendingPathComponent:databaseName];
	[self checkAndCreateDatabase];
		///[self GetFileUpdates];
	return self;	
}

- (void) GetFileUpdates
{
	NSString *updatefile = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"dbupdate.txt"];
	BOOL success;
	NSFileManager *fileManager = [NSFileManager defaultManager];
	success = [fileManager fileExistsAtPath:updatefile];
	if(success)
	{
		NSString *strFile = [NSString stringWithContentsOfFile:updatefile usedEncoding:nil error:nil];
		NSMutableArray *ArrQuery = [strFile componentsSeparatedByString:@";"];
		
		for(int i=0;i<[ArrQuery count]-1;i++)
		{
			sqlite3_stmt *sqlStmt = [self OpenSQL:[[ArrQuery objectAtIndex:i] UTF8String]];		
			if(sqlStmt != nil)sqlite3_step(sqlStmt); 
			sqlite3_finalize(sqlStmt);
		}
		
		[[NSFileManager defaultManager] removeItemAtPath:updatefile error:nil];
	}	
}

- (NSString *)applicationDocumentsDirectory 
{	
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}

-(void) checkAndCreateDatabase
{
	BOOL success;
	NSFileManager *fileManager = [NSFileManager defaultManager];
	success = [fileManager fileExistsAtPath:databasePath];
	if(success) return;
	NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:databaseName];
	[fileManager copyItemAtPath:databasePathFromApp toPath:databasePath error:nil];	
	//[fileManager release];
}

-(BOOL)CheckValidDatabase:(NSString *)FileName
{
	NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDir = [documentPaths objectAtIndex:0];
	NSString *sPath = [documentsDir stringByAppendingPathComponent:@"SNG.sqlite"];
	databasePath = [documentsDir stringByAppendingPathComponent:FileName];

	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"ddMMyyyyHHmmss"];
    NSString *renamepath = [documentsDir stringByAppendingPathComponent:[NSString stringWithFormat:@"ndotsql%@.sqlite",[formatter stringFromDate:[NSDate date]]]];
	
	sqlite3_stmt *compiledStatement=[self OpenSQL:"select count(*) from tb_history"];
	if(!compiledStatement)
	{
		[self CloseSQL];
		[[NSFileManager defaultManager] removeItemAtPath:databasePath error:nil];
		return NO;
	}
	else 
	{
		[self CloseSQL];
		[[NSFileManager defaultManager] moveItemAtPath:sPath toPath:renamepath error:nil];
		[[NSFileManager defaultManager] moveItemAtPath:databasePath toPath:sPath error:nil];
		return YES;
	}
}

- (sqlite3_stmt *) OpenSQL:(const char *)stmt
{
	sqlite3_stmt *compiledStatement;
	const char *sqlStatement = stmt;
	if(sqlite3_open([databasePath UTF8String], &dbsql) == SQLITE_OK)
	sqlite3_prepare_v2(dbsql, sqlStatement, -1, &compiledStatement, NULL);
	return compiledStatement;
}

- (NSInteger) GetInsertId
{
	return sqlite3_last_insert_rowid(dbsql);
}

- (BOOL) Execute:(const char *)stmt
{
	sqlite3_stmt *compiledStatement;
	const char *sqlStatement = stmt;
	if(sqlite3_open([databasePath UTF8String], &dbsql) == SQLITE_OK)
	{
	if (sqlite3_prepare_v2(dbsql, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
	{
		sqlite3_finalize(compiledStatement);
		return YES;
	}
	}
	else
	{
		////////(@"connection failure");
	}
	return NO;
}

- (NSInteger) GetCode:(const char *)stmt
{
	sqlite3_stmt *compiledStatement;
	const char *sqlStatement = stmt;
	NSInteger iCode=0;
	
	if(sqlite3_open([databasePath UTF8String], &dbsql) == SQLITE_OK)
	{
		sqlite3_prepare_v2(dbsql, sqlStatement, -1, &compiledStatement, NULL);
		if(compiledStatement != nil)		
		if(sqlite3_step(compiledStatement) == SQLITE_ROW) 
		iCode = sqlite3_column_int(compiledStatement, 0);
		sqlite3_finalize(compiledStatement);
	}
	return iCode+1;
}
	
- (void) CloseSQL
{
	sqlite3_close(dbsql);
}


@end
