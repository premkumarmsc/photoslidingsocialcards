//
//  MyCell.h
//  TestCollectionViewWithXIB
//
//  Created by Quy Sang Le on 2/3/13.
//  Copyright (c) 2013 Quy Sang Le. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface receiptentCell : UICollectionViewCell
@property (retain, nonatomic) IBOutlet UITextView *cellLabel;
@property (retain, nonatomic) IBOutlet UILabel *cell_title_Label;
@property (retain, nonatomic) IBOutlet UIImageView *image_view;
@property (nonatomic,retain)IBOutlet UIProgressView *progress;
@property(nonatomic,retain )IBOutlet UIButton *deletebtn;

@property (retain, nonatomic) IBOutlet UIImageView *check_image_view;

@property (retain, nonatomic) IBOutlet UIImageView *source_image_view;

@property (retain, nonatomic) IBOutlet UIImageView *add_image_view;

@end
