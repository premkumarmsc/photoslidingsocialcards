//
//  dealsViewController.m
//  PhotoSliding
//
//  Created by ephronsystems on 6/27/13.
//
//

#import "Listofreceiptions.h"
#import "receiptentCell.h"
#import "Image_cell.h"
#import "ReceipCell.h"

@interface Listofreceiptions ()

@end

@implementation Listofreceiptions

@synthesize filterButton;
@synthesize collectionView;
@synthesize messageText,cropImage,senderImage;
@synthesize avilableTableView1;
@synthesize comingView;
receiptentCell *cell;

NSMutableArray *address1Array;
NSMutableArray *address2Array;
NSMutableArray *address_enteredArray;
NSMutableArray *birth_dateArray;
NSMutableArray *cityArray;
NSMutableArray *countryArray;
NSMutableArray *ListOfCountries;
NSMutableArray *firstnameArray;
NSMutableArray *friend_facebookproimgArray;
NSMutableArray *friend_facebookuserid1Array;
NSMutableArray *idArray;
NSMutableArray *lastnameArray;
NSMutableArray *phonenoArray;
NSMutableArray *snguseridArray;
NSMutableArray *stateArray;
NSMutableArray *zipcodeArray;


NSMutableArray *emilIDArray;
NSMutableArray *phoneNoArray;


NSString *address1text;
NSString *address2text;
NSString *citystr;
NSString *statestr;
NSString *zipCodeStr;

NSString *emailStr;
NSString *phoneStr;

NSString *countryString;
NSString *selectedContactID;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
     avilableTableView1.hidden=YES;
    
    ListOfCountries=[[NSMutableArray alloc]init];
    
    NSUserDefaults *valueAdd=[NSUserDefaults standardUserDefaults];
      ListOfCountries=[valueAdd objectForKey:@"COUNTRY_ARRAY"];
    
 //   ////(@"LIST OF COUNTRIES:%@",ListOfCountries);
    
   
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    
    NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:@"profileImage.jpg"];
    UIImage *profileIMG = [UIImage imageWithContentsOfFile:getImagePath];
    
    
    [_profileButton setImage:profileIMG forState:UIControlStateNormal];

    [self.view addSubview:_contactAddView];
    _contactAddView.hidden=YES;
    
   // [self getCountry];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/socialcards/retrieveCard",CONFIG_BASE_URL]];
    
    
    __block  ASIFormDataRequest *requestmethod = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    NSString *cardID=[check objectForKey:@"CARD_ID"];
       
  
   
    [requestmethod setPostValue:cardID forKey:@"id"];
    [requestmethod setPostValue:[Config get_token] forKey:@"apikey"];
    [requestmethod setTimeOutSeconds:30];
    
    
    [requestmethod setCompletionBlock:^{
        NSString *responseString23 = [requestmethod responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        ////(@"RESULTS CARD DETAILS:%@",results11);
        
        
         NSString *contactid= [results11 valueForKeyPath:@"socialcards.contactid"];
        NSArray *contactidArr= [results11 valueForKeyPath:@"socialcards.contactid"];
        
         NSArray *card_name= [results11 valueForKeyPath:@"socialcards.card_name"];
         NSArray *date_created= [results11 valueForKeyPath:@"socialcards.date_created"];
         NSArray *date_updated= [results11 valueForKeyPath:@"socialcards.date_updated"];
         NSArray *cardid= [results11 valueForKeyPath:@"socialcards.id"];
         NSArray *image_reference= [results11 valueForKeyPath:@"socialcards.image_reference"];
         NSArray *message_text= [results11 valueForKeyPath:@"socialcards.message_text"];
         NSArray *saved_status= [results11 valueForKeyPath:@"socialcards.saved_status"];
         NSArray *smiley_ids= [results11 valueForKeyPath:@"socialcards.smiley_ids"];
         NSArray *thumbnail_url= [results11 valueForKeyPath:@"socialcards.thumbnail_url"];
        NSArray *audio_ids= [results11 valueForKeyPath:@"socialcards.audio_id"];
        
        
        NSUserDefaults *update=[NSUserDefaults standardUserDefaults];
        
        [update setObject:card_name[0] forKey:@"CARD_NAME"];
         [update setObject:date_created[0] forKey:@"CREATED"];
         [update setObject:date_updated[0] forKey:@"UPDATED"];
         [update setObject:cardid[0] forKey:@"CARD_ID"];
         [update setObject:image_reference[0] forKey:@"IMAGE"];
         [update setObject:message_text[0] forKey:@"MESSAGE"];
         [update setObject:saved_status[0] forKey:@"SAVED_STATUS"];
         [update setObject:smiley_ids[0] forKey:@"SMILIES"];
         [update setObject:thumbnail_url[0] forKey:@"THUMP"];
        [update setObject:audio_ids[0] forKey:@"AUDIO"];
        
          [update setObject:contactidArr[0] forKey:@"CONTACT_ID"];
        
        
        ////(@"TEMPARY:%@",results11);
        
        ////(@"Contact ID:%@",contactidArr[0]);
       
        [self retriveContacts1:contactidArr[0]];
        
    }];
    [requestmethod setFailedBlock:^{
        NSError *error = [requestmethod error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [requestmethod startAsynchronous];
    
    
    
    

    
    
   
}
-(void)getCountry
{
    ListOfCountries=[[NSMutableArray alloc]init];
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/rules/getCountry",CONFIG_BASE_URL]];
    __block ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request setCompletionBlock:^{
        // Use when fetching text data
        NSString *responseString = [request responseString];
        
        
        NSMutableData *results1 = [responseString JSONValue];
        
        ////(@"HELLO:%@",results1);
        
        
        NSArray *temp= [results1 valueForKeyPath:@"response.countries"];
        
        
        ////(@"TEMPARY GET COUNTEY:%@",temp);
        
        for(NSDictionary *value in temp)
        {
            [ListOfCountries  addObject:[value valueForKey:@"country"]];
            
            
        }
        
        ////(@"LIST OF COUNTEY:%@",ListOfCountries);
        
    }];
    [request setFailedBlock:^{
      //  NSError *error = [request error];
    }];
    [request startAsynchronous];
}


-(void)retriveContacts:(NSString *)contacts
{
    
    address1Array=[[NSMutableArray alloc]init];
    address2Array=[[NSMutableArray alloc]init];
    address_enteredArray=[[NSMutableArray alloc]init];
    birth_dateArray=[[NSMutableArray alloc]init];
    cityArray=[[NSMutableArray alloc]init];
    countryArray=[[NSMutableArray alloc]init];
    firstnameArray=[[NSMutableArray alloc]init];
    friend_facebookproimgArray=[[NSMutableArray alloc]init];
    friend_facebookuserid1Array=[[NSMutableArray alloc]init];
    idArray=[[NSMutableArray alloc]init];
    lastnameArray=[[NSMutableArray alloc]init];
    phonenoArray=[[NSMutableArray alloc]init];
    snguseridArray=[[NSMutableArray alloc]init];
    stateArray=[[NSMutableArray alloc]init];
    zipcodeArray=[[NSMutableArray alloc]init];
    ListOfCountries=[[NSMutableArray alloc]init];
    
    emilIDArray=[[NSMutableArray alloc]init];
    phoneNoArray=[[NSMutableArray alloc]init];
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    
    ////(@"USER:%@",user);
    
    ////(@"CONTACTS:%@",contacts);

    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/contacts/getCardContacts?userID=%@&contactids=%@",CONFIG_BASE_URL,user,@"550,560"]];
    
    
    ////(@"URL:%@",url);
   
    
    __block ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request setCompletionBlock:^{
        // Use when fetching text data
        NSString *responseString = [request responseString];
        
        ////(@"RESS:%@",responseString);
        
        
        NSMutableData *results1 = [responseString JSONValue];
        
        ////(@"HELLO:%@",results1);
        
        ////(@"RESULTS:%@",results1);
        
        
        
       
        
        // ////(@"HELLO:%@",results1);
        
        
        NSArray *temp= [results1 valueForKeyPath:@"contacts"];
        
       // NSArray *temp=[results1 objectForKey:@"contacts"];
        
        ////(@"TEMP:%@",temp);
        
        for(NSDictionary *value in temp)
        {
            
            @try {
                [address1Array  addObject:[value valueForKey:@"address1"]];
                [address2Array  addObject:[value valueForKey:@"address2"]];
                [address_enteredArray  addObject:[value valueForKey:@"address_entered"]];
                
                [idArray  addObject:[value valueForKey:@"id"]];
                [friend_facebookuserid1Array  addObject:[value valueForKey:@"friend_facebookuserid"]];
                [friend_facebookproimgArray  addObject:[value valueForKey:@"friend_facebookproimg"]];
                
                
                [firstnameArray  addObject:[value valueForKey:@"firstname"]];
                [lastnameArray  addObject:[value valueForKey:@"lastname"]];
                [cityArray  addObject:[value valueForKey:@"city"]];
                
                [stateArray  addObject:[value valueForKey:@"state"]];
                [countryArray  addObject:[value valueForKey:@"country"]];
                [zipcodeArray  addObject:[value valueForKey:@"zipcode"]];
                
                [phonenoArray  addObject:[value valueForKey:@"phoneno"]];
                
                 [emilIDArray  addObject:[value valueForKey:@"emailid"]];
                
            }
            @catch (NSException *exception) {
                ////(@"BOOL");
            }
            
            
        }
        
        ////(@"emilIDArray:%@",emilIDArray);
        
        NSUserDefaults *update=[NSUserDefaults standardUserDefaults];
        
        [update setObject:firstnameArray forKey:@"FIRST_NAME"];
        [update setObject:lastnameArray forKey:@"LAST_NAME"];
        [update setObject:address1Array forKey:@"ADDRESS1"];
        [update setObject:address2Array forKey:@"ADDRESS2"];
        [update setObject:cityArray forKey:@"CITY"];
        [update setObject:stateArray forKey:@"STATE"];
        [update setObject:countryArray forKey:@"COUNTRY"];
        [update setObject:zipcodeArray forKey:@"ZIP"];
        [update setObject:friend_facebookproimgArray forKey:@"TO_IMAGE"];
        [update setObject:emilIDArray forKey:@"EMAILS"];
         [update setObject:phonenoArray forKey:@"PHONES"];
         [update setObject:idArray forKey:@"TO_CONTACT_ID"];
        [avilableTableView1 reloadData];
        
        avilableTableView1.hidden=NO;
        
        _continueButton.enabled=YES;
        _continueButton.titleLabel.textColor=[UIColor whiteColor];
        
        for (int i=0; i<[address_enteredArray count]; i++)
        {
            int value=[address_enteredArray[i]intValue];
            
            if (value==0)
            {
                _continueButton.enabled=NO;
                _continueButton.titleLabel.textColor=[UIColor grayColor];
            }
            
        }

        
        
    }];
    [request setFailedBlock:^{
        NSError *error = [request error];
    }];
    [request startAsynchronous];
}

-(void)retriveContacts1:(NSString *)contacts
{
   
    address1Array=[[NSMutableArray alloc]init];
    address2Array=[[NSMutableArray alloc]init];
    address_enteredArray=[[NSMutableArray alloc]init];
    birth_dateArray=[[NSMutableArray alloc]init];
    cityArray=[[NSMutableArray alloc]init];
    countryArray=[[NSMutableArray alloc]init];
    firstnameArray=[[NSMutableArray alloc]init];
    friend_facebookproimgArray=[[NSMutableArray alloc]init];
    friend_facebookuserid1Array=[[NSMutableArray alloc]init];
    idArray=[[NSMutableArray alloc]init];
    lastnameArray=[[NSMutableArray alloc]init];
    phonenoArray=[[NSMutableArray alloc]init];
    snguseridArray=[[NSMutableArray alloc]init];
    stateArray=[[NSMutableArray alloc]init];
    zipcodeArray=[[NSMutableArray alloc]init];
   ListOfCountries=[[NSMutableArray alloc]init];
     emilIDArray=[[NSMutableArray alloc]init];
    
    
    
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/contacts/retrieveCardContacts",CONFIG_BASE_URL]];
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    
    ////(@"USER:%@",user);
    
    ////(@"CONTACTS:%@",contacts);
    
   // contacts=@"2060,2062";
    
    //
   //  [requestmethod setPostValue:[Config get_token] forKey:@"apikey"];
    
    
    NSString *post =[NSString stringWithFormat:@"userID=%@&contactids=%@&apikey=%@",user,contacts,[Config get_token]];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *str=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    
    ////(@"GET:%@",str);
    
   NSString *str1 = [str stringByReplacingOccurrencesOfString:@"false,"
                                         withString:@""];
    
   // ////(@"STRR:%@",str1);
    
    
    NSMutableDictionary *results11 = [str1 JSONValue];
    
    
    ////(@"RESULTS ARR:%@",results11);
    
   
    
    NSArray *temp=[results11 objectForKey:@"contacts"];
    
    ////(@"TEMP:%@",temp);
    
    for(NSDictionary *value in temp)
    {
        
        @try {
            [address1Array  addObject:[value valueForKey:@"address1"]];
            [address2Array  addObject:[value valueForKey:@"address2"]];
            [address_enteredArray  addObject:[value valueForKey:@"address_entered"]];
            
            [idArray  addObject:[value valueForKey:@"id"]];
            [friend_facebookuserid1Array  addObject:[value valueForKey:@"friend_facebookuserid"]];
            [friend_facebookproimgArray  addObject:[value valueForKey:@"friend_facebookproimg"]];
            
            
            [firstnameArray  addObject:[value valueForKey:@"firstname"]];
            [lastnameArray  addObject:[value valueForKey:@"lastname"]];
            [cityArray  addObject:[value valueForKey:@"city"]];
            
            [stateArray  addObject:[value valueForKey:@"state"]];
            [countryArray  addObject:[value valueForKey:@"country"]];
            [zipcodeArray  addObject:[value valueForKey:@"zipcode"]];
            
            [phonenoArray  addObject:[value valueForKey:@"phoneno"]];
            [emilIDArray  addObject:[value valueForKey:@"emailid"]];
           
        }
        @catch (NSException *exception) {
            ////(@"BOOL");
        }
        
        
    }

    ////(@"RESU:%@",address_enteredArray);
    
    NSUserDefaults *update=[NSUserDefaults standardUserDefaults];
    
    [update setObject:firstnameArray forKey:@"FIRST_NAME"];
    [update setObject:lastnameArray forKey:@"LAST_NAME"];
    [update setObject:address1Array forKey:@"ADDRESS1"];
    [update setObject:address2Array forKey:@"ADDRESS2"];
    [update setObject:cityArray forKey:@"CITY"];
    [update setObject:stateArray forKey:@"STATE"];
    [update setObject:countryArray forKey:@"COUNTRY"];
    [update setObject:zipcodeArray forKey:@"ZIP"];
    [update setObject:emilIDArray forKey:@"EMAILS"];
    [update setObject:phonenoArray forKey:@"PHONES"];
    [update setObject:idArray forKey:@"TO_CONTACT_ID"];
    [update setObject:friend_facebookproimgArray forKey:@"TO_IMAGE"];
    [avilableTableView1 reloadData];
    
    avilableTableView1.hidden=NO;

     _continueButton.enabled=YES;
    _continueButton.titleLabel.textColor=[UIColor whiteColor];
    
    for (int i=0; i<[address_enteredArray count]; i++)
    {
        int value=[address_enteredArray[i]intValue];
        
        if (value==0)
        {
            _continueButton.enabled=NO;
             _continueButton.titleLabel.textColor=[UIColor grayColor];
        }
        
    }
    
    [self continueButtonClick];
    

}
- (IBAction)countryButton:(id)sender
{
    
    ListOfCountries=[[NSMutableArray alloc]init];
    
    NSUserDefaults *valueAdd=[NSUserDefaults standardUserDefaults];
    ListOfCountries=[valueAdd objectForKey:@"COUNTRY_ARRAY"];
    
    ////(@"LIST OF COUNTRIES:%@",ListOfCountries);

    
    LeveyPopListView *lplv = [[LeveyPopListView alloc] initWithTitle:@"United States" options:ListOfCountries handler:^(NSInteger anIndex)
                              {
                                  ////(@"%@",[NSString stringWithFormat:@"You have CLICKED %@", ListOfCountries[anIndex]]);
                                  
                                  NSString *displayStr=[NSString stringWithFormat:@"    %@",ListOfCountries[anIndex]];
                                  
                                  [_countrybtn setTitle:displayStr forState:UIControlStateNormal];
                                  
                                 // _countrybtn=ListOfCountries[anIndex];
                                  
                                  countryString=ListOfCountries[anIndex];
                                  
                              }];
    //    lplv.delegate = self;
    [lplv showInView:self.view animated:YES];
}

#pragma mark - LeveyPopListView delegates
- (void)leveyPopListView:(LeveyPopListView *)popListView didSelectedIndex:(NSInteger)anIndex
{
    ////(@"%@",[NSString stringWithFormat:@"You have selected %@", ListOfCountries[anIndex]]);
}
- (void)leveyPopListViewDidCancel
{
    ////(@"CANCELLED");
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
    
    
   // ////(@"ID:%d",[idArray count]);
    
    
    return [idArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    ReceipCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        
        NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"ReceipCell" owner:nil options:nil];
        
        for (UIView *view in views)
        {
            if([view isKindOfClass:[UITableViewCell class]])
            {
                cell = (ReceipCell*)view;
                //cell.img=@"date.png";
                
            }
        }
    }
    
    cell.selectionStyle=UITableViewCellSelectionStyleGray;
    
    
    @try {
        NSString *temp=[friend_facebookproimgArray objectAtIndex:indexPath.row];
        [cell.img setImageWithURL:[NSURL URLWithString:temp]
                        placeholderImage:[UIImage imageNamed:@"bigicon.png"]];
        
        
        
    }
    @catch (NSException *exception) {
        ////////////(@"CHJECK");
    }
    
    @try {
         cell.title.text=[NSString stringWithFormat:@"%@ %@",firstnameArray[indexPath.row],lastnameArray[indexPath.row]];
    }
    @catch (NSException *exception) {
        
    }
  
   
    
    
    [cell.updateBtn setTag:indexPath.row];
    [cell.updateBtn addTarget:self action:@selector(updatebuttonClicked:)
                  forControlEvents:UIControlEventTouchDown];

    @try
    {
//        if ([address_enteredArray[indexPath.row]intValue]==0||[emilIDArray[indexPath.row]isEqualToString:@""])
//        {
//            if ([address_enteredArray[indexPath.row]intValue]==0)
//            {
//                 cell.details.text=[NSString stringWithFormat:@"No Address found."];
//            }
//            else
//            {
//                cell.details.text=[NSString stringWithFormat:@"Email ID Missing."];
//            }
//           
//            
//            cell.details.textColor=[UIColor redColor];
//            
//            // [cell.updateBtn setTitle:@"Add" forState:UIControlStateNormal];
//            
//            [cell.updateBtn setImage:[UIImage imageNamed:@"add_btn.png"] forState:UIControlStateNormal];
//            
//        }
        if ([address_enteredArray[indexPath.row]intValue]==0)
        {
            
                cell.details.text=[NSString stringWithFormat:@"No Address found."];
                        
            cell.details.textColor=[UIColor redColor];
            
            // [cell.updateBtn setTitle:@"Add" forState:UIControlStateNormal];
            
            [cell.updateBtn setImage:[UIImage imageNamed:@"add_btn.png"] forState:UIControlStateNormal];
            
        }

        else
        {
            
            cell.details.text=[NSString stringWithFormat:@"%@ %@,%@,%@,%@,%@",address1Array[indexPath.row],address2Array[indexPath.row],cityArray[indexPath.row],stateArray[indexPath.row],countryArray[indexPath.row],zipcodeArray[indexPath.row]];
            
            cell.details.textColor=[UIColor colorWithRed:21/255.0 green:149/255.0 blue:39/255.0 alpha:1];

            
            [cell.updateBtn setImage:[UIImage imageNamed:@"edit.png"] forState:UIControlStateNormal];
            
            // [cell.updateBtn setTitle:@"Edit" forState:UIControlStateNormal];
        }
  
    }
    @catch (NSException *exception) {
        
    }
   
       
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
      
    return cell;
    
    
    
}
-(void)updatebuttonClicked:(UIButton*)button
{
    
    if ([address_enteredArray[(long int)[button tag]]intValue]==1)
    {
        ////(@"EDIT");
        
        _address1.text=address1Array[(long int)[button tag]];
        _address2.text=address2Array[(long int)[button tag]];
        _city.text=cityArray[(long int)[button tag]];
        _state.text=stateArray[(long int)[button tag]];
        _zipCode.text=zipcodeArray[(long int)[button tag]];
        
        
        NSString *countryStrValue;
        
        NSString *value=countryArray[(long int)[button tag]];
        if ([countryArray[(long int)[button tag]] isEqualToString:@""]||[value length]==0) {
            
            countryStrValue=@"United States";
        }

        
        [_countrybtn setTitle:[NSString stringWithFormat:@"    %@",countryStrValue] forState:UIControlStateNormal];
        
        _emailID.text=emilIDArray[(long int)[button tag]];
        _phoneNo.text=phonenoArray[(long int)[button tag]];
        
        countryString=countryArray[(long int)[button tag]];
        selectedContactID=idArray[(long int)[button tag]];
        
    }
    else
    {
        ////(@"ADD");
         selectedContactID=idArray[(long int)[button tag]];
        
        _address1.text=address1Array[(long int)[button tag]];
        _address2.text=address2Array[(long int)[button tag]];
        _city.text=cityArray[(long int)[button tag]];
        _state.text=stateArray[(long int)[button tag]];
        _zipCode.text=zipcodeArray[(long int)[button tag]];
        _emailID.text=emilIDArray[(long int)[button tag]];
        _phoneNo.text=phonenoArray[(long int)[button tag]];
        NSString *countryStrValue;
        
        NSString *value=countryArray[(long int)[button tag]];
                         
        
        if ([countryArray[(long int)[button tag]] isEqualToString:@""]||[value length]==0) {
            
            countryStrValue=@"United States";
        }
        
        [_countrybtn setTitle:[NSString stringWithFormat:@"    %@",countryStrValue] forState:UIControlStateNormal];
        
        countryString=countryArray[(long int)[button tag]];
        selectedContactID=idArray[(long int)[button tag]];
        
        
    }
    
    
    ////(@"update Button %ld clicked.",(long int)[button tag]);
     
     _contactAddView.hidden=NO;
    
}

-(IBAction)settingsButton
{
    ViewController *viewController=[[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        //////(@"SUCCESS");
    }
     ];
}

-(IBAction)saveContact
{
    
    
   
    address1text=_address1.text;
   address2text=_address2.text;
    citystr=_city.text;
    statestr=_state.text;
    zipCodeStr=_zipCode.text;
    
    emailStr=_emailID.text;
    phoneStr=_phoneNo.text;
    
    
    ////(@"COUNTRY:%@",countryString);
    ////(@"COUNTRY:%@",selectedContactID);
    
  
       
            if ([address1text isEqualToString:@""])
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Enter the address" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                [alert show];
            }
            else
            {
                if ([citystr isEqualToString:@""])
                {
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Enter the city" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    
                    [alert show];
                    
                }
                else
                {
                    if ([statestr isEqualToString:@""])
                    {
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Enter the state" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        
                        [alert show];
                    }
                    else
                    {
                        if ([countryString isEqualToString:@""])
                        {
                            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"United States" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                            
                            [alert show];
                        }
                        else
                        {
                            if ([zipCodeStr isEqualToString:@""])
                            {
                                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Enter the zip code" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                
                                [alert show];
                            }
                            else
                            {
                                if ([countryString isEqualToString:@"United States"])
                                {
                                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"United States" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                    
                                    [alert show];
                                }
                                else
                                {
                                ////(@"SUCCESS");
                                
                                
                                                                    
                                    if ([emailStr isEqualToString:@""])
                                    {
                                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Enter the email ID" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                        
                                        [alert show];
                                    }
                                    else
                                    {
                                        ////(@"SUCCESS");
                                        
                                        
                                        
                                        NSString* emailRegex = @"[A-Z0-9a-z._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}";
                                        NSPredicate* emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
                                        NSString *subjectString =emailStr;
                                        
                                        
                                        if((emailStr.length!=0) && ([emailTest evaluateWithObject:subjectString] != YES))
                                        {
                                            
                                            if ([emailTest evaluateWithObject:subjectString] != YES)
                                                
                                            {
                                                
                                                
                                                UIAlertView *loginalert = [[UIAlertView alloc] initWithTitle:@"Enter Valid Email ID" message:@"abc@example.com format" delegate:self
                                                                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                
                                                [loginalert show];
                                                
                                                
                                            }
                                            
                                            
                                        }
                                        
                                        else
                                        {
                                            

                                        
                                        [self addcontectFun];
                                            
                                        }
                                    }

                                
                                
                                }
                            }
                            
                        }
                    }
                }
            }
        
    
    
    
    
}
-(void)addcontectFun
{
    
    address1text=_address1.text;
    address2text=_address2.text;
    citystr=_city.text;
    statestr=_state.text;
    zipCodeStr=_zipCode.text;
    emailStr=_emailID.text;
    phoneStr=_phoneNo.text;
    
    
    ////(@"COUNTRY:%@",countryString);
    ////(@"COUNTRY:%@",selectedContactID);
    
    ////(@"COUNTRY:%@",address1text);
    ////(@"COUNTRY:%@",address2text);
    
    ////(@"COUNTRY:%@",citystr);
    ////(@"COUNTRY:%@",statestr);
    
    ////(@"COUNTRY:%@",zipCodeStr);
   
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/contacts/updateContact",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post1234 = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *userImage=[check objectForKey:@"USER_IMAGE"];
    NSString *user=[check objectForKey:@"USERID"];
    
    if ([userImage isEqualToString:@"NULL"])
    {
        userImage=@"http://dev.socialnetgateapp.com/bundles/acmeregistration/images/no-user-image.png";
    }
    
    
    ////(@"USER IMAGE:%@",userImage);
    
     
    
    [request_post1234 setPostValue:selectedContactID forKey:@"id"];
    [request_post1234 setPostValue:address1text forKey:@"address1"];
    [request_post1234 setPostValue:address2text forKey:@"address2"];
    [request_post1234 setPostValue:citystr forKey:@"city"];
    [request_post1234 setPostValue:statestr forKey:@"state"];
    [request_post1234 setPostValue:countryString forKey:@"country"];
    
     [request_post1234 setPostValue:@"1" forKey:@"address_entered"];
    [request_post1234 setPostValue:zipCodeStr forKey:@"zipcode"];
    [request_post1234 setPostValue:emailStr forKey:@"emailid"];
    [request_post1234 setPostValue:phoneStr forKey:@"phoneno"];
         [request_post1234 setPostValue:[Config get_token] forKey:@"apikey"];
    
    
    [request_post1234 setTimeOutSeconds:30];
    
    
    [request_post1234 setCompletionBlock:^{
        NSString *responseString23 = [request_post1234 responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        
        ////(@"RESULTS LOGIN:%@",results11);
        
        
        NSString *des=[results11 valueForKeyPath:@"header.description"];
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:des delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
        
               
        [_countrybtn setTitle:@"United States" forState:UIControlStateNormal];
        _contactAddView.hidden=YES;
        
        _contactAddView.hidden=YES;
        
        _address1.text=@"";
        _address2.text=@"";
        _city.text=@"";
        _state.text=@"";
        _zipCode.text=@"";
        [_countrybtn setTitle:@"United States" forState:UIControlStateNormal];
        
        
        [_address1 resignFirstResponder];
        [_address2 resignFirstResponder];
        [_city resignFirstResponder];
        [_state resignFirstResponder];
        [_zipCode resignFirstResponder];

        
        [self viewDidLoad];
        
    }];
    [request_post1234 setFailedBlock:^{
        NSError *error = [request_post1234 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1234 startAsynchronous];
}
- (void) animateTextField: (UITextField*) textField up: (BOOL) up

{
	
    int txtPosition = (textField.frame.origin.y - 200);
	
    const int movementDistance = (txtPosition < 0 ? 0 : txtPosition); // tweak as needed
	
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
	
    [UIView setAnimationBeginsFromCurrentState: YES];
	
    [UIView setAnimationDuration: movementDuration];
	
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
	
    [UIView commitAnimations];
	
}
- (void)textFieldDidBeginEditing:(UITextField *)textField

{
    
    
    [self animateTextField: textField up: YES];
    
}


- (void)textFieldDidEndEditing:(UITextField *)textField

{
	
    [self animateTextField: textField up: NO];
   	
}

-(IBAction)cancelButton
{
    _contactAddView.hidden=YES;
    
    _address1.text=@"";
    _address2.text=@"";
    _city.text=@"";
    _state.text=@"";
    _zipCode.text=@"";
    [_countrybtn setTitle:@"United States" forState:UIControlStateNormal];
    
    
    [_address1 resignFirstResponder];
    [_address2 resignFirstResponder];
    [_city resignFirstResponder];
    [_state resignFirstResponder];
    [_zipCode resignFirstResponder];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    return 84;
    
}



-(IBAction)backButton
{
    
    if ([comingView isEqualToString:@"SAVED_CARD"])
    {
        
        if (IS_IPHONE_5)
        {
            savedCards *viewController=[[savedCards alloc]initWithNibName:@"savedCards" bundle:nil];
            [self presentViewController:viewController animated:NO completion:^{
                //////(@"SUCCESS");
            }
             ];

        }
        else
        {
            savedCards *viewController=[[savedCards alloc]initWithNibName:@"savedCards~iPhone" bundle:nil];
            [self presentViewController:viewController animated:NO completion:^{
                //////(@"SUCCESS");
            }
             ];

        }
        
          }
    else
    {
   
    
        if (IS_IPHONE_5)
        {
            
            receiptentViewController *viewController=[[receiptentViewController alloc]initWithNibName:@"receiptentViewController" bundle:nil];
            [self presentViewController:viewController animated:NO completion:^{
                //////(@"SUCCESS");
            }
             ];
        }
        else
        {
            
            receiptentViewController *viewController=[[receiptentViewController alloc]initWithNibName:@"receiptentViewController~iPhone" bundle:nil];
            [self presentViewController:viewController animated:NO completion:^{
                //////(@"SUCCESS");
            }
             ];
        }

    }
    
       
}
 - (void)handleSingleTap:(UIGestureRecognizer *)gestureRecognizer {
     
     [messageText resignFirstResponder];
 }

-(IBAction)continueButtonClick
{
    
    NSUserDefaults *value=[NSUserDefaults standardUserDefaults];
    
    //[value setObject:netCoins forKey:@"TOTAL_COINS"];

    NSString *tatal=[value objectForKey:@"TOTAL_COINS"];
    
    ////(@"TOTAL COINS:%@",tatal);
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    NSString *cardID=[check objectForKey:@"CARD_ID"];
    NSString *imageID=[check objectForKey:@"IMAGE_ID"];
    NSString *imageSource=[check objectForKey:@"EDIT_IMAGE"];
    ////(@"USER:%@",imageID);

    
    
    if (IS_IPHONE_5) {
        sendViewController *viewController=[[sendViewController alloc]initWithNibName:@"sendViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        
        sendViewController *viewController=[[sendViewController alloc]initWithNibName:@"sendViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
        
    }
    
}

-(IBAction)meButton
{
    if (IS_IPHONE_5) {
        MeViewController *viewController=[[MeViewController alloc]initWithNibName:@"MeViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        
        MeViewController *viewController=[[MeViewController alloc]initWithNibName:@"MeViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }

}
-(IBAction)dealsButton
{
    dealsViewController *viewController=[[dealsViewController alloc]initWithNibName:@"dealsViewController" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        //////(@"SUCCESS");
    }
     ];
}
-(IBAction)socialButton
{
    if (IS_IPHONE_5) {
        netCoinsViewController *viewController=[[netCoinsViewController alloc]initWithNibName:@"netCoinsViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        netCoinsViewController *viewController=[[netCoinsViewController alloc]initWithNibName:@"netCoinsViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }

}
-(IBAction)netCoinsButton
{
    if (IS_IPHONE_5) {
        socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }

}
-(IBAction)myBusinessButton
{
    myBusinessViewController *viewController=[[myBusinessViewController alloc]initWithNibName:@"myBusinessViewController" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        //////(@"SUCCESS");
    }
     ];
}
- (BOOL)textFieldShouldReturn: (UITextView *)textField
{
  
    [textField resignFirstResponder];
    
    return YES;
}
#ifdef IOS_OLDER_THAN_6
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return (toInterfaceOrientation == UIInterfaceOrientationMaskPortrait);
}
#endif
#ifdef IOS_NEWER_OR_EQUAL_TO_6
-(BOOL)shouldAutorotate {
    return YES;
}
- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
#endif

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
