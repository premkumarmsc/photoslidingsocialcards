//
//  dealsViewController.h
//  PhotoSliding
//
//  Created by ephronsystems on 6/27/13.
//
//

#import <UIKit/UIKit.h>

@interface albumViewController : GAITrackedViewController<UICollectionViewDelegate,UICollectionViewDataSource,UIActionSheetDelegate,UIAlertViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property(retain,nonatomic)IBOutlet UICollectionView *collectionViewMain;
@property(retain,nonatomic)IBOutlet UICollectionView *collectionViewIMAGE;

@property(retain,nonatomic)IBOutlet UICollectionView *collectionViewFacebook;


@property (retain, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (weak, nonatomic) IBOutlet UILabel *selectedSegmentLabel;
@property (nonatomic,retain)IBOutlet UITableView *avilableTableView;
@property (retain, nonatomic) IBOutlet UILabel *detailLabel;

@property (retain, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIView *imageBackView;
@property (weak, nonatomic) IBOutlet UIView  *imageAlbumView;
@property (weak, nonatomic) IBOutlet UIView  *imageMobileview;
@property (weak, nonatomic) IBOutlet UIView  *imageDetailView;

/*
@property (retain, nonatomic) IBOutlet UIView  *newback_view;
@property (retain, nonatomic) IBOutlet UIView  *newAlbumView;
@property (retain, nonatomic) IBOutlet UIView  *newMobileView;
@property (retain, nonatomic) IBOutlet UIView  *newImageDetailView;
 */




@property (weak, nonatomic) IBOutlet UIView  *imageeditView;
@property (weak, nonatomic) IBOutlet UIButton  *profileButton;
- (IBAction)segmentDidChange:(id)sender;
-(IBAction)meButton;
-(IBAction)dealsButton;
-(IBAction)netCoinsButton;
-(IBAction)socialButton;
-(IBAction)myBusinessButton;
-(IBAction)filterButtonClick;
-(IBAction)dealbackButtonClick;
-(IBAction)settingsButton;

-(IBAction)imageAdd;


-(IBAction)nextViewButton;

-(IBAction)detailBack;


-(IBAction)backButton;

-(IBAction)albumClick;
-(IBAction)cameraClick;

-(IBAction)cancel;
-(IBAction)save;


@property (weak, nonatomic) IBOutlet UIImageView *rootImageView;

@property (weak, nonatomic) IBOutlet UIImageView *scrollImageView;

@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;
@property (weak, nonatomic) IBOutlet UISegmentedControl *seg;
@property (weak, nonatomic) IBOutlet UIImage *currentImage;
@property (weak, nonatomic) UIImagePickerController *imagePicker;
@property (weak, nonatomic) IBOutlet NSData *data;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollerView;

@property (weak, nonatomic) IBOutlet UIImage *rootImage;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

- (UIImage*)imageWithImageSimple:(UIImage*)image scaledToSize:(CGSize)newSize;
@end
