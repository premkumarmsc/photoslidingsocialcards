//
//  dealsViewController.h
//  PhotoSliding
//
//  Created by ephronsystems on 6/27/13.
//
//

#import <UIKit/UIKit.h>

@interface receivedCardViewController : GAITrackedViewController
@property (nonatomic,retain)IBOutlet UITableView *avilableTableView;
-(IBAction)meButton;
-(IBAction)dealsButton;
-(IBAction)netCoinsButton;
-(IBAction)socialButton;
-(IBAction)sendCardButton;
-(IBAction)myBusinessButton;
-(IBAction)settingsButton;

-(IBAction)backCard;

@property (weak, nonatomic) IBOutlet UIButton  *profileButton;
@end
