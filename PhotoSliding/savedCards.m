//
//  dealsViewController.m
//  PhotoSliding
//
//  Created by ephronsystems on 6/27/13.
//
//

#import "savedCards.h"
#import "receiptentCell.h"
#import "Image_cell.h"
#import "ReceipCell.h"
#import "savedCardsCell.h"

@interface savedCards ()

@end

@implementation savedCards

@synthesize filterButton;
@synthesize collectionView;
@synthesize messageText,cropImage,senderImage;
@synthesize avilableTableView1;
receiptentCell *cell;


NSMutableArray *idArray;
NSMutableArray *snguseridArray;
NSMutableArray *contactidArray;
NSMutableArray *card_nameArray;
NSMutableArray *fbphoto_idArray;
NSMutableArray *thumbnail_urlArray;
NSMutableArray *image_referenceArray;
NSMutableArray *message_textArray;
NSMutableArray *smiley_idsArray;
NSMutableArray *date_createdArray;
NSMutableArray *date_updatedArray;
NSMutableArray *saved_statusArray;

NSMutableArray *audioIDArray;


NSString *address1text;
NSString *address2text;
NSString *citystr;
NSString *statestr;
NSString *zipCodeStr;

UIAlertView *firstalert;
NSString *cardIDName;

NSString *countryString;
NSString *selectedContactID;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   
     self.trackedViewName = @"Saved cards Screen";
    
    idArray=[[NSMutableArray alloc]init];
    snguseridArray=[[NSMutableArray alloc]init];
    contactidArray=[[NSMutableArray alloc]init];
    card_nameArray=[[NSMutableArray alloc]init];
    fbphoto_idArray=[[NSMutableArray alloc]init];
    thumbnail_urlArray=[[NSMutableArray alloc]init];
    image_referenceArray=[[NSMutableArray alloc]init];
    message_textArray=[[NSMutableArray alloc]init];
    smiley_idsArray=[[NSMutableArray alloc]init];
    date_createdArray=[[NSMutableArray alloc]init];
     audioIDArray=[[NSMutableArray alloc]init];
    
    date_updatedArray=[[NSMutableArray alloc]init];
    saved_statusArray=[[NSMutableArray alloc]init];
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/socialcards/retrieveSavedCardsnew",CONFIG_BASE_URL]];
    
    
    __block  ASIFormDataRequest *requestmethod = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
  
       
    
   
    [requestmethod setPostValue:user forKey:@"userID"];
        [requestmethod setPostValue:[Config get_token] forKey:@"apikey"];
    [requestmethod setTimeOutSeconds:30];
    
    
    [requestmethod setCompletionBlock:^{
        NSString *responseString23 = [requestmethod responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        
        NSArray *temp= [results11 valueForKeyPath:@"socialcards"];
        
        
        //(@"TEMPARY GET COUNTEY:%@",temp);
        
        for(NSDictionary *value in temp)
        {
          [ idArray addObject: [value valueForKeyPath:@"cardid"]];
        //  [snguseridArray addObject:  [value valueForKeyPath:@"snguserid"]];
            
          [contactidArray addObject:  [value valueForKeyPath:@"contactid"]];
         [ card_nameArray addObject:  [value valueForKeyPath:@"card_name"]];
         // [ fbphoto_idArray addObject:  [value valueForKeyPath:@"fbphoto_id"]];
           [thumbnail_urlArray addObject:  [value valueForKeyPath:@"cardimg"]];
           [image_referenceArray addObject:  [value valueForKeyPath:@"cardimg"]];
           [message_textArray addObject:  [value valueForKeyPath:@"message_text"]];
           [smiley_idsArray addObject:  [value valueForKeyPath:@"smiley_ids"]];
          [date_createdArray addObject:  [value valueForKeyPath:@"date_created"]];
           [date_updatedArray addObject:  [value valueForKeyPath:@"date_updated"]];
            
            [audioIDArray addObject:  [value valueForKeyPath:@"audio_id"]];
            
            [saved_statusArray addObject:  [value valueForKeyPath:@"saved_status"]];
          
            
            
         
            
        }

      
        
        ////(@"TEMPARY:%@",idArray);
        
        [avilableTableView1 reloadData];
        
        
    }];
    [requestmethod setFailedBlock:^{
        NSError *error = [requestmethod error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [requestmethod startAsynchronous];
    
    
    
    

    
    
   
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
    
    
   // ////(@"ID:%d",[idArray count]);
    
    
    return [idArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    savedCardsCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        
        NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"savedCardsCell" owner:nil options:nil];
        
        for (UIView *view in views)
        {
            if([view isKindOfClass:[UITableViewCell class]])
            {
                cell = (savedCardsCell*)view;
                //cell.img=@"date.png";
                
            }
        }
    }
    
    cell.selectionStyle=UITableViewCellSelectionStyleGray;
    
    
    @try {
        NSString *temp=[image_referenceArray objectAtIndex:indexPath.row];
        [cell.img setImageWithURL:[NSURL URLWithString:temp]
                        placeholderImage:[UIImage imageNamed:@"bigicon.png"]];
        
        
        
    }
    @catch (NSException *exception) {
        ////////////(@"CHJECK");
    }
    
    
    cell.title.text=[NSString stringWithFormat:@"%@",card_nameArray[indexPath.row]];
    
    
    [cell.updateBtn setTag:indexPath.row];
    [cell.updateBtn addTarget:self action:@selector(updatebuttonClicked:)
                  forControlEvents:UIControlEventTouchDown];
    
    
     NSString *newString = [[message_textArray[indexPath.row] componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
    
    cell.details.text=newString;
    
    
         // [cell.updateBtn setTitle:@"Delete" forState:UIControlStateNormal];
       
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
      
    return cell;
    
    
    
}


- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex

{
    if (alertView==firstalert)
    {
        if (buttonIndex == 0)
        {
          
            
        }else if (buttonIndex == 1)
        {
         
            
            
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/socialcards/deleteCard",CONFIG_BASE_URL]];
            
            
            __block  ASIFormDataRequest *requestmethod = [ASIFormDataRequest requestWithURL:url];
            
            
            
            NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
            NSString *user=[check objectForKey:@"USERID"];
            
            
            
            
            [requestmethod setPostValue:user forKey:@"userID"];
            [requestmethod setPostValue:cardIDName forKey:@"id"];
             [requestmethod setPostValue:[Config get_token] forKey:@"apikey"];
            [requestmethod setTimeOutSeconds:30];
            
            
            [requestmethod setCompletionBlock:^{
                NSString *responseString23 = [requestmethod responseString];
                NSMutableData *results11 = [responseString23 JSONValue];
                
                
                
                
                ////(@"RESULTS:%@",results11);
                
                [self viewDidLoad];
                
                
            }];
            [requestmethod setFailedBlock:^{
                NSError *error = [requestmethod error];
                UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                    message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                          cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                
                
                
            }];
            
            [requestmethod startAsynchronous];
            
        }
    }
    
    
}


-(void)updatebuttonClicked:(UIButton*)button
{
    NSString *cardID=idArray[(long int)[button tag]];
    
    cardIDName=idArray[(long int)[button tag]];
    
    firstalert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Do you want to delete this card?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    
    [firstalert show];
    
    
    

        
}

-(IBAction)settingsButton
{
    ViewController *viewController=[[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        //////(@"SUCCESS");
    }
     ];
}





- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *contact_id_str=contactidArray[indexPath.row];
    
    /*
    
    [ idArray addObject: [value valueForKeyPath:@"id"]];
    [snguseridArray addObject:  [value valueForKeyPath:@"snguserid"]];
    
    [contactidArray addObject:  [value valueForKeyPath:@"contactid"]];
    [ card_nameArray addObject:  [value valueForKeyPath:@"card_name"]];
    [ fbphoto_idArray addObject:  [value valueForKeyPath:@"fbphoto_id"]];
    [thumbnail_urlArray addObject:  [value valueForKeyPath:@"thumbnail_url"]];
    [image_referenceArray addObject:  [value valueForKeyPath:@"image_reference"]];
    [message_textArray addObject:  [value valueForKeyPath:@"message_text"]];
    [smiley_idsArray addObject:  [value valueForKeyPath:@"smiley_ids"]];
    [date_createdArray addObject:  [value valueForKeyPath:@"date_created"]];
    [date_updatedArray addObject:  [value valueForKeyPath:@"date_updated"]];
    
    [saved_statusArray addObject:  [value valueForKeyPath:@"saved_status"]];
     */
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    
    [check setObject:idArray[indexPath.row] forKey:@"CARD_ID_REFERENCE"];
    [check setObject:image_referenceArray[indexPath.row] forKey:@"IMAGE_ID_REFERENCE"];
    
    [check setObject:card_nameArray[indexPath.row] forKey:@"CARD_NAME_REFERENCE"];
    [check setObject:message_textArray[indexPath.row] forKey:@"MESSAGE_REFERENCE"];
     [check setObject:smiley_idsArray[indexPath.row] forKey:@"SMILEY_REFERENCE"];
    [check setObject:audioIDArray[indexPath.row] forKey:@"AUDIO_REFERENCE"];
    
     [check setObject:contactidArray[indexPath.row] forKey:@"CONTACT_ID_REFERENCE"];
    
    [check setObject:@"SAVED_CARDS" forKey:@"VIEW_REFERENCE"];
    
    
    
    if ([contact_id_str isEqualToString:@""])
    {
        NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
        
        [check setObject:idArray[indexPath.row] forKey:@"CARD_ID"];
        [check setObject:image_referenceArray[indexPath.row] forKey:@"IMAGE_ID"];
        
        
        if (IS_IPHONE_5)
        {
            
            receiptentViewController *viewController=[[receiptentViewController alloc]initWithNibName:@"receiptentViewController" bundle:nil];
            [self presentViewController:viewController animated:NO completion:^{
                //////(@"SUCCESS");
            }
             ];
        }
        else
        {
            
            receiptentViewController *viewController=[[receiptentViewController alloc]initWithNibName:@"receiptentViewController~iPhone" bundle:nil];
            [self presentViewController:viewController animated:NO completion:^{
                //////(@"SUCCESS");
            }
             ];
        }
    }
    else
    {
        NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
        
        [check setObject:idArray[indexPath.row] forKey:@"CARD_ID"];
        [check setObject:image_referenceArray[indexPath.row] forKey:@"IMAGE_ID"];
        
        
        ////(@"LIST OF RECEIP");
        
        if (IS_IPHONE_5)
        {
            
            Listofreceiptions *viewController=[[Listofreceiptions alloc]initWithNibName:@"Listofreceiptions" bundle:nil];
            [self presentViewController:viewController animated:NO completion:nil];
            
        }
        else
        {
            
            Listofreceiptions *viewController=[[Listofreceiptions alloc]initWithNibName:@"Listofreceiptions~iPhone" bundle:nil];
            [self presentViewController:viewController animated:NO completion:nil];
        }

    }
    
   
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    return 84;
    
}



-(IBAction)backButton
{
    
  
   
    
    if (IS_IPHONE_5) {
        socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }


    
    
       
}
 - (void)handleSingleTap:(UIGestureRecognizer *)gestureRecognizer {
     
     [messageText resignFirstResponder];
 }

-(IBAction)continueButtonClick
{
    
    NSUserDefaults *value=[NSUserDefaults standardUserDefaults];
    
    //[value setObject:netCoins forKey:@"TOTAL_COINS"];

    NSString *tatal=[value objectForKey:@"TOTAL_COINS"];
    
    ////(@"TOTAL COINS:%@",tatal);
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    NSString *cardID=[check objectForKey:@"CARD_ID"];
    NSString *imageID=[check objectForKey:@"IMAGE_ID"];
    NSString *imageSource=[check objectForKey:@"EDIT_IMAGE"];
    ////(@"USER:%@",imageID);

    
    
    if (IS_IPHONE_5) {
        sendViewController *viewController=[[sendViewController alloc]initWithNibName:@"sendViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        
        sendViewController *viewController=[[sendViewController alloc]initWithNibName:@"sendViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
        
    }
    
}

-(IBAction)meButton
{
    if (IS_IPHONE_5) {
        MeViewController *viewController=[[MeViewController alloc]initWithNibName:@"MeViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        
        MeViewController *viewController=[[MeViewController alloc]initWithNibName:@"MeViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }

}
-(IBAction)dealsButton
{
    dealsViewController *viewController=[[dealsViewController alloc]initWithNibName:@"dealsViewController" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        //////(@"SUCCESS");
    }
     ];
}
-(IBAction)socialButton
{
    if (IS_IPHONE_5) {
        netCoinsViewController *viewController=[[netCoinsViewController alloc]initWithNibName:@"netCoinsViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        netCoinsViewController *viewController=[[netCoinsViewController alloc]initWithNibName:@"netCoinsViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }

}
-(IBAction)netCoinsButton
{
    if (IS_IPHONE_5) {
        socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }

}
-(IBAction)myBusinessButton
{
    myBusinessViewController *viewController=[[myBusinessViewController alloc]initWithNibName:@"myBusinessViewController" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        //////(@"SUCCESS");
    }
     ];
}
- (BOOL)textFieldShouldReturn: (UITextView *)textField
{
  
    [textField resignFirstResponder];
    
    return YES;
}
#ifdef IOS_OLDER_THAN_6
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return (toInterfaceOrientation == UIInterfaceOrientationMaskPortrait);
}
#endif
#ifdef IOS_NEWER_OR_EQUAL_TO_6
-(BOOL)shouldAutorotate {
    return YES;
}
- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
#endif

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
