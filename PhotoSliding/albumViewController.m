//
//  dealsViewController.m
//  PhotoSliding
//
//  Created by ephronsystems on 6/27/13.
//
//

#import "albumViewController.h"
#import "LibraryCell.h"
#import "Image_cell.h"
#import "ImageUtil.h"
#import "ColorMatrix.h"
#import <QuartzCore/QuartzCore.h>
#import "GalleryCell.h"
#import "Album_cell.h"
#import "SDSegmentedControl.h"
#import <QuartzCore/QuartzCore.h>
#import "ImageCropperView.h"


@interface albumViewController ()
@property (nonatomic, retain) IBOutlet ImageCropperView *cropper;
@property (nonatomic, retain) IBOutlet UIImageView *result;
@property (nonatomic, retain) IBOutlet UIButton *btn;
@end

@implementation albumViewController

//@synthesize filterButton;
@synthesize collectionViewMain;
@synthesize scrollerView,seg,segmentedControl;
@synthesize currentImage;
@synthesize rootImage,rootImageView,imagePicker,imageView;
@synthesize collectionViewIMAGE;
@synthesize collectionViewFacebook;
@synthesize cropper, result, btn;

Image_cell *cell;
GalleryCell *cell1;
Album_cell *album;

NSMutableArray *imageThumpArray;
NSMutableArray *imageSourceArray;
NSMutableArray *imageIDArray;

NSMutableArray *albumThumpArray;
NSMutableArray *albumNameArray;
NSMutableArray *albumIDArray;
NSMutableArray *albumCountArray;


NSMutableArray *numberArray;

NSArray *nameArr;
NSMutableArray *imageArr;

UIImage *selectedImage;


int imageIndexCheck;

int selectIndex;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //NSUserDefaults *check12=[NSUserDefaults standardUserDefaults];
    //[check12 setObject:@"NULL" forKey:@"VIEW_REFERENCE"];
    
    
     self.trackedViewName = @"Album Screen";
    
    
    _saveButton.hidden=YES;
    
    imageIndexCheck=0;
    
    SDSegmentView *segmenteViewAppearance = SDSegmentView.appearance;
    [segmenteViewAppearance setTitleColor:UIColor.grayColor forState:UIControlStateNormal];
    [segmenteViewAppearance setTitleColor:UIColor.whiteColor forState:UIControlStateSelected];
    [segmenteViewAppearance setTitleColor:UIColor.orangeColor forState:UIControlStateDisabled];
    
    
    SDStainView *stainViewAppearance = SDStainView.appearance;
    stainViewAppearance.backgroundColor = [UIColor colorWithRed:245/255.0 green:127/255.0 blue:0/255.0 alpha:1];
    stainViewAppearance.shadowColor = [UIColor colorWithRed:245/255.0 green:127/255.0 blue:0/255.0 alpha:1];
    stainViewAppearance.shadowBlur = 5;

    
    [_imageBackView addSubview:_imageAlbumView];
     [_imageBackView addSubview:_imageDetailView];
     [_imageBackView addSubview:_imageMobileview];
    
    
    _imageMobileview.hidden=YES;
    _imageAlbumView.hidden=NO;
    _imageDetailView.hidden=YES;
    
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    
    NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:@"profileImage.jpg"];
    UIImage *profileIMG = [UIImage imageWithContentsOfFile:getImagePath];
    
    
    [_profileButton setImage:profileIMG forState:UIControlStateNormal];

    
    collectionViewMain.hidden=NO;
    //_back_view.hidden=YES;
    
    [self.view addSubview:_imageeditView];
    
    _imageeditView.hidden=YES;
    
    
    selectIndex=0;
    
    nameArr = [NSArray arrayWithObjects:@"Artwork",@"LOMO",@"B/W",@"Retro",@"Gothic",@"Sharp",@"Elegant",@"Burgundy",@"Lime",@"Romantic",@"Halo",@"Blues",@"Dream",@"Night", nil];
    
    imageArr=[[NSMutableArray alloc]init];
     imageSourceArray=[[NSMutableArray alloc]init];
      imageIDArray=[[NSMutableArray alloc]init];
    
    
    albumCountArray=[[NSMutableArray alloc]init];
    albumIDArray=[[NSMutableArray alloc]init];
    albumNameArray=[[NSMutableArray alloc]init];
    albumThumpArray=[[NSMutableArray alloc]init];
    
    
    for (int i=0; i<[nameArr count]; i++)
    {
      
        
        [imageArr addObject:[NSString stringWithFormat:@"%d.png",i]];
    }
    
    
    ////(@"NAME:%@",imageArr);
    
    imageThumpArray=[[NSMutableArray alloc]init];
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/photos/getFacebookAlbums",CONFIG_BASE_URL]];
    
   NSLog(@"URL:%@",url);
    
    __block  ASIFormDataRequest *request_post1234 = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
     NSString *access=[check objectForKey:@"ACCESS_TOKEN"];
    
   NSLog (@"USER:%@",user);
   NSLog  (@"ACCESS:%@",access);
    
    
    
    ////(@"USER:%@",user);
    
      
    
    [request_post1234 setPostValue:access forKey:@"fbtoken"];
    [request_post1234 setPostValue:user forKey:@"userID"];
    [request_post1234 setPostValue:[Config get_token] forKey:@"apikey"];
    
    [request_post1234 setTimeOutSeconds:30];
    
    
    [request_post1234 setCompletionBlock:^{
        NSString *responseString23 = [request_post1234 responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        
      NSLog(@"RESULTS LOGIN ABACC:%@",responseString23);
        
        
      
      
        
       NSArray *temp= [results11 valueForKeyPath:@"response.albums"];
        
        
        ////(@"TEMPARY:%@",temp);
        
        
        @try {
            for(NSDictionary *value in temp)
            {
                
                NSString *album_id_str=[value valueForKey:@"albumid"];
                NSString *photoCount=[value valueForKey:@"photocount"];
                
                  NSString *coverPhoto=[value valueForKey:@"cover_photo_url"];
                
                @try {
                    
                    
                    int photo_count=[photoCount intValue];
                    
                    ////(@"Album_id:%@",album_id_str);
                    ////(@"PhotoCount:%@",photoCount);
                    
                    if ([coverPhoto isEqualToString:@""]||[coverPhoto length]==0||coverPhoto==[NSNull null])
                    {
                        
                    }
                    else
                    {
                    [albumCountArray  addObject:photoCount];
                    
                    [albumIDArray  addObject:album_id_str];
                    
                    [albumThumpArray  addObject:[value valueForKey:@"cover_photo_url"]];
                    
                    
                    [albumNameArray  addObject:[value valueForKey:@"albumname"]];
                    }
                    
                    
                }
                @catch (NSException *exception)
                {
                      ////(@"Exception PhotoCount:%@",photoCount);
                }
               
               
                
               
                
                
            }
            
            
           
        }
        @catch (NSException *exception) {
            ////(@"CATCHED");
            
           // ////(@"COUNT:%@",[temp count]);
                       
        }
        
        ////(@"ALBUM:%@",albumThumpArray);
        ////(@"ARRAY:%@",albumCountArray);
     ////(@"ALBUM:%@",albumIDArray);
     ////(@"ARRAY:%@",albumNameArray);

     
        [collectionViewMain reloadData];
        
        
    }];
    [request_post1234 setFailedBlock:^{
        NSError *error = [request_post1234 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1234 startAsynchronous];
    
    
   [self.collectionViewMain registerNib:[UINib nibWithNibName:@"Album_cell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
    
     [self.collectionViewIMAGE registerNib:[UINib nibWithNibName:@"GalleryCell" bundle:nil] forCellWithReuseIdentifier:@"CELL1"];
    
     [self.collectionViewFacebook registerNib:[UINib nibWithNibName:@"Image_cell" bundle:nil] forCellWithReuseIdentifier:@"CELL2"];
    
    // Do any additional setup after loading the view from its nib.
}
-(IBAction)settingsButton
{
    ViewController *viewController=[[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        //////(@"SUCCESS");
    }
     ];
}
-(IBAction)albumClick
{
    
    // Do any additional setup after loading the view, typically from a nib.
    cropper.layer.borderWidth = 2.0;
    //cropper.layer.borderColor = [UIColor colorWithRed:245/255.0 green:127/255.0 blue:0/255.0 alpha:1].CGColor;
    
    cropper.layer.borderColor = [UIColor clearColor].CGColor;
    
    [cropper setup];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:@"savedImage.jpg"];
    UIImage *img = [UIImage imageWithContentsOfFile:getImagePath];
    
    //cropper.image=nil;
    
    cropper.image = [UIImage imageNamed:@"WhiteBackground_Adjustable.png"];
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = NO;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
}
-(IBAction)cameraClick
{
    ////(@"CAMERA CLICK");
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        
        
        // Do any additional setup after loading the view, typically from a nib.
        cropper.layer.borderWidth = 2.0;
        //cropper.layer.borderColor = [UIColor colorWithRed:245/255.0 green:127/255.0 blue:0/255.0 alpha:1].CGColor;
        
        cropper.layer.borderColor = [UIColor clearColor].CGColor;
        
        [cropper setup];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
        NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:@"savedImage.jpg"];
        UIImage *img = [UIImage imageWithContentsOfFile:getImagePath];
        
        //cropper.image=nil;
        
        cropper.image = [UIImage imageNamed:@"WhiteBackground_Adjustable.png"];
        
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        [picker setDelegate:self];
        
        [picker setSourceType:UIImagePickerControllerSourceTypeCamera];
        [picker setShowsCameraControls:YES];
        [picker setAllowsEditing:NO];
        [self presentViewController:picker animated:YES completion:NULL];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"The device does not have camera" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
      // [alert release];
    }

}
-(IBAction)backButton
{
    
    if (IS_IPHONE_5) {
        socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    
   
    
    
}

-(IBAction)cancel
{
    _imageeditView.hidden=YES;
    rootImageView.image=nil;
}
-(IBAction)save
{
    
    
    UIImage *finalImage=rootImageView.image;
    
    NSArray *paths1 = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory1 = [paths1 objectAtIndex:0];
    NSString *savedImagePath1 = [documentsDirectory1 stringByAppendingPathComponent:@"savedImage.jpg"];
    UIImage *image11 = rootImageView.image; // imageView is my image from camera
    
    
    
    NSData *imageData1 =UIImageJPEGRepresentation(image11, 1.0);
    [imageData1 writeToFile:savedImagePath1 atomically:NO];
    
    
    
    
    NSString *getImagePath = [documentsDirectory1 stringByAppendingPathComponent:@"savedImage.jpg"];
    UIImage *img12 = [UIImage imageWithContentsOfFile:getImagePath];
    
    
    [cropper finishCropping];
    //result.image = cropper.croppedImage;
    
    
    cropper.image=cropper.croppedImage;
    
    //  cropper.hidden = YES;
    // [btn setTitle:@"Back" forState:UIControlStateNormal];
    // [btn setTitle:@"Back" forState:UIControlStateHighlighted];
    
    
    UIImageView *resultView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 230, 305)];
    
    CGSize newSize = resultView.frame.size;
    
    UIImage *oldImage =cropper.croppedImage ;
	UIImage *newImage;
	
    
    //CGRect newFrame;
    //newFrame.size = CGSizeMake(115, 155);
    
    //newImage = [oldImage imageScaledToFitSize:newFrame.size];
    newImage = [oldImage imageScaledToFitSize:newSize];
    
    
    UIImageView *img=[[UIImageView alloc]initWithImage:newImage];
    
    
    ////(@"IMG:%@",img.image);
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:@"cropImage.png"];
    // UIImage *image = img.image; // imageView is my image from camera
    
    
    
    NSData *imageData = UIImagePNGRepresentation(newImage);
    [imageData writeToFile:savedImagePath atomically:NO];
    
    
    
    NSUserDefaults *storeImageValues=[NSUserDefaults standardUserDefaults];
    NSString *getFrom=[storeImageValues objectForKey:@"VIEW_REFERENCE"];
    
    
    ////(@"GET FROM:%@",getFrom);
    
    if ([getFrom isEqualToString:@"SAVED_CARDS"])
    {
        
        if (IS_IPHONE_5) {
            messageViewController *viewController=[[messageViewController alloc]initWithNibName:@"messageViewController" bundle:nil];
            
            viewController.comingFrom=@"CROP";
            
            [self presentViewController:viewController animated:NO completion:^{
                //////(@"SUCCESS");
            }
             ];
        }
        else
        {
            
            messageViewController *viewController=[[messageViewController alloc]initWithNibName:@"messageViewController~iPhone" bundle:nil];
            
            viewController.comingFrom=@"CROP";
            
            [self presentViewController:viewController animated:NO completion:^{
                //////(@"SUCCESS");
            }
             ];
        }
        
    }
    else
    {
        
        
        
        NSUserDefaults *storeImageValues=[NSUserDefaults standardUserDefaults];
        
        
        
        NSString *imageSource=[storeImageValues objectForKey:@"IMAGE_REFERENCE"];
        NSString *imageThump=[storeImageValues objectForKey:@"THUMPNAIL"];
        NSString *imageID=[storeImageValues objectForKey:@"IMAGE_ID"];
        
        ////(@"S:P%@",imageSource);
        ////(@"S:P%@",imageThump);
        ////(@"S:P%@",imageID);
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/socialcards/createCard",CONFIG_BASE_URL]];
        
        //NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/photos/retrieveFacebookPhotos",CONFIG_BASE_URL]];
        
        __block  ASIFormDataRequest *requestmethod = [ASIFormDataRequest requestWithURL:url];
        
        
        
        NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
        NSString *user=[check objectForKey:@"USERID"];
        
        ////(@"USER:%@",user);
        
        if ([imageSource length]==0||imageSource==[NSNull null])
        {
            imageSource=@"";
            
            ////(@"ENTER");
            
            NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            
            NSMutableString *randomString = [NSMutableString stringWithCapacity: 10];
            
            for (int i=0; i<10; i++) {
                [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random() % [letters length]]];
            }
            
            imageID=randomString;
            imageSource=@"http://dev.socialnetgateapp.com/bundles/acmeregistration/images/no-user-image.png";
        }
        
        
        
        [requestmethod setPostValue:user forKey:@"snguserid"];
        [requestmethod setPostValue:imageID forKey:@"fbphoto_id"];
        [requestmethod setPostValue:imageSource forKey:@"image_reference"];
        [requestmethod setPostValue:imageThump forKey:@"thumbnail_url"];
         [requestmethod setPostValue:[Config get_token] forKey:@"apikey"];
         [requestmethod setPostValue:[Config get_token] forKey:@"apikey"];
        [requestmethod setTimeOutSeconds:30];
        
        
        [requestmethod setCompletionBlock:^{
            NSString *responseString23 = [requestmethod responseString];
            NSMutableData *results11 = [responseString23 JSONValue];
            
            
            
            NSString *cardID= [results11 valueForKeyPath:@"cardID"];
            
            ////(@"CARD_ID:%@",cardID);
            
            NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
            [check setObject:cardID forKey:@"CARD_ID"];
            
            [check setObject:@"NULL" forKey:@"IMAGE_ID_REFERENCE"];
            
            
            
            ////(@"TEMPARY:%@",results11);
            
            
            
            if (IS_IPHONE_5)
            {
                messageViewController *viewController=[[messageViewController alloc]initWithNibName:@"messageViewController" bundle:nil];
                
                
                
                
                [self presentViewController:viewController animated:NO completion:^{
                    //////(@"SUCCESS");
                }
                 ];
            }
            else
            {
                
                messageViewController *viewController=[[messageViewController alloc]initWithNibName:@"messageViewController~iPhone" bundle:nil];
                
                
                
                [self presentViewController:viewController animated:NO completion:^{
                    //////(@"SUCCESS");
                }
                 ];
            }
            
            
            
            
        }];
        [requestmethod setFailedBlock:^{
            NSError *error = [requestmethod error];
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                      cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            
            
            
        }];
        
        [requestmethod startAsynchronous];
        
        
    }
    

    
   /* UIImage *finalImage=rootImageView.image;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:@"savedImage.jpg"];
    UIImage *image = rootImageView.image; // imageView is my image from camera
    
    
    
    NSData *imageData =UIImageJPEGRepresentation(image, 1.0);
    [imageData writeToFile:savedImagePath atomically:NO];
    
    
   
  
    NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:@"savedImage.jpg"];
    UIImage *img = [UIImage imageWithContentsOfFile:getImagePath];
    
    
    
    if (IS_IPHONE_5)
    {
        ViewController1 *viewController1=[[ViewController1 alloc]initWithNibName:@"ViewController_iPhone" bundle:nil];
        [self presentViewController:viewController1 animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        ViewController1 *viewController1=[[ViewController1 alloc]initWithNibName:@"ViewController_iPhone~iPhone" bundle:nil];
        [self presentViewController:viewController1 animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    
  */
    
//
//    RKCropImageController *cropController = [[RKCropImageController alloc] initWithImage:img];
//    //cropController.delegate = self;
//    [self presentModalViewController:cropController animated:NO];
    
}
#ifdef IOS_OLDER_THAN_6
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return (toInterfaceOrientation == UIInterfaceOrientationMaskPortrait);
}
#endif
#ifdef IOS_NEWER_OR_EQUAL_TO_6
-(BOOL)shouldAutorotate {
    return YES;
}
- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
#endif

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    if (collectionView==collectionViewIMAGE)
    {
        return 14;
    }
    if (collectionView==collectionViewFacebook)
    {
        return [imageIDArray count];
    }
   if (collectionView==collectionViewMain)
    {
        return [albumIDArray count];
    }
    
}


- (Image_cell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
     if (collectionView==collectionViewMain)
     {
    
    album = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELL" forIndexPath:indexPath];
    
    
    @try {
        NSString *temp=[albumThumpArray objectAtIndex:indexPath.row];
        [album.image_view setImageWithURL:[NSURL URLWithString:temp]
                     placeholderImage:[UIImage imageNamed:@"vbigicon.png"]];
        
        
        
    }
    @catch (NSException *exception) {
        ////////////(@"CHJECK");
    }
         
         
         album.cell_count_Label.text=[NSString stringWithFormat:@"%@ photos",albumCountArray[indexPath.row]];

     album.cell_title_Label.text=[NSString stringWithFormat:@"%@",albumNameArray[indexPath.row]];
    
    // cell.progress.hidden=YES;
    
    
    
    //cell.image_view.image=[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@", image_path]];
    
    return album;
         
         
         
     }
    
    if (collectionView==collectionViewFacebook)
    {
        
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELL2" forIndexPath:indexPath];
        
        
        @try {
            NSString *temp=[imageThumpArray objectAtIndex:indexPath.row];
            [cell.image_view setImageWithURL:[NSURL URLWithString:temp]
                             placeholderImage:[UIImage imageNamed:@"vbigicon.png"]];
            
            
            
        }
        @catch (NSException *exception) {
            ////////////(@"CHJECK");
        }
        
        
                
        // cell.progress.hidden=YES;
        
        
        
        //cell.image_view.image=[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@", image_path]];
        
        return cell;
        
        
        
    }
    
   if (collectionView==collectionViewIMAGE)
    {
        
        cell1 = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELL1" forIndexPath:indexPath];
        
        if (indexPath.row==selectIndex)
        {
            cell1.frameImage.image=[UIImage imageNamed:@"110.png"];
        }
        else
        {
            cell1.frameImage.image=[UIImage imageNamed:@"110C.png"];
        }
        
        cell1.image_view.image=[UIImage imageNamed:[NSString stringWithFormat:@"%d.png",indexPath.row]];
        
        cell1.cell_title_Label.text=nameArr[indexPath.row];
        
        return cell1;
        
        
        
    }

}



- (IBAction)segmentDidChange:(id)sender
{
    
    //_detailView.hidden=YES;
   // _back_view.hidden=NO;
    
    
    
    
    [self updateSelectedSegmentLabel];
}

-(IBAction)detailBack
{
    _imageMobileview.hidden=YES;
    _imageAlbumView.hidden=NO;
    _imageDetailView.hidden=YES;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    ////(@"Select %d",indexPath.row);
    
    
    if (collectionView==collectionViewIMAGE)
    {
        
        selectIndex=indexPath.row;
        
        if (indexPath.row==0)
        {
                rootImageView.image = selectedImage;
                
               
            }
              if (indexPath.row==1)
            {
                
                rootImageView.image = selectedImage;
                               
                rootImageView.image = [ImageUtil imageWithImage:selectedImage withColorMatrix:colormatrix_lomo];
                
                              
            }
           if (indexPath.row==2)
            {
                rootImageView.image = [ImageUtil imageWithImage:selectedImage withColorMatrix:colormatrix_heibai];
                
              
            }
          if (indexPath.row==3)
            {
                rootImageView.image = [ImageUtil imageWithImage:selectedImage withColorMatrix:colormatrix_huajiu];
                
                              
            }
         if (indexPath.row==4)
            {
                rootImageView.image = [ImageUtil imageWithImage:selectedImage withColorMatrix:colormatrix_gete];
                
             
            }
        if (indexPath.row==5)
            {
                rootImageView.image = [ImageUtil imageWithImage:selectedImage withColorMatrix:colormatrix_ruise];
                
             
            }
              if (indexPath.row==6)
            {
                rootImageView.image = [ImageUtil imageWithImage:selectedImage withColorMatrix:colormatrix_danya];
                
           
            }
        if (indexPath.row==7)
            {
                rootImageView.image = [ImageUtil imageWithImage:selectedImage withColorMatrix:colormatrix_jiuhong];
                        }
     if (indexPath.row==8)
            {
                rootImageView.image = [ImageUtil imageWithImage:selectedImage withColorMatrix:colormatrix_qingning];
                
                         }
       if (indexPath.row==9)
            {
                rootImageView.image = [ImageUtil imageWithImage:selectedImage withColorMatrix:colormatrix_langman];
                
            
            }
     if (indexPath.row==10)
            {
                rootImageView.image = [ImageUtil imageWithImage:selectedImage withColorMatrix:colormatrix_guangyun];
                         }
        if (indexPath.row==11)
            {
                rootImageView.image = [ImageUtil imageWithImage:selectedImage withColorMatrix:colormatrix_landiao];
            
            }
         if (indexPath.row==12)
            {
                rootImageView.image = [ImageUtil imageWithImage:selectedImage withColorMatrix:colormatrix_menghuan];
                       }
      if (indexPath.row==13)
            {
                rootImageView.image = [ImageUtil imageWithImage:selectedImage withColorMatrix:colormatrix_yese];
                           }
        
        [collectionViewIMAGE reloadData];

    }
     if (collectionView==collectionViewMain)
    {
        
        
        
       
        imageSourceArray=[[NSMutableArray alloc]init];
        imageIDArray=[[NSMutableArray alloc]init];
        
               
        imageThumpArray=[[NSMutableArray alloc]init];
        
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/photos/getFacebookAlbumPhotos",CONFIG_BASE_URL]];
     
        
        __block  ASIFormDataRequest *request_post1234 = [ASIFormDataRequest requestWithURL:url];
        
        
        
        NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
        NSString *user=[check objectForKey:@"USERID"];
        NSString *access=[check objectForKey:@"ACCESS_TOKEN"];
        
        NSLog(@"URL:%@",url);
        
         NSLog(@"albumID:%@",albumIDArray[indexPath.row]);
          NSLog(@"fbtoken:%@",access);
          NSLog(@"userID:%@",user);
          NSLog(@"apikey:%@",[Config get_token]);
        NSLog(@"item_count:%d",1000);
        NSLog(@"current_count:%d",0);
        
        [request_post1234 setPostValue:access forKey:@"fbtoken"];
        [request_post1234 setPostValue:user forKey:@"userID"];
         [request_post1234 setPostValue:albumIDArray[indexPath.row] forKey:@"albumID"];
        [request_post1234 setPostValue:@"1000" forKey:@"item_count"];
        [request_post1234 setPostValue:@"0" forKey:@"current_count"];
         [request_post1234 setPostValue:[Config get_token] forKey:@"apikey"];
        [request_post1234 setTimeOutSeconds:30];
        
        
        [request_post1234 setCompletionBlock:^{
            NSString *responseString23 = [request_post1234 responseString];
            NSMutableData *results11 = [responseString23 JSONValue];
            
            
            //////(@"RESULTS LOGIN:%@",results11);
            
            
            NSArray *temp= [results11 valueForKeyPath:@"response.photos"];
            
            
            ////(@"TEMPARY:%@",temp);
            
            for(NSDictionary *value in temp)
            {
                [imageIDArray  addObject:[value valueForKey:@"photoid"]];
                [imageThumpArray  addObject:[value valueForKey:@"picture"]];
                [imageSourceArray  addObject:[value valueForKey:@"source"]];
                
            }
            
            
            ////(@"ARRAY:%@",imageThumpArray);
            
            [collectionViewFacebook reloadData];
            
            
            _detailLabel.text=albumNameArray[indexPath.row];
            _imageMobileview.hidden=YES;
            _imageAlbumView.hidden=YES;
            _imageDetailView.hidden=NO;

            
        }];
        [request_post1234 setFailedBlock:^{
            NSError *error = [request_post1234 error];
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                      cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            
            
            
        }];
        
        [request_post1234 startAsynchronous];
        
        
        
        
        
       
        
    }
    
    if (collectionView==collectionViewFacebook) {
        
         
         NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
         NSString *imageURL = imageSourceArray[indexPath.row];
         
         ////(@"IMAGE URL:%@",imageSourceArray[indexPath.row]);
         
         
         NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
         UIImage *image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imageURL]]];
         
         NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"savedImage.jpg"]];
         if(image != NULL)
         {
         //Store the image in Document
         
         
         NSData *imageData = UIImagePNGRepresentation(image);
         [imageData writeToFile: imagePath atomically:YES];
         }
         
         
         NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:@"savedImage.jpg"];
         UIImage *img = [UIImage imageWithContentsOfFile:getImagePath];
         
         
         NSString *sourceImage = imageSourceArray[indexPath.row];
         NSString *thumpImage = imageThumpArray[indexPath.row];
         NSString *idImage = imageIDArray[indexPath.row];
         
         
         NSUserDefaults *storeImageValues=[NSUserDefaults standardUserDefaults];
         [storeImageValues setObject:sourceImage forKey:@"IMAGE_REFERENCE"];
         [storeImageValues setObject:thumpImage forKey:@"THUMPNAIL"];
         [storeImageValues setObject:idImage forKey:@"IMAGE_ID"];
        
        
        
        
        if (IS_IPHONE_5)
        {
            ViewController1 *viewController1=[[ViewController1 alloc]initWithNibName:@"ViewController_iPhone" bundle:nil];
            [self presentViewController:viewController1 animated:NO completion:^{
                //////(@"SUCCESS");
            }
             ];
        }
        else
        {
            ViewController1 *viewController1=[[ViewController1 alloc]initWithNibName:@"ViewController_iPhone~iPhone" bundle:nil];
            [self presentViewController:viewController1 animated:NO completion:^{
                //////(@"SUCCESS");
            }
             ];
        }
         
       
         
    }
}


- (void)updateSelectedSegmentLabel
{
    
   
    
    self.selectedSegmentLabel.font = [UIFont boldSystemFontOfSize:self.selectedSegmentLabel.font.pointSize];
    self.selectedSegmentLabel.text = [NSString stringWithFormat:@"%d", self.segmentedControl.selectedSegmentIndex];
    
    
    if (self.segmentedControl.selectedSegmentIndex==0)
    {
        
        //collectionView.hidden=NO;
        //_back_view.hidden=YES;
        
        _saveButton.hidden=YES;
        
        _imageMobileview.hidden=YES;
        _imageAlbumView.hidden=NO;
        _imageDetailView.hidden=YES;
        
    }
    
    if (self.segmentedControl.selectedSegmentIndex==1)
    {
        
        // collectionView.hidden=YES;
        //_back_view.hidden=NO;
        
        if(imageIndexCheck==0)
        
        {
        _saveButton.hidden=YES;
        
        NSString *actionSheetTitle = @"Select your Image from"; //Action Sheet Title
        //NSString *destructiveTitle = @"Destructive Button"; //Action Sheet Button Titles
        NSString *other1 = @"Camera";
        NSString *other2 = @"Albums";
        NSString *cancelTitle = @"Cancel";
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:actionSheetTitle
                                      delegate:self
                                      cancelButtonTitle:cancelTitle
                                      destructiveButtonTitle:nil
                                      otherButtonTitles:other1, other2, nil];
        [actionSheet showInView:self.view];
        
        
        }
        else
        {
             _saveButton.hidden=NO;
        }
        _imageMobileview.hidden=NO;
        _imageAlbumView.hidden=YES;
        _imageDetailView.hidden=YES;
    
    }
    
  
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, .5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^(void)
                   {
                       self.selectedSegmentLabel.font = [UIFont systemFontOfSize:self.selectedSegmentLabel.font.pointSize];
                   });
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (buttonIndex==0)
    {
        ////(@"IND1");
        
        [self cameraClick];
        
        //self.segmentedControl.selectedSegmentIndex=0;
       // [self updateSelectedSegmentLabel];
    }
    else if (buttonIndex==1)
    {
        ////(@"IND2");
        
        [self albumClick];
        
        //self.segmentedControl.selectedSegmentIndex=0;
        // [self updateSelectedSegmentLabel];
        
    }
    else
    {
        
        if (imageIndexCheck==0)
        {
            
            self.segmentedControl.selectedSegmentIndex=0;
            [self updateSelectedSegmentLabel];
            
            
            _imageMobileview.hidden=YES;
            _imageAlbumView.hidden=NO;
            _imageDetailView.hidden=YES;
        }
        else
        {
            _saveButton.hidden=NO;
        }
       
    }
}






- (void) pushMenuItem:(id)sender
{
    ////(@"%@", sender);
    NSString *string=[NSString stringWithFormat:@"%@",sender];
    ////(@"HELLO:%@",string);
    
    
    
    
    
}
-(IBAction)meButton
{
    if (IS_IPHONE_5) {
        MeViewController *viewController=[[MeViewController alloc]initWithNibName:@"MeViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
    
    MeViewController *viewController=[[MeViewController alloc]initWithNibName:@"MeViewController~iPhone" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        //////(@"SUCCESS");
    }
     ];
    }
}
-(IBAction)dealsButton
{
    dealsViewController *viewController=[[dealsViewController alloc]initWithNibName:@"dealsViewController" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        //////(@"SUCCESS");
    }
     ];
}
-(IBAction)socialButton
{
    if (IS_IPHONE_5) {
        netCoinsViewController *viewController=[[netCoinsViewController alloc]initWithNibName:@"netCoinsViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
    netCoinsViewController *viewController=[[netCoinsViewController alloc]initWithNibName:@"netCoinsViewController~iPhone" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        //////(@"SUCCESS");
    }
     ];
    }
}
-(IBAction)netCoinsButton
{
    if (IS_IPHONE_5) {
        socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }

}
-(IBAction)myBusinessButton
{
    myBusinessViewController *viewController=[[myBusinessViewController alloc]initWithNibName:@"myBusinessViewController" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        //////(@"SUCCESS");
    }
     ];
}
- (BOOL)textFieldShouldReturn: (UITextField *)textField
{
  
    [textField resignFirstResponder];
    
    return YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)but1
{
    
    UIImage *ch=[UIImage imageNamed:@"ADE.jpeg"];
    
    rootImageView.image = [ImageUtil imageWithImage:selectedImage withColorMatrix:colormatrix_lomo];
  
}
-(IBAction)but2
{
    
     UIImage *ch=[UIImage imageNamed:@"ADE.jpeg"];
    rootImageView.image = [ImageUtil imageWithImage:selectedImage withColorMatrix:colormatrix_huajiu];
    
    
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //UIImagePickerControllerOriginalImage
    //UIImagePickerControllerEditedImage
    
    UIImage *image;
    
    if (picker.sourceType==UIImagePickerControllerSourceTypeCamera)
    {
        ////(@"CAMERA");
        image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];//获取图片
    }
    else
    {
        ////(@"ALBUM");
        
          image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];//获取图片
    }
    
    
   
    
    
    
    float ori_height=image.size.height;
     float ori_width=image.size.width;
    
    
    //(@"HEIGHT ORI:%f",ori_height);
    //(@"WIDTH ORI:%f",ori_width);
    
    
    NSString *status=@"";
    
    if (ori_height>ori_width)
    {
        status=@"PORTRAIT";
    }
    
    if (ori_width>ori_height)
    {
        status=@"LANDSCAPE";
    }

  
    
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera)
    {
      //  UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);//将拍到的图片保存到相册
    }
    
    
    
    //_imageeditView.hidden=NO;
    
    currentImage = [self imageWithImageSimple:image scaledToSize:CGSizeMake(308, 397)];
    
   // selectedImage  = [self imageWithImageSimple:image scaledToSize:CGSizeMake(308, 397)];
    
    
    selectedImage=image;
    currentImage=image;
    
    
    
    
    if ([status isEqualToString:@"LANDSCAPE"])
    {
        
        
        
        ////(@"landscape");
        
        
        if (ori_height<1000)
        {
            UIGraphicsBeginImageContext(CGSizeMake(ori_width,ori_height));
            
            CGContextRef            context = UIGraphicsGetCurrentContext();
            
            [currentImage drawInRect: CGRectMake(0, 0, ori_width, ori_width)];
        }
        else
        {
        int height_ratio=ori_height/4;
        int width_ratio=ori_width/4;

        
        
        UIGraphicsBeginImageContext(CGSizeMake(width_ratio,height_ratio));
        
        CGContextRef            context = UIGraphicsGetCurrentContext();
        
        [currentImage drawInRect: CGRectMake(0, 0, width_ratio, height_ratio)];
        }
        
    }
    else if ([status isEqualToString:@"PORTRAIT"])
    {
        ////(@"portrait");
        if (ori_height<1000)
        {
            UIGraphicsBeginImageContext(CGSizeMake(ori_width,ori_height));
            
            CGContextRef            context = UIGraphicsGetCurrentContext();
            
            [currentImage drawInRect: CGRectMake(0, 0, ori_width, ori_width)];
        }
        else
        {
        int height_ratio=ori_height/4;
        int width_ratio=ori_width/4;
        
        
        
        UIGraphicsBeginImageContext(CGSizeMake(width_ratio,height_ratio));
        CGContextRef            context = UIGraphicsGetCurrentContext();
        
        [currentImage drawInRect: CGRectMake(0, 0, width_ratio, height_ratio)];
            
        }
    }

    
    
    
   
    /*
    if (picker.sourceType==UIImagePickerControllerSourceTypeCamera)
    {
    
    if (image.imageOrientation == UIImageOrientationUp)
    {
       
        
        
        ////(@"landscape");
        
        UIGraphicsBeginImageContext(CGSizeMake(480,320));
        
        CGContextRef            context = UIGraphicsGetCurrentContext();
        
        [currentImage drawInRect: CGRectMake(0, 0, 480, 320)];
        
        
    }
    else if (image.imageOrientation == UIImageOrientationLeft || image.imageOrientation == UIImageOrientationRight)
    {
        ////(@"portrait");
        
        UIGraphicsBeginImageContext(CGSizeMake(320,480));
        
        CGContextRef            context = UIGraphicsGetCurrentContext();
        
        [currentImage drawInRect: CGRectMake(0, 0, 320, 480)];
    }
    
    }
    else
    {
        
        UIGraphicsBeginImageContext(CGSizeMake(320,480));
        
        CGContextRef            context = UIGraphicsGetCurrentContext();
        
        [currentImage drawInRect: CGRectMake(0, 0, 320, 480)];
    }
    
   */
    
    UIImage        *smallImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    
   // UIImage *scaledImage = [UIImage imageWithCGImage:[currentImage CGImage] scale:50/800 orientation:UIImageOrientationUp];
    
    ////(@"HEIGHT A:%f",smallImage.size.height);
    ////(@"WIDTH A:%f",smallImage.size.width);
    
    
   
    _saveButton.hidden=NO;
    
    imageIndexCheck=1;
    
    
    //    currentImage = image;
    
    //调用imageWithImageSimple:scaledToSize:方法
    
   // [currentImage retain];
    
   // rootImageView.image = smallImage;
    
    
    
    
   
    
    
    
    
    
    // Do any additional setup after loading the view, typically from a nib.
    cropper.layer.borderWidth = 2.0;
    //cropper.layer.borderColor = [UIColor colorWithRed:245/255.0 green:127/255.0 blue:0/255.0 alpha:1].CGColor;
    
    cropper.layer.borderColor = [UIColor clearColor].CGColor;
    
    [cropper setup];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:@"savedImage.jpg"];
    UIImage *img = [UIImage imageWithContentsOfFile:getImagePath];
    
    //cropper.image=nil;
    
    cropper.image = smallImage;
    
    
    NSString *getImagePath1 = [documentsDirectory stringByAppendingPathComponent:@"profileImage.jpg"];
    UIImage *profileIMG = [UIImage imageWithContentsOfFile:getImagePath1];
    
    
   // [_profileButton setImage:profileIMG forState:UIControlStateNormal];
    
    
    [btn addTarget:self action:@selector(buttonClicked) forControlEvents:UIControlEventTouchUpInside];

    
     [self dismissModalViewControllerAnimated:YES];//关闭模态视图控制器
    
    
    /*
    imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 45, 50)];
    rootImage = [UIImage imageNamed:@"110.png"];
    imageView.image = rootImage;
    [scrollerView addSubview:imageView];
    //[imageView release];
    
    seg.userInteractionEnabled = YES;
    [self.view addSubview:rootImageView];*/
   
    
    ////(@"AFTER LAUNCH");
    
   // [self save];
    
    /*
    
    UIImage *finalImage=currentImage;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:@"savedImage.jpg"];
    UIImage *image1 = currentImage; // imageView is my image from camera
    NSData *imageData = UIImagePNGRepresentation(image1);
    [imageData writeToFile:savedImagePath atomically:NO];
    
    
    NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:@"savedImage.jpg"];
    UIImage *img = [UIImage imageWithContentsOfFile:getImagePath];
    
    
   // [self nextViewSend:nil];
     */
    
    
    
    
}



-(IBAction)imageAdd
{
    
    _saveButton.hidden=YES;
    
    NSString *actionSheetTitle = @"Select your Image from"; //Action Sheet Title
    //NSString *destructiveTitle = @"Destructive Button"; //Action Sheet Button Titles
    NSString *other1 = @"Camera";
    NSString *other2 = @"Albums";
    NSString *cancelTitle = @"Cancel";
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:actionSheetTitle
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:other1, other2, nil];
    [actionSheet showInView:self.view];
    
    
    _imageMobileview.hidden=NO;
    _imageAlbumView.hidden=YES;
    _imageDetailView.hidden=YES;
}
-(IBAction)nextViewSend:(id)sender

{
    if (IS_IPHONE_5)
    {
        ViewController1 *viewController1=[[ViewController1 alloc]initWithNibName:@"ViewController_iPhone" bundle:nil];
        [self presentViewController:viewController1 animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        ViewController1 *viewController1=[[ViewController1 alloc]initWithNibName:@"ViewController_iPhone~iPhone" bundle:nil];
        [self presentViewController:viewController1 animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
}
-(UIImage *)imageWithImageSimple:(UIImage*)image scaledToSize:(CGSize)newSize
{
    
    UIGraphicsBeginImageContext(newSize);//根据当前大小创建一个基于位图图形的环境
    
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];//根据新的尺寸画出传过来的图片
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();//从当前环境当中得到重绘的图片
    
    UIGraphicsEndImageContext();//关闭当前环境
    
    return newImage;
}



-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1)
    {
        
        //        NSData *newData = UIImageJPEGRepresentation(currentImage, 1);
        //        NSString *docpath = @"/Users/ibokan/Desktop";
        //        NSString *path = [docpath stringByAppendingPathComponent:@"aaa.jpg"];
        //        [newData writeToFile:path atomically:YES];
        
        UIImage *img = rootImageView.image;// 要保存的图片
        UIImageWriteToSavedPhotosAlbum(img, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);// 保存图片到相册中
        
        
    }
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error
  contextInfo:(void *)contextInfo
{
    if (error != NULL)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Prompt" message:@"Save failed, please re-save" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil, nil];
        //        alert.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
        [alert show];
      //  [alert release];
        
    }
    else// 没有error
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Information" message:@"Successfully saved" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
       // [alert release];
    }
}

//-(void)move
//{
//    CGPoint fromPoint = imageView.bounds.origin;
//
//    UIBezierPath *movePath = [UIBezierPath bezierPath];
//    [movePath moveToPoint:fromPoint];
//    CGPoint toPoint = CGPointMake(46, 30);
//    [movePath addQuadCurveToPoint:toPoint controlPoint:CGPointMake(toPoint.x, fromPoint.x)];
//
//    //关键帧
//    CAKeyframeAnimation *moveAnim = [CAKeyframeAnimation animationWithKeyPath:@"position"];
//    moveAnim.path = movePath.CGPath;
//    moveAnim.removedOnCompletion = YES;

//    //旋转变化
//    CABasicAnimation *scaleAnim = [CABasicAnimation animationWithKeyPath:@"transform"];
//    scaleAnim.fromValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
//    //x，y轴缩小到0.1,Z 轴不变
//    scaleAnim.toValue = [NSValue valueWithCATransform3D:CATransform3DMakeScale(0.1, 0.1, 1.0)];
//    scaleAnim.removedOnCompletion = YES;
//
//    //关键帧，旋转组合起来执行
//    CAAnimationGroup *animGroup = [CAAnimationGroup animation];
//    animGroup.animations = [NSArray arrayWithObjects:moveAnim, scaleAnim, nil];
//    animGroup.duration = 1;
//    [imageView.layer addAnimation:moveAnim forKey:nil];
//}


-(void)changeImage:(UISegmentedControl *)segment
{
    switch (segment.selectedSegmentIndex) {
        case 0:
        {
            rootImageView.image = currentImage;
            
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDelay:0.3];
            imageView.frame = CGRectMake(0, 0, 45, 50);
            [UIView commitAnimations];
        }
            break;
        case 1:
        {
            rootImageView.image = [ImageUtil imageWithImage:currentImage withColorMatrix:colormatrix_lomo];
            
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDelay:0.3];
            imageView.Frame = CGRectMake(46, 0, 45, 50);
            [UIView commitAnimations];
            
        }
            break;
        case 2:
        {
            rootImageView.image = [ImageUtil imageWithImage:currentImage withColorMatrix:colormatrix_heibai];
            
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDelay:0.3];
            imageView.frame = CGRectMake(46*2, 0, 45, 50);
            [UIView commitAnimations];
        }
            break;
        case 3:
        {
            rootImageView.image = [ImageUtil imageWithImage:currentImage withColorMatrix:colormatrix_huajiu];
            
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDelay:0.3];
            imageView.frame = CGRectMake(46*3, 0, 45, 50);
            [UIView commitAnimations];
            
        }
            break;
        case 4:
        {
            rootImageView.image = [ImageUtil imageWithImage:currentImage withColorMatrix:colormatrix_gete];
            
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDelay:0.3];
            imageView.frame = CGRectMake(46*4, 0, 45, 50);
            [UIView commitAnimations];
        }
            break;
        case 5:
        {
            rootImageView.image = [ImageUtil imageWithImage:currentImage withColorMatrix:colormatrix_ruise];
            
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDelay:0.3];
            imageView.frame = CGRectMake(46*5, 0, 45, 50);
            [UIView commitAnimations];
        }
            break;
        case 6:
        {
            rootImageView.image = [ImageUtil imageWithImage:currentImage withColorMatrix:colormatrix_danya];
            
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDelay:0.3];
            imageView.frame = CGRectMake(46*6, 0, 45, 50);
            [UIView commitAnimations];
        }
            break;
        case 7:
        {
            rootImageView.image = [ImageUtil imageWithImage:currentImage withColorMatrix:colormatrix_jiuhong];
            
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDelay:0.3];
            imageView.frame = CGRectMake(46*7, 0, 45, 50);
            [UIView commitAnimations];
        }
            break;
        case 8:
        {
            rootImageView.image = [ImageUtil imageWithImage:currentImage withColorMatrix:colormatrix_qingning];
            
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDelay:0.3];
            imageView.frame = CGRectMake(46*8, 0, 45, 50);
            [UIView commitAnimations];
        }
            break;
        case 9:
        {
            rootImageView.image = [ImageUtil imageWithImage:currentImage withColorMatrix:colormatrix_langman];
            
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDelay:0.3];
            imageView.frame = CGRectMake(46*9, 0, 45, 50);
            [UIView commitAnimations];
        }
            break;
        case 10:
        {
            rootImageView.image = [ImageUtil imageWithImage:currentImage withColorMatrix:colormatrix_guangyun];
            
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDelay:0.3];
            imageView.frame = CGRectMake(46*10, 0, 45, 50);
            [UIView commitAnimations];
        }
            break;
        case 11:
        {
            rootImageView.image = [ImageUtil imageWithImage:currentImage withColorMatrix:colormatrix_landiao];
            
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDelay:0.3];
            imageView.frame = CGRectMake(46*11, 0, 45, 50);
            [UIView commitAnimations];
        }
            break;
        case 12:
        {
            rootImageView.image = [ImageUtil imageWithImage:currentImage withColorMatrix:colormatrix_menghuan];
            
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDelay:0.3];
            imageView.frame = CGRectMake(46*12, 0, 45, 50);
            [UIView commitAnimations];
        }
            break;
        case 13:
        {
            rootImageView.image = [ImageUtil imageWithImage:currentImage withColorMatrix:colormatrix_yese];
            
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDelay:0.3];
            imageView.frame = CGRectMake(46*13, 0, 45, 50);
            [UIView commitAnimations];
        }
        default:
            break;
    }
}


@end
