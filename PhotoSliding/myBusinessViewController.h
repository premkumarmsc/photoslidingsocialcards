//
//  dealsViewController.h
//  PhotoSliding
//
//  Created by ephronsystems on 6/27/13.
//
//

#import <UIKit/UIKit.h>

@interface myBusinessViewController : UIViewController
-(IBAction)meButton;
-(IBAction)dealsButton;
-(IBAction)netCoinsButton;
-(IBAction)socialButton;
-(IBAction)myBusinessButton;
@end
