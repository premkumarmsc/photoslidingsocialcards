//
//  UpdatesTableViewCell.h
//  UpdatesListView
//
//  Created by Tope on 10/11/2011.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReceipCell : UITableViewCell
@property(nonatomic,retain )IBOutlet UIImageView *img;
@property(nonatomic,retain )IBOutlet UILabel *title;
@property(nonatomic,retain )IBOutlet UILabel *details;
@property(nonatomic,retain )IBOutlet UILabel *date;

@property(nonatomic,retain )IBOutlet UIButton *updateBtn;
@property(nonatomic,retain )IBOutlet UIButton *reademBtn;


@end
