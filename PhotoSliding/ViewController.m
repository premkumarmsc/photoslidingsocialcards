//
//  ViewController.m
//  PhotoSliding
//
//  Created by Neon Spark on 1/21/12.
//  Copyright (c) 2012 http://sugartin.info. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"
@implementation ViewController
NSTimer * myTimer;
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle


NSArray *img_arr;


- (void)viewDidLoad
{
    
    
    
    FBLoginView *loginview = [[FBLoginView alloc] init];
    loginview.readPermissions = @[@"email", @"user_likes",@"user_hometown",
                                  @"user_location",
                                  @"user_about_me",
                                  @"friends_birthday",
                                  @"friends_photos",
                                  @"friends_work_history",
                                  @"friends_location",
                                  @"user_subscriptions",
                                  @"read_stream",
                                  @"user_birthday",
                                  @"friends_about_me",
                                  @"friends_likes",
                                  @"user_likes",
                                  @"user_photos"
                                  ];
    
    //loginview.publishPermissions=@[@"publish_stream",@"publish_actions"];
    
    
    if (IS_IPHONE_5)
    {
         loginview.frame = CGRectMake(50, 470, 271, 37);
    }
    else
        
    {
       loginview.frame = CGRectMake(50, 390, 271, 37);
    }
   
    
    
    
    for (id obj in loginview.subviews)
    {
        if ([obj isKindOfClass:[UIButton class]])
        {
            UIButton * loginButton =  obj;
            UIImage *loginImage = [UIImage imageNamed:@"fffacebook.png"];
            [loginButton setBackgroundImage:loginImage forState:UIControlStateNormal];
            [loginButton setBackgroundImage:nil forState:UIControlStateSelected];
            [loginButton setBackgroundImage:nil forState:UIControlStateHighlighted];
            [loginButton sizeToFit];
        }
        if ([obj isKindOfClass:[UILabel class]])
        {
            UILabel * loginLabel =  obj;
            loginLabel.text = @"";
            loginLabel.textAlignment = UITextAlignmentCenter;
            loginLabel.frame = CGRectMake(0, 0, 271, 37);
        }
    }
    
    loginview.delegate = self;
    
    [self.view addSubview:loginview];
    
    
    /*
     FBLoginView *loginview = [[FBLoginView alloc] init];
     loginview.frame = CGRectOffset(loginview.frame, 5, 5);
     loginview.delegate = self;
     
     [self.view addSubview:loginview];
     */
    //[loginview sizeToFit];
    
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.activityIndicator.hidesWhenStopped = YES;
    [self.view addSubview:self.activityIndicator];
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    [super viewDidLoad];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    
     self.trackedViewName = @"Home Screen";
    
    NSUserDefaults *add=[NSUserDefaults standardUserDefaults];
    
      ////(@"DEVICE TOKEN 123:%@",[add objectForKey:@"DEVICE_TOKEN"]);
    
    // [[UIScreen mainScreen] setBrightness:0.5];
    
    if (IS_IPHONE_5)
    {
        UIScrollView *scr=[[UIScrollView alloc] initWithFrame:CGRectMake(0, 200, 320, 250)];
        
        [scr setShowsHorizontalScrollIndicator:NO];
        [scr setShowsVerticalScrollIndicator:NO];
        
        scr.tag = 1;
        scr.autoresizingMask=UIViewAutoresizingNone;
        [self.view addSubview:scr];
        [self setupScrollView:scr];
        UIPageControl *pgCtr = [[UIPageControl alloc] initWithFrame:CGRectMake(25, 264, 480, 36)];
        
        [pgCtr setTag:12];
        pgCtr.numberOfPages=10;
        pgCtr.autoresizingMask=UIViewAutoresizingNone;
        pgCtr.hidden=YES;
        [self.view addSubview:pgCtr];
    }
    else
    {
        
        UIScrollView *scr=[[UIScrollView alloc] initWithFrame:CGRectMake(0, 160, 320, 250)];
        
        [scr setShowsHorizontalScrollIndicator:NO];
        [scr setShowsVerticalScrollIndicator:NO];
        
        scr.tag = 1;
        scr.autoresizingMask=UIViewAutoresizingNone;
        [self.view addSubview:scr];
        [self setupScrollView:scr];
        UIPageControl *pgCtr = [[UIPageControl alloc] initWithFrame:CGRectMake(25, 264, 480, 36)];
        
        [pgCtr setTag:12];
        pgCtr.numberOfPages=10;
        pgCtr.autoresizingMask=UIViewAutoresizingNone;
        pgCtr.hidden=YES;
        [self.view addSubview:pgCtr];
    }
    
  
     
    
    img_arr=[NSArray arrayWithObjects:@"s1.png",@"s2.png",@"s3.png",@"s4.png", nil];
    
    
    //[self getImage];
    
  // myTimer = [NSTimer scheduledTimerWithTimeInterval:3.0
                                          //               target:self
                                                //       selector:@selector(getImage)
                                                //       userInfo:nil
                                                 //       repeats:YES];
    
  
    
    
    
}

-(void)getImage
{
    _imgplatinum.hidden=YES;
   
    CATransition *transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transition.duration = 0.5;
    transition.type = kCAScrollNone;
    [self.view.layer addAnimation:transition forKey:nil];
   
    _imgplatinum.hidden=NO;
    
    NSUInteger randomIndex = arc4random() % [img_arr count];
        
    _imgplatinum.image=[UIImage imageNamed:img_arr[randomIndex]];
}



- (void)setupScrollView:(UIScrollView*)scrMain
{
    
    
    for (int i=1; i<=4; i++)
    {
           
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"s%d.png",i]];
       
        UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake((i-1)*scrMain.frame.size.width+25, 0, 265, 189)];
      
        imgV.contentMode=UIViewContentModeScaleToFill;
       
        [imgV setImage:image];
               imgV.tag=i+1;
       
        [scrMain addSubview:imgV];
       
    }
  
    [scrMain setContentSize:CGSizeMake(scrMain.frame.size.width*4, scrMain.frame.size.height)];
   
    [NSTimer scheduledTimerWithTimeInterval:4 target:self selector:@selector(scrollingTimer) userInfo:nil repeats:YES];
}

-(void)deletebuttonClicked:(UIButton*)button
{
    
    ////////////(@"update Button %ld clicked.",(long int)[button tag]);
    
    
    
    //////(@"DELETE INDEX:%ld",(long int)[button tag]);
    
    
}


- (void)scrollingTimer {
    // access the scroll view with the tag
    UIScrollView *scrMain = (UIScrollView*) [self.view viewWithTag:1];
    // same way, access pagecontroll access
    UIPageControl *pgCtr = (UIPageControl*) [self.view viewWithTag:6];
    // get the current offset ( which page is being displayed )
    CGFloat contentOffset = scrMain.contentOffset.x;
    // calculate next page to display
    int nextPage = (int)(contentOffset/scrMain.frame.size.width) + 1 ;
    // if page is not 10, display it
    if( nextPage!=4 )
    {
        [scrMain scrollRectToVisible:CGRectMake(nextPage*scrMain.frame.size.width, 0, scrMain.frame.size.width, scrMain.frame.size.height) animated:YES];
        pgCtr.currentPage=nextPage;
    // else start sliding form 1 :)
    } else
    {
        [scrMain scrollRectToVisible:CGRectMake(0, 0, scrMain.frame.size.width, scrMain.frame.size.height) animated:YES];
        pgCtr.currentPage=0;
    }
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];
}


#ifdef IOS_OLDER_THAN_6
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return (toInterfaceOrientation == UIInterfaceOrientationMaskPortrait);
}
#endif
#ifdef IOS_NEWER_OR_EQUAL_TO_6
-(BOOL)shouldAutorotate {
    return YES;
}
- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
#endif



// FACEBOOK START

-(IBAction)faceBookClick:(id)sender
{
    
    //[self sen1];
   // [self sen2];
}



/*
-(void)sen2
{
    AppDelegate *getdelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    getdelegate.facebook.sessionDelegate=self;
    
    {
        
        
        [getdelegate.facebook authorize:[NSArray arrayWithObjects:@"user_photos",
                                         @"publish_stream",
                                         @"email",
                                         @"read_stream",
                                         @"user_birthday",
                                         @"friends_about_me",
                                         @"friends_likes",
                                         @"user_likes",
                                         @"user_photos",
                                         @"user_photos",
                                         @"publish_actions",
                                         @"user_hometown",
                                         @"user_location",
                                         @"user_about_me",
                                         @"friends_birthday",
                                         @"friends_photos",
                                         @"friends_work_history",
                                         @"friends_location",
                                         @"user_subscriptions",
                                         nil]];
        
    }
}
- (void)fbDidLogin
{
    AppDelegate *getdelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    //[[NSUserDefaults standardUserDefaults] setValue: getdelegate.facebook.accessToken forKey: @"access_token"];
    //[[NSUserDefaults standardUserDefaults] setValue: getdelegate.facebook.expirationDate forKey: @"expiration_date"];
    [getdelegate.facebook requestWithGraphPath:@"me" andDelegate:self];
    
    NSLog(@"Logined into facebook");
    
}



- (void) request:(FBRequest*)request didLoad:(id)result
{
    AppDelegate *getdelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSLog(@"TOKEN:%@",getdelegate.facebook.accessToken);
    
    
    NSString *facebookAccessToken=[NSString stringWithFormat:@"%@",getdelegate.facebook.accessToken];
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    if ([result isKindOfClass:[NSDictionary class]])
    {
        
        
        
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/authenticateUser",CONFIG_BASE_URL]];
        
        
        ////(@"HELLO:%@",url);
        
        __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
        
        
        
        
        
        [request_post setValidatesSecureCertificate:NO];
        [request_post setPostValue:facebookAccessToken forKey:@"facebook_access_token"];
        [request_post setPostValue:[Config get_token] forKey:@"apikey"];
        
        
        [request_post setTimeOutSeconds:30];
        
        
        [request_post setCompletionBlock:^{
            NSString *responseString = [request_post responseString];
            NSMutableData *results1 = [responseString JSONValue];
            
            
            ////(@"RESULTS HELL:%@",results1);
            
            NSString *fbUserID=[results1 valueForKeyPath:@"Response.facebook_user_id"];
            NSString *userID=[results1 valueForKeyPath:@"Response.userID"];
            NSString *facebookToken=[results1 valueForKeyPath:@"Response.facebook_access_token"];
            
            ////(@"FB USER:%@",fbUserID);
            ////(@"userID:%@",userID);
            
            
            NSUserDefaults *add=[NSUserDefaults standardUserDefaults];
            
            [add setObject:fbUserID forKey:@"FB_USERID"];
            [add setObject:userID forKey:@"USERID"];
            [add setObject:facebookToken forKey:@"FB_ACCESS_TOKEN"];
            
            
            ////(@"DEVICE TOKEN:%@",[add objectForKey:@"DEVICE_TOKEN"]);
            
            // NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/setExtraction",CONFIG_BASE_URL]];
            
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/setExtraction",CONFIG_BASE_URL]];
            
            __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
            
            
            ////(@"USER NE NE:%@",userID);
            
            
            if (results1)
            {
                
                NSUserDefaults *chk=[NSUserDefaults standardUserDefaults];
                [chk setObject:@"YES" forKey:@"IS_FIRSTTIME"];
                
                
                if( IS_IPHONE_5 )
                {
                    socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController" bundle:nil];
                    
                    // viewController.comingString=@"FIRSTTIME";
                    
                    [self presentViewController:viewController animated:NO completion:^{
                        
                    }
                     ];
                    
                }
                else
                {
                    socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController~iPhone" bundle:nil];
                    
                    // viewController.comingString=@"FIRSTTIME";
                    
                    [self presentViewController:viewController animated:NO completion:^{
                        
                    }
                     ];
                    
                }
                
                [myTimer invalidate];
                
                              
            }
            
            
            [request_post setValidatesSecureCertificate:NO];
            [request_post setPostValue:userID forKey:@"userID"];
            [request_post setPostValue:@"facebook" forKey:@"network"];
            
            
            
            [request_post setTimeOutSeconds:30];
            
            
            [request_post setCompletionBlock:^{
                NSString *responseString = [request_post responseString];
                NSMutableData *results1 = [responseString JSONValue];
                
                
                ////(@"RESULTS ASAS:%@",results1);
                
                
                
                
                
                
            }];
            [request_post setFailedBlock:^{
                NSError *error = [request_post error];
                UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                    message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                          cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                
                
                
            }];
            
            [request_post startAsynchronous];
            
            
            
        }];
        [request_post setFailedBlock:^{
            NSError *error = [request_post error];
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                      cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            
            
            
        }];
        
        [request_post startAsynchronous];
        
        
        
        
        
        
        
        
        //getdelegate.fbuserName = [result objectForKey: @"username"];
    }
    //[getdelegate  fbauthenticationDidFinish:self];
    //[[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:@"FacebookEnginePosted" object:result]];
}
-(void)fbDidNotLogin:(BOOL)cancelled
{
    NSLog(@"Login Cancelled");
    // [DownloadManager showAlert:@"Facebook login cancelled" :@"Warning!"];
}
-(void)fbDidExtendToken:(NSString *)accessToken expiresAt:(NSDate *)expiresAt
{
}
- (void)fbDidLogout
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"FBAccessTokenKey"];
    [defaults removeObjectForKey:@"FBExpirationDateKey"];
    
    [defaults synchronize];
    
    //        Finding the Facebook Cookies and deleting them
    NSHTTPCookieStorage* cookies = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSArray* facebookCookies = [cookies cookiesForURL:
                                [NSURL URLWithString:@"http://login.facebook.com"]];
    for (NSHTTPCookie* cookie in facebookCookies) {
        [cookies deleteCookie:cookie];
    }
    
}


*/


-(void)sen1
{
    
    
    
    
    // NSString *client_id = @"147759358751025";
    
    
    NSString *client_id = FB_KEY;
    
    self->fbGraph = [[FbGraph alloc] initWithFbClientID:client_id];
    
    [fbGraph authenticateUserWithCallbackObject:self andSelector:@selector(fbGraphCallback) andExtendedPermissions:@"user_photos,publish_stream,email,read_stream,user_birthday,friends_about_me,friends_likes,user_likes,user_photos,publish_actions,user_hometown,user_location,user_about_me,friends_birthday,friends_photos,friends_work_history,friends_location,user_subscriptions"];
    
    
    
}
-(void)fbGraphCallback
{
    
    
    
    FbGraphResponse *fb_graph_response = [fbGraph doGraphGet:@"me" withGetVars:nil];
    
    
    
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    
    //if (appDelegate.session.isOpen) {
    SBJSON *parser = [[SBJSON alloc] init];
    
    //NSData *response = fb_graph_response.htmlResponse;
    
    
    
    NSString *json_string = fb_graph_response.htmlResponse;
    
    
    
    NSDictionary *statuses = [parser objectWithString:json_string error:nil];
    
    ////(@"STATUS:%@",statuses);
    
    NSUserDefaults *checkval=[NSUserDefaults standardUserDefaults];
    
    NSString *val=[checkval objectForKey:@"ACCESS_TOKEN"];
    
    
    ////(@"ACCESS:%@",val);
    
    if ([val isEqualToString:@"NULL"])
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults removeObjectForKey:@"FBAccessTokenKey"];
        [defaults removeObjectForKey:@"FBExpirationDateKey"];
        
        [defaults synchronize];
        
        NSHTTPCookie *cookie;
        NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
        for (cookie in [storage cookies]) {
            [storage deleteCookie:cookie];
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else
    {
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/authenticateUser",CONFIG_BASE_URL]];
        
        
        ////(@"HELLO:%@",url);
        
        __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
        
        
        
        
        
        [request_post setValidatesSecureCertificate:NO];
        [request_post setPostValue:val forKey:@"facebook_access_token"];
         [request_post setPostValue:[Config get_token] forKey:@"apikey"];
        
        
        [request_post setTimeOutSeconds:30];
        
        
        [request_post setCompletionBlock:^{
            NSString *responseString = [request_post responseString];
            NSMutableData *results1 = [responseString JSONValue];
            
            
            ////(@"RESULTS HELL:%@",results1);
            
            NSString *fbUserID=[results1 valueForKeyPath:@"Response.facebook_user_id"];
            NSString *userID=[results1 valueForKeyPath:@"Response.userID"];
            NSString *facebookToken=[results1 valueForKeyPath:@"Response.facebook_access_token"];
            
            ////(@"FB USER:%@",fbUserID);
            ////(@"userID:%@",userID);
            
            
            NSUserDefaults *add=[NSUserDefaults standardUserDefaults];
            
            [add setObject:fbUserID forKey:@"FB_USERID"];
            [add setObject:userID forKey:@"USERID"];
            [add setObject:facebookToken forKey:@"FB_ACCESS_TOKEN"];
            
            
            ////(@"DEVICE TOKEN:%@",[add objectForKey:@"DEVICE_TOKEN"]);
            
            // NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/setExtraction",CONFIG_BASE_URL]];
            
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/setExtraction",CONFIG_BASE_URL]];
            
            __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
            
            
            ////(@"USER NE NE:%@",userID);
            
            
            if (results1)
            {
                
                NSUserDefaults *chk=[NSUserDefaults standardUserDefaults];
                [chk setObject:@"YES" forKey:@"IS_FIRSTTIME"];
                
                
                if( IS_IPHONE_5 )
                {
                    socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController" bundle:nil];
                    
                   // viewController.comingString=@"FIRSTTIME";
                    
                    [self presentViewController:viewController animated:NO completion:^{
                        
                    }
                     ];
                    
                }
                else
                {
                    socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController~iPhone" bundle:nil];
                    
                   // viewController.comingString=@"FIRSTTIME";
                    
                    [self presentViewController:viewController animated:NO completion:^{
                        
                    }
                     ];
                    
                }
                
                [myTimer invalidate];
                
                
                /*
                if( IS_IPHONE_5 )
                {
                    MeViewController *viewController=[[MeViewController alloc]initWithNibName:@"MeViewController" bundle:nil];
                    
                    viewController.comingString=@"FIRSTTIME";
                    
                    [self presentViewController:viewController animated:NO completion:^{
                        
                    }
                     ];
                    
                }
                else
                {
                    MeViewController *viewController=[[MeViewController alloc]initWithNibName:@"MeViewController~iPhone" bundle:nil];
                    
                    viewController.comingString=@"FIRSTTIME";
                    
                    [self presentViewController:viewController animated:NO completion:^{
                        
                    }
                     ];
                    
                }
                
                [myTimer invalidate];
                 */
                
            }
            
            
            [request_post setValidatesSecureCertificate:NO];
            [request_post setPostValue:userID forKey:@"userID"];
            [request_post setPostValue:@"facebook" forKey:@"network"];
            
            
            
            [request_post setTimeOutSeconds:30];
            
            
            [request_post setCompletionBlock:^{
                NSString *responseString = [request_post responseString];
                NSMutableData *results1 = [responseString JSONValue];
                
                
                ////(@"RESULTS ASAS:%@",results1);
                
                
                
                
                
                
            }];
            [request_post setFailedBlock:^{
                NSError *error = [request_post error];
                UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                    message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                          cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                
                
                
            }];
            
            [request_post startAsynchronous];
            
            
            
        }];
        [request_post setFailedBlock:^{
            NSError *error = [request_post error];
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                      cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            
            
            
        }];
        
        [request_post startAsynchronous];
        
    }
    
    
}

////// FACEBOOK END //
- (void)centerAndShowActivityIndicator
{
    CGRect frame = self.view.frame;
    CGPoint center = CGPointMake(CGRectGetMidX(frame), CGRectGetMidY(frame));
    self.activityIndicator.center = center;
    [self.activityIndicator startAnimating];
}

#pragma mark - FBLoginViewDelegate

- (void)loginViewShowingLoggedInUser:(FBLoginView *)loginView
{
    // first get the buttons set for login mode
    
    
    
    
 
    
   
    NSString *val=[NSString stringWithFormat:@"%@",[[[FBSession activeSession] accessTokenData] accessToken]];
    
    
       NSLog(@"ACCESS:%@",val);
    
    
    NSUserDefaults *checkval=[NSUserDefaults standardUserDefaults];
    
    [checkval setObject:val forKey:@"ACCESS_TOKEN"];
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/authenticateUser",CONFIG_BASE_URL]];
    
    
    ////(@"HELLO:%@",url);
    
    __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
    
    
    
    
    
    [request_post setValidatesSecureCertificate:NO];
    [request_post setPostValue:val forKey:@"facebook_access_token"];
    [request_post setPostValue:[Config get_token] forKey:@"apikey"];
    
    
    [request_post setTimeOutSeconds:30];
    
    
    [request_post setCompletionBlock:^{
        NSString *responseString = [request_post responseString];
        NSMutableData *results1 = [responseString JSONValue];
        
        
        ////(@"RESULTS HELL:%@",results1);
        
        NSString *fbUserID=[results1 valueForKeyPath:@"Response.facebook_user_id"];
        NSString *userID=[results1 valueForKeyPath:@"Response.userID"];
        NSString *facebookToken=[results1 valueForKeyPath:@"Response.facebook_access_token"];
        
        ////(@"FB USER:%@",fbUserID);
        ////(@"userID:%@",userID);
        
        
        NSUserDefaults *add=[NSUserDefaults standardUserDefaults];
        
        [add setObject:fbUserID forKey:@"FB_USERID"];
        [add setObject:userID forKey:@"USERID"];
        [add setObject:facebookToken forKey:@"FB_ACCESS_TOKEN"];
        
        
        ////(@"DEVICE TOKEN:%@",[add objectForKey:@"DEVICE_TOKEN"]);
        
        // NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/setExtraction",CONFIG_BASE_URL]];
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/setExtraction",CONFIG_BASE_URL]];
        
        __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
        
        
        ////(@"USER NE NE:%@",userID);
        
        
        if (results1)
        {
            
            NSUserDefaults *chk=[NSUserDefaults standardUserDefaults];
            [chk setObject:@"YES" forKey:@"IS_FIRSTTIME"];
            
            
            if( IS_IPHONE_5 )
            {
                socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController" bundle:nil];
                
                // viewController.comingString=@"FIRSTTIME";
                
                [self presentViewController:viewController animated:NO completion:^{
                    
                }
                 ];
                
            }
            else
            {
                socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController~iPhone" bundle:nil];
                
                // viewController.comingString=@"FIRSTTIME";
                
                [self presentViewController:viewController animated:NO completion:^{
                    
                }
                 ];
                
            }
            
            [myTimer invalidate];
            
            
            /*
             if( IS_IPHONE_5 )
             {
             MeViewController *viewController=[[MeViewController alloc]initWithNibName:@"MeViewController" bundle:nil];
             
             viewController.comingString=@"FIRSTTIME";
             
             [self presentViewController:viewController animated:NO completion:^{
             
             }
             ];
             
             }
             else
             {
             MeViewController *viewController=[[MeViewController alloc]initWithNibName:@"MeViewController~iPhone" bundle:nil];
             
             viewController.comingString=@"FIRSTTIME";
             
             [self presentViewController:viewController animated:NO completion:^{
             
             }
             ];
             
             }
             
             [myTimer invalidate];
             */
            
        }
        
        
        [request_post setValidatesSecureCertificate:NO];
        [request_post setPostValue:userID forKey:@"userID"];
        [request_post setPostValue:@"facebook" forKey:@"network"];
        
        
        
        [request_post setTimeOutSeconds:30];
        
        
        [request_post setCompletionBlock:^{
            NSString *responseString = [request_post responseString];
            NSMutableData *results1 = [responseString JSONValue];
            
            
            ////(@"RESULTS ASAS:%@",results1);
            
            
            
            
            
            
        }];
        [request_post setFailedBlock:^{
            NSError *error = [request_post error];
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                      cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            
            
            
        }];
        
        [request_post startAsynchronous];
        
        
        
    }];
    [request_post setFailedBlock:^{
        NSError *error = [request_post error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post startAsynchronous];
    
    
    
    
    // self.shareOnFacebook.enabled = YES;
}



- (void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView
{
    
    NSLog(@"HELLO");
    
    //BOOL canShareAnyhow = [FBNativeDialogs canPresentShareDialogWithSession:nil];
    // self.shareOnFacebook.enabled = canShareAnyhow;
    
    //self.loggedInUser = nil;
}

- (void)loginView:(FBLoginView *)loginView handleError:(NSError *)error
{
    NSString *alertMessage, *alertTitle;
    
    if (error.fberrorShouldNotifyUser) {
        // If the SDK has a message for the user, surface it. This conveniently
        // handles cases like password change or iOS6 app slider state.
        alertTitle = @"Something Went Wrong";
        alertMessage = error.fberrorUserMessage;
    } else if (error.fberrorCategory == FBErrorCategoryAuthenticationReopenSession) {
        // It is important to handle session closures as mentioned. You can inspect
        // the error for more context but this sample generically notifies the user.
        alertTitle = @"Session Error";
        alertMessage = @"Your current session is no longer valid. Please log in again.";
    } else if (error.fberrorCategory == FBErrorCategoryUserCancelled) {
        // The user has cancelled a login. You can inspect the error
        // for more context. For this sample, we will simply ignore it.
        NSLog(@"user cancelled login");
        [FBSession.activeSession close];
        
    } else {
        // For simplicity, this sample treats other errors blindly, but you should
        // refer to https://developers.facebook.com/docs/technical-guides/iossdk/errors/ for more information.
        alertTitle  = @"Unknown Error";
        alertMessage = @"Error. Please try again later.";
        NSLog(@"Unexpected error:%@", error);
    }
    
    if (alertMessage)
    {
        [[[UIAlertView alloc] initWithTitle:alertTitle
                                    message:alertMessage
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    }
}



#pragma mark -

// Convenience method to perform some action that requires the "publish_actions" permissions.
- (void) performPublishAction:(void (^)(void)) action
{
    
    NSLog(@"ENTER PERMISSIONS");
    
    
    // we defer request for permission to post to the moment of post, then we check for the permission
    if ([FBSession.activeSession.permissions indexOfObject:@"publish_actions"] == NSNotFound) {
        // if we don't already have the permission, then we request it now
        
        [FBSession.activeSession requestNewPublishPermissions:@[@"publish_actions",@"publish_stream"]
                                              defaultAudience:FBSessionDefaultAudienceFriends
                                            completionHandler:^(FBSession *session, NSError *error) {
                                                if (!error) {
                                                    action();
                                                }
                                                //For this example, ignore errors (such as if user cancels).
                                            }];
    } else {
        action();
    }
    
}


-(void)LoginToFacebook
{
    
    NSLog(@"LOGIn");
    
    if (!FBSession.activeSession.isOpen)
    {
        // if the session is closed, then we open it here, and establish a handler for state changes
        [FBSession.activeSession openWithCompletionHandler:^(FBSession *session,
                                                             FBSessionState state,
                                                             NSError *error) {
            switch (state) {
                case FBSessionStateClosedLoginFailed:
                {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                        message:error.localizedDescription
                                                                       delegate:nil
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                    [alertView show];
                }
                    break;
                default:
                    break;
            }
        }];
    }
    
}

- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView
                            user:(id<FBGraphUser>)user
{
    NSLog(@"%@", [NSString stringWithFormat:@"Hello %@!", user.first_name] );
    
    
}


// Pick Friends button handler
- (IBAction)pickFriendsList:(UIButton *)sender
{
    
    
    [FBSession.activeSession closeAndClearTokenInformation];
    
    /*
     NSLog(@"ACCESS:%@",[[[FBSession activeSession] accessTokenData] accessToken]);
     
     [self LoginToFacebook];
     
     */
    
}


- (void)showAlert:(NSString *)message result:(id)result error:(NSError *)error
{
    
    NSString *alertMsg;
    NSString *alertTitle;
    if (error)
    {
        alertTitle = @"Error";
        if (error.fberrorShouldNotifyUser ||
            error.fberrorCategory == FBErrorCategoryPermissions ||
            error.fberrorCategory == FBErrorCategoryAuthenticationReopenSession)
        {
            alertMsg = error.fberrorUserMessage;
        }
        
        else
        {
            alertMsg = @"Operation failed due to a connection problem, retry later.";
        }
    }
    else
    {
        NSDictionary *resultDict = (NSDictionary *)result;
        alertMsg = [NSString stringWithFormat:@"Successfully posted '%@'.\nPost ID: %@",
                    message, [resultDict valueForKey:@"id"]];
        alertTitle = @"Success";
    }
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:alertTitle
                                                        message:alertMsg
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
}


@end
