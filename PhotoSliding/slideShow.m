//
//  dealsViewController.m
//  PhotoSliding
//
//  Created by ephronsystems on 6/27/13.
//
//

#import "slideShow.h"
#import "receiptentCell.h"
#import "Image_cell.h"
#import "History.h"





@interface slideShow ()

{
    AVAudioRecorder *recorder;
    AVAudioPlayer *player;
}

@end

@implementation slideShow

@synthesize filterButton;
@synthesize collectionView;
@synthesize messageText,cropImage,senderImage;
@synthesize getCardID;
@synthesize getCreated,getImage,getMessage,getName,getUpdated,getStatus;
@synthesize collectionViewNew;
@synthesize getContact;
@synthesize getSmileyIDs;
@synthesize getAudioID;
@synthesize getViewName;
History *cell;

NSMutableArray *contactID;
NSMutableArray *currentStatus;
NSMutableArray *created_date;
NSMutableArray *idValue;

NSTimer * myTimer;

NSMutableArray *senderID;
NSMutableArray *senderSNGID;
NSMutableArray *senderImageArr;
NSMutableArray *senderBirthdate;
NSMutableArray *senderFirstName;
NSMutableArray *senderLastName;
NSMutableArray *senderdate_created;
NSMutableArray *senderdate_updated;

NSMutableArray *senderImageArray;

NSMutableArray *nameArray;
NSMutableArray *statusArray;

NSMutableArray *cardIDArray;
NSMutableArray *snguseridArray;
NSMutableArray *contactidArray;
NSMutableArray *card_nameArray;
NSMutableArray *thumbnail_urlArray;
NSMutableArray *image_referenceArray;
NSMutableArray *message_textArray;
NSMutableArray *date_createdArray;
NSMutableArray *date_updatedArray;
NSMutableArray *saved_statusArray;
NSMutableArray *smileyIDArr;
NSMutableArray *audioIDArr;

NSMutableArray *smiley0Arr;
NSMutableArray *smiley1Arr;
NSMutableArray *smiley2Arr;


NSMutableArray *emailFlagArr;
NSMutableArray *encryptedCardIDArr;



NSMutableArray *receiptentsArray;
NSMutableArray *statussArray;

int slideId;


#ifdef IOS_OLDER_THAN_6
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return (toInterfaceOrientation == UIInterfaceOrientationMaskLandscape);
}
#endif
#ifdef IOS_NEWER_OR_EQUAL_TO_6
-(BOOL)shouldAutorotate {
    return YES;
}
- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscape;
}
#endif


- (IBAction)AudioPlay:(id)sender
{
    
    _playAudio.selected=_playAudio.selected?NO : YES;
    
    if (_playAudio.selected)
    {
        
        
                
                                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                
                NSString *audioString1 = [documentsDirectory stringByAppendingPathComponent:@"SendAudio"];
                
                NSString *audioString=[NSString stringWithFormat:@"%@.m4a",audioString1];
                
                
                NSUserDefaults *Confirmation=[NSUserDefaults standardUserDefaults];
                
                [Confirmation setObject:audioString forKey:@"AUDIO_STRING"];
                
                
                NSURL *url = [NSURL fileURLWithPath:audioString];
                
                
                
                NSError *error;
                audioPlayer = [[AVAudioPlayer alloc]
                               initWithContentsOfURL:url
                               error:&error];
                //audioPlayer.numberOfLoops=-1;
                [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
                [[AVAudioSession sharedInstance] setActive: YES error: nil];
                [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
                
                
                ////(@"TOTAL:%f",audioPlayer.duration);
                
                                
                if (error)
                {
                    ////(@"Error in audioPlayer: %@",
                        //  [error localizedDescription]);
                    
                    
                             
                    
                    
                    
                    
                    
                    
                    
                }
                else
                    
                {
                    audioPlayer.delegate = self;
                    
                   
                    
                    [audioPlayer prepareToPlay];
                }
                
            
                   
        
            audioPlayer.volume=1.5;
        
       
        [audioPlayer play];
    }

    else
    {
        ////(@"It is deselected");
        
        
                
        NSUserDefaults *Confirmation=[NSUserDefaults standardUserDefaults];
        
        [Confirmation setObject:@"YES" forKey:@"AUDIO_STOP"];
        
        
        [audioPlayer stop];
    }
}

-(void)audioPlayerDidFinishPlaying:
(AVAudioPlayer *)player successfully:(BOOL)flag
{
    _playAudio.selected=NO;
    ////(@"audioPlayerDidFinishPlaying");
  
}



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)oneTap:(UIGestureRecognizer *)gesture
{
    
    _bottomView.hidden=NO;
    _topView.hidden=NO;
    
    [NSTimer scheduledTimerWithTimeInterval:3.0
                                     target:self
                                   selector:@selector(hidedenValue)
                                   userInfo:nil
                                    repeats:NO];

}

-(void)hidedenValue
{
    _bottomView.hidden=YES;
    _topView.hidden=YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(oneTap:)];
    [singleTap setNumberOfTapsRequired:1];
     [self.view addGestureRecognizer:singleTap];
    
    
      [_slidebackView addSubview:_cardView1];
      [_slidebackView addSubview:_cardView2];
      [_slidebackView addSubview:_cardView3];
      [_slidebackView addSubview:_cardView4];
    
    _cardView1.hidden=YES;
     _cardView2.hidden=YES;
     _cardView3.hidden=YES;
     _cardView4.hidden=YES;
    
    slideId=0;
    
    cardIDArray=[[NSMutableArray alloc]init];
    snguseridArray=[[NSMutableArray alloc]init];
    contactidArray=[[NSMutableArray alloc]init];
    card_nameArray=[[NSMutableArray alloc]init];
    thumbnail_urlArray=[[NSMutableArray alloc]init];
    image_referenceArray=[[NSMutableArray alloc]init];
    message_textArray=[[NSMutableArray alloc]init];
    date_createdArray=[[NSMutableArray alloc]init];
    date_updatedArray=[[NSMutableArray alloc]init];
    saved_statusArray=[[NSMutableArray alloc]init];
    smileyIDArr=[[NSMutableArray alloc]init];
    receiptentsArray=[[NSMutableArray alloc]init];
    statussArray=[[NSMutableArray alloc]init];
    audioIDArr=[[NSMutableArray alloc]init];
    senderImageArray=[[NSMutableArray alloc]init];
    
    
     smiley0Arr=[[NSMutableArray alloc]init];
     smiley1Arr=[[NSMutableArray alloc]init];
     smiley2Arr=[[NSMutableArray alloc]init];
    
    
    emailFlagArr=[[NSMutableArray alloc]init];
    encryptedCardIDArr=[[NSMutableArray alloc]init];
    
    
    if ([getViewName isEqualToString:@"RECEIVED"])
    {
        
        
        ////(@"ENTER");
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/socialcards/receivedCards",CONFIG_BASE_URL]];
        
        __block  ASIFormDataRequest *request_post1234 = [ASIFormDataRequest requestWithURL:url];
        
        
        
        NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
        NSString *user=[check objectForKey:@"USERID"];
        
        
        [request_post1234 setPostValue:user forKey:@"userID"];
         [request_post1234 setPostValue:[Config get_token] forKey:@"apikey"];
              
        [request_post1234 setTimeOutSeconds:30];
        
        
        [request_post1234 setCompletionBlock:^{
            NSString *responseString23 = [request_post1234 responseString];
            NSMutableData *results11 = [responseString23 JSONValue];
            
            
            //////(@"RESULTS LOGIN:%@",results11);
            
            
            NSArray *temp= [results11 valueForKeyPath:@"socialcards"];
            
            
            //(@"TEMPARY:%@",temp);
            
            for(NSDictionary *value in temp)
            {
                //[cardIDArray  addObject:[value valueForKey:@"id"]];
                //[snguseridArray  addObject:[value valueForKey:@"snguserid"]];
               // [contactidArray  addObject:[value valueForKey:@"contactid"]];
                
                
                NSString *card_cancel=[value valueForKey:@"card_cancel"];
                
                if (![card_cancel isEqualToString:@"yes"])
                {
                    
                
                
                
                [card_nameArray  addObject:[value valueForKey:@"socialcard_title"]];
                [thumbnail_urlArray  addObject:[value valueForKey:@"cardimg"]];
                [image_referenceArray  addObject:[value valueForKey:@"cardimg"]];
                [message_textArray  addObject:[value valueForKey:@"socialcard_msg"]];
                [date_createdArray  addObject:[value valueForKey:@"created_date"]];
                [date_updatedArray  addObject:[value valueForKey:@"created_date"]];
                [saved_statusArray  addObject:[value valueForKey:@"current_status"]];
                 [senderImageArray  addObject:[value valueForKey:@"fbsender_pic"]];
                
                //[receiptentsArray  addObject:[value valueForKey:@"recipients"]];
               // [smileyIDArr  addObject:[value valueForKey:@"smiley_ids"]];
                //[audioIDArr  addObject:[value valueForKey:@"audio_id"]];
                
                @try
                {
                    NSArray *temp1=[value valueForKeyPath:@"smileys.smiley_0"];
                    
                    [smiley0Arr  addObject:temp1[0]];
                }
                @catch (NSException *exception)
                {
                    [smiley0Arr  addObject:@""];
                }
                
                @try
                {
                    NSArray *temp1=[value valueForKeyPath:@"smileys.smiley_1"];
                    
                    [smiley1Arr  addObject:temp1[1]];
                    
                }
                @catch (NSException *exception)
                {
                    [smiley1Arr  addObject:@""];
                }
                
                @try
                {
                    NSArray *temp1=[value valueForKeyPath:@"smileys.smiley_2"];
                    
                    [smiley2Arr  addObject:temp1[2]];
                }
                @catch (NSException *exception)
                {
                    [smiley2Arr  addObject:@""];
                }
                
                
                [emailFlagArr  addObject:[value valueForKey:@"email_flag"]];
                [encryptedCardIDArr  addObject:[value valueForKey:@"encryt_recip_id"]];
                }
            }
            
            
            ////(@"ARRAY 0:%@",smiley0Arr);
            ////(@"ARRAY 1:%@",smiley1Arr);
            ////(@"ARRAY 2:%@",smiley2Arr);
            
            _bottomView.hidden=YES;
            _topView.hidden=YES;
            
            
            if ([image_referenceArray count]==0)
            {
                
                _bottomView.hidden=NO;
                _topView.hidden=NO;
                
                UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                    message:[NSString stringWithFormat:@"No Slide show Available"] delegate:self
                                                          cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
                
                
            }
            else
            {
                
                [self setImage];
            }
            
            
            
        }];
        [request_post1234 setFailedBlock:^{
            NSError *error = [request_post1234 error];
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                      cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            
            
            
        }];
        
        [request_post1234 startAsynchronous];
        
    }
    else
    {
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/socialcards/retrieveUserCards",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post1234 = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    
    
    [request_post1234 setPostValue:user forKey:@"userID"];
     [request_post1234 setPostValue:[Config get_token] forKey:@"apikey"];
    
    if ([getViewName isEqualToString:@"ALL"])
    {
         [request_post1234 setPostValue:@"" forKey:@"card_status"];
    }
    
    if ([getViewName isEqualToString:@"SENT"])
    {
        [request_post1234 setPostValue:@"" forKey:@"card_status"];
    }
        
        //Sandra Evans & Associates <sandra@seandassoc.com>
    
    if ([getViewName isEqualToString:@"SHARE"])
    {
        [request_post1234 setPostValue:@"share" forKey:@"card_status"];
    }
    
    [request_post1234 setTimeOutSeconds:30];
    
    
    [request_post1234 setCompletionBlock:^{
        NSString *responseString23 = [request_post1234 responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        
        //(@"RESULTS LOGIN:%@",results11);
        
        
        NSArray *temp= [results11 valueForKeyPath:@"socialcards"];
        
        
        ////(@"TEMPARY:%@",temp);
        
        for(NSDictionary *value in temp)
        {
            
            
            
                      
            
            NSString *card_cancel=[value valueForKey:@"card_cancel"];
            
            if (![card_cancel isEqualToString:@"yes"])
            {
                
                
                
                [cardIDArray  addObject:[value valueForKey:@"id"]];
                [snguseridArray  addObject:[value valueForKey:@"snguserid"]];
                [contactidArray  addObject:[value valueForKey:@"contactid"]];
                [card_nameArray  addObject:[value valueForKey:@"card_name"]];
                [thumbnail_urlArray  addObject:[value valueForKey:@"cardimg"]];
                [image_referenceArray  addObject:[value valueForKey:@"cardimg"]];
                [message_textArray  addObject:[value valueForKey:@"message_text"]];
                [date_createdArray  addObject:[value valueForKey:@"date_created"]];
                [date_updatedArray  addObject:[value valueForKey:@"date_updated"]];
                [saved_statusArray  addObject:[value valueForKey:@"saved_status"]];
                [receiptentsArray  addObject:[value valueForKey:@"recipients"]];
                [smileyIDArr  addObject:[value valueForKey:@"smiley_ids"]];
                [audioIDArr  addObject:[value valueForKey:@"audio_id"]];
                
                @try
                {
                    NSArray *temp1=[value valueForKeyPath:@"smileys.smiley_0"];
                    
                    [smiley0Arr  addObject:temp1[0]];
                }
                @catch (NSException *exception)
                {
                    [smiley0Arr  addObject:@""];
                }
                
                @try
                {
                    NSArray *temp1=[value valueForKeyPath:@"smileys.smiley_1"];
                    
                    [smiley1Arr  addObject:temp1[1]];
                    
                }
                @catch (NSException *exception)
                {
                    [smiley1Arr  addObject:@""];
                }
                
                @try
                {
                    NSArray *temp1=[value valueForKeyPath:@"smileys.smiley_2"];
                    
                    [smiley2Arr  addObject:temp1[2]];
                }
                @catch (NSException *exception)
                {
                    [smiley2Arr  addObject:@""];
                }
                
                
                [emailFlagArr  addObject:[value valueForKey:@"email_flag"]];
                [encryptedCardIDArr  addObject:[value valueForKey:@"encrypt_cardid"]];
 
            }
            
         
        }
        
        
        //(@"ARRAY 0:%@",smiley0Arr);
        //(@"ARRAY 1:%@",contactidArray);
        ////(@"ARRAY 2:%@",smiley2Arr);
        
        _bottomView.hidden=YES;
        _topView.hidden=YES;
        
        
        if ([image_referenceArray count]==0)
        {
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                message:[NSString stringWithFormat:@"No Slide show Available"] delegate:self
                                                      cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
        }
        else
        {
        
        [self setImage];
        }
        
        
        
    }];
    [request_post1234 setFailedBlock:^{
        NSError *error = [request_post1234 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1234 startAsynchronous];
    
    }
 

   
}


-(void)setImage
{

    
    
    if (slideId!=2335)
    {
   // ////(@"SLIDE:%d",slideId);
        
        NSString *label_value=[NSString stringWithFormat:@"%d/%d Cards",slideId+1,[card_nameArray count]];
        
        _listLabel.text=[NSString stringWithFormat:@"%@",label_value];
    
        _front_img.image=[UIImage imageNamed:@"sfrontact1.png"];
        _bage_img.image=[UIImage imageNamed:@"sback.png"];
        
        CATransition *transition = [CATransition animation];
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
        transition.duration = 0.75;
        transition.type = kCAScrollNone;
        [self.view.layer addAnimation:transition forKey:nil];
        
    _titleLabel.text=card_nameArray[slideId];
    messageText.text=message_textArray[slideId];
        
        @try {
            [_sm1 setImageWithURL:[NSURL URLWithString:smiley0Arr[slideId]]
                 placeholderImage:[UIImage imageNamed:nil]];

        }
        @catch (NSException *exception) {
            
        }
     
        @try {
            
            [_sm2 setImageWithURL:[NSURL URLWithString:smiley1Arr[slideId]]
                 placeholderImage:[UIImage imageNamed:nil]];
            
        }
        @catch (NSException *exception) {
            
        }

        
        @try {
            [_sm3 setImageWithURL:[NSURL URLWithString:smiley2Arr[slideId]]
                 placeholderImage:[UIImage imageNamed:nil]];
            
        }
        @catch (NSException *exception) {
            
        }

      
        
       
        
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:@"profileImage.jpg"];
    UIImage *profileIMG = [UIImage imageWithContentsOfFile:getImagePath];
    
        if ([getViewName isEqualToString:@"RECEIVED"])
        {
            [senderImage setImageWithURL:[NSURL URLWithString:senderImageArray[slideId]]
                      placeholderImage:[UIImage imageNamed:nil]];
        }
        else
        {
        
    senderImage.image=profileIMG;
            
        }
    
    [cropImage setImageWithURL:[NSURL URLWithString:image_referenceArray[slideId]]
              placeholderImage:[UIImage imageNamed:nil]];
    
    
    
    _cardView1.hidden=NO;
    _cardView2.hidden=YES;
    _cardView3.hidden=YES;
    _cardView4.hidden=YES;

        
        if (slideId==[card_nameArray count]-1)
        {
            slideId=0;
        }
        else
        {
            slideId=slideId+1;
        }
    
    [NSTimer scheduledTimerWithTimeInterval:3.0
                                               target:self
                                             selector:@selector(setImage)
                                             userInfo:nil
                                              repeats:NO];
    }
    
}

-(void)setImage2
{
    
    // ////(@"ARRAY:%d",[message_textArray count]);
    
    if (slideId!=2335)
    {
        
        
        _front_img.image=[UIImage imageNamed:@"sfrontact2.png"];
        _bage_img.image=[UIImage imageNamed:@"sback.png"];
        
        CATransition *transition = [CATransition animation];
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
        transition.duration = 0.75;
        transition.type = kCAScrollNone;
        [self.view.layer addAnimation:transition forKey:nil];
        
        
    _cardView1.hidden=YES;
    _cardView2.hidden=NO;
    _cardView3.hidden=YES;
    _cardView4.hidden=YES;

    
    
    [NSTimer scheduledTimerWithTimeInterval:3.0
                                     target:self
                                   selector:@selector(setImage3)
                                   userInfo:nil
                                    repeats:NO];
    }
    
}

-(void)setImage3
{
    
    if (slideId!=2335)
    {
    
        _front_img.image=[UIImage imageNamed:@"sfront.png"];
        _bage_img.image=[UIImage imageNamed:@"sbackact1.png"];
        
        CATransition *transition = [CATransition animation];
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
        transition.duration = 0.75;
        transition.type = kCAScrollNone;
        [self.view.layer addAnimation:transition forKey:nil];
        
    _cardView1.hidden=YES;
    _cardView2.hidden=YES;
    _cardView3.hidden=NO;
    _cardView4.hidden=YES;
    
    
        if (slideId==[card_nameArray count]-1)
        {
            slideId=0;
        }
        else
        {
            slideId=slideId+1;
        }
        
        [NSTimer scheduledTimerWithTimeInterval:3.0
                                         target:self
                                       selector:@selector(setImage)
                                       userInfo:nil
                                        repeats:NO];
    }

    
    
}

-(void)setImage4
{
    
    if (slideId!=2335)
    {
        
        
     
        
        
        _front_img.image=[UIImage imageNamed:@"sfront.png"];
        _bage_img.image=[UIImage imageNamed:@"sbackact2.png"];
        
        CATransition *transition = [CATransition animation];
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
        transition.duration = 0.75;
        transition.type = kCAScrollNone;
        [self.view.layer addAnimation:transition forKey:nil];
        
        _cardView1.hidden=YES;
        _cardView2.hidden=YES;
        _cardView3.hidden=YES;
        _cardView4.hidden=NO;
        
        if (slideId==[card_nameArray count]-1)
        {
            slideId=0;
        }
        else
        {
            slideId=slideId+1;
        }
        
        [NSTimer scheduledTimerWithTimeInterval:3.0
                                         target:self
                                       selector:@selector(setImage)
                                       userInfo:nil
                                        repeats:NO];
    }
   
    
}



-(IBAction)b1_Click
{
    
    CGPoint bottomOffset = CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
    [self.scrollView setContentOffset:bottomOffset animated:YES];
    
    //
    //
    //    [_b1 setTitle:@"A" forState:UIControlStateNormal];
    //    [_b2 setTitle:@"B" forState:UIControlStateNormal];
    //    [_b3 setTitle:@"B" forState:UIControlStateNormal];
    //    [_b4 setTitle:@"B" forState:UIControlStateNormal];
    
}
-(IBAction)b2_Click
{
    
    CGPoint bottomOffset = CGPointMake(320, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
    [self.scrollView setContentOffset:bottomOffset animated:YES];
    
    //    [_b1 setTitle:@"B" forState:UIControlStateNormal];
    //    [_b2 setTitle:@"A" forState:UIControlStateNormal];
    //    [_b3 setTitle:@"B" forState:UIControlStateNormal];
    //    [_b4 setTitle:@"B" forState:UIControlStateNormal];
    
	
    
}

-(IBAction)b3_Click
{
    CGPoint bottomOffset = CGPointMake(640, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
    [self.scrollView setContentOffset:bottomOffset animated:YES];
    
    //
    //    [_b1 setTitle:@"B" forState:UIControlStateNormal];
    //    [_b2 setTitle:@"B" forState:UIControlStateNormal];
    //    [_b3 setTitle:@"A" forState:UIControlStateNormal];
    //    [_b4 setTitle:@"B" forState:UIControlStateNormal];
    
}
-(IBAction)b4_Click
{
    
    CGPoint bottomOffset = CGPointMake(960, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
    [self.scrollView setContentOffset:bottomOffset animated:YES];
    //
    //    [_b1 setTitle:@"B" forState:UIControlStateNormal];
    //    [_b2 setTitle:@"B" forState:UIControlStateNormal];
    //    [_b3 setTitle:@"B" forState:UIControlStateNormal];
    //    [_b4 setTitle:@"A" forState:UIControlStateNormal];
    
}

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    
    // ////(@"CURRENT:%d",self.pageControl.currentPage);
    
    if (self.pageControl.currentPage==0)
    {
        //        [_b1 setTitle:@"A" forState:UIControlStateNormal];
        //        [_b2 setTitle:@"B" forState:UIControlStateNormal];
        //        [_b3 setTitle:@"B" forState:UIControlStateNormal];
        //        [_b4 setTitle:@"B" forState:UIControlStateNormal];
        
        _front_img.image=[UIImage imageNamed:@"sfrontact1.png"];
        _bage_img.image=[UIImage imageNamed:@"sback.png"];
        
    }
    
    if (self.pageControl.currentPage==1)
    {
        //        [_b1 setTitle:@"B" forState:UIControlStateNormal];
        //        [_b2 setTitle:@"A" forState:UIControlStateNormal];
        //        [_b3 setTitle:@"B" forState:UIControlStateNormal];
        //        [_b4 setTitle:@"B" forState:UIControlStateNormal];
        
        _front_img.image=[UIImage imageNamed:@"sfrontact2.png"];
        _bage_img.image=[UIImage imageNamed:@"sback.png"];
        
    }
    
    if (self.pageControl.currentPage==2)
    {
        //        [_b1 setTitle:@"B" forState:UIControlStateNormal];
        //        [_b2 setTitle:@"B" forState:UIControlStateNormal];
        //        [_b3 setTitle:@"A" forState:UIControlStateNormal];
        //        [_b4 setTitle:@"B" forState:UIControlStateNormal];
        
        _front_img.image=[UIImage imageNamed:@"sfront.png"];
        _bage_img.image=[UIImage imageNamed:@"sbackact1.png"];
        
        
    }
    
    if (self.pageControl.currentPage==3)
    {
        //        [_b1 setTitle:@"B" forState:UIControlStateNormal];
        //        [_b2 setTitle:@"B" forState:UIControlStateNormal];
        //        [_b3 setTitle:@"B" forState:UIControlStateNormal];
        //        [_b4 setTitle:@"A" forState:UIControlStateNormal];
        
        _front_img.image=[UIImage imageNamed:@"sfront.png"];
        _bage_img.image=[UIImage imageNamed:@"sbackact2.png"];
        
    }
    
    
	if (!pageControlBeingUsed) {
		// Switch the indicator when more than 50% of the previous/next page is visible
		CGFloat pageWidth = self.scrollView.frame.size.width;
		int page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
		self.pageControl.currentPage = page;
	}
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
	pageControlBeingUsed = NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
	pageControlBeingUsed = NO;
}

- (IBAction)changePage {
	// Update the scroll view to the appropriate page
	CGRect frame;
	frame.origin.x = self.scrollView.frame.size.width * self.pageControl.currentPage;
	frame.origin.y = 0;
	frame.size = self.scrollView.frame.size;
	[self.scrollView scrollRectToVisible:frame animated:YES];
	
	// Keep track of when scrolls happen in response to the page control
	// value changing. If we don't do this, a noticeable "flashing" occurs
	// as the the scroll delegate will temporarily switch back the page
	// number.
	pageControlBeingUsed = YES;
}



-(void)getAudio:(NSString *)audio
{
    ////(@"AUDIO:%@",audio);
    
    
       
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/audios/retrieveCardAudios",CONFIG_BASE_URL]];
    
    
    __block  ASIFormDataRequest *requestmethod = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    NSString *cardID=[check objectForKey:@"CARD_ID"];
    
    NSMutableArray *smilyIDArr=[[NSMutableArray alloc]init];
    NSMutableArray *smilyName=[[NSMutableArray alloc]init];
    
    [requestmethod setPostValue:user forKey:@"userID"];
    [requestmethod setPostValue:audio forKey:@"audio_id"];
     [requestmethod setPostValue:[Config get_token] forKey:@"apikey"];
    [requestmethod setTimeOutSeconds:30];
    
    
    [requestmethod setCompletionBlock:^{
        NSString *responseString23 = [requestmethod responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        ////(@"RESULTS:%@",results11);
        
        
        NSArray *temp= [results11 valueForKeyPath:@"audio.audio_url"];
        
        
        
        ////(@"TEMP TEMP:%@",temp[0]);
        
    
        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSURL *url=[NSURL URLWithString:temp[0]];
        
        
        ////(@"Dowload Start");
        
        ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
        
        
        
        
        //  [collectionView reloadData];
        
        [request setShouldContinueWhenAppEntersBackground:YES];
        
        
        [request setCompletionBlock:^{
            // Use when fetching text data
            
            NSString *responseString = [request responseString];
            
            
            
            // Use when fetching binary data
            NSData *responseData = [request responseData];
            
            NSString *epub_name=[NSString stringWithFormat:@"SendAudio.m4a"];
            
            
            NSString *filePath = [documentsDirectory stringByAppendingPathComponent:epub_name];
            
            [responseData writeToFile:filePath atomically:YES];
            
            ////(@"Dowload Completes");
            
           _playAudio.hidden=NO;
            
            
        }];
        [request setFailedBlock:^
         {
             // NSError *error = [request error];
             
             
             UIAlertView *error_Alert=[[UIAlertView alloc]initWithTitle:@"Download Error" message:[NSString stringWithFormat:@"%@",[request error]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             
             [error_Alert show];
             
             
         }];
        [request startAsynchronous];
        
        
        
        
    }];
    [requestmethod setFailedBlock:^{
        NSError *error = [requestmethod error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [requestmethod startAsynchronous];
    
    
}

-(void)getSmilies:(NSString *)smiley
{
    ////(@"SMILEY:%@",smiley);
    
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/smiley/retrieveCardSmileys",CONFIG_BASE_URL]];
    
    
    __block  ASIFormDataRequest *requestmethod = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    NSString *cardID=[check objectForKey:@"CARD_ID"];
    
    NSMutableArray *smilyIDArr=[[NSMutableArray alloc]init];
    NSMutableArray *smilyName=[[NSMutableArray alloc]init];
    
    [requestmethod setPostValue:user forKey:@"userID"];
    [requestmethod setPostValue:smiley forKey:@"smiley_ids"];
     [requestmethod setPostValue:[Config get_token] forKey:@"apikey"];
    [requestmethod setTimeOutSeconds:30];
    
    
    [requestmethod setCompletionBlock:^{
        NSString *responseString23 = [requestmethod responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        ////(@"RESULTS:%@",results11);
        
        
        NSArray *temp= [results11 valueForKeyPath:@"smileys"];
        
        
        
        
        
        ////(@"TEMPARY:%@",temp);
        
        for(NSDictionary *value in temp)
        {
            [smilyIDArr  addObject:[value valueForKey:@"id"]];
            [smilyName  addObject:[value valueForKey:@"smiley_url"]];
            //[contactidArray  addObject:[value valueForKey:@"contactid"]];
            
        }
        
        
        if ([smilyName count]==1)
        {
            
            
            NSString *temp1=@"";
            [_sm1 setImageWithURL:[NSURL URLWithString:temp1]
                 placeholderImage:nil];
            
            
            NSString *temp2=@"";
            [_sm3 setImageWithURL:[NSURL URLWithString:temp2]
                 placeholderImage:nil];
            
            NSString *temp=[smilyName objectAtIndex:0];
            [_sm2 setImageWithURL:[NSURL URLWithString:temp]
                 placeholderImage:nil];
            
        }
        
        if ([smilyName count]==2)
        {
            
            NSString *temp=[smilyName objectAtIndex:0];
            [_sm1 setImageWithURL:[NSURL URLWithString:temp]
                 placeholderImage:nil];
            
            NSString *temp1=[smilyName objectAtIndex:1];
            [_sm2 setImageWithURL:[NSURL URLWithString:temp1]
                 placeholderImage:nil];
            
            NSString *temp2=@"";
            [_sm3 setImageWithURL:[NSURL URLWithString:temp2]
                 placeholderImage:nil];
            
            
        }
        
        if ([smilyName count]==3)
        {
            
            NSString *temp=[smilyName objectAtIndex:0];
            [_sm1 setImageWithURL:[NSURL URLWithString:temp]
                 placeholderImage:nil];
            
            NSString *temp1=[smilyName objectAtIndex:1];
            [_sm2 setImageWithURL:[NSURL URLWithString:temp1]
                 placeholderImage:nil];
            
            NSString *temp2=[smilyName objectAtIndex:2];
            [_sm3 setImageWithURL:[NSURL URLWithString:temp2]
                 placeholderImage:nil];
        }
        
        
     
        
        
        
    }];
    [requestmethod setFailedBlock:^{
        NSError *error = [requestmethod error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [requestmethod startAsynchronous];
    
    
}





- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [senderFirstName count];
}


- (History *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELL" forIndexPath:indexPath];
    
    
    
    // cell.progress.hidden=YES;
    
    @try {
        NSString *temp=[senderImageArr objectAtIndex:indexPath.row];
        [cell.image_view setImageWithURL:[NSURL URLWithString:temp]
                        placeholderImage:[UIImage imageNamed:@"bigicon.png"]];
        
        
        
    }
    @catch (NSException *exception) {
        ////////////(@"CHJECK");
    }
    
    cell.cell_title_Label.text=[NSString stringWithFormat:@"%@",senderFirstName[indexPath.row]];
    
    
    NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc] init];
    
    [dateFormat1 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    
    NSString *date_updated=senderdate_updated[indexPath.row];
    ////(@"Original date:%@",date_updated);
        NSDate *todaydate = [dateFormat1 dateFromString:date_updated];// date with yyyy-MM-dd format
      // ////(@"DATE:%@",todaydate);
    
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"LLL d, yyyy HH:mm a"];
    NSString *dateString = [dateFormat stringFromDate:todaydate];
    
   // ////(@"DATE STRING:%@",dateString);
    
    
    cell.cell_date.text=[NSString stringWithFormat:@"%@",dateString];
    
    
    NSString *status_str=@"";
    
    if ([senderLastName[indexPath.row]isEqualToString:@"O"])
    {
        status_str=@"Order Received";
    }
    
    if ([senderLastName[indexPath.row]isEqualToString:@"S"])
    {
        status_str=@"Saved";
    }
    
    if ([senderLastName[indexPath.row]isEqualToString:@"C"])
    {
        status_str=@"Created";
    }
    
    if ([senderLastName[indexPath.row]isEqualToString:@"A"])
    {
        status_str=@"Approved";
    }
    
    if ([senderLastName[indexPath.row]isEqualToString:@"R"])
    {
        status_str=@"Rejected";
    }
    
    if ([senderLastName[indexPath.row]isEqualToString:@"P"])
    {
        status_str=@"Printed";
    }
    
    if ([senderLastName[indexPath.row]isEqualToString:@"M"])
    {
        status_str=@"Mailed";
    }
    
    if ([senderLastName[indexPath.row]isEqualToString:@"E"])
    {
        status_str=@"E-Mailed";
    }
    
    if ([senderLastName[indexPath.row]isEqualToString:@"D"])
    {
        status_str=@"Delivered";
    }
    
    if ([senderLastName[indexPath.row]isEqualToString:@"ED"])
    {
        status_str=@"Email-Delivered";
    }
     
            cell.cell_status.text=[NSString stringWithFormat:@"Status:%@",status_str];
           
        //cell.image_view.image=[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@", image_path]];
    
    return cell;
}




-(void)getContects:(NSString *)contactIDs
{
    
    
   
    
    senderID=[[NSMutableArray alloc]init];
    senderSNGID=[[NSMutableArray alloc]init];
    senderImageArr=[[NSMutableArray alloc]init];
    senderBirthdate=[[NSMutableArray alloc]init];
    senderFirstName=[[NSMutableArray alloc]init];
    senderLastName=[[NSMutableArray alloc]init];
    senderdate_created=[[NSMutableArray alloc]init];
    senderdate_updated=[[NSMutableArray alloc]init];
    
    
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/socialcards/orderDetails",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post1234 = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    
    
    [request_post1234 setPostValue:user forKey:@"userID"];
    [request_post1234 setPostValue:getCardID forKey:@"cardID"];
     [request_post1234 setPostValue:[Config get_token] forKey:@"apikey"];
    
    [request_post1234 setTimeOutSeconds:30];
    
    
    [request_post1234 setCompletionBlock:^{
        NSString *responseString23 = [request_post1234 responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        
        ////(@"RESULTS LOGIN:%@",results11);
        
        
        
        
        NSArray *temp= [results11 valueForKeyPath:@"response.socialcards"];
        
        
        ////(@"TEMPARY:%@",temp);
        
        
        @try {
            for(NSDictionary *value in temp)
            {
                
                
                [senderImageArr addObject:[value valueForKey:@"friend_facebookproimg"]];
                [senderFirstName addObject:[value valueForKey:@"recipient"]];
               [senderLastName addObject:[value valueForKey:@"current_status"]];
                [senderdate_updated addObject:[value valueForKey:@"created_date"]];
            }
        }
        @catch (NSException *exception)
        {
         
            ////(@"Exception");
        }
        
        
        
        
        
       
     [collectionViewNew reloadData];
     
        
        
    }];
    [request_post1234 setFailedBlock:^{
        NSError *error = [request_post1234 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1234 startAsynchronous];
}


-(IBAction)backButton
{
    
    slideId=2335;
    
           if (IS_IPHONE_5)
           {
            socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController" bundle:nil];
            [self presentViewController:viewController animated:NO completion:^{
                //////(@"SUCCESS");
            }
             ];
        }
        else
        {
            socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController~iPhone" bundle:nil];
            [self presentViewController:viewController animated:NO completion:^{
                //////(@"SUCCESS");
            }
             ];
        }

       
   

    
    
}
 - (void)handleSingleTap:(UIGestureRecognizer *)gestureRecognizer {
     
     [messageText resignFirstResponder];
 }

-(IBAction)continueButtonClick
{
    
    if (IS_IPHONE_5)
    {
        
        receiptentViewController *viewController=[[receiptentViewController alloc]initWithNibName:@"receiptentViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
    
    receiptentViewController *viewController=[[receiptentViewController alloc]initWithNibName:@"receiptentViewController~iPhone" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        //////(@"SUCCESS");
    }
     ];
    }
}

-(IBAction)meButton
{
    if (IS_IPHONE_5) {
        MeViewController *viewController=[[MeViewController alloc]initWithNibName:@"MeViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        
        MeViewController *viewController=[[MeViewController alloc]initWithNibName:@"MeViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }

}
-(IBAction)dealsButton
{
    dealsViewController *viewController=[[dealsViewController alloc]initWithNibName:@"dealsViewController" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        //////(@"SUCCESS");
    }
     ];
}
-(IBAction)socialButton
{
    if (IS_IPHONE_5) {
        netCoinsViewController *viewController=[[netCoinsViewController alloc]initWithNibName:@"netCoinsViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        netCoinsViewController *viewController=[[netCoinsViewController alloc]initWithNibName:@"netCoinsViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }

}
-(IBAction)netCoinsButton
{
    if (IS_IPHONE_5) {
        socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }

}
-(IBAction)myBusinessButton
{
    myBusinessViewController *viewController=[[myBusinessViewController alloc]initWithNibName:@"myBusinessViewController" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        //////(@"SUCCESS");
    }
     ];
}
- (BOOL)textFieldShouldReturn: (UITextView *)textField
{
  
    [textField resignFirstResponder];
    
    return YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
