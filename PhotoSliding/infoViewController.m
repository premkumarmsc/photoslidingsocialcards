//
//  dealsViewController.m
//  PhotoSliding
//
//  Created by ephronsystems on 6/27/13.
//
//

#import "infoViewController.h"
#import "LibraryCell.h"
#import "DetailHistory.h"
#import "receivedCardViewController.h"


@interface infoViewController ()

@end

@implementation infoViewController
@synthesize avilableTableView;

NSMutableArray *nameArray;
NSMutableArray *statusArray;

NSMutableArray *cardIDArray;
NSMutableArray *snguseridArray;
NSMutableArray *contactidArray;
NSMutableArray *card_nameArray;
NSMutableArray *thumbnail_urlArray;
NSMutableArray *image_referenceArray;
NSMutableArray *message_textArray;
NSMutableArray *date_createdArray;
NSMutableArray *date_updatedArray;
NSMutableArray *saved_statusArray;
NSMutableArray *smileyIDArr;
NSMutableArray *audioIDArr;
UIAlertView *  alertExit;

NSMutableArray *receiptentsArray;
NSMutableArray *statussArray;


int totalCount;
int receivedCount;
int savedCount;
int sentCount;
int shareCount;

#ifdef IOS_OLDER_THAN_6
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return (toInterfaceOrientation == UIInterfaceOrientationMaskPortrait);
}
#endif
#ifdef IOS_NEWER_OR_EQUAL_TO_6
-(BOOL)shouldAutorotate {
    return YES;
}
- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
#endif


-(IBAction)visit
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.socialnetgateapp.com/"]];
}
-(IBAction)faq
{
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.socialnetgateapp.com/faq"]];
}
-(IBAction)terms
{
      [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.socialnetgateapp.com/termsofuse"]];
}
-(IBAction)contact
{
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.socialnetgateapp.com/privacypolicy"]];
}




- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
     self.trackedViewName = @"Info Screen";
    
    
    NSDictionary *infoDictionary = [[NSBundle mainBundle]infoDictionary];
    
    NSNumber *version    = infoDictionary[(NSString*)kCFBundleVersionKey];
    
    
    NSString *ver_str=[NSString stringWithFormat:@"Version : %@",version];
    
    _appVersion.text=ver_str;
        
    // Do any additional setup after loading the view from its nib.
}
-(IBAction)back
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)receivedCard
{
    
    if (receivedCount==0)
    {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"You do not have any Received SocialCards"] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
    }
    else
    {

    
    if (IS_IPHONE_5)
    {
        receivedCardViewController *viewController=[[receivedCardViewController alloc]initWithNibName:@"receivedCardViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
        
    }
    else
    {
        receivedCardViewController *viewController=[[receivedCardViewController alloc]initWithNibName:@"receivedCardViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
        
    }
        
    }

}

-(IBAction)sharedListCard
{
    if (shareCount==0)
    {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"You do not have any Shared SocialCards"] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
    }
    else
    {

    if (IS_IPHONE_5)
    {
        sharedCardViewController *viewController=[[sharedCardViewController alloc]initWithNibName:@"sharedCardViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
        
    }
    else
    {
        sharedCardViewController *viewController=[[sharedCardViewController alloc]initWithNibName:@"sharedCardViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
        
    }
    }

}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    ////(@"TOT:%d",totalCount);
    
    ////(@"TOT:%d",sentCount);
    
    ////(@"TOT:%d",receivedCount);
    
    ////(@"TOT:%d",shareCount);
    
    
    
          if (buttonIndex==3)
        {
                 ////(@"AS0");
            
            if (totalCount==0)
            {
                UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                    message:[NSString stringWithFormat:@"No SocialCards fount"] delegate:self
                                                          cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
            }
            else
            {
            
            
            if (IS_IPHONE_5)
            {
                slideShow *viewController=[[slideShow alloc]initWithNibName:@"slideShow" bundle:nil];
                
                viewController.getViewName=@"ALL";
                
                [self presentViewController:viewController animated:NO completion:^{
                    //////(@"SUCCESS");
                }
                 ];
                
            }
            else
            {
                slideShow *viewController=[[slideShow alloc]initWithNibName:@"slideShow~iPhone" bundle:nil];
                
                 viewController.getViewName=@"ALL";
                
                [self presentViewController:viewController animated:NO completion:^{
                    //////(@"SUCCESS");
                }
                 ];
                
            }
            }

            
        }
        if (buttonIndex==0)
        {
            ////(@"AS1");
            if (sentCount==0)
            {
                UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                    message:[NSString stringWithFormat:@"You do not have any Sent SocialCards"] delegate:self
                                                          cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
            }
            else
            {

            if (IS_IPHONE_5)
            {
                slideShow *viewController=[[slideShow alloc]initWithNibName:@"slideShow" bundle:nil];
                
                viewController.getViewName=@"SENT";
                
                [self presentViewController:viewController animated:NO completion:^{
                    //////(@"SUCCESS");
                }
                 ];
                
            }
            else
            {
                slideShow *viewController=[[slideShow alloc]initWithNibName:@"slideShow~iPhone" bundle:nil];
                
                viewController.getViewName=@"SENT";
                
                [self presentViewController:viewController animated:NO completion:^{
                    //////(@"SUCCESS");
                }
                 ];
                
            }
            }

            
        }
        
        if (buttonIndex==1)
        {
                       
              ////(@"AS2");
            if (receivedCount==0)
            {
                UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                    message:[NSString stringWithFormat:@"You do not have any Received SocialCards"] delegate:self
                                                          cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
            }
            else
            {

            if (IS_IPHONE_5)
            {
                slideShow *viewController=[[slideShow alloc]initWithNibName:@"slideShow" bundle:nil];
                
                viewController.getViewName=@"RECEIVED";
                
                [self presentViewController:viewController animated:NO completion:^{
                    //////(@"SUCCESS");
                }
                 ];
                
            }
            else
            {
                slideShow *viewController=[[slideShow alloc]initWithNibName:@"slideShow~iPhone" bundle:nil];
                
                viewController.getViewName=@"RECEIVED";
                
                [self presentViewController:viewController animated:NO completion:^{
                    //////(@"SUCCESS");
                }
                 ];
                
            }
            }
            

        }
        
    if (buttonIndex==2)
    {
        
        ////(@"AS3");
        if (shareCount==0) {
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                message:[NSString stringWithFormat:@"You do not have any Shared SocialCards"] delegate:self
                                                      cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
        }
        else
        {

        if (IS_IPHONE_5)
        {
            slideShow *viewController=[[slideShow alloc]initWithNibName:@"slideShow" bundle:nil];
            
            viewController.getViewName=@"SHARE";
            
            [self presentViewController:viewController animated:NO completion:^{
                //////(@"SUCCESS");
            }
             ];
            
        }
        else
        {
            slideShow *viewController=[[slideShow alloc]initWithNibName:@"slideShow~iPhone" bundle:nil];
            
            viewController.getViewName=@"SHARE";
            
            [self presentViewController:viewController animated:NO completion:^{
                //////(@"SUCCESS");
            }
             ];
            
        }
        }

    }
    
    
}



-(IBAction)SlideShowCard
{
    NSString *actionSheetTitle = @""; //Action Sheet Title
    //NSString *destructiveTitle = @"Destructive Button"; //Action Sheet Button Titles
     NSString *other0 = @"All of the above";
    NSString *other1 = @"Sent Cards";
    NSString *other2 = @"Received Cards";
     NSString *other3 = @"Shared Cards";
    NSString *cancelTitle = @"Cancel";
    
    if (totalCount==0)
    {
        
    }
    else
    {

    
   UIActionSheet *actionSheetGet = [[UIActionSheet alloc]
                      initWithTitle:actionSheetTitle
                      delegate:self
                      cancelButtonTitle:cancelTitle
                      destructiveButtonTitle:nil
                      otherButtonTitles:other1, other2,other3,other0, nil];
    [actionSheetGet showInView:self.view];
        
    }
    
  
}
-(IBAction)saveCard
{
    if (savedCount==0)
    {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"You do not have any Saved SocialCards"] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];

    }
    else
    {
    if (IS_IPHONE_5)
    {
        savedCards *viewController=[[savedCards alloc]initWithNibName:@"savedCards" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
        
    }
    else
    {
        savedCards *viewController=[[savedCards alloc]initWithNibName:@"savedCards~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
        
    }
    }

}


-(void)getNetCoins
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/getUserNetCoins",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post1 = [ASIFormDataRequest requestWithURL:url];
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    
    //[request_post setValidatesSecureCertificate:NO];
    [request_post1 setPostValue:user forKey:@"userID"];
     [request_post1 setPostValue:[Config get_token] forKey:@"apikey"];
    
    
    [request_post1 setCompletionBlock:^{
        NSString *responseString = [request_post1 responseString];
        NSMutableData *results1 = [responseString JSONValue];
        
        
        
        
        
        NSString *netCoins=[results1 valueForKeyPath:@"response.totalNetCoins"];
        
        
        NSUserDefaults *value=[NSUserDefaults standardUserDefaults];
        
        [value setObject:netCoins forKey:@"TOTAL_COINS"];
        
        
        ////(@"influenceLevelStr:%@",netCoins);
        
        NSString *netCoinsStr=[NSString stringWithFormat:@"You have a total of %@ NetCoin(s)",netCoins];
        
        
       // _totalNetCoinslabel.text=netCoinsStr;
        
        //  [self getInfluenceLevel:total_scores];
        
        
    }];
    [request_post1 setFailedBlock:^{
        NSError *error = [request_post1 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1 startAsynchronous];
    
    
}




-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    //u need to change 0 to other value(,1,2,3) if u have more buttons.then u can check which button was pressed.
    
    if (alertView==alertExit)
    {
        if (buttonIndex == 0)
        {
            
            ////(@"0");
            
            
        }
        else
        {
            [self fbDidLogout];
            
            NSUserDefaults *add=[NSUserDefaults standardUserDefaults];
            
            ////(@"DEVICE TOKEN 123:%@",[add objectForKey:@"DEVICE_TOKEN"]);
            
            NSString *device_token=[add objectForKey:@"DEVICE_TOKEN"];
            
            if([device_token length]==0)
            {
                device_token=@"";
                
                ////(@"DEVICE TOKEN 789:%@",device_token);
            }
            else
            {
                
                ////(@"DEVICE TOKEN 789:%@",device_token);
                
                NSString *user=[add objectForKey:@"USERID"];
                
                //[request_post setValidatesSecureCertificate:NO];
                
                
                
                NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/pushnotification/updatedevicetoken",CONFIG_BASE_URL]];
                
                __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
                
                
                [request_post setValidatesSecureCertificate:NO];
                [request_post setPostValue:user forKey:@"userID"];
                [request_post setPostValue:device_token forKey:@"deviceToken"];
                [request_post setPostValue:[Config get_token] forKey:@"apikey"];
                [request_post setPostValue:@"logout" forKey:@"userStatus"];
                
                
                [request_post setTimeOutSeconds:30];
                
                
                [request_post setCompletionBlock:^{
                    NSString *responseString = [request_post responseString];
                    NSMutableData *results1 = [responseString JSONValue];
                    
                    
                    ////(@"RESULTS:%@",results1);
                    
                    
                    
                    
                    
                    
                    
                }];
                [request_post setFailedBlock:^{
                    NSError *error = [request_post error];
                    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                        message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                              cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    
                    
                    
                    
                }];
                
                [request_post startAsynchronous];
                
                
            }

            
            NSUserDefaults *add1=[NSUserDefaults standardUserDefaults];
            
            [add1 removeObjectForKey:@"USERID"];
            
            // [add setObject:@"" forKey:@"USERID"];
            
            
            
            if (IS_IPHONE_5)
            {
                ViewController *viewController=[[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
                [self presentViewController:viewController animated:NO completion:^{
                    ////////(@"SUCCESS");
                }
                 ];
            }
            else
            {
                ViewController *viewController=[[ViewController alloc]initWithNibName:@"ViewController~iPhone" bundle:nil];
                [self presentViewController:viewController animated:NO completion:^{
                    ////////(@"SUCCESS");
                }
                 ];
            }
            
            
            
        }
    }
    else
    {
        
        
    }
    
}

-(IBAction)settingsButton
{
    
    
    alertExit = [[UIAlertView alloc] initWithTitle:@"Logout"
                                           message:@"Do you want to really exit?" delegate:self cancelButtonTitle: @"No"
                                 otherButtonTitles:@"Yes", nil];
    
    //note above delegate property
    
    [alertExit show];
    
    
    
}
- (void)fbDidLogout
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"FBAccessTokenKey"];
    [defaults removeObjectForKey:@"FBExpirationDateKey"];
    
    [defaults synchronize];
    
    NSHTTPCookie *cookie;
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (cookie in [storage cookies]) {
        [storage deleteCookie:cookie];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(IBAction)viewsendCardButton
{
    
    if (sentCount==0)
    {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"You do not have any Sent SocialCards"] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
    }
    else
    {
    
    if (IS_IPHONE_5)
    {
        sendCardViewController *viewController=[[sendCardViewController alloc]initWithNibName:@"sendCardViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
        
    }
    else
    {
        sendCardViewController *viewController=[[sendCardViewController alloc]initWithNibName:@"sendCardViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
        
    }
        
    }
 
}


-(IBAction)sendCardButton
{
    NSUserDefaults *storeImageValues=[NSUserDefaults standardUserDefaults];
     [storeImageValues setObject:@"" forKey:@"AUDIO_REFERENCE"];
    
    [storeImageValues setObject:@"NEW_CARDS" forKey:@"VIEW_REFERENCE"];
    
    if (IS_IPHONE_5)
    {
        albumViewController *viewController=[[albumViewController alloc]initWithNibName:@"albumViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
    
    albumViewController *viewController=[[albumViewController alloc]initWithNibName:@"albumViewController~iPhone" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        //////(@"SUCCESS");
    }
     ];
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)meButton
{
    if (IS_IPHONE_5) {
        MeViewController *viewController=[[MeViewController alloc]initWithNibName:@"MeViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        
        MeViewController *viewController=[[MeViewController alloc]initWithNibName:@"MeViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }

}
-(IBAction)dealsButton
{
    dealsViewController *viewController=[[dealsViewController alloc]initWithNibName:@"dealsViewController" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        //////(@"SUCCESS");
    }
     ];
}
-(IBAction)socialButton
{
    if (IS_IPHONE_5) {
        netCoinsViewController *viewController=[[netCoinsViewController alloc]initWithNibName:@"netCoinsViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        netCoinsViewController *viewController=[[netCoinsViewController alloc]initWithNibName:@"netCoinsViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }

}
-(IBAction)netCoinsButton
{
    if (IS_IPHONE_5) {
        socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }

}
-(IBAction)myBusinessButton
{
    myBusinessViewController *viewController=[[myBusinessViewController alloc]initWithNibName:@"myBusinessViewController" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        //////(@"SUCCESS");
    }
     ];
}


@end
