//
//  MeViewController.m
//  PhotoSliding
//
//  Created by ephronsystems on 6/27/13.
//
//
#import <Foundation/NSNotificationQueue.h>
#import "MeViewController.h"
#import <Twitter/Twitter.h>
#import "OAuth+Additions.h"
#import "TWAPIManager.h"
#import "TWSignedRequest.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import <GoogleOpenSource/GoogleOpenSource.h>
#import <GooglePlus/GooglePlus.h>
#import <QuartzCore/QuartzCore.h>

#define ERROR_TITLE_MSG @"No Account Access"
#define ERROR_NO_ACCOUNTS @"You must add a Twitter account in Settings"
#define ERROR_PERM_ACCESS @"We weren't granted access to the user's accounts"
#define ERROR_NO_KEYS @"You need to add your Twitter app keys to Info.plist to use this demo.\nPlease see README.md for more info."
#define ERROR_OK @"OK"
@interface MeViewController ()<GPPSignInDelegate>
- (void)enableSignInSettings:(BOOL)enable;
- (void)reportAuthStatus;
- (void)retrieveUserInfo;

@end

@implementation MeViewController
@synthesize scrollView,pageControl;
@synthesize goldView,platinumView,silverView;
@synthesize signInButton = signInButton_;
@synthesize signInAuthStatus = signInAuthStatus_;
@synthesize signInDisplayName = signInDisplayName_;
@synthesize signOutButton = signOutButton_;
@synthesize disconnectButton = disconnectButton_;
@synthesize userinfoEmailScope = userinfoEmailScope_;
@synthesize button, name, headline, oAuthLoginView,
status, postButton, postButtonLabel,
statusTextView, updateStatusLabel;
@synthesize comingString;
@synthesize qrView;
@synthesize reader,scan_view;

@synthesize slideToUnlock, myLabel, lockButton, Container;
BOOL UNLOCKED = NO;


@synthesize slideToUnlockTw, myLabelTw, lockButtonTw, ContainerTw;
BOOL UNLOCKEDTW = NO;

@synthesize slideToUnlockLi, myLabelLi, lockButtonLi, ContainerLi;
BOOL UNLOCKEDLI = NO;


@synthesize slideToUnlockGp, myLabelGp, lockButtonGp, ContainerGp;
BOOL UNLOCKEDGP = NO;


NSString *userID;
NSString *fbuserID;
NSString *fbAccessToken;

UIAlertView *  alertExit;

NSString *fbImageStr;

NSString *profileImageStr;

NSString *twitterImageStr;
NSString *gpImageStr;
NSString *liImageStr;
NSString *totalLinkedinstr;

NSString *userstatusStr;
NSString *totalscoreStr;


NSString *twitterNameStr;
NSString *fbNameStr;
NSString *gpNameStr;
NSString *liNameStr;


NSString *influenceLevelStr;

int total_scores;
int facebook_score;
int twitter_score;
int linkdin_score;
int  googleplus_score;

NSString *platinumScoreReach;
NSString *goldScoreReach;


NSString *checkValidation;


-(IBAction)browseImage
{
  
    checkValidation=@"IMAGE";
    ;
    NSString *actionSheetTitle = @"Select your Picture"; //Action Sheet Title
    // NSString *destructiveTitle = @"Destructive Button"; //Action Sheet Button Titles
    NSString *other1 = @"Camera";
    NSString *other2 = @"Album";
    
    NSString *cancelTitle = @"Cancel";
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:actionSheetTitle
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:other1, other2,  nil];
    [actionSheet showInView:self.view];
    
    
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    if (buttonIndex==0)
    {
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            
            
            
            [self presentViewController:picker animated:YES completion:NULL];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"The device does not have camera" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            // [alert release];
        }
        
    }
    else  if (buttonIndex==1)
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:NULL];
        
    }
    
    
}


-(void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary*)info
{
    
    if ([checkValidation isEqualToString:@"IMAGE"])
    {
        
    
    
    NSData *imagedata = UIImageJPEGRepresentation([info objectForKey:@"UIImagePickerControllerEditedImage" ],1);
    UIImage *image = [[UIImage alloc] initWithData:imagedata];
        
        float ori_height=image.size.height;
        float ori_width=image.size.width;
        
        NSLog(@"H:%f|W:%f",ori_height,ori_width);

                   
            float height_ratio1=ori_height/4;
            float width_ratio1=ori_width/4;
        
        NSLog(@"HJ:%f|WJ:%f",height_ratio1,width_ratio1);
            
       
        
     image = [self imageWithImageSimple:image scaledToSize:CGSizeMake(width_ratio1, height_ratio1)];
        
    
    _MEIMAGE.image=image;
    
   
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/files/postUserProImgfile",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
    
    
    //[request_post setFile:@"/Users/apple/Desktop/Free-Grammar-Ebook-Level-2.pdf" forKey:@"filename"];
    
    //UIImage * ourImage = [UIImage imageNamed:@"rajini-bday2.jpg"];
    NSData * ourImageData = UIImageJPEGRepresentation(_MEIMAGE.image, 100);
    
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"HH:mm:ss zzz"];
    NSString *dateString = [dateFormat stringFromDate:date];
    
    //////(@"DATE STRING:%@",dateString);
    
    NSString *newString = [dateString stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *newString1 = [newString stringByReplacingOccurrencesOfString:@":" withString:@""];
    NSString *newString2 = [newString1 stringByReplacingOccurrencesOfString:@"GMT" withString:@""];
    NSString *newString3 = [newString2 stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    NSString *imageName=[NSString stringWithFormat:@"%@.jpg",newString3];
    
    
    
    [request_post addData:ourImageData withFileName:imageName andContentType:@"image/jpeg" forKey:@"file"];
    
    [request_post setTimeOutSeconds:60];
    
    
    [request_post setCompletionBlock:^{
        // Use when fetching text data
        NSString *responseString = [request_post responseString];
        
        
       NSLog(@"RES:%@",responseString);
        
        NSMutableData *results11 = [responseString JSONValue];
        
        
       NSLog(@"RESULTS LOGIN:%@",results11);
        
        
        NSString *temp= [results11 valueForKeyPath:@"photos.profile_img_url"];
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/updateprofileImg",CONFIG_BASE_URL]];
        
        __block  ASIFormDataRequest *request_post1234 = [ASIFormDataRequest requestWithURL:url];
        
        
        
        NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
        NSString *user=[check objectForKey:@"USERID"];
        
        
        [request_post1234 setPostValue:user forKey:@"userID"];
        [request_post1234 setPostValue:[Config get_token] forKey:@"apikey"];
         [request_post1234 setPostValue:temp forKey:@"proImg"];
        
        
        [request_post1234 setTimeOutSeconds:30];
        
        
        [request_post1234 setCompletionBlock:^{
            NSString *responseString23 = [request_post1234 responseString];
            NSMutableData *results11 = [responseString23 JSONValue];
            
            
            
            //(@"TEMPARY:%@",results11);
           
            [self updateView];
            
        }];
        [request_post1234 setFailedBlock:^{
            NSError *error = [request_post1234 error];
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                      cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            
            
            
        }];
        
        [request_post1234 startAsynchronous];

               
        
        if ([temp length]==0) {
            
            NSString *temp= [results11 valueForKeyPath:@"error.text"];
            
            
            NSUserDefaults *imgtemp=[NSUserDefaults standardUserDefaults];
            
            [imgtemp setObject:@"NULL" forKey:@"USER_IMAGE"];
            
            [_imageButton setImage:nil forState:UIControlStateNormal];
            [_imageButton setTitle:@"Select Image" forState:UIControlStateNormal];
            
        }
        
        // Use when fetching binary data
        // NSData *responseData = [request_post responseData];
    }];
    [request_post setFailedBlock:^{
        NSError *error = [request_post error];
        
        
        
        
    }];
    [request_post startAsynchronous];
    
    
    [picker dismissModalViewControllerAnimated:YES];
    
    }
    else
    {
        // ADD: get the decode results
        id<NSFastEnumeration> results =
        [info objectForKey: ZBarReaderControllerResults];
        ////(@"info");
        ZBarSymbol *symbol = nil;
        for(symbol in results)
            break;
        
        ////(@"datas %@",symbol.data);
        
        NSString *result=[NSString stringWithFormat:@"%@",symbol.data];
        
        NSArray *arrString = [result componentsSeparatedByString:@" "];
        
        NSString *finalString=@"";
        
        for(int i=0; i<arrString.count;i++){
            if([[arrString objectAtIndex:i] rangeOfString:@"http"].location != NSNotFound)
                ////(@"ARRA:%@", [arrString objectAtIndex:i]);
                
                finalString=[arrString objectAtIndex:i];
        }
        
        
        
        
        
        
        if ([finalString rangeOfString:@"http"].location == NSNotFound)
        {
            ////(@"string does not contain bla");
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"This code not contains valid URL" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
        }
        else
        {
            ////(@"string contains bla!");
            
            qrView.hidden=YES;
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:finalString]];
            
            [reader.view removeFromSuperview];
        }
        
        
        // resultImage.image =
        [info objectForKey: UIImagePickerControllerOriginalImage];
        
        
        //  

    }
    
    
}

-(UIImage *)imageWithImageSimple:(UIImage*)image scaledToSize:(CGSize)newSize
{
    
    UIGraphicsBeginImageContext(newSize);//根据当前大小创建一个基于位图图形的环境
    
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];//根据新的尺寸画出传过来的图片
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();//从当前环境当中得到重绘的图片
    
    UIGraphicsEndImageContext();//关闭当前环境
    
    return newImage;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissModalViewControllerAnimated:YES];
}




- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)clickLI:(id)sender
{
    
    NSString *deviceStr = [UIDevice currentDevice].model;
    ////(@"device:%@",deviceStr);
    
    
    if ([deviceStr isEqualToString:@"iPod touch"])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Linkdin" message:@"Current Device not support for Linkdin Connection.Please try again some other devices." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
    
        if (IS_IPHONE_5) {
            oAuthLoginView = [[OAuthLoginView alloc] initWithNibName:@"OAuthLoginView" bundle:nil];
            //[oAuthLoginView retain];
            
            // register to be told when the login is finished
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(loginViewDidFinish:)
                                                         name:@"loginViewDidFinish"
                                                       object:oAuthLoginView];
            
            [self presentModalViewController:oAuthLoginView animated:YES];
        }
        else
        {
            oAuthLoginView = [[OAuthLoginView alloc] initWithNibName:@"OAuthLoginView~iPhone" bundle:nil];
            //[oAuthLoginView retain];
            
            // register to be told when the login is finished
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(loginViewDidFinish:)
                                                         name:@"loginViewDidFinish"
                                                       object:oAuthLoginView];
            
            [self presentModalViewController:oAuthLoginView animated:YES];
        }
   
    }
}

-(void) loginViewDidFinish:(NSNotification*)notification
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
    
    // We're going to do these calls serially just for easy code reading.
    // They can be done asynchronously
    // Get the profile, then the network updates
    [self profileApiCall];
	
}

-(void)calLinkedinconnect:(NSString *)token
{
    ////(@"LISTR:%@",token);
    
    NSArray *testArray2 = [token componentsSeparatedByString:@"oauth_token_secret"];
    
    ////(@"TEST ARRAY:%@",testArray2);
    
    
    NSString *LItoken=testArray2[0];
    
    NSString *stringWithoutSpaces = [LItoken
                                     stringByReplacingOccurrencesOfString:@"oauth_token" withString:@""];
    stringWithoutSpaces = [LItoken
                           stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    
    ////(@"WITHOUT:%@",stringWithoutSpaces);
    
    
    NSString *newKK=[stringWithoutSpaces
                      stringByReplacingOccurrencesOfString:@"oauth_token " withString:@""];
    
    ////(@"NEW TOKEN:%@",newKK);
    
    NSString *newTok=[newKK
                     stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    
    
    
    NSString* result = [newTok stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    ////(@"NEW TOKEN:%@A",result);
    
    
    
    if (result==[NSNull null]||[result length]==0)
    {
        
    }
    else
    {
    
    NSString *hell=[NSString stringWithFormat:@"%@A",result];
    
    
    NSString *newString = [hell substringToIndex:[hell length]-1];
    
    
    
    
    NSString *LISEC=testArray2[1];
    
    NSString *stringWithoutSpaces1 = [LISEC
                                     stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    NSString *stringWithoutSpaces2 = [stringWithoutSpaces1
                           stringByReplacingOccurrencesOfString:@"oauth_verifier " withString:@""];
    
    ////(@"WITHOUT:%@",stringWithoutSpaces2);
    
    
    NSString *newTT=[stringWithoutSpaces2
                      stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
    
    NSString *newsec=[newTT
                     stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    ////(@"NEW SEC:%@",newsec);
    
    
    
    
    
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/marketplace/authenticateLinkedin",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post1234 = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    
    ////(@"USER:%@",user);
    
    //[request_post setValidatesSecureCertificate:NO];
    [request_post1234 setPostValue:user forKey:@"userID"];
     [request_post1234 setPostValue:[Config get_token] forKey:@"apikey"];
    
    
    //[request_post1234 setPostValue:@"d80742bb-6423-4317-9282-2dfe1bfa4ba0" forKey:@"linkedin_access_token"];
   // [request_post1234 setPostValue:@"80dbf059-a3d5-4f31-9405-86b981480f96" forKey:@"linkedin_secret_token"];
    
    
    
    ////(@"NEW STR:%@",newString);
    
    [request_post1234 setPostValue:newString forKey:@"linkedin_access_token"];
     [request_post1234 setPostValue:newsec forKey:@"linkedin_secret_token"];
    
    [request_post1234 setTimeOutSeconds:30];
    
    
    [request_post1234 setCompletionBlock:^{
        NSString *responseString23 = [request_post1234 responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        
        ////(@"RESULTS LOGIN ADASD:%@",results11);
        
        
         [self getFBScore];
        
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/setExtraction",CONFIG_BASE_URL]];
        
        __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
        
        
        ////(@"USER NE NE:%@",userID);
        
       
        
        [request_post setValidatesSecureCertificate:NO];
        [request_post setPostValue:userID forKey:@"userID"];
        [request_post setPostValue:@"linkedin" forKey:@"network"];
         [request_post setPostValue:[Config get_token] forKey:@"apikey"];
        
        
        [request_post setTimeOutSeconds:30];
        
        
        [request_post setCompletionBlock:^{
            NSString *responseString = [request_post responseString];
            NSMutableData *results1 = [responseString JSONValue];
            
            
            ////(@"RESULTS ASAS Hello:%@",results1);
            
            
            
            
            
            
        }];
        [request_post setFailedBlock:^{
            NSError *error = [request_post error];
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                      cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            
            
            
        }];
        
        [request_post startAsynchronous];

        
        
       
        
        
    }];
    [request_post1234 setFailedBlock:^{
        NSError *error = [request_post1234 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1234 startAsynchronous];
     
    }
}

- (void)profileApiCall
{
    
    
    ////(@"TOKEN:%@",oAuthLoginView.accessToken);
    
    
    totalLinkedinstr=[NSString stringWithFormat:@"%@",oAuthLoginView.accessToken];
    
   // [self calLinkedinconnect:str];
    
    
    NSURL *url = [NSURL URLWithString:@"http://api.linkedin.com/v1/people/~"];
    OAMutableURLRequest *request =
    [[OAMutableURLRequest alloc] initWithURL:url
                                    consumer:oAuthLoginView.consumer
                                       token:oAuthLoginView.accessToken
                                    callback:nil
                           signatureProvider:nil];
    
    [request setValue:@"json" forHTTPHeaderField:@"x-li-format"];
    
    OADataFetcher *fetcher = [[OADataFetcher alloc] init];
    [fetcher fetchDataWithRequest:request
                         delegate:self
                didFinishSelector:@selector(profileApiCallResult:didFinish:)
                  didFailSelector:@selector(profileApiCallResult:didFail:)];
   // [request release];
    
}

- (void)profileApiCallResult:(OAServiceTicket *)ticket didFinish:(NSData *)data
{
    NSString *responseBody = [[NSString alloc] initWithData:data
                                                   encoding:NSUTF8StringEncoding];
    
    
    ////(@"RES:%@",responseBody);
    
    NSDictionary *profile = [responseBody objectFromJSONString];
   // [responseBody release];
    
    if ( profile )
    {
        name.text = [[NSString alloc] initWithFormat:@"%@ %@",
                     [profile objectForKey:@"firstName"], [profile objectForKey:@"lastName"]];
        headline.text = [profile objectForKey:@"headline"];
        
        
       
        
        
    }
    else
    {
        ////(@"NOT PROFILE:%@",responseBody);
    }
    
    
    
   
   
    
    // The next thing we want to do is call the network updates
    [self networkApiCall];
    
}

- (void)profileApiCallResult:(OAServiceTicket *)ticket didFail:(NSData *)error
{
    ////(@"%@",[error description]);
}

- (void)networkApiCall
{
    NSURL *url = [NSURL URLWithString:@"http://api.linkedin.com/v1/people/~/network/updates?scope=self&count=1&type=STAT"];
    OAMutableURLRequest *request =
    [[OAMutableURLRequest alloc] initWithURL:url
                                    consumer:oAuthLoginView.consumer
                                       token:oAuthLoginView.accessToken
                                    callback:nil
                           signatureProvider:nil];
    
    [request setValue:@"json" forHTTPHeaderField:@"x-li-format"];
    
    OADataFetcher *fetcher = [[OADataFetcher alloc] init];
    [fetcher fetchDataWithRequest:request
                         delegate:self
                didFinishSelector:@selector(networkApiCallResult:didFinish:)
                  didFailSelector:@selector(networkApiCallResult:didFail:)];
   // [request release];
    
}

- (void)networkApiCallResult:(OAServiceTicket *)ticket didFinish:(NSData *)data
{
    NSString *responseBody = [[NSString alloc] initWithData:data
                                                   encoding:NSUTF8StringEncoding];
    
    NSDictionary *person = [[[[[responseBody objectFromJSONString]
                               objectForKey:@"values"]
                              objectAtIndex:0]
                             objectForKey:@"updateContent"]
                            objectForKey:@"person"];
    
   // [responseBody release];
    
    if ( [person objectForKey:@"currentStatus"] )
    {
        [postButton setHidden:false];
        [postButtonLabel setHidden:false];
        [statusTextView setHidden:false];
        [updateStatusLabel setHidden:false];
        status.text = [person objectForKey:@"currentStatus"];
    }
    else
    {
        [postButton setHidden:false];
        [postButtonLabel setHidden:false];
        [statusTextView setHidden:false];
        [updateStatusLabel setHidden:false];
        status.text = [[[[person objectForKey:@"personActivities"]
                         objectForKey:@"values"]
                        objectAtIndex:0]
                       objectForKey:@"body"];
        
    }
    
    
    ////(@"FINISHED HE:%@",totalLinkedinstr);
    
    if (totalLinkedinstr==[NSNull null] ||[totalLinkedinstr length]==0||[totalLinkedinstr isEqualToString:@"(null)"])
    {
        ////(@"Enter");
    }
    else
    {
    [self calLinkedinconnect:totalLinkedinstr];
    }
   // [self dismissModalViewControllerAnimated:YES];
}

- (void)networkApiCallResult:(OAServiceTicket *)ticket didFail:(NSData *)error
{
    ////(@"%@",[error description]);
}

- (IBAction)postButton_TouchUp:(UIButton *)sender
{
    [statusTextView resignFirstResponder];
    NSURL *url = [NSURL URLWithString:@"http://api.linkedin.com/v1/people/~/shares"];
    OAMutableURLRequest *request =
    [[OAMutableURLRequest alloc] initWithURL:url
                                    consumer:oAuthLoginView.consumer
                                       token:oAuthLoginView.accessToken
                                    callback:nil
                           signatureProvider:nil];
    
    NSDictionary *update = [[NSDictionary alloc] initWithObjectsAndKeys:
                            [[NSDictionary alloc]
                             initWithObjectsAndKeys:
                             @"anyone",@"code",nil], @"visibility",
                            statusTextView.text, @"comment", nil];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSString *updateString = [update JSONString];
    
    [request setHTTPBodyWithString:updateString];
	[request setHTTPMethod:@"POST"];
    
    OADataFetcher *fetcher = [[OADataFetcher alloc] init];
    [fetcher fetchDataWithRequest:request
                         delegate:self
                didFinishSelector:@selector(postUpdateApiCallResult:didFinish:)
                  didFailSelector:@selector(postUpdateApiCallResult:didFail:)];
    //[request release];
}

- (void)postUpdateApiCallResult:(OAServiceTicket *)ticket didFinish:(NSData *)data
{
    // The next thing we want to do is call the network updates
    [self networkApiCall];
    
}

- (void)postUpdateApiCallResult:(OAServiceTicket *)ticket didFail:(NSData *)error
{
    ////(@"%@",[error description]);
}


-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    //u need to change 0 to other value(,1,2,3) if u have more buttons.then u can check which button was pressed.
    
    if (alertView==alertExit)
    {
        if (buttonIndex == 0)
        {
            
            ////(@"0");
            
            
        }
        else
        {
           //[self fbDidLogout];
            
           [FBSession.activeSession closeAndClearTokenInformation];
            
            // [add setObject:@"" forKey:@"USERID"];
            
            NSUserDefaults *add=[NSUserDefaults standardUserDefaults];
            
            ////(@"DEVICE TOKEN 123:%@",[add objectForKey:@"DEVICE_TOKEN"]);
            
            NSString *device_token=[add objectForKey:@"DEVICE_TOKEN"];
            
            if([device_token length]==0)
            {
                device_token=@"";
                
                ////(@"DEVICE TOKEN 789:%@",device_token);
            }
            else
            {
                
                ////(@"DEVICE TOKEN 789:%@",device_token);
                
                NSString *user=[add objectForKey:@"USERID"];
                
                //[request_post setValidatesSecureCertificate:NO];
                
                
                
                NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/pushnotification/updatedevicetoken",CONFIG_BASE_URL]];
                
                __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
                
                
                [request_post setValidatesSecureCertificate:NO];
                [request_post setPostValue:user forKey:@"userID"];
                [request_post setPostValue:device_token forKey:@"deviceToken"];
                [request_post setPostValue:[Config get_token] forKey:@"apikey"];
                [request_post setPostValue:@"logout" forKey:@"userStatus"];
                
                
                [request_post setTimeOutSeconds:30];
                
                
                [request_post setCompletionBlock:^{
                    NSString *responseString = [request_post responseString];
                    NSMutableData *results1 = [responseString JSONValue];
                    
                    
                    ////(@"RESULTS LOG :%@",results1);
                    
                    
                    
                    
                    
                    
                    
                }];
                [request_post setFailedBlock:^{
                    NSError *error = [request_post error];
                    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                        message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                              cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    
                    
                    
                    
                }];
                
                [request_post startAsynchronous];
                
                
            }

            NSUserDefaults *add1=[NSUserDefaults standardUserDefaults];
            
            [add1 removeObjectForKey:@"USERID"];
            if (IS_IPHONE_5)
            {
                ViewController *viewController=[[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
                [self presentViewController:viewController animated:NO completion:^{
                    ////(@"SUCCESS");
                }
                 ];
            }
            else
            {
                ViewController *viewController=[[ViewController alloc]initWithNibName:@"ViewController~iPhone" bundle:nil];
                [self presentViewController:viewController animated:NO completion:^{
                    ////(@"SUCCESS");
                }
                 ];
            }
            
            

        }
    }
    else
    {
    
    
    }
    
}

-(IBAction)settingsButton
{
    
    
  alertExit = [[UIAlertView alloc] initWithTitle:@"Logout"
                                                    message:@"Do you want to really exit?" delegate:self cancelButtonTitle: @"No"
                                          otherButtonTitles:@"Yes", nil];
    
    //note above delegate property
    
    [alertExit show];
    
    
      
}

- (void)fbDidLogout
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"FBAccessTokenKey"];
    [defaults removeObjectForKey:@"FBExpirationDateKey"];
    
    [defaults synchronize];
    
    NSHTTPCookie *cookie;
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (cookie in [storage cookies]) {
        [storage deleteCookie:cookie];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/*
- (void)logout:(id<FBSessionDelegate>)delegate {
    // [self logout];
    
    
    AppDelegate *getdelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    getdelegate.facebook.sessionDelegate=self;
    
    if (delegate != getdelegate.facebook.sessionDelegate &&
        [delegate respondsToSelector:@selector(fbDidLogout)]) {
        [delegate fbDidLogout];
    }
}
*/
-(IBAction)clickGP:(id)sender
{
    
    if (userinfoEmailScope_.on)
    {
        ////(@"HELLO");
    }
    
    [self signOut:nil];
    
    [GPPSignIn sharedInstance].shouldFetchGoogleUserEmail =YES;
    //userinfoEmailScope_.on;
    
    
     GPPSignIn *signIn = [GPPSignIn sharedInstance];
    [signIn authenticate];
}
-(IBAction)LockIt {
    slideToUnlock.enabled=YES;
	slideToUnlock.hidden = NO;
	lockButton.hidden = YES;
	Container.hidden = NO;
	myLabel.hidden = NO;
	myLabel.alpha = 1.0;
	UNLOCKED = NO;
	slideToUnlock.value = 0.0;
	NSString *str = @"The iPhone is Locked!";
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Locked" message:str delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
	[alert show];
	//[alert release];
}

-(IBAction)fadeLabel {
    
	myLabel.alpha = 1.0 - slideToUnlock.value;
	
}

-(IBAction)UnLockIt {
    
	
	if (!UNLOCKED)
    {
		
		
        
		if (slideToUnlock.value ==1.0)
        {  // if user slide far enough, stop the operation
            // Put here what happens when it is unlocked
            
            
            ////(@"YES FACEBOOK");
            
            
            [self faceBookClick:nil];
            
            
            slideToUnlock.hidden = NO;
            
			lockButton.hidden = NO;
			Container.hidden = NO;
			myLabel.hidden = YES;
			UNLOCKED = YES;
            slideToUnlock.enabled=NO;
            
			/*
             slideToUnlock.hidden = YES;
             lockButton.hidden = NO;
             Container.hidden = YES;
             myLabel.hidden = YES;
             UNLOCKED = YES;
             */
		}
        else
        {
			// user did not slide far enough, so return back to 0 position
            
            ////(@"CANCEL FACE");
            
			[UIView beginAnimations: @"SlideCanceled" context: nil];
			[UIView setAnimationDelegate: self];
			[UIView setAnimationDuration: 0.35];
			// use CurveEaseOut to create "spring" effect
			[UIView setAnimationCurve: UIViewAnimationCurveEaseOut];
			slideToUnlock.value = 0.0;
			
			[UIView commitAnimations];
			
			
		}
		
	}
//    else
//        
//    {
//        
//        ////(@"CANCELLLL");
//        
//        [UIView beginAnimations: @"SlideCanceled" context: nil];
//        [UIView setAnimationDelegate: self];
//        [UIView setAnimationDuration: 0.35];
//        // use CurveEaseOut to create "spring" effect
//        [UIView setAnimationCurve: UIViewAnimationCurveEaseOut];
//        slideToUnlock.value = 1.0;
//        
//        [UIView commitAnimations];
//    }
	
}



-(IBAction)LockItTw {
    slideToUnlockTw.enabled=YES;
	slideToUnlockTw.hidden = NO;
	lockButtonTw.hidden = YES;
	ContainerTw.hidden = NO;
	myLabelTw.hidden = NO;
	myLabelTw.alpha = 1.0;
	UNLOCKEDTW = NO;
	slideToUnlockTw.value = 0.0;
	NSString *str = @"The iPhone is Locked!";
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Locked" message:str delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
	[alert show];
	//[alert release];
}

-(IBAction)fadeLabelTw {
    
	myLabelTw.alpha = 1.0 - slideToUnlockTw.value;
	
}

-(IBAction)UnLockItTw {
    
	 ////(@"TWITTER");
    
    
	if (!UNLOCKEDTW)
    {
		
		
        
		if (slideToUnlockTw.value ==1.0)
        {  // if user slide far enough, stop the operation
            // Put here what happens when it is unlocked
            
            
            ////(@"YES TWITTER");
            
            
            [self clickTwitter:nil];
            
            /*
            slideToUnlockTw.hidden = NO;
            
			lockButtonTw.hidden = NO;
			ContainerTw.hidden = NO;
			myLabelTw.hidden = YES;
			UNLOCKEDTW = YES;
            slideToUnlockTw.enabled=NO;
             */
            
			/*
             slideToUnlock.hidden = YES;
             lockButton.hidden = NO;
             Container.hidden = YES;
             myLabel.hidden = YES;
             UNLOCKED = YES;
             */
		}
        else
        {
			// user did not slide far enough, so return back to 0 position
			[UIView beginAnimations: @"SlideCanceled" context: nil];
			[UIView setAnimationDelegate: self];
			[UIView setAnimationDuration: 0.35];
			// use CurveEaseOut to create "spring" effect
			[UIView setAnimationCurve: UIViewAnimationCurveEaseOut];
			slideToUnlockTw.value = 0.0;
			
			[UIView commitAnimations];
			
			
		}
		
	}
//    else
//    {
//        [UIView beginAnimations: @"SlideCanceled" context: nil];
//        [UIView setAnimationDelegate: self];
//        [UIView setAnimationDuration: 0.35];
//        // use CurveEaseOut to create "spring" effect
//        [UIView setAnimationCurve: UIViewAnimationCurveEaseOut];
//        slideToUnlockTw.value = 1.0;
//        
//        [UIView commitAnimations];
//    }
	
}



-(IBAction)LockItLi {
    slideToUnlockLi.enabled=YES;
	slideToUnlockLi.hidden = NO;
	lockButtonLi.hidden = YES;
	ContainerLi.hidden = NO;
	myLabelLi.hidden = NO;
	myLabelLi.alpha = 1.0;
	UNLOCKEDLI = NO;
	slideToUnlockLi.value = 0.0;
	NSString *str = @"The iPhone is Locked!";
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Locked" message:str delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
	[alert show];
	//[alert release];
}

-(IBAction)fadeLabelLi {
    
	myLabelLi.alpha = 1.0 - slideToUnlockLi.value;
	
}

-(IBAction)UnLockItLi{
    
	 ////(@"LINKDIN");
    
	if (!UNLOCKEDLI)
    {
		
		
        
        
        
		if (slideToUnlockLi.value ==1.0)
        {  // if user slide far enough, stop the operation
            // Put here what happens when it is unlocked
            
            
            ////(@"YES LINKDIN");
            
            
            [self clickLI:nil];
            
            /*
            slideToUnlockLi.hidden = NO;
            
			lockButtonLi.hidden = NO;
			ContainerLi.hidden = NO;
			myLabelLi.hidden = YES;
			UNLOCKEDLI = YES;
            slideToUnlockLi.enabled=NO;
             */
            
			/*
             slideToUnlock.hidden = YES;
             lockButton.hidden = NO;
             Container.hidden = YES;
             myLabel.hidden = YES;
             UNLOCKED = YES;
             */
            
            // user did not slide far enough, so return back to 0 position
			[UIView beginAnimations: @"SlideCanceled" context: nil];
			[UIView setAnimationDelegate: self];
			[UIView setAnimationDuration: 0.35];
			// use CurveEaseOut to create "spring" effect
			[UIView setAnimationCurve: UIViewAnimationCurveEaseOut];
			slideToUnlockLi.value = 0.0;
			
			[UIView commitAnimations];
		}
        else
        {
			// user did not slide far enough, so return back to 0 position
			[UIView beginAnimations: @"SlideCanceled" context: nil];
			[UIView setAnimationDelegate: self];
			[UIView setAnimationDuration: 1.35];
			// use CurveEaseOut to create "spring" effect
			[UIView setAnimationCurve: UIViewAnimationCurveEaseOut];
			slideToUnlockLi.value = 0.0;
			
			[UIView commitAnimations];
			
			
		}
		
	}
//    else
//    {
//        [UIView beginAnimations: @"SlideCanceled" context: nil];
//        [UIView setAnimationDelegate: self];
//        [UIView setAnimationDuration: 0.35];
//        // use CurveEaseOut to create "spring" effect
//        [UIView setAnimationCurve: UIViewAnimationCurveEaseOut];
//        slideToUnlockLi.value = 1.0;
//        
//        [UIView commitAnimations];
//    }

	
}

-(IBAction)LockItGp {
    slideToUnlockGp.enabled=YES;
	slideToUnlockGp.hidden = NO;
	lockButtonGp.hidden = YES;
	ContainerGp.hidden = NO;
	myLabelGp.hidden = NO;
	myLabelGp.alpha = 1.0;
	UNLOCKEDGP = NO;
	slideToUnlockGp.value = 0.0;
	NSString *str = @"The iPhone is Locked!";
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Locked" message:str delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
	[alert show];
	//[alert release];
}

-(IBAction)fadeLabelGp {
    
	myLabelGp.alpha = 1.0 - slideToUnlockGp.value;
	
}

-(IBAction)UnLockItGp{
    
	
	if (!UNLOCKEDGP)
    {
		
		
        
		if (slideToUnlockGp.value ==1.0)
        {  // if user slide far enough, stop the operation
            // Put here what happens when it is unlocked
            
            
            ////(@"YES GOOGLE");
            
            [self clickGP:nil];
            
            /*
            slideToUnlockGp.hidden = NO;
            
			lockButtonGp.hidden = NO;
			ContainerGp.hidden = NO;
			myLabelGp.hidden = YES;
			UNLOCKEDGP = YES;
            slideToUnlockGp.enabled=NO;
            
			/*
             slideToUnlock.hidden = YES;
             lockButton.hidden = NO;
             Container.hidden = YES;
             myLabel.hidden = YES;
             UNLOCKED = YES;
             */
            
            // user did not slide far enough, so return back to 0 position
			[UIView beginAnimations: @"SlideCanceled" context: nil];
			[UIView setAnimationDelegate: self];
			[UIView setAnimationDuration: 0.35];
			// use CurveEaseOut to create "spring" effect
			[UIView setAnimationCurve: UIViewAnimationCurveEaseOut];
			slideToUnlockGp.value = 0.0;
			
			[UIView commitAnimations];
		}
        else
        {
			// user did not slide far enough, so return back to 0 position
			[UIView beginAnimations: @"SlideCanceled" context: nil];
			[UIView setAnimationDelegate: self];
			[UIView setAnimationDuration: 0.35];
			// use CurveEaseOut to create "spring" effect
			[UIView setAnimationCurve: UIViewAnimationCurveEaseOut];
			slideToUnlockGp.value = 0.0;
			
			[UIView commitAnimations];
			
			
		}
		
	}
//    else
//    {
//        [UIView beginAnimations: @"SlideCanceled" context: nil];
//        [UIView setAnimationDelegate: self];
//        [UIView setAnimationDuration: 0.35];
//        // use CurveEaseOut to create "spring" effect
//        [UIView setAnimationCurve: UIViewAnimationCurveEaseOut];
//        slideToUnlockGp.value = 1.0;
//        
//        [UIView commitAnimations];
//    }

	
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    NSUserDefaults *add=[NSUserDefaults standardUserDefaults];
    
    ////(@"DEVICE TOKEN AS:%@",[add objectForKey:@"DEVICE_TOKEN"]);
    
    NSString *device_token=[add objectForKey:@"DEVICE_TOKEN"];
    
    if([device_token length]==0)
    {
        device_token=@"";
        
        ////(@"DEVICE TOKEN SD:%@",device_token);
    }
    else
    {
        
        ////(@"DEVICE TOKEN 1010101:%@",device_token);
        
        NSString *user=[add objectForKey:@"USERID"];
        
        //[request_post setValidatesSecureCertificate:NO];
        
        
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/pushnotification/updatedevicetoken",CONFIG_BASE_URL]];
        
        __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
        
        
        [request_post setValidatesSecureCertificate:NO];
        [request_post setPostValue:user forKey:@"userID"];
        [request_post setPostValue:device_token forKey:@"deviceToken"];
        [request_post setPostValue:[Config get_token] forKey:@"apikey"];
        [request_post setPostValue:@"login" forKey:@"userStatus"];
        
        
        [request_post setTimeOutSeconds:30];
        
        
        [request_post setCompletionBlock:^{
            NSString *responseString = [request_post responseString];
            NSMutableData *results1 = [responseString JSONValue];
            
            
            ////(@"RESULTS SA:%@",responseString);
            
            
            
            
            
            
            
        }];
        [request_post setFailedBlock:^{
            NSError *error = [request_post error];
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                      cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            
            
            
        }];
        
        [request_post startAsynchronous];
        
        
    }

    
    
    
    
    
    
    
    [self.view addSubview:qrView];
    
    // [[UIScreen mainScreen] setBrightness: 0.8];
    
    //[[UIScreen mainScreen] setBrightness:0.0];
   
    
     self.trackedViewName = @"Me Screen";
    
    
    qrView.hidden=YES;
    
    
    NSUserDefaults *chk=[NSUserDefaults standardUserDefaults];
    //[chk setObject:@"NO" forKey:@"IS_FIRSTTIME"];
    
    comingString =[chk objectForKey:@"IS_FIRSTTIME"];
    
    if ([comingString isEqualToString:@"YES"])
    {
        
        _totalHideView.hidden=NO;
        [_activity startAnimating];
        
        _userStatusLabel.hidden=YES;
        _totalScoreLabel.hidden=YES;
        _statusImageView.hidden=YES;
        _goldLabel.hidden=YES;
        _platinumLabel.hidden=YES;
        _addImage.hidden=YES;
        
        _platinumImageView.hidden=YES;
        
        _fbHide.hidden=YES;
        _gpHide.hidden=YES;
        _twHide.hidden=YES;
        _liHide.hidden=YES;
        
        [_meButton11 setImage:[UIImage imageNamed:@"iconDefault.png" ] forState:UIControlStateNormal];
        
        
        UIImage *stetchLeftTrack= [[UIImage imageNamed:@"Nothing.png"]
                                   stretchableImageWithLeftCapWidth:30.0 topCapHeight:0.0];
        UIImage *stetchRightTrack= [[UIImage imageNamed:@"Nothing.png"]
                                    stretchableImageWithLeftCapWidth:30.0 topCapHeight:0.0];
        
        
        [slideToUnlock setThumbImage: [UIImage imageNamed:@"SlideToStop.png"] forState:UIControlStateNormal];
        [slideToUnlock setMinimumTrackImage:stetchLeftTrack forState:UIControlStateNormal];
        [slideToUnlock setMaximumTrackImage:stetchRightTrack forState:UIControlStateNormal];
        
        
        [slideToUnlockTw setThumbImage: [UIImage imageNamed:@"SlideToStop.png"] forState:UIControlStateNormal];
        [slideToUnlockTw setMinimumTrackImage:stetchLeftTrack forState:UIControlStateNormal];
        [slideToUnlockTw setMaximumTrackImage:stetchRightTrack forState:UIControlStateNormal];
        
        
        
        [slideToUnlockGp setThumbImage: [UIImage imageNamed:@"SlideToStop.png"] forState:UIControlStateNormal];
        [slideToUnlockGp setMinimumTrackImage:stetchLeftTrack forState:UIControlStateNormal];
        [slideToUnlockGp setMaximumTrackImage:stetchRightTrack forState:UIControlStateNormal];
        
        
        [slideToUnlockLi setThumbImage: [UIImage imageNamed:@"SlideToStop.png"] forState:UIControlStateNormal];
        [slideToUnlockLi setMinimumTrackImage:stetchLeftTrack forState:UIControlStateNormal];
        [slideToUnlockLi setMaximumTrackImage:stetchRightTrack forState:UIControlStateNormal];
        
        
        /*
        
        //slideToUnlock.value ==1.0;
        
        [GPPSignInButton class];
        
        GPPSignIn *signIn = [GPPSignIn sharedInstance];
        userinfoEmailScope_.on =
        signIn.shouldFetchGoogleUserEmail;
        
        
        [GPPSignIn sharedInstance].shouldFetchGoogleUserEmail =
        userinfoEmailScope_.on;
        
        
        // Set up sign-out and disconnect buttons.
        [self setUpButton:signOutButton_];
        [self setUpButton:disconnectButton_];
        
        // Set up sample view of Google+ sign-in.
        // The client ID has been set in the app delegate.
        signIn.delegate = self;
        signIn.shouldFetchGoogleUserEmail = userinfoEmailScope_.on;
        signIn.scopes==[NSArray arrayWithObjects:
                        @"https://www.googleapis.com/auth/userinfo.profile",
                        @"https://www.googleapis.com/auth/userinfo.email",
                        @"http://www.google.com/m8/feeds",
                        nil];
        signIn.actions = [NSArray arrayWithObjects:
                          @"http://schemas.google.com/AddActivity",
                          @"http://schemas.google.com/BuyActivity",
                          @"http://schemas.google.com/CheckInActivity",
                          @"http://schemas.google.com/CommentActivity",
                          @"http://schemas.google.com/CreateActivity",
                          @"http://schemas.google.com/ListenActivity",
                          @"http://schemas.google.com/ReserveActivity",
                          @"http://schemas.google.com/ReviewActivity",
                          nil];
        
        
        //    https://www.googleapis.com/auth/userinfo.profile','http://www.google.com/m8/feeds','https://www.googleapis.com/auth/userinfo.email'));
        
        [self reportAuthStatus];
        [signIn trySilentAuthentication];
        */
        pageControlBeingUsed = NO;
        
        
        _fbImage.hidden=NO;
        _MEIMAGE.hidden=NO;
        _twNameLabel.hidden=YES;
        _liNameLabel.hidden=YES;
        _gpNameLabel.hidden=YES;
        _fbNameLabel.hidden=YES;
        _MeLabel.hidden=YES;
        
        
        _fbImage.layer.cornerRadius = 5.0;
        _fbImage.clipsToBounds = YES;
        
        
        _twitterImage.layer.cornerRadius = 5.0;
        _twitterImage.clipsToBounds = YES;
        
        _gpImage.layer.cornerRadius = 5.0;
        _gpImage.clipsToBounds = YES;
        
        _linkdinImage.layer.cornerRadius = 5.0;
        _linkdinImage.clipsToBounds = YES;
        
        
        
        // [self updateView];
        
          [self netCoinsButton];
        
        [self getFBScore];
        
        
        
    }
    else
    {
        
        
        _totalHideView.hidden=YES;
        [_activity stopAnimating];
        
        _userStatusLabel.hidden=YES;
        _totalScoreLabel.hidden=YES;
        _statusImageView.hidden=YES;
        _goldLabel.hidden=YES;
        _platinumLabel.hidden=YES;
        _addImage.hidden=YES;
        
        _platinumImageView.hidden=YES;
        
        _fbHide.hidden=YES;
        _gpHide.hidden=YES;
        _twHide.hidden=YES;
        _liHide.hidden=YES;
        
        [_meButton11 setImage:[UIImage imageNamed:@"iconDefault.png" ] forState:UIControlStateNormal];
        
        
        UIImage *stetchLeftTrack= [[UIImage imageNamed:@"Nothing.png"]
                                   stretchableImageWithLeftCapWidth:30.0 topCapHeight:0.0];
        UIImage *stetchRightTrack= [[UIImage imageNamed:@"Nothing.png"]
                                    stretchableImageWithLeftCapWidth:30.0 topCapHeight:0.0];
        
        
        [slideToUnlock setThumbImage: [UIImage imageNamed:@"SlideToStop.png"] forState:UIControlStateNormal];
        [slideToUnlock setMinimumTrackImage:stetchLeftTrack forState:UIControlStateNormal];
        [slideToUnlock setMaximumTrackImage:stetchRightTrack forState:UIControlStateNormal];
        
        
        [slideToUnlockTw setThumbImage: [UIImage imageNamed:@"SlideToStop.png"] forState:UIControlStateNormal];
        [slideToUnlockTw setMinimumTrackImage:stetchLeftTrack forState:UIControlStateNormal];
        [slideToUnlockTw setMaximumTrackImage:stetchRightTrack forState:UIControlStateNormal];
        
        
        
        [slideToUnlockGp setThumbImage: [UIImage imageNamed:@"SlideToStop.png"] forState:UIControlStateNormal];
        [slideToUnlockGp setMinimumTrackImage:stetchLeftTrack forState:UIControlStateNormal];
        [slideToUnlockGp setMaximumTrackImage:stetchRightTrack forState:UIControlStateNormal];
        
        
        [slideToUnlockLi setThumbImage: [UIImage imageNamed:@"SlideToStop.png"] forState:UIControlStateNormal];
        [slideToUnlockLi setMinimumTrackImage:stetchLeftTrack forState:UIControlStateNormal];
        [slideToUnlockLi setMaximumTrackImage:stetchRightTrack forState:UIControlStateNormal];
        
        
        
        
        //slideToUnlock.value ==1.0;
        /*
        [GPPSignInButton class];
        
        GPPSignIn *signIn = [GPPSignIn sharedInstance];
        userinfoEmailScope_.on =
        signIn.shouldFetchGoogleUserEmail;
        
        
        [GPPSignIn sharedInstance].shouldFetchGoogleUserEmail =
        userinfoEmailScope_.on;
        
        
        // Set up sign-out and disconnect buttons.
        [self setUpButton:signOutButton_];
        [self setUpButton:disconnectButton_];
        
        // Set up sample view of Google+ sign-in.
        // The client ID has been set in the app delegate.
        signIn.delegate = self;
        signIn.shouldFetchGoogleUserEmail = userinfoEmailScope_.on;
        signIn.scopes==[NSArray arrayWithObjects:
                        @"https://www.googleapis.com/auth/userinfo.profile",
                        @"https://www.googleapis.com/auth/userinfo.email",
                        @"http://www.google.com/m8/feeds",
                        nil];
        signIn.actions = [NSArray arrayWithObjects:
                          @"http://schemas.google.com/AddActivity",
                          @"http://schemas.google.com/BuyActivity",
                          @"http://schemas.google.com/CheckInActivity",
                          @"http://schemas.google.com/CommentActivity",
                          @"http://schemas.google.com/CreateActivity",
                          @"http://schemas.google.com/ListenActivity",
                          @"http://schemas.google.com/ReserveActivity",
                          @"http://schemas.google.com/ReviewActivity",
                          nil];
        
        
        //    https://www.googleapis.com/auth/userinfo.profile','http://www.google.com/m8/feeds','https://www.googleapis.com/auth/userinfo.email'));
        
        [self reportAuthStatus];
        [signIn trySilentAuthentication];
         */
        
        pageControlBeingUsed = NO;
        
        
        _fbImage.hidden=NO;
        _MEIMAGE.hidden=NO;
        _twNameLabel.hidden=YES;
        _liNameLabel.hidden=YES;
        _gpNameLabel.hidden=YES;
        _fbNameLabel.hidden=YES;
        _MeLabel.hidden=YES;
        
        
        _fbImage.layer.cornerRadius = 5.0;
        _fbImage.clipsToBounds = YES;
        
        
        _twitterImage.layer.cornerRadius = 5.0;
        _twitterImage.clipsToBounds = YES;
        
        _gpImage.layer.cornerRadius = 5.0;
        _gpImage.clipsToBounds = YES;
        
        _linkdinImage.layer.cornerRadius = 5.0;
        _linkdinImage.clipsToBounds = YES;
        
        
        
        // [self updateView];
        
        
        
        
        
        
        
        
        
        NSUserDefaults *add_values=[NSUserDefaults standardUserDefaults];
        
       
        
        
        fbNameStr=[add_values objectForKey:@"FB_NAME"];
        twitterNameStr=[add_values objectForKey:@"TW_NAME"];
        gpNameStr=[add_values objectForKey:@"GP_NAME"];
        liNameStr=[add_values objectForKey:@"LI_NAME"];
        fbImageStr=[add_values objectForKey:@"FB_IMAGE"];
        twitterImageStr=[add_values objectForKey:@"TW_IMAGE"];
        liImageStr=[add_values objectForKey:@"LI_IMAGE"];
        gpImageStr=[add_values objectForKey:@"GP_IMAGE"];
        
       
        
        NSString *liPublic=[add_values objectForKey:@"LI_PUBLIC"];
         NSString *googlePublic=[add_values objectForKey:@"GP_PUBLIC"];
        
        
        _fbNameLabel.text=fbNameStr;
        _MeLabel.text=fbNameStr;
        _twNameLabel.text=twitterNameStr;
        _gpNameLabel.text=gpNameStr;
        _liNameLabel.text=liNameStr;
        _fbNameLabel.hidden=NO;
        _MeLabel.hidden=NO;
        
        
        [slideToUnlock setValue:1.0 animated:YES];
        slideToUnlock.hidden = NO;
        
        lockButton.hidden = NO;
        Container.hidden = NO;
        myLabel.hidden = YES;
        UNLOCKED = YES;
        // slideToUnlock.enabled=NO;
        
        [slideToUnlock setThumbImage: [UIImage imageNamed:@"connected.png"] forState:UIControlStateNormal];
        
        
        [ _fbImage setImageWithURL:[NSURL URLWithString:fbImageStr]
                  placeholderImage:[UIImage imageNamed:@"iconDefault.png"]];
        
        
       
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        
         NSString *documentsDirectory = [paths objectAtIndex:0];
        
        NSString *imageURL = profileImageStr;
        
        if ([imageURL isEqualToString:@"NULL"]) {
            
            imageURL=fbImageStr;
        }

        
        
        NSURL *url=[NSURL URLWithString:imageURL];
        
        
        ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
        
        
        [request setShouldContinueWhenAppEntersBackground:YES];
        [request setTimeOutSeconds:180];
        
        [request setCompletionBlock:^{
            NSString *responseString = [request responseString];
            
            NSData *responseData = [request responseData];
            
            NSString *epub_name=[NSString stringWithFormat:@"profileImage.jpg"];
            
            
            NSString *filePath = [documentsDirectory stringByAppendingPathComponent:epub_name];
            
            [responseData writeToFile:filePath atomically:YES];
            
            //(@"Dowload Completes");
            
            
            
            
        }];
        [request setFailedBlock:^
         {
             NSError *error = [request error];
             
             
             //(@"Error");
             
             
         }];
        [request startAsynchronous];

        
        
        
        NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:@"profileImage.jpg"];
        UIImage *profileIMG = [UIImage imageWithContentsOfFile:getImagePath];
        
        
        NSString *imageURL1 = profileImageStr;
        
        if ([imageURL isEqualToString:@"NULL"]) {
            
            imageURL=fbImageStr;
        }

        
        
//        NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
//        UIImage *image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imageURL]]];
//        
//        NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"profileImage.jpg"]];
//        if(image != NULL)
//        {
//            //Store the image in Document
//            
//            
//            NSData *imageData = UIImagePNGRepresentation(image);
//            [imageData writeToFile: imagePath atomically:YES];
//        }
        
        
    
        
        [ _MEIMAGE setImageWithURL:[NSURL URLWithString:imageURL1]
                  placeholderImage:[UIImage imageNamed:@"iconDefault.png"]];
        
      
        
        
        _fbHide.hidden=NO;
        
        
        NSUserDefaults *connect=[NSUserDefaults standardUserDefaults];
        [connect setObject:@"YES" forKey:@"FACEBOOK_CONNECT"];
        [connect setObject:@"NO" forKey:@"LINKDIN_CONNECT"];
        [connect setObject:@"NO" forKey:@"TWITTER_CONNECT"];
         [connect setObject:@"NO" forKey:@"GOOGLE_CONNECT"];
        
        //[_meButton11 addSubview:_fbImage];
        
        [_meButton11 setImage:profileIMG forState:UIControlStateNormal];
        
        
        
        
        if (![twitterNameStr isEqualToString:@""])
        {
            
            NSUserDefaults *connect=[NSUserDefaults standardUserDefaults];
            [connect setObject:@"YES" forKey:@"TWITTER_CONNECT"];
            
            _twNotLabel.hidden=YES;
            // _clickTwitterBtn.hidden=YES;
            
            _twNameLabel.hidden=NO;
            
            _twHide.hidden=NO;
            
            
            [slideToUnlockTw setThumbImage: [UIImage imageNamed:@"connected.png"] forState:UIControlStateNormal];
            
            
            [slideToUnlockTw setValue:1.0 animated:YES];
            slideToUnlockTw.hidden = NO;
            
			lockButtonTw.hidden = NO;
			ContainerTw.hidden = NO;
			myLabelTw.hidden = YES;
			UNLOCKEDTW = YES;
            //slideToUnlockTw.enabled=NO;
            
            
            
            [_clickTwitterBtn setTitle:@"Referesh" forState:UIControlStateNormal];
            
            // [self getTwitterScore:nil];
        }
        else
        {
            UNLOCKEDTW = NO;
        }
        
        [ _twitterImage setImageWithURL:[NSURL URLWithString:twitterImageStr]
                       placeholderImage:[UIImage imageNamed:@"iconDefault.png"]];
        
        if (![liPublic isEqualToString:@""])
        {
            
            NSUserDefaults *connect=[NSUserDefaults standardUserDefaults];
           
            [connect setObject:@"YES" forKey:@"LINKDIN_CONNECT"];

            
            _liHide.hidden=NO;
            
            [slideToUnlockLi setValue:1.0 animated:YES];
            slideToUnlockLi.hidden = NO;
            
			lockButtonLi.hidden = NO;
			ContainerLi.hidden = NO;
			myLabelLi.hidden = YES;
			UNLOCKEDLI = YES;
            
            [slideToUnlockLi setThumbImage: [UIImage imageNamed:@"connected.png"] forState:UIControlStateNormal];
            
            _imageBackLi.hidden=NO;
            _imageLogoLi.hidden=NO;
            
            // slideToUnlockLi.enabled=NO;
            
            _liNotLabel.hidden=YES;
            // _clickTwitterBtn.hidden=YES;
            
            
            _liNameLabel.hidden=NO;
            
            [_clickLIBtn setTitle:@"Referesh" forState:UIControlStateNormal];
            
            
        }
        else
        {
            NSString *deviceStr = [UIDevice currentDevice].model;
            ////(@"device:%@",deviceStr);
            
            UNLOCKEDLI = NO;
            if ([deviceStr isEqualToString:@"iPod touch"])
            {
                _imageBackLi.hidden=YES;
                _imageLogoLi.hidden=YES;
                
                
                
                _linkdinImage.hidden=YES;
                _liNotLabel.hidden=NO;
                _liHide.hidden=YES;
                slideToUnlockLi.hidden=YES;
                lockButtonLi.hidden = YES;
                ContainerLi.hidden = YES;
                myLabelLi.hidden = YES;
                
            }
        }
        
        
        [ _linkdinImage setImageWithURL:[NSURL URLWithString:liImageStr]
                       placeholderImage:[UIImage imageNamed:@"iconDefault.png"]];
        
        if (![googlePublic isEqualToString:@""])
        {
            
            NSUserDefaults *connect=[NSUserDefaults standardUserDefaults];
           
            [connect setObject:@"YES" forKey:@"GOOGLE_CONNECT"];
            
            _gpHide.hidden=NO;
            [slideToUnlockGp setThumbImage: [UIImage imageNamed:@"connected.png"] forState:UIControlStateNormal];
            [slideToUnlockGp setValue:1.0 animated:YES];
            slideToUnlockGp.hidden = NO;
            
			lockButtonGp.hidden = NO;
			ContainerGp.hidden = NO;
			myLabelGp.hidden = YES;
			UNLOCKEDGP = YES;
            // slideToUnlockGp.enabled=NO;
            
            _imageBackGP.hidden=NO;
            _imageLogoGP.hidden=NO;
            
            _gpNotLabel.hidden=YES;
            // _clickTwitterBtn.hidden=YES;
            
            
            _gpNameLabel.hidden=NO;
            
            [_clickGPBtn setTitle:@"Referesh" forState:UIControlStateNormal];
           
            [ _gpImage setImageWithURL:[NSURL URLWithString:gpImageStr]
                      placeholderImage:[UIImage imageNamed:@"iconDefault.png"]];
            
        }
        
        
        else
        {
            _imageBackGP.hidden=YES;
            _imageLogoGP.hidden=YES;
            _gpImage.hidden=YES;
            _gpNotLabel.hidden=NO;
            _gpHide.hidden=YES;
            slideToUnlockGp.hidden=YES;
            lockButtonGp.hidden = YES;
			ContainerGp.hidden = YES;
            myLabelGp.hidden = YES;
            
            _gpNameLabel.hidden=YES;
        }
        
       
        
        
        NSString *platinumScore=[add_values objectForKey:@"PLATINUM_SCORE"];
        NSString *goldScore=[add_values objectForKey:@"GOLD_SCORE"];
        influenceLevelStr=[add_values objectForKey:@"INFLUENCE_LEVEL"];
         NSString *total_scoresStr=[add_values objectForKey:@"TOTAL_SCORE"];
        
        
        total_scores=[total_scoresStr intValue];
        
        
        
        if ([influenceLevelStr isEqualToString:@"SILVER"])
        {
            int plat=[platinumScore intValue]-total_scores;
            
            ////(@"PLAT:%d",plat);
            
            
            int gold=[goldScore intValue]-total_scores;
            
            ////(@"gold:%d",gold);
            
            _statusImageView.image=[UIImage imageNamed:@"leval_points.png"];
            
            _goldLabel.hidden=NO;
            _platinumLabel.hidden=NO;
            _addImage.hidden=NO;
            
            
            _userStatusLabel.hidden=NO;
            _totalScoreLabel.hidden=NO;
            _statusImageView.hidden=NO;
            _goldLabel.hidden=NO;
            _platinumLabel.hidden=NO;
            
            
            _userStatusLabel.frame=CGRectMake(100, 56, 123, 27);
            _totalScoreLabel.frame=CGRectMake(100, 65, 112, 63);
            
            NSNumberFormatter *numberFormatter1 = [[NSNumberFormatter alloc]init];
            NSNumberFormatter *formatter1 = [[NSNumberFormatter alloc] init] ;
            [formatter1 setFormatterBehavior:NSNumberFormatterBehavior10_4];
            [formatter1 setNumberStyle:NSNumberFormatterDecimalStyle];
            [formatter1 setGroupingSeparator:@""];
            [formatter1 setDecimalSeparator:@"."];
            
            // Decimal values read from any db are always written with no grouping separator and a comma for decimal.
            
            NSNumber *numberFromString1 = [formatter1 numberFromString:[NSString stringWithFormat:@"%d",plat]];
            
            [formatter1 setGroupingSeparator:@","]; // Whatever you want here
            [formatter1 setDecimalSeparator:@","]; // Whatever you want here
            
            NSString *finalValue1 = [formatter1 stringFromNumber:numberFromString1];
            
            ////(@"%@",finalValue1); // Print 12 000,54

            
            
            _platinumLabel.text=[NSString stringWithFormat:@"+%@ Platinum",finalValue1];
            
            
            NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc]init];
            NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init] ;
            [formatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
            [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
            [formatter setGroupingSeparator:@""];
            [formatter setDecimalSeparator:@"."];
            
            // Decimal values read from any db are always written with no grouping separator and a comma for decimal.
            
            NSNumber *numberFromString = [formatter numberFromString:[NSString stringWithFormat:@"%d",gold]];
            
            [formatter setGroupingSeparator:@","]; // Whatever you want here
            [formatter setDecimalSeparator:@","]; // Whatever you want here
            
            NSString *finalValue = [formatter stringFromNumber:numberFromString];
            
            ////(@"%@",finalValue); // Print 12 000,54
            
            _goldLabel.text=[NSString stringWithFormat:@"+%@ Gold",finalValue];

            
            _userStatusLabel.text=@"SILVER";
            
        }
        else if ([influenceLevelStr isEqualToString:@"GOLD"])
        {
            int plat=[platinumScore intValue]-total_scores;
            
            ////(@"PLAT:%d",plat);
            _statusImageView.image=[UIImage imageNamed:@"platinam.png"];
            _goldLabel.hidden=YES;
            _platinumLabel.hidden=NO;
            
            _userStatusLabel.hidden=NO;
            _totalScoreLabel.hidden=NO;
            _statusImageView.hidden=NO;
            _addImage.hidden=NO;
            
            _platinumLabel.frame=CGRectMake(170, 146, 88, 29);
            
            NSNumberFormatter *numberFormatter1 = [[NSNumberFormatter alloc]init];
            NSNumberFormatter *formatter1 = [[NSNumberFormatter alloc] init] ;
            [formatter1 setFormatterBehavior:NSNumberFormatterBehavior10_4];
            [formatter1 setNumberStyle:NSNumberFormatterDecimalStyle];
            [formatter1 setGroupingSeparator:@""];
            [formatter1 setDecimalSeparator:@"."];
            
            // Decimal values read from any db are always written with no grouping separator and a comma for decimal.
            
            NSNumber *numberFromString1 = [formatter1 numberFromString:[NSString stringWithFormat:@"%d",plat]];
            
            [formatter1 setGroupingSeparator:@","]; // Whatever you want here
            [formatter1 setDecimalSeparator:@","]; // Whatever you want here
            
            NSString *finalValue1 = [formatter1 stringFromNumber:numberFromString1];
            
            ////(@"%@",finalValue1); // Print 12 000,54

            _platinumLabel.text=[NSString stringWithFormat:@"+%@ Platinum",finalValue1];
            
            _userStatusLabel.text=@"GOLD";
        }
        else if ([influenceLevelStr isEqualToString:@"PLATINUM"])
        {
            _userStatusLabel.text=@"PLATINUM";
            
            _statusImageView.hidden=YES;
            
            _platinumImageView.hidden=NO;
            
            _goldLabel.hidden=YES;
            _platinumLabel.hidden=YES;
            
            _userStatusLabel.hidden=NO;
            _totalScoreLabel.hidden=NO;
            
            _userStatusLabel.frame=CGRectMake(115, 96, 123, 27);
            _totalScoreLabel.frame=CGRectMake(115, 105, 112, 63);
            
            
            
            
            _statusImageView.hidden=YES;
            _goldLabel.hidden=YES;
            _platinumLabel.hidden=YES;
            
            // _userStatusLabel.text=userstatusStr;
            
        }
        
        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc]init];
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init] ;
        [formatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        [formatter setGroupingSeparator:@""];
        [formatter setDecimalSeparator:@"."];
        
        // Decimal values read from any db are always written with no grouping separator and a comma for decimal.
        
        NSNumber *numberFromString = [formatter numberFromString:totalscoreStr];
        
        [formatter setGroupingSeparator:@","]; // Whatever you want here
        [formatter setDecimalSeparator:@","]; // Whatever you want here
        
        NSString *finalValue = [formatter stringFromNumber:numberFromString];
        
        ////(@"%@",finalValue); // Print 12 000,54
        
        if ([finalValue length]==0) {
         finalValue=@"";
        }
        
        _totalScoreLabel.text=[NSString stringWithFormat:@"%@",finalValue];
        
         [self netCoinsButton];
        
        [self getFBScore];
        
        
        
    }
    

    
       
    
  
    
    
    
    // Do any additional setup after loading the view from its nib.
}

-(void)getFBScore
{
    
 
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/marketplace/getFBScore",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post1 = [ASIFormDataRequest requestWithURL:url];
    
   
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    
    //[request_post setValidatesSecureCertificate:NO];
    [request_post1 setPostValue:user forKey:@"userID"];
    
     [request_post1 setPostValue:[Config get_token] forKey:@"apikey"];
    
    
   // [request_post setTimeOutSeconds:30];
    
    
    [request_post1 setCompletionBlock:^{
        NSString *responseString = [request_post1 responseString];
        NSMutableData *results1 = [responseString JSONValue];
        
        
        ////(@"ABCC:%@",responseString);
        
        
        NSString *fb_score_str=[results1 valueForKeyPath:@"response.facebookScore"];
        
        ////(@"FB:%@",fb_score_str);
        
        facebook_score=[fb_score_str intValue];
        
         [self getTwitterScore];
                
        
    }];
    [request_post1 setFailedBlock:^{
        NSError *error = [request_post1 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1 startAsynchronous];
}

-(void)getTwitterScore
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/marketplace/getTwitterScore",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post1 = [ASIFormDataRequest requestWithURL:url];
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    
    //[request_post setValidatesSecureCertificate:NO];
    [request_post1 setPostValue:user forKey:@"userID"];
     [request_post1 setPostValue:[Config get_token] forKey:@"apikey"];
    
    
    // [request_post setTimeOutSeconds:30];
    
    
    [request_post1 setCompletionBlock:^{
        NSString *responseString = [request_post1 responseString];
        NSMutableData *results1 = [responseString JSONValue];
        
        
        ////(@"ABCC:%@",responseString);
        
        
        NSString *twit_score_str=[results1 valueForKeyPath:@"response.twitterScore"];
        
        ////(@"twit_score_str:%@",twit_score_str);
        
        twitter_score=[twit_score_str intValue];
        
        
        ////(@"FBS:%d",facebook_score);
         ////(@"TWSS:%d",twitter_score);
        
        [self LinkdinScore];
        
        
    }];
    [request_post1 setFailedBlock:^{
        NSError *error = [request_post1 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1 startAsynchronous];
}
-(void)LinkdinScore
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/marketplace/getLinkedInScore",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post1 = [ASIFormDataRequest requestWithURL:url];
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    
    //[request_post setValidatesSecureCertificate:NO];
    [request_post1 setPostValue:user forKey:@"userID"];
     [request_post1 setPostValue:[Config get_token] forKey:@"apikey"];
    
    
    // [request_post setTimeOutSeconds:30];
    
    
    [request_post1 setCompletionBlock:^{
        NSString *responseString = [request_post1 responseString];
        NSMutableData *results1 = [responseString JSONValue];
        
        
        ////(@"ABCC:%@",responseString);
        
        
        NSString *twit_score_str=[results1 valueForKeyPath:@"response.linkedInScore"];
        
        ////(@"twit_score_str:%@",twit_score_str);
        
        linkdin_score=[twit_score_str intValue];
        
        
        ////(@"FBS:%d",facebook_score);
        ////(@"TWSS:%d",twitter_score);
        ////(@"LINKDIN:%d",linkdin_score);
        
        [self googleScore];
        
        
    }];
    [request_post1 setFailedBlock:^{
        NSError *error = [request_post1 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1 startAsynchronous];
}
-(void)googleScore
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/marketplace/getGoogleScore",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post1 = [ASIFormDataRequest requestWithURL:url];
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    
    //[request_post setValidatesSecureCertificate:NO];
    [request_post1 setPostValue:user forKey:@"userID"];
     [request_post1 setPostValue:[Config get_token] forKey:@"apikey"];
    
    
    // [request_post setTimeOutSeconds:30];
    
    
    [request_post1 setCompletionBlock:^{
        NSString *responseString = [request_post1 responseString];
        NSMutableData *results1 = [responseString JSONValue];
        
        
        ////(@"ABCC:%@",responseString);
        
        
        NSString *twit_score_str=[results1 valueForKeyPath:@"response.googleScore"];
        
        ////(@"twit_score_str:%@",twit_score_str);
        
        googleplus_score=[twit_score_str intValue];
        
        
        ////(@"FBS:%d",facebook_score);
        ////(@"TWSS:%d",twitter_score);
        ////(@"LINKDIN:%d",linkdin_score);
         ////(@"LINKDIN:%d",googleplus_score);
        
        
        total_scores=facebook_score+twitter_score+linkdin_score+googleplus_score;
        
        [self getInfluenceLevel:total_scores];
        
        
    }];
    [request_post1 setFailedBlock:^{
        NSError *error = [request_post1 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1 startAsynchronous];
}
-(void)getInfluenceLevel:(int)totalScore
{
    
   
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/marketplace/getInfluenceLevel",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post1 = [ASIFormDataRequest requestWithURL:url];
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    
    ////(@"USER:%@",user);
    
    NSString *total=[NSString stringWithFormat:@"%d",totalScore];
    
    ////(@"TOTAL:%@",total);
    
    //[request_post setValidatesSecureCertificate:NO];
    [request_post1 setPostValue:user forKey:@"userID"];
     [request_post1 setPostValue:total forKey:@"total_score"];
     [request_post1 setPostValue:[Config get_token] forKey:@"apikey"];
    
    
    // [request_post setTimeOutSeconds:30];
    
    
    [request_post1 setCompletionBlock:^{
        NSString *responseString = [request_post1 responseString];
        NSMutableData *results1 = [responseString JSONValue];
        
        
        ////(@"influenceLevelStr:%@",responseString);
        
        
        influenceLevelStr=[results1 valueForKeyPath:@"response.userInfluence"];
        
        
       // influenceLevelStr=[results1 valueForKeyPath:@""];
        
        /*
        NSString *twit_score_str=[results1 valueForKeyPath:@"response.googleScore"];
        
        ////(@"twit_score_str:%@",twit_score_str);
        
        googleplus_score=[twit_score_str intValue];
        
        
        ////(@"FBS:%d",facebook_score);
        ////(@"TWSS:%d",twitter_score);
        ////(@"LINKDIN:%d",linkdin_score);
        ////(@"LINKDIN:%d",googleplus_score);
        
        //[self googleScore];
         */
     
     [self updateView];
     
        
    }];
    [request_post1 setFailedBlock:^{
        NSError *error = [request_post1 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1 startAsynchronous];
}
#pragma mark - GPPSignInDelegate

- (void)finishedWithAuth:(GTMOAuth2Authentication *)auth
                   error:(NSError *)error {
    if (error) {
        signInAuthStatus_.text =
        [NSString stringWithFormat:@"Status: Authentication error: %@", error];
        return;
    }
    [self reportAuthStatus];
}

- (void)didDisconnectWithError:(NSError *)error {
    if (error) {
        signInAuthStatus_.text =
        [NSString stringWithFormat:@"Status: Failed to disconnect: %@", error];
    } else {
        signInAuthStatus_.text =
        [NSString stringWithFormat:@"Status: Disconnected"];
        signInDisplayName_.text = @"";
        [self enableSignInSettings:YES];
    }
}

#pragma mark - Helper methods

- (void)setUpButton:(UIButton *)button {
    [[button layer] setCornerRadius:5];
    [[button layer] setMasksToBounds:YES];
    CGColorRef borderColor = [[UIColor colorWithWhite:203.0/255.0
                                                alpha:1.0] CGColor];
    [[button layer] setBorderColor:borderColor];
    [[button layer] setBorderWidth:1.0];
}

- (void)enableSignInSettings:(BOOL)enable {
    userinfoEmailScope_.enabled = YES;
}

- (void)reportAuthStatus {
    if ([GPPSignIn sharedInstance].authentication) {
        signInAuthStatus_.text = @"Status: Authenticated";
        [self retrieveUserInfo];
        [self enableSignInSettings:NO];
    } else {
        // To authenticate, use Google+ sign-in button.
        signInAuthStatus_.text = @"Status: Not authenticated";
        [self enableSignInSettings:YES];
    }
}

- (void)retrieveUserInfo
{
    signInDisplayName_.text = [NSString stringWithFormat:@"Email: %@",
                               [GPPSignIn sharedInstance].authentication.userEmail];
    
//    
//    ////(@"EMAIL:%@",[NSString stringWithFormat:@"%@",
//                          [GPPSignIn sharedInstance].authentication.userEmail]);
//    
//    ////(@"TOKEN KEY:%@",[NSString stringWithFormat:@"%@",
//                           [GPPSignIn sharedInstance].authentication.accessToken]);
//    
//    ////(@"TOKEN KEY:%@",[NSString stringWithFormat:@"%@",
//                           [GPPSignIn sharedInstance].authentication.parameters]);
    
    NSMutableDictionary *get_Parameters=[[NSMutableDictionary alloc]init];
    
    get_Parameters=[GPPSignIn sharedInstance].authentication.parameters;
    
    [self getResults:get_Parameters];
    
}
-(void)getResults:(NSMutableDictionary *)dict

{
    NSString *token=[dict objectForKey:@"access_token"] ;
    
    ////(@"Access Token:%@",token);
    
    
    SBJSON *parser = [[SBJSON alloc] init];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=%@",token]]];
    
    
    NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
    
    ////(@"json_string:%@",json_string);
    
    NSDictionary *statuses = [parser objectWithString:json_string error:nil];
    
    NSString *img_url=[statuses objectForKey:@"picture"] ;
    
    
    
    
   // img_view.image=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:img_url]]];
    
    //signInDisplayName_.text=[NSString stringWithFormat:@"%@",statuses];
    
}
#pragma mark - IBActions

- (IBAction)signOut:(id)sender {
    [[GPPSignIn sharedInstance] signOut];
    
    [self reportAuthStatus];
    signInDisplayName_.text = @"";
}

- (IBAction)disconnect:(id)sender {
    [[GPPSignIn sharedInstance] disconnect];
}

- (IBAction)userinfoEmailScopeToggle:(id)sender {
    [GPPSignIn sharedInstance].shouldFetchGoogleUserEmail =
    userinfoEmailScope_.on;
}


-(void)refreshInterfaceBasedOnSignIn
{
    if ([[GPPSignIn sharedInstance] authentication]) {
        // The user is signed in.
        self.signInButton.hidden = YES;
        // Perform other actions here, such as showing a sign-out button
    } else {
        self.signInButton.hidden = NO;
        // Perform other actions here
    }
}

-(void)updateView
{
    NSUserDefaults *add=[NSUserDefaults standardUserDefaults];
    
    
    userID=[add objectForKey:@"USERID"];
    fbuserID=[add objectForKey:@"FB_USERID"];
    fbAccessToken=[add objectForKey:@"FB_ACCESS_TOKEN"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTwitterAccounts) name:ACAccountStoreDidChangeNotification object:nil];
    ////(@"USER ID:%@",userID);
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/getUserDetails",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
    
    
    
    
    
    [request_post setValidatesSecureCertificate:NO];
    [request_post setPostValue:userID forKey:@"userid"];
     [request_post setPostValue:[Config get_token] forKey:@"apikey"];
    
    
    [request_post setTimeOutSeconds:30];
    
    
    [request_post setCompletionBlock:^{
        NSString *responseString = [request_post responseString];
        NSMutableData *results1 = [responseString JSONValue];
        
        
        //(@"RESULTS:%@",results1);
        
        NSArray *fbImgArr;
        NSArray *twitterImgArr;
        NSArray *gpImgArr;
        NSArray *liImgArr;
        
        NSArray *userStatusArr;
        NSArray *totalScoreArr;
        NSArray *LipubilcURLArr;
        
        NSArray *twNameArr;
        NSArray *fbFNameArr;
         NSArray *fbLNameArr;
        NSArray *gpNameArr;
        NSArray *liFNameArr;
        
         NSArray *googleURLArr;
        
          NSArray *profileURLArr;
        
        fbImgArr=[results1 valueForKeyPath:@"userDetails.fbsender_pic"];
        fbImageStr=fbImgArr[0];
        
        profileURLArr=[results1 valueForKeyPath:@"userDetails.profile_img"];
        profileImageStr=profileURLArr[0];
        
        @try
        {
            if ([profileImageStr isEqualToString:@"<null>"])
            {
                
                profileImageStr=fbImageStr;
                
            }
        }
        @catch (NSException *exception)
        {
             profileImageStr=fbImageStr;
            
           
        }
      
       
        
        twitterImgArr=[results1 valueForKeyPath:@"userDetails.twtprofileimage"];
        twitterImageStr=twitterImgArr[0];
        
        liImgArr=[results1 valueForKeyPath:@"userDetails.lnkprofileimage"];
        liImageStr=liImgArr[0];
        gpImgArr=[results1 valueForKeyPath:@"userDetails.gprofileimage"];
        gpImageStr=gpImgArr[0];
        googleURLArr=[results1 valueForKeyPath:@"userDetails.googleurl"];
      NSString *googleUrlStr=gpImgArr[0];
        
        userStatusArr=[results1 valueForKeyPath:@"userDetails.userstatus"];
        userstatusStr=userStatusArr[0];
        twitterImgArr=[results1 valueForKeyPath:@"userDetails.totalscore"];
        totalscoreStr=twitterImgArr[0];
        
        twNameArr=[results1 valueForKeyPath:@"userDetails.twitterurl"];
        twitterNameStr=twNameArr[0];
        fbFNameArr=[results1 valueForKeyPath:@"userDetails.firstname"];
         fbLNameArr=[results1 valueForKeyPath:@"userDetails.lastname"];
        
        
        liFNameArr=[results1 valueForKeyPath:@"userDetails.name"];
        liNameStr=liFNameArr[0];
        
        LipubilcURLArr=[results1 valueForKeyPath:@"userDetails.linkedinurl"];
       NSString *liPublicStr=LipubilcURLArr[0];
        
        fbNameStr=[NSString stringWithFormat:@"%@%@",fbFNameArr[0],fbLNameArr[0]];
        
        ////(@"FB IMAGE:%@",fbImageStr);
        
        gpNameStr=fbNameStr;
        
        _fbNameLabel.text=fbNameStr;
        _MeLabel.text=fbNameStr;
        _twNameLabel.text=twitterNameStr;
        _gpNameLabel.text=gpNameStr;
        _liNameLabel.text=liNameStr;
        _fbNameLabel.hidden=NO;
        _MeLabel.hidden=NO;
        
        
        @try {
           
            
            NSUserDefaults *add_values=[NSUserDefaults standardUserDefaults];
            
            [add_values setObject:fbNameStr forKey:@"FB_NAME"];
            [add_values setObject:twitterNameStr forKey:@"TW_NAME"];
            [add_values setObject:gpNameStr forKey:@"GP_NAME"];
            [add_values setObject:liNameStr forKey:@"LI_NAME"];
            [add_values setObject:fbImageStr forKey:@"FB_IMAGE"];
            [add_values setObject:twitterImageStr forKey:@"TW_IMAGE"];
            [add_values setObject:liImageStr forKey:@"LI_IMAGE"];
            [add_values setObject:gpImageStr forKey:@"GP_IMAGE"];
            [add_values setObject:liPublicStr forKey:@"LI_PUBLIC"];
            [add_values setObject:googleUrlStr forKey:@"GP_PUBLIC"];
        }
        @catch (NSException *exception) {
            
        }
      
     
        
       
        [slideToUnlock setValue:1.0 animated:YES];
        slideToUnlock.hidden = NO;
        
        lockButton.hidden = NO;
        Container.hidden = NO;
        myLabel.hidden = YES;
        UNLOCKED = YES;
       // slideToUnlock.enabled=NO;
        
        [slideToUnlock setThumbImage: [UIImage imageNamed:@"connected.png"] forState:UIControlStateNormal];
        
        
        [ _fbImage setImageWithURL:[NSURL URLWithString:fbImageStr]
                        placeholderImage:[UIImage imageNamed:@"iconDefault.png"]];
        
        
        
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        
        
        
        NSString *imageURL = profileImageStr;
        
        if ([imageURL isEqualToString:@"NULL"]) {
            
            imageURL=fbImageStr;
        }
        
        
         NSString *documentsDirectory = [paths objectAtIndex:0];
        
        NSURL *url=[NSURL URLWithString:imageURL];
        
        
        ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
        
        
        [request setShouldContinueWhenAppEntersBackground:YES];
        [request setTimeOutSeconds:180];
        
        [request setCompletionBlock:^{
            NSString *responseString = [request responseString];
            
            NSData *responseData = [request responseData];
            
            NSString *epub_name=[NSString stringWithFormat:@"profileImage.jpg"];
            
            
            NSString *filePath = [documentsDirectory stringByAppendingPathComponent:epub_name];
            
            [responseData writeToFile:filePath atomically:YES];
            
            //(@"Dowload Completes");
            
            
            
            
        }];
        [request setFailedBlock:^
         {
             NSError *error = [request error];
             
             
             //(@"Error");
             
             
         }];
        [request startAsynchronous];
        
        
        
        /*
        // Get documents folder
        UIImage *image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imageURL]]];
        
        NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"profileImage.jpg"]];
        if(image != NULL)
        {
            //Store the image in Document
            
            
            NSData *imageData = UIImagePNGRepresentation(image);
            [imageData writeToFile: imagePath atomically:YES];
        }
        */
        
        
        
        
        NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:@"profileImage.jpg"];
        UIImage *profileIMG = [UIImage imageWithContentsOfFile:getImagePath];
        
        
        NSString *imageURL1 = profileImageStr;
        
        if ([imageURL isEqualToString:@"NULL"]) {
            
            imageURL=fbImageStr;
        }
        
        
        [ _MEIMAGE setImageWithURL:[NSURL URLWithString:imageURL1]
                  placeholderImage:[UIImage imageNamed:@"iconDefault.png"]];
      
        
               
        
        _fbHide.hidden=NO;
       
        
        //[_meButton11 addSubview:_fbImage];
        
        [_meButton11 setImage:profileIMG forState:UIControlStateNormal];
        
        
        ////(@"HERE>>>.");
        
        
        NSUserDefaults *connect=[NSUserDefaults standardUserDefaults];
        [connect setObject:@"YES" forKey:@"FACEBOOK_CONNECT"];
        [connect setObject:@"NO" forKey:@"LINKDIN_CONNECT"];
        [connect setObject:@"NO" forKey:@"TWITTER_CONNECT"];
        [connect setObject:@"NO" forKey:@"GOOGLE_CONNECT"];
        
        
        ////(@"TW",twitterNameStr);
        ////(@"Li",liPublicStr);
        ////(@"GP",googleUrlStr);
       
        
        if (![twitterNameStr isEqualToString:@""])
        {
            NSUserDefaults *connect=[NSUserDefaults standardUserDefaults];
            [connect setObject:@"YES" forKey:@"TWITTER_CONNECT"];
            _twNotLabel.hidden=YES;
            // _clickTwitterBtn.hidden=YES;
            
            _twNameLabel.hidden=NO;
            
            _twHide.hidden=NO;
           
            
            [slideToUnlockTw setThumbImage: [UIImage imageNamed:@"connected.png"] forState:UIControlStateNormal];
            
            
            [slideToUnlockTw setValue:1.0 animated:YES];
            slideToUnlockTw.hidden = NO;
            
			lockButtonTw.hidden = NO;
			ContainerTw.hidden = NO;
			myLabelTw.hidden = YES;
			UNLOCKEDTW = YES;
            //slideToUnlockTw.enabled=NO;
             
            
            
            [_clickTwitterBtn setTitle:@"Referesh" forState:UIControlStateNormal];
            
            // [self getTwitterScore:nil];
        }
        else
        {
            UNLOCKEDTW = NO;
        }
        
        [ _twitterImage setImageWithURL:[NSURL URLWithString:twitterImageStr]
                  placeholderImage:[UIImage imageNamed:@"iconDefault.png"]];
        
        ////(@"TW C");
        @try
        {
            if (![liPublicStr isEqualToString:@""])
            {
                
                NSUserDefaults *connect=[NSUserDefaults standardUserDefaults];
                
                [connect setObject:@"YES" forKey:@"LINKDIN_CONNECT"];
                
                _liHide.hidden=NO;
                
                [slideToUnlockLi setValue:1.0 animated:YES];
                slideToUnlockLi.hidden = NO;
                
                lockButtonLi.hidden = NO;
                ContainerLi.hidden = NO;
                myLabelLi.hidden = YES;
                UNLOCKEDLI = YES;
                
                [slideToUnlockLi setThumbImage: [UIImage imageNamed:@"connected.png"] forState:UIControlStateNormal];
                
                // slideToUnlockLi.enabled=NO;
                
                
                _imageLogoLi.hidden=NO;
                _imageBackLi.hidden=NO;
                
                _liNotLabel.hidden=YES;
                // _clickTwitterBtn.hidden=YES;
                
                
                _liNameLabel.hidden=NO;
                
                [_clickLIBtn setTitle:@"Referesh" forState:UIControlStateNormal];
                
                
            }
            else
            {
                NSString *deviceStr = [UIDevice currentDevice].model;
                ////(@"device:%@",deviceStr);
                UNLOCKEDLI = NO;
                
                if ([deviceStr isEqualToString:@"iPod touch"])
                {
                    _imageBackLi.hidden=YES;
                    _imageLogoLi.hidden=YES;
                    
                    
                    
                    _linkdinImage.hidden=YES;
                    _liNotLabel.hidden=NO;
                    _liHide.hidden=YES;
                    slideToUnlockLi.hidden=YES;
                    lockButtonLi.hidden = YES;
                    ContainerLi.hidden = YES;
                    myLabelLi.hidden = YES;
                    
                }
            }
        }
        @catch (NSException *exception) {
            
        }
       
        
      

         ////(@"LI C");
        
        [ _linkdinImage setImageWithURL:[NSURL URLWithString:liImageStr]
                  placeholderImage:[UIImage imageNamed:@"iconDefault.png"]];
        
        
        @try {
            if (![googleUrlStr isEqualToString:@""])
            {
                
                
                NSUserDefaults *connect=[NSUserDefaults standardUserDefaults];
                
                [connect setObject:@"YES" forKey:@"GOOGLE_CONNECT"];
                
                _gpHide.hidden=NO;
                [slideToUnlockGp setThumbImage: [UIImage imageNamed:@"connected.png"] forState:UIControlStateNormal];
                [slideToUnlockGp setValue:1.0 animated:YES];
                slideToUnlockGp.hidden = NO;
                
                lockButtonGp.hidden = NO;
                ContainerGp.hidden = NO;
                myLabelGp.hidden = YES;
                UNLOCKEDGP = YES;
                // slideToUnlockGp.enabled=NO;
                
                
                _gpNotLabel.hidden=YES;
                // _clickTwitterBtn.hidden=YES;
                _imageBackGP.hidden=NO;
                _imageLogoGP.hidden=NO;
                
                _gpNameLabel.hidden=NO;
                
                [_clickGPBtn setTitle:@"Referesh" forState:UIControlStateNormal];
                
                [ _gpImage setImageWithURL:[NSURL URLWithString:gpImageStr]
                          placeholderImage:[UIImage imageNamed:@"iconDefault.png"]];
            }
            
            else
            {
                _imageBackGP.hidden=YES;
                _imageLogoGP.hidden=YES;
                _gpImage.hidden=YES;
                _gpNameLabel.hidden=YES;
                _gpNotLabel.hidden=NO;
                _gpHide.hidden=YES;
                slideToUnlockGp.hidden=YES;
                lockButtonGp.hidden = YES;
                ContainerGp.hidden = YES;
                myLabelGp.hidden = YES;
            }
        }
        @catch (NSException *exception)
        {
            _imageBackGP.hidden=YES;
            _imageLogoGP.hidden=YES;
            _gpImage.hidden=YES;
            _gpNameLabel.hidden=YES;
            _gpNotLabel.hidden=NO;
            _gpHide.hidden=YES;
            slideToUnlockGp.hidden=YES;
            lockButtonGp.hidden = YES;
            ContainerGp.hidden = YES;
            myLabelGp.hidden = YES;
        }
      
     
       
               
        _totalHideView.hidden=YES;
        [_activity stopAnimating];
         ////(@"ALL  C");
        
        [self getReachLevel];
        
    }];
    [request_post setFailedBlock:^{
        NSError *error = [request_post error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post startAsynchronous];
}


-(void)getReachLevel
{
           NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/marketplace/getReachLevel",CONFIG_BASE_URL]];
        
        __block  ASIFormDataRequest *request_post1 = [ASIFormDataRequest requestWithURL:url];
        
        
        NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
        NSString *user=[check objectForKey:@"USERID"];
        
        //[request_post setValidatesSecureCertificate:NO];
        [request_post1 setPostValue:user forKey:@"userID"];
        [request_post1 setPostValue:influenceLevelStr forKey:@"influence_level"];
         [request_post1 setPostValue:[Config get_token] forKey:@"apikey"];
        
        
        // [request_post setTimeOutSeconds:30];
        
        
        [request_post1 setCompletionBlock:^{
            NSString *responseString = [request_post1 responseString];
            NSMutableData *results1 = [responseString JSONValue];
            
            
            ////(@"influenceLevelStr:%@",responseString);
            
            
            NSString *platinumScore=[results1 valueForKeyPath:@"response.platinumScore"];
            
             NSString *goldScore=[results1 valueForKeyPath:@"response.goldScore"];
            
            
            
            
            NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
            
            [check setObject:influenceLevelStr forKey:@"INFLUENCE_LEVEL"];
             [check setObject:platinumScore forKey:@"PLATINUM_SCORE"];
             [check setObject:goldScore forKey:@"GOLD_SCORE"];
             [check setObject:[NSString stringWithFormat:@"%d",total_scores] forKey:@"TOTAL_SCORE"];
            
            
            
            
            if ([influenceLevelStr isEqualToString:@"SILVER"])
            {
                int plat=[platinumScore intValue]-total_scores;
                
                ////(@"PLAT:%d",plat);
                
                
                int gold=[goldScore intValue]-total_scores;
                
                ////(@"gold:%d",gold);
                
                _statusImageView.image=[UIImage imageNamed:@"leval_points.png"];
                
                _goldLabel.hidden=NO;
                _platinumLabel.hidden=NO;
                _addImage.hidden=NO;
                
                
                _userStatusLabel.hidden=NO;
                _totalScoreLabel.hidden=NO;
                _statusImageView.hidden=NO;
                _goldLabel.hidden=NO;
                _platinumLabel.hidden=NO;
                
                
                _userStatusLabel.frame=CGRectMake(100, 56, 123, 27);
                _totalScoreLabel.frame=CGRectMake(100, 65, 112, 63);
                
                NSNumberFormatter *numberFormatter1 = [[NSNumberFormatter alloc]init];
                NSNumberFormatter *formatter1 = [[NSNumberFormatter alloc] init] ;
                [formatter1 setFormatterBehavior:NSNumberFormatterBehavior10_4];
                [formatter1 setNumberStyle:NSNumberFormatterDecimalStyle];
                [formatter1 setGroupingSeparator:@""];
                [formatter1 setDecimalSeparator:@"."];
                
                // Decimal values read from any db are always written with no grouping separator and a comma for decimal.
                
                NSNumber *numberFromString1 = [formatter1 numberFromString:[NSString stringWithFormat:@"%d",plat]];
                
                [formatter1 setGroupingSeparator:@","]; // Whatever you want here
                [formatter1 setDecimalSeparator:@","]; // Whatever you want here
                
                NSString *finalValue1 = [formatter1 stringFromNumber:numberFromString1];
                
                ////(@"%@",finalValue1); // Print 12 000,54
                
                _platinumLabel.text=[NSString stringWithFormat:@"+%@ Platinum",finalValue1];
                
                
                
                
                
                
                NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc]init];
                NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init] ;
                [formatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
                [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
                [formatter setGroupingSeparator:@""];
                [formatter setDecimalSeparator:@"."];
                
                // Decimal values read from any db are always written with no grouping separator and a comma for decimal.
                
                NSNumber *numberFromString = [formatter numberFromString:[NSString stringWithFormat:@"%d",gold]];
                
                [formatter setGroupingSeparator:@","]; // Whatever you want here
                [formatter setDecimalSeparator:@","]; // Whatever you want here
                
                NSString *finalValue = [formatter stringFromNumber:numberFromString];
                
                ////(@"%@",finalValue); // Print 12 000,54
                
                 _goldLabel.text=[NSString stringWithFormat:@"+%@ Gold",finalValue];
                
                _userStatusLabel.text=@"SILVER";
                
            }
           else if ([influenceLevelStr isEqualToString:@"GOLD"])
            {
                int plat=[platinumScore intValue]-total_scores;
                
                ////(@"PLAT:%d",plat);
                 _statusImageView.image=[UIImage imageNamed:@"platinam.png"];
                _goldLabel.hidden=YES;
                _platinumLabel.hidden=NO;
                
                _userStatusLabel.hidden=NO;
                _totalScoreLabel.hidden=NO;
                _statusImageView.hidden=NO;
                _addImage.hidden=NO;
                
                _platinumLabel.frame=CGRectMake(170, 146, 88, 29);
                
                
                NSNumberFormatter *numberFormatter1 = [[NSNumberFormatter alloc]init];
                NSNumberFormatter *formatter1 = [[NSNumberFormatter alloc] init] ;
                [formatter1 setFormatterBehavior:NSNumberFormatterBehavior10_4];
                [formatter1 setNumberStyle:NSNumberFormatterDecimalStyle];
                [formatter1 setGroupingSeparator:@""];
                [formatter1 setDecimalSeparator:@"."];
                
                // Decimal values read from any db are always written with no grouping separator and a comma for decimal.
                
                NSNumber *numberFromString1 = [formatter1 numberFromString:[NSString stringWithFormat:@"%d",plat]];
                
                [formatter1 setGroupingSeparator:@","]; // Whatever you want here
                [formatter1 setDecimalSeparator:@","]; // Whatever you want here
                
                NSString *finalValue1 = [formatter1 stringFromNumber:numberFromString1];
                
                ////(@"%@",finalValue1); // Print 12 000,54
                
                _platinumLabel.text=[NSString stringWithFormat:@"+%@ Platinum",finalValue1];
                
                _userStatusLabel.text=@"GOLD";
            }
            else if ([influenceLevelStr isEqualToString:@"PLATINUM"])
            {
                 _userStatusLabel.text=@"PLATINUM";
                
                _statusImageView.hidden=YES;
                
                _platinumImageView.hidden=NO;
                
                _goldLabel.hidden=YES;
                _platinumLabel.hidden=YES;
                
                _userStatusLabel.hidden=NO;
                _totalScoreLabel.hidden=NO;
                
                _userStatusLabel.frame=CGRectMake(120, 96, 123, 27);
                _totalScoreLabel.frame=CGRectMake(120, 105, 112, 63);

                
                
                
                _statusImageView.hidden=YES;
                _goldLabel.hidden=YES;
                _platinumLabel.hidden=YES;
                
               // _userStatusLabel.text=userstatusStr;
               
            }
  
            NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc]init];
            NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init] ;
            [formatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
            [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
            [formatter setGroupingSeparator:@""];
            [formatter setDecimalSeparator:@"."];
            
            // Decimal values read from any db are always written with no grouping separator and a comma for decimal.
            
            NSNumber *numberFromString = [formatter numberFromString:totalscoreStr];
            
            [formatter setGroupingSeparator:@","]; // Whatever you want here
            [formatter setDecimalSeparator:@","]; // Whatever you want here
            
            NSString *finalValue = [formatter stringFromNumber:numberFromString];
            
            ////(@"%@",finalValue); // Print 12 000,54
            if ([finalValue length]==0) {
                finalValue=@"";
            }
            _totalScoreLabel.text=[NSString stringWithFormat:@"%@",finalValue];
            
           
            
          //  [self getInfluenceLevel:total_scores];
            
            
        }];
        [request_post1 setFailedBlock:^{
            NSError *error = [request_post1 error];
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                      cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            
            
            
        }];
        
        [request_post1 startAsynchronous];
    

}


-(IBAction)getTwitterScore:(id)sender
{
    
   
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/marketplace/getTwitterScore",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
    
    
    
    
    
    [request_post setValidatesSecureCertificate:NO];
    [request_post setPostValue:userID forKey:@"userid"];
     [request_post setPostValue:[Config get_token] forKey:@"apikey"];
    
    
    [request_post setTimeOutSeconds:30];
    
    
    [request_post setCompletionBlock:^{
        NSString *responseString = [request_post responseString];
        NSMutableData *results1 = [responseString JSONValue];
        
        
        ////(@"RESULTS KK:%@",results1);
        
        
        NSArray *twitterScoreArr;
        
               twitterScoreArr=[results1 valueForKeyPath:@"response.twitterScore"];
       NSString *twitterscoreStr=twitterScoreArr[0];
        
        
        
        ////(@"FB IMAGE 1:%@",twitterscoreStr);
        
    
        
        
        total_scores=[twitterscoreStr intValue]+[totalscoreStr intValue];
        
        
        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc]init];
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init] ;
        [formatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        [formatter setGroupingSeparator:@""];
        [formatter setDecimalSeparator:@"."];
        
        // Decimal values read from any db are always written with no grouping separator and a comma for decimal.
        
        NSNumber *numberFromString = [formatter numberFromString:totalscoreStr];
        
        [formatter setGroupingSeparator:@","]; // Whatever you want here
        [formatter setDecimalSeparator:@","]; // Whatever you want here
        
        NSString *finalValue = [formatter stringFromNumber:numberFromString];
        
        ////(@"%@",finalValue); // Print 12 000,54
        if ([finalValue length]==0) {
            finalValue=@"";
        }
        _totalScoreLabel.text=[NSString stringWithFormat:@"%@",finalValue];
        
        
        
        _userStatusLabel.text=userstatusStr;
        
    }];
    [request_post setFailedBlock:^{
        NSError *error = [request_post error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post startAsynchronous];

    
}

- (IBAction)tipsClick
{
    UIFont * titleFont = [UIFont fontWithName:@"System" size:20.0f];
    UIFont * messageFont = [UIFont fontWithName:@"System" size:12.0f];
    
    // Set default appearnce block for all WCAlertViews
    // Similar functionality to UIAppearnce Proxy
    
    [WCAlertView setDefaultCustomiaztonBlock:^(WCAlertView *alertView) {
        alertView.labelTextColor = [UIColor colorWithRed:0.11f green:0.08f blue:0.39f alpha:1.00f];
        alertView.labelShadowColor = [UIColor whiteColor];
        
        UIColor *topGradient = [UIColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:1.0f];
        UIColor *middleGradient = [UIColor colorWithRed:0.93f green:0.94f blue:0.96f alpha:1.0f];
        UIColor *bottomGradient = [UIColor colorWithRed:0.89f green:0.89f blue:0.92f alpha:1.00f];
        alertView.gradientColors = @[topGradient,middleGradient,bottomGradient];
        
        alertView.outerFrameColor = [UIColor colorWithRed:250.0f/255.0f green:250.0f/255.0f blue:250.0f/255.0f alpha:1.0f];
        
        alertView.buttonTextColor = [UIColor colorWithRed:0.11f green:0.08f blue:0.39f alpha:1.00f];
        alertView.buttonShadowColor = [UIColor whiteColor];
        
        alertView.titleFont = titleFont;
        alertView.messageFont = messageFont;
    }];
    
    
    
            
    [WCAlertView showAlertWithTitle:@"" message:@"* Integrate your Facebook, Twitter and LinkedIn accounts with Marketplace. Based on your friends count and activities, your influence score will improve.\n* Keep extending your network of friends on Facebook and connections on LinkedIn and move on to the next influence level.\n* Stay active and engaged on your social media profiles with the activities including but not limited to Likes, Comments, Shares and Retweets.     \n* Pull your contacts from Gmail account and gain more score, we assure that we won't send any mails to those imported contacts without your permission. \n* Don't forget to refresh the connected social media networks regularly" customizationBlock:^(WCAlertView *alertView) {
            
            // You can also set different appearance for this alert using customization block
            
            alertView.style = WCAlertViewStyleBlackHatched;
            //        alertView.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
        } completionBlock:^(NSUInteger buttonIndex, WCAlertView *alertView) {
            if (buttonIndex == alertView.cancelButtonIndex) {
                ////(@"Cancel");
            } else {
                ////(@"Ok");
               
            }
        } cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    
    _accountStore = [[ACAccountStore alloc] init];
    _apiManager = [[TWAPIManager alloc] init];
    
    [self refreshTwitterAccounts];
}

- (void)refreshTwitterAccounts
{
    //TWDLog(@"Refreshing Twitter Accounts \n");
    
    if (![TWAPIManager hasAppKeys]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ERROR_TITLE_MSG message:ERROR_NO_KEYS delegate:nil cancelButtonTitle:ERROR_OK otherButtonTitles:nil];
        [alert show];
    }
    else if (![TWAPIManager isLocalTwitterAccountAvailable]) {
        //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ERROR_TITLE_MSG message:ERROR_NO_ACCOUNTS delegate:nil cancelButtonTitle:ERROR_OK otherButtonTitles:nil];
        //        [alert show];
    }
    else {
        [self obtainAccessToAccountsWithBlock:^(BOOL granted) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (granted) {
                    _reverseAuthBtn.enabled = YES;
                }
                else {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ERROR_TITLE_MSG message:ERROR_PERM_ACCESS delegate:nil cancelButtonTitle:ERROR_OK otherButtonTitles:nil];
                    [alert show];
                    // TWALog(@"You were not granted access to the Twitter accounts.");
                }
            });
        }];
    }
}
- (void)obtainAccessToAccountsWithBlock:(void (^)(BOOL))block
{
    ACAccountType *twitterType = [_accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
    ACAccountStoreRequestAccessCompletionHandler handler = ^(BOOL granted, NSError *error) {
        if (granted) {
            self.accounts = [_accountStore accountsWithAccountType:twitterType];
        }
        
        block(granted);
    };
    
    //  This method changed in iOS6. If the new version isn't available, fall back to the original (which means that we're running on iOS5+).
    if ([_accountStore respondsToSelector:@selector(requestAccessToAccountsWithType:options:completion:)]) {
        [_accountStore requestAccessToAccountsWithType:twitterType options:nil completion:handler];
    }
    else {
        [_accountStore requestAccessToAccountsWithType:twitterType withCompletionHandler:handler];
    }
}


-(IBAction)clickTwitter:(id)sender
{
    
    if (![TWAPIManager isLocalTwitterAccountAvailable])
    {
        
        // user did not slide far enough, so return back to 0 position
        [UIView beginAnimations: @"SlideCanceled" context: nil];
        [UIView setAnimationDelegate: self];
        [UIView setAnimationDuration: 0.35];
        // use CurveEaseOut to create "spring" effect
        [UIView setAnimationCurve: UIViewAnimationCurveEaseOut];
        slideToUnlockTw.value = 0.0;
        
        [UIView commitAnimations];

        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ERROR_TITLE_MSG message:ERROR_NO_ACCOUNTS delegate:nil cancelButtonTitle:ERROR_OK otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        
        [_apiManager performReverseAuthForAccount:_accounts[0] withHandler:^(NSData *responseData, NSError *error) {
            if (responseData) {
                NSString *responseStr = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
                
                //TWDLog(@"Reverse Auth process returned: %@", responseStr);
                
                NSArray *parts = [responseStr componentsSeparatedByString:@"&"];
                
                NSString *oauth_token=parts[0];
                
                oauth_token = [oauth_token stringByReplacingOccurrencesOfString:@"oauth_token=" withString:@""];
                
                NSString *oauth_token_secret=parts[1];
                
                oauth_token_secret = [oauth_token_secret stringByReplacingOccurrencesOfString:@"oauth_token_secret=" withString:@""];
                
                NSString *user_id=parts[2];
                
                 user_id = [user_id stringByReplacingOccurrencesOfString:@"user_id=" withString:@""];
                
                NSString *screen_name=parts[3];
                
                screen_name = [screen_name stringByReplacingOccurrencesOfString:@"screen_name=" withString:@""];
                
                NSString *lined = [parts componentsJoinedByString:@"\n"];
                
                ////(@"LINED:%@|%@|%@|%@",oauth_token,oauth_token_secret,user_id,screen_name);
                
                
                NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/marketplace/authenticateTwitter",CONFIG_BASE_URL]];
                
                __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
                
                
                
                
                
                [request_post setValidatesSecureCertificate:NO];
                [request_post setPostValue:userID forKey:@"userID"];
                 [request_post setPostValue:oauth_token forKey:@"twt_access_token"];
                 [request_post setPostValue:oauth_token_secret forKey:@"twt_secret_token"];
                 [request_post setPostValue:user_id forKey:@"twt_id"];
                 [request_post setPostValue:screen_name forKey:@"twt_screen_name"];
                 [request_post setPostValue:[Config get_token] forKey:@"apikey"];
                
                
                [request_post setTimeOutSeconds:30];
                
                
                [request_post setCompletionBlock:^{
                    NSString *responseString = [request_post responseString];
                    NSMutableData *results1 = [responseString JSONValue];
                    
                    
                    ////(@"RESULTS:%@",results1);
                    
                    [self getFBScore];
                    
                    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/setExtraction",CONFIG_BASE_URL]];
                    
                    __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
                    
                    
                    ////(@"USER NE NE:%@",userID);
                    
                    
                    
                    [request_post setValidatesSecureCertificate:NO];
                    [request_post setPostValue:userID forKey:@"userID"];
                    [request_post setPostValue:@"twitter" forKey:@"network"];
                     [request_post setPostValue:[Config get_token] forKey:@"apikey"];
                    
                    
                    [request_post setTimeOutSeconds:30];
                    
                    
                    [request_post setCompletionBlock:^{
                        NSString *responseString = [request_post responseString];
                        NSMutableData *results1 = [responseString JSONValue];
                        
                        
                        ////(@"RESULTS ASAS:%@",results1);
                        
                        
                        
                        
                        
                        
                    }];
                    [request_post setFailedBlock:^{
                        NSError *error = [request_post error];
                        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        
                        
                        
                        
                    }];
                    
                    [request_post startAsynchronous];
                                       
                    
                }];
                [request_post setFailedBlock:^{
                    NSError *error = [request_post error];
                    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                        message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                              cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    
                    
                    
                    
                }];
                
                [request_post startAsynchronous];

                
                
                dispatch_async(dispatch_get_main_queue(), ^{
//                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success!" message:lined delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                    [alert show];
                });
            }
            else {
                // TWALog(@"Reverse Auth process failed. Error returned was: %@\n", [error localizedDescription]);
            }
        }];
    }
    
}


- (void)scrollViewDidScroll:(UIScrollView *)sender {
	if (!pageControlBeingUsed) {
		// Switch the indicator when more than 50% of the previous/next page is visible
		CGFloat pageWidth = self.scrollView.frame.size.width;
		int page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
		self.pageControl.currentPage = page;
	}
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
	pageControlBeingUsed = NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
	pageControlBeingUsed = NO;
}

- (IBAction)changePage {
	// Update the scroll view to the appropriate page
	CGRect frame;
	frame.origin.x = self.scrollView.frame.size.width * self.pageControl.currentPage;
	frame.origin.y = 0;
	frame.size = self.scrollView.frame.size;
	[self.scrollView scrollRectToVisible:frame animated:YES];
	
	// Keep track of when scrolls happen in response to the page control
	// value changing. If we don't do this, a noticeable "flashing" occurs
	// as the the scroll delegate will temporarily switch back the page
	// number.
	pageControlBeingUsed = YES;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)meButton
{
    if (IS_IPHONE_5)
    {
        MeViewController *viewController=[[MeViewController alloc]initWithNibName:@"MeViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            ////(@"SUCCESS");
        }
         ];
    }
    else
    {
        
        MeViewController *viewController=[[MeViewController alloc]initWithNibName:@"MeViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            ////(@"SUCCESS");
        }
         ];
    }

}
-(IBAction)dealsButton
{
    dealsViewController *viewController=[[dealsViewController alloc]initWithNibName:@"dealsViewController" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        ////(@"SUCCESS");
    }
     ];
}
-(IBAction)socialButton
{
    if (IS_IPHONE_5) {
        netCoinsViewController *viewController=[[netCoinsViewController alloc]initWithNibName:@"netCoinsViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            ////(@"SUCCESS");
        }
         ];
    }
    else
    {
        netCoinsViewController *viewController=[[netCoinsViewController alloc]initWithNibName:@"netCoinsViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            ////(@"SUCCESS");
        }
         ];
    }

}
-(IBAction)netCoinsButton
{
    if (IS_IPHONE_5) {
        socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            ////(@"SUCCESS");
        }
         ];
    }
    else
    {
        socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            ////(@"SUCCESS");
        }
         ];
    }

}
-(IBAction)myBusinessButton
{
    myBusinessViewController *viewController=[[myBusinessViewController alloc]initWithNibName:@"myBusinessViewController" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        ////(@"SUCCESS");
    }
     ];
}


-(IBAction)faceBookClick:(id)sender
{

    [self sen1];
    
}

-(void)sen1
{
    
    
    
    
    // NSString *client_id = @"147759358751025";
    
    
    NSString *client_id = @"139271596272054";
    
    self->fbGraph = [[FbGraph alloc] initWithFbClientID:client_id];
    
    [fbGraph authenticateUserWithCallbackObject:self andSelector:@selector(fbGraphCallback) andExtendedPermissions:@"user_photos,publish_stream,email,read_stream,user_birthday,friends_about_me,friends_likes,user_likes,user_photos,user_hometown,user_location,user_about_me,friends_birthday,friends_photos,friends_work_history,friends_location,user_subscriptions,publish_actions"];
    
    
    
}
-(void)fbGraphCallback
{
    
    
    NSUserDefaults *checkval=[NSUserDefaults standardUserDefaults];
    
    NSString *val=[checkval objectForKey:@"ACCESS_TOKEN"];
    
    
    ////(@"ACCESS:%@",val);
    
    if ([val isEqualToString:@"NULL"])
    {
        
    }
    else
    {

    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/authenticateUser",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
    
    
    
    
    
    [request_post setValidatesSecureCertificate:NO];
    [request_post setPostValue:val forKey:@"facebook_access_token"];
     [request_post setPostValue:[Config get_token] forKey:@"apikey"];
    
    
    [request_post setTimeOutSeconds:30];
    
    
    [request_post setCompletionBlock:^{
        NSString *responseString = [request_post responseString];
        NSMutableData *results1 = [responseString JSONValue];
        
        
        ////(@"RESULTS:%@",results1);
        
        NSString *fbUserID=[results1 valueForKeyPath:@"Response.facebook_user_id"];
        NSString *userID=[results1 valueForKeyPath:@"Response.userID"];
        NSString *facebookToken=[results1 valueForKeyPath:@"Response.facebook_access_token"];
        
        ////(@"FB USER:%@",fbUserID);
        ////(@"userID:%@",userID);
        
        
        NSUserDefaults *add=[NSUserDefaults standardUserDefaults];
        
        [add setObject:fbUserID forKey:@"FB_USERID"];
        [add setObject:userID forKey:@"USERID"];
        [add setObject:facebookToken forKey:@"FB_ACCESS_TOKEN"];
        
        
        if (results1) {
            if (IS_IPHONE_5) {
                MeViewController *viewController=[[MeViewController alloc]initWithNibName:@"MeViewController" bundle:nil];
                [self presentViewController:viewController animated:NO completion:^{
                    ////(@"SUCCESS");
                }
                 ];
            }
            else
            {
                
                MeViewController *viewController=[[MeViewController alloc]initWithNibName:@"MeViewController~iPhone" bundle:nil];
                [self presentViewController:viewController animated:NO completion:^{
                    ////(@"SUCCESS");
                }
                 ];
            }

        }
        
        
        
        
    }];
    [request_post setFailedBlock:^{
        NSError *error = [request_post error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post startAsynchronous];
    }

}

- (IBAction)closeButton
{
    qrView.hidden=YES;
}
-(IBAction)clickQRCODE:(id)sender
{
    qrView.hidden=NO;
    
    reader = [ZBarReaderViewController new];
    reader.readerDelegate = self;
    
    
    reader.wantsFullScreenLayout = NO;
    
    reader.showsZBarControls = NO;
    
    reader.supportedOrientationsMask = ZBarOrientationMaskAll;
	
    [reader.view setFrame:CGRectMake(0, 0, 320,540)];
    
    ZBarImageScanner *scanner = reader.scanner;
    // TODO: (optional) additional reader configuration here
	
    // EXAMPLE: disable rarely used I2/5 to improve performance
    [scanner setSymbology: ZBAR_I25
				   config: ZBAR_CFG_ENABLE
					   to: 0];
    
    // hideview.frame=CGRectMake(0, 420, 320, 40);
	
    // present and release the controller
    
    [self.scan_view addSubview:reader.view];
}
/*
- (void) imagePickerController: (UIImagePickerController*) reader
 didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    // ADD: get the decode results
    id<NSFastEnumeration> results =
	[info objectForKey: ZBarReaderControllerResults];
    ////(@"info");
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        break;
	
    ////(@"datas %@",symbol.data);
    
    NSString *result=[NSString stringWithFormat:@"%@",symbol.data];
    
    NSArray *arrString = [result componentsSeparatedByString:@" "];
    
    NSString *finalString=@"";
    
    for(int i=0; i<arrString.count;i++){
        if([[arrString objectAtIndex:i] rangeOfString:@"http"].location != NSNotFound)
            ////(@"ARRA:%@", [arrString objectAtIndex:i]);
        
        finalString=[arrString objectAtIndex:i];
    }
    
    
    
    
    
    
    if ([finalString rangeOfString:@"http"].location == NSNotFound)
    {
        ////(@"string does not contain bla");
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"This code not contains valid URL" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    else
    {
        ////(@"string contains bla!");
        
        qrView.hidden=YES;
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:finalString]];
        
         [reader.view removeFromSuperview];
    }
    
    
   // resultImage.image =
	[info objectForKey: UIImagePickerControllerOriginalImage];
	
    
  //  
    
    
   
    
}
 */

#ifdef IOS_OLDER_THAN_6
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return (toInterfaceOrientation == UIInterfaceOrientationMaskPortrait);
}
#endif
#ifdef IOS_NEWER_OR_EQUAL_TO_6
-(BOOL)shouldAutorotate {
    return YES;
}
- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
#endif

@end
