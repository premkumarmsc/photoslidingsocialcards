//
//  ViewController.h
//  PhotoSliding
//
//  Created by Neon Spark on 1/21/12.
//  Copyright (c) 2012 http://sugartin.info. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "FbGraph.h"
#import "GAITrackedViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import <AddressBook/AddressBook.h>
#import <Social/Social.h>



@interface ViewController : GAITrackedViewController <FBLoginViewDelegate,FBFriendPickerDelegate>
{
     FbGraph *fbGraph;
    UIButton *shareOnFacebook;
    UIButton *friendsList;
    NSArray *selectedFriends;
    UIButton *announceButton;
    UIActivityIndicatorView *activityIndicator;
}

- (void)setupScrollView:(UIScrollView*)scrMain ;


-(IBAction)faceBookClick:(id)sender;

@property(nonatomic,retain)IBOutlet UIImageView *imggold;
@property(nonatomic,retain)IBOutlet UIImageView *imgsilver;
@property(nonatomic,retain)IBOutlet UIImageView *imgplatinum;
@property (strong,nonatomic) IBOutlet UIButton *shareOnFacebook;
@property (strong,nonatomic) IBOutlet UIButton *friendsList;
@property (strong,nonatomic) IBOutlet UIButton *announceButton;
@property (strong,nonatomic) UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) id<FBGraphUser> loggedInUser;


-(IBAction)pickFriendsList:(UIButton *)sender;
-(IBAction)postStatusUpdate:(UIButton *)sender;

- (void)showAlert:(NSString *)message
           result:(id)result
            error:(NSError *)error;
@end
