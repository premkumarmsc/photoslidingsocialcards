//
//  MeViewController.h
//  PhotoSliding
//
//  Created by ephronsystems on 6/27/13.
//
//

#import <UIKit/UIKit.h>
#import <Twitter/Twitter.h>
#import "OAuth+Additions.h"
#import "TWAPIManager.h"
#import "TWSignedRequest.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import <GooglePlus/GooglePlus.h>
#import "OAuthLoginView.h"
#import "JSONKit.h"
#import "OAConsumer.h"
#import "OAMutableURLRequest.h"
#import "OADataFetcher.h"
#import "OATokenManager.h"
#import "AppDelegate.h"
#import "FbGraph.h"
#import <Foundation/Foundation.h>
#import "ZBarSDK.h"

@class GPPSignInButton;
@interface MeViewController : GAITrackedViewController<GPPSignInDelegate,UIAlertViewDelegate>
{
    BOOL pageControlBeingUsed;
     FbGraph *fbGraph;
}

@property (nonatomic, retain) IBOutlet UIView *goldView;
@property (nonatomic, retain) IBOutlet UIView *platinumView;
@property (nonatomic, retain) IBOutlet UIView *silverView;

@property (nonatomic, retain) IBOutlet UIScrollView* scrollView;
@property (nonatomic, retain) IBOutlet UIPageControl* pageControl;
@property (nonatomic, retain) ACAccountStore *accountStore;
@property (nonatomic, strong) TWAPIManager *apiManager;
@property (nonatomic, strong) NSArray *accounts;
@property (nonatomic, strong) UIButton *reverseAuthBtn;

-(IBAction)faceBookClick:(id)sender;
@property (nonatomic, retain)IBOutlet UIButton *clickGPBtn;
@property (nonatomic, retain)IBOutlet UIButton *clickLIBtn;
@property (nonatomic, retain)IBOutlet UIButton *clickTwitterBtn;

-(IBAction)clickTwitter:(id)sender;

-(IBAction)clickQRCODE:(id)sender;

@property(nonatomic,retain)IBOutlet UIView *qrView;

-(IBAction)clickGP:(id)sender;

-(IBAction)clickLI:(id)sender;

-(IBAction)clickFB:(id)sender;

- (IBAction)tipsClick;

- (IBAction)changePage;

- (IBAction)closeButton;
@property(nonatomic,retain)IBOutlet ZBarReaderViewController *reader;

@property(nonatomic,retain)IBOutlet UIView *scan_view;

-(IBAction)meButton;
-(IBAction)dealsButton;
-(IBAction)netCoinsButton;
-(IBAction)socialButton;
-(IBAction)myBusinessButton;

-(IBAction)settingsButton;

@property (nonatomic, retain) IBOutlet UIImageView* fbImage;
@property (nonatomic, retain) IBOutlet UIImageView* twitterImage;
@property (nonatomic, retain) IBOutlet UIImageView* gpImage;
@property (nonatomic, retain) IBOutlet UIImageView* linkdinImage;


@property (nonatomic, retain) IBOutlet UILabel *userStatusLabel;
@property (nonatomic, retain) IBOutlet UILabel *totalScoreLabel;


@property (nonatomic, retain) IBOutlet UILabel *goldLabel;
@property (nonatomic, retain) IBOutlet UILabel *platinumLabel;

@property (nonatomic, retain) IBOutlet UILabel *twNotLabel;
@property (nonatomic, retain) IBOutlet UILabel *gpNotLabel;
@property (nonatomic, retain) IBOutlet UILabel *liNotLabel;


@property (nonatomic, retain) IBOutlet UILabel *twNameLabel;
@property (nonatomic, retain) IBOutlet UILabel *gpNameLabel;
@property (nonatomic, retain) IBOutlet UILabel *liNameLabel;
@property (nonatomic, retain) IBOutlet UILabel *fbNameLabel;

@property (retain, nonatomic) IBOutlet GPPSignInButton *signInButton;
// A label to display the result of the sign-in action.
@property (retain, nonatomic) IBOutlet UILabel *signInAuthStatus;
// A label to display the signed-in user's display name.
@property (retain, nonatomic) IBOutlet UILabel *signInDisplayName;
// A button to sign out of this application.
@property (retain, nonatomic) IBOutlet UIButton *signOutButton;
// A button to disconnect user from this application.
@property (retain, nonatomic) IBOutlet UIButton *disconnectButton;
// A switch for whether to request
// https://www.googleapis.com/auth/userinfo.email scope to get user's email
// address after the sign-in action.
@property (retain, nonatomic) IBOutlet UISwitch *userinfoEmailScope;

// Called when the user presses the "Sign out" button.
- (IBAction)signOut:(id)sender;
// Called when the user presses the "Disconnect" button.
- (IBAction)disconnect:(id)sender;
// Called when the user toggles the
// https://www.googleapis.com/auth/userinfo.email scope.
- (IBAction)userinfoEmailScopeToggle:(id)sender;

@property (nonatomic, retain) IBOutlet UIButton *button;
@property (nonatomic, retain) IBOutlet UIButton *postButton;
@property (nonatomic, retain) IBOutlet UILabel *postButtonLabel;
@property (nonatomic, retain) IBOutlet UILabel *name;
@property (nonatomic, retain) IBOutlet UILabel *headline;
@property (nonatomic, retain) IBOutlet UILabel *status;
@property (nonatomic, retain) IBOutlet UILabel *updateStatusLabel;
@property (nonatomic, retain) IBOutlet UITextField *statusTextView;
@property (nonatomic, retain) OAuthLoginView *oAuthLoginView;


- (IBAction)button_TouchUp:(UIButton *)sender;
- (void)profileApiCall;
- (void)networkApiCall;


@property (nonatomic, retain)IBOutlet UISlider *slideToUnlock;
@property (nonatomic, retain)IBOutlet UIButton *lockButton;
@property (nonatomic, retain)IBOutlet UILabel *myLabel;
@property (nonatomic, retain)IBOutlet UIImageView *Container;

@property (nonatomic, retain)NSString *comingString;


-(IBAction)UnLockIt;
-(IBAction)LockIt;
-(IBAction)fadeLabel;


@property (weak, nonatomic) IBOutlet UIButton *imageButton;
-(IBAction)browseImage;


@property (nonatomic, retain)IBOutlet UISlider *slideToUnlockTw;
@property (nonatomic, retain)IBOutlet UIButton *lockButtonTw;
@property (nonatomic, retain)IBOutlet UILabel *myLabelTw;
@property (nonatomic, retain)IBOutlet UIImageView *ContainerTw;

-(IBAction)UnLockItTw;
-(IBAction)LockItTw;
-(IBAction)fadeLabelTw;


@property (nonatomic, retain)IBOutlet UISlider *slideToUnlockLi;
@property (nonatomic, retain)IBOutlet UIButton *lockButtonLi;
@property (nonatomic, retain)IBOutlet UILabel *myLabelLi;
@property (nonatomic, retain)IBOutlet UIImageView *ContainerLi;


@property (nonatomic, retain)IBOutlet UIImageView *imageLogoGP;
@property (nonatomic, retain)IBOutlet UIImageView *imageBackGP;


@property (nonatomic, retain)IBOutlet UIImageView *imageLogoLi;
@property (nonatomic, retain)IBOutlet UIImageView *imageBackLi;

@property (nonatomic, retain)IBOutlet UIView *totalHideView;
@property (nonatomic, retain)IBOutlet UIActivityIndicatorView *activity;

-(IBAction)UnLockItLi;
-(IBAction)LockItLi;
-(IBAction)fadeLabelLi;


@property (nonatomic, retain)IBOutlet UISlider *slideToUnlockGp;
@property (nonatomic, retain)IBOutlet UIButton *lockButtonGp;
@property (nonatomic, retain)IBOutlet UILabel *myLabelGp;
@property (nonatomic, retain)IBOutlet UIImageView *ContainerGp;

@property (nonatomic, retain)IBOutlet UIImageView *addImage;

@property(nonatomic,retain)IBOutlet UIButton *meButton11;
@property (nonatomic, retain)IBOutlet UIImageView *MEIMAGE;

@property (nonatomic, retain)IBOutlet UIImageView *statusImageView;

@property (nonatomic, retain)IBOutlet UIImageView *platinumImageView;
@property (nonatomic, retain)IBOutlet UILabel *MeLabel;

-(IBAction)UnLockItGp;
-(IBAction)LockItGp;
-(IBAction)fadeLabelGp;



@property (nonatomic, retain) IBOutlet UIView *fbHide;
@property (nonatomic, retain) IBOutlet UIView *gpHide;
@property (nonatomic, retain) IBOutlet UIView *liHide;
@property (nonatomic, retain) IBOutlet UIView *twHide;



@end
