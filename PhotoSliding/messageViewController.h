//
//  dealsViewController.h
//  PhotoSliding
//
//  Created by ephronsystems on 6/27/13.
//
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "UIInputToolbar.h"

@interface messageViewController : GAITrackedViewController<UICollectionViewDelegate,UICollectionViewDataSource,AVAudioRecorderDelegate, AVAudioPlayerDelegate,UIAlertViewDelegate,UIInputToolbarDelegate>
{
    UIInputToolbar *inputToolbar;
    
@private
    BOOL keyboardIsVisible;
}
@property (nonatomic, retain) UIInputToolbar *inputToolbar;
@property(retain,nonatomic)IBOutlet UICollectionView *collectionView;
@property (retain, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (weak, nonatomic) IBOutlet UILabel *selectedSegmentLabel;
@property (nonatomic,retain)IBOutlet UITableView *avilableTableView;
@property (retain, nonatomic) IBOutlet UITextField *searchTxtEield;
@property (weak, nonatomic) IBOutlet UIButton *filterButton;
@property (weak, nonatomic) IBOutlet UIView  *detailView;
@property (weak, nonatomic) IBOutlet UIView  *back_view;
@property (retain, nonatomic) IBOutlet UITextField *titleTxtEield;
@property (weak, nonatomic) IBOutlet UITextView *messageText;
@property (weak, nonatomic) IBOutlet UIImageView *cropImage;


@property (retain, nonatomic) IBOutlet UITextView *neMessageTxtEield;


@property (weak, nonatomic) IBOutlet UIImageView *sm1;
@property (weak, nonatomic) IBOutlet UIImageView *sm2;
@property (weak, nonatomic) IBOutlet UIImageView *sm3;


@property(retain,nonatomic)IBOutlet UICollectionView *collectionViewSmily;


@property (weak, nonatomic) IBOutlet UIImageView *cropImageGetView;
@property (weak, nonatomic) IBOutlet UIView *cropImageGet;

@property (weak, nonatomic) IBOutlet UIView *longPress;


@property (weak, nonatomic) IBOutlet UIImageView *senderImage;
@property (weak, nonatomic) IBOutlet UIButton  *profileButton;

@property (weak, nonatomic) IBOutlet UIView  *back_textView;
@property (weak, nonatomic) IBOutlet UITextView  *temp_textView;

@property (retain, nonatomic)NSString *imageURL;

@property (retain, nonatomic)NSString *comingFrom;

-(IBAction)messageButton;


-(IBAction)backButton;
-(IBAction)settingsButton;
- (IBAction)segmentDidChange:(id)sender;
-(IBAction)meButton;
-(IBAction)dealsButton;
-(IBAction)netCoinsButton;
-(IBAction)socialButton;
-(IBAction)myBusinessButton;
-(IBAction)continueButtonClick;
-(IBAction)dealbackButtonClick;
-(IBAction)keyboardButton;

- (IBAction)messageDidChange:(id)sender;

//// AUDIO RECORDING

@property (weak, nonatomic) IBOutlet UIButton *recordPauseButton;
@property (weak, nonatomic) IBOutlet UIButton *stopButton;
@property (weak, nonatomic) IBOutlet UIButton *playButton;

@property (weak, nonatomic) IBOutlet UILabel *titleLength;
@property (weak, nonatomic) IBOutlet UILabel *textLength;
@property (weak, nonatomic) IBOutlet UILabel *textLength1;
@property (weak, nonatomic) IBOutlet UILabel *labelButton;

- (IBAction)recordPauseTapped:(id)sender;
- (IBAction)stopTapped:(id)sender;
- (IBAction)playTapped:(id)sender;

///

@end
