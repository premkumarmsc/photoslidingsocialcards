//
//  dealsViewController.h
//  PhotoSliding
//
//  Created by ephronsystems on 6/27/13.
//
//

#import <UIKit/UIKit.h>

@interface savedCards : GAITrackedViewController<UICollectionViewDelegate,UICollectionViewDataSource,UIAlertViewDelegate>
@property(retain,nonatomic)IBOutlet UICollectionView *collectionView;
@property (retain, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (weak, nonatomic) IBOutlet UILabel *selectedSegmentLabel;
@property (nonatomic,retain)IBOutlet UITableView *avilableTableView;
@property (retain, nonatomic) IBOutlet UITextField *searchTxtEield;
@property (weak, nonatomic) IBOutlet UIButton *filterButton;
@property (weak, nonatomic) IBOutlet UIView  *detailView;
@property (weak, nonatomic) IBOutlet UIView  *back_view;
@property (retain, nonatomic) IBOutlet UITextField *titleTxtEield;
@property (weak, nonatomic) IBOutlet UITextView *messageText;
@property (weak, nonatomic) IBOutlet UIImageView *cropImage;
@property (weak, nonatomic) IBOutlet UIImageView *senderImage;
@property (nonatomic,retain)IBOutlet UITableView *avilableTableView1;
@property (weak, nonatomic) IBOutlet UIButton  *profileButton;

@property (weak, nonatomic) IBOutlet UITextField *address1;
@property (weak, nonatomic) IBOutlet UITextField *address2;
@property (weak, nonatomic) IBOutlet UITextField *zipCode;
@property (weak, nonatomic) IBOutlet UITextField *firstName;
@property (weak, nonatomic) IBOutlet UITextField *lastname;
@property (weak, nonatomic) IBOutlet UITextField *city;
@property (weak, nonatomic) IBOutlet UITextField *state;
@property (weak, nonatomic) IBOutlet UIButton *countrybtn;
@property (weak, nonatomic) IBOutlet UIView  *contactAddView;

- (IBAction)countryButton:(id)sender;
-(IBAction)backButton;
-(IBAction)settingsButton;
- (IBAction)segmentDidChange:(id)sender;
-(IBAction)meButton;
-(IBAction)dealsButton;
-(IBAction)netCoinsButton;
-(IBAction)socialButton;
-(IBAction)myBusinessButton;
-(IBAction)continueButtonClick;
-(IBAction)dealbackButtonClick;

-(IBAction)cancelButton;

@end
