//
//  AppDelegate.h
//  PhotoSliding
//
//  Created by Neon Spark on 1/21/12.
//  Copyright (c) 2012 http://sugartin.info. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "Facebook.h"
#import <FacebookSDK/FacebookSDK.h>
@class ViewController;

@class MeViewController;

@class GTMOAuth2Authentication;
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (retain, nonatomic) ViewController *viewController;

@property (retain, nonatomic) MeViewController *meViewController;

//@property (nonatomic, strong) Facebook *facebook;
@end
