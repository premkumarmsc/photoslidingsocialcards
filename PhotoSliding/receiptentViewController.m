//
//  dealsViewController.m
//  PhotoSliding
//
//  Created by ephronsystems on 6/27/13.
//
//

#import "receiptentViewController.h"
#import "receiptentCell.h"
#import "Image_cell.h"
#import "SVPullToRefresh.h"

@interface receiptentViewController ()

@end

@implementation receiptentViewController

@synthesize filterButton;
@synthesize collectionView;
@synthesize countrybtn;
@synthesize firstalert;
@synthesize comingView;
@synthesize picker1;
@synthesize picker2;
receiptentCell *cell;

NSMutableArray *imageThumpArray;
NSMutableArray *FnameArray;
NSMutableArray *LnameArray;
NSMutableArray *countryArray;
NSMutableArray *checkmark_array;
NSString *countrystr;
NSMutableArray* checked_status_arr;
UIActivityIndicatorView *Activity;
UIAlertView *progressAlert;

NSMutableArray* address_entered_arr;

NSMutableArray *contactFname;
NSMutableArray *contactMobile_numbers;
NSMutableArray *contacttype_array;
NSMutableArray *contactLastNames;
NSMutableArray *selectContactArray1;

NSMutableArray *contactIDArray;

NSMutableArray *contactSourceArray;

NSMutableArray *editAddress1Arr;
NSMutableArray *editAddress2Arr;
NSMutableArray *editCityArr;
NSMutableArray *editStateArr;
NSMutableArray *editCountryArr;
NSMutableArray *editZipArr;
NSMutableArray *editPhoneArr;
NSMutableArray *editEmailArr;

NSString *selectedContactID;


int tag_contact=0;


int from_count;
int to_count;
int total_count;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(UIImage *)imageWithImageSimple:(UIImage*)image scaledToSize:(CGSize)newSize
{
    
    UIGraphicsBeginImageContext(newSize);//根据当前大小创建一个基于位图图形的环境
    
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];//根据新的尺寸画出传过来的图片
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();//从当前环境当中得到重绘的图片
    
    UIGraphicsEndImageContext();//关闭当前环境
    
    return newImage;
}

-(IBAction)donePicker
{
    _pickerBack.hidden=YES;
}

-(IBAction)donePicker2
{
    _pickerBack2.hidden=YES;
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    
    return 1;//Or return whatever as you intend
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
    
    return [countryArray count];//Or, return as suitable for you...normally we use array for dynamic
}
- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [NSString stringWithFormat:@"%@",countryArray[row]];//Or, your suitable title; like Choice-a, etc.
}
- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    
    if (thePickerView==picker1) {
        
        //Here, like the table view you can get the each section of each row if you've multiple sections
        ////(@"Selected Color: %@.",countryArray[row]);
        
        [_editcountrybtn setTitle:[NSString stringWithFormat:@"    %@", countryArray[row]] forState:UIControlStateNormal];
        
        countrystr=countryArray[row];
    }
    else
    {
        
        //Here, like the table view you can get the each section of each row if you've multiple sections
        ////(@"Selected Color: %@.",countryArray[row]);
        
        [countrybtn setTitle:[NSString stringWithFormat:@"    %@", countryArray[row]] forState:UIControlStateNormal];
        
        countrystr=countryArray[row];
    }
    
   
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.trackedViewName = @"Receipient selection Screen";
    
    
    _pickerBack.hidden=YES;
     _pickerBack2.hidden=YES;
    
    [self.view addSubview:_editView];
    _editView.hidden=YES;
    
    from_count=0;
    to_count=30;
    
    [self getCountry];
    
    NSUserDefaults *checkVal=[NSUserDefaults standardUserDefaults];
    
    
    NSString *getFrom=[checkVal objectForKey:@"VIEW_REFERENCE"];
    
    NSString *getCardID;
    NSString *getcardname;
    NSString *getImage;
    NSString *getMessage;
    NSString *getSmileyID;
    NSString *getContactIDS;
    
    
    if ([getFrom isEqualToString:@"SAVED_CARDS"])
    {
        getCardID=[checkVal objectForKey:@"CARD_ID_REFERENCE"];
        getcardname=[checkVal objectForKey:@"CARD_NAME_REFERENCE"];
        getImage=[checkVal objectForKey:@"IMAGE_ID_REFERENCE"];
        getMessage=[checkVal objectForKey:@"MESSAGE_REFERENCE"];
        getSmileyID=[checkVal objectForKey:@"SMILEY_REFERENCE"];
        getContactIDS=[checkVal objectForKey:@"CONTACT_ID_REFERENCE"];
        
        
        
        
        
        
        
        
        
        //////(@"SMILEY IDS:%@",getContactIDS);
        
        if (![getContactIDS isEqualToString:@""]) {
            
            
            
            NSArray *lines = [getContactIDS componentsSeparatedByString: @","];
            
            //////(@"HELLO:%@",lines);
            
            // selectContactArray1=lines;
            
            // //////(@"SELECTED CONTACT:%@",selectContactArray1);
            
        }
    }
    
    /*
     if (tag_contact==0) {
     _continueBtn.enabled=NO;
     }
     else
     {
     _continueBtn.enabled=YES;
     }
     */
    
    _continueBtn.titleLabel.textColor=[UIColor grayColor];
    
    
    NSUserDefaults *img=[NSUserDefaults standardUserDefaults];
    
    [img setObject:@"NULL" forKey:@"USER_IMAGE"];
    
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    
    NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:@"profileImage.jpg"];
    UIImage *profileIMG = [UIImage imageWithContentsOfFile:getImagePath];
    
    
    [_profileButton setImage:profileIMG forState:UIControlStateNormal];
    
    
    NSUserDefaults *checkSta=[NSUserDefaults standardUserDefaults];
    
    
    NSString *addressStatus=[checkSta objectForKey:@"FIRSTTIME"];
    
    
    NSString *deviceStr = [UIDevice currentDevice].model;
    //////(@"device:%@",deviceStr);
    
    
    if (![deviceStr isEqualToString:@"iPod touch"])
    {
        /*
        if ([addressStatus isEqualToString:@"YES"])
        {
            
            firstalert=[[UIAlertView alloc]initWithTitle:@"" message:@"\"Social Netgate\" Would Like to  Access Your Contacts" delegate:self cancelButtonTitle:@"Don't Allow" otherButtonTitles:@"Allow", nil];
            
            [firstalert show];
            
            
            
            
            
        }
         */
    }
    
    
    [self.view addSubview:_contactAddView];
    _contactAddView.hidden=YES;
    
    
    imageThumpArray=[[NSMutableArray alloc]init];
    FnameArray=[[NSMutableArray alloc]init];
    LnameArray=[[NSMutableArray alloc]init];
    checkmark_array=[[NSMutableArray alloc]init];
    contactIDArray=[[NSMutableArray alloc]init];
    checked_status_arr=[[NSMutableArray alloc]init];
    address_entered_arr=[[NSMutableArray alloc]init];
    
    
    editAddress1Arr=[[NSMutableArray alloc]init];
    editAddress2Arr=[[NSMutableArray alloc]init];
    editCityArr=[[NSMutableArray alloc]init];
    editCountryArr=[[NSMutableArray alloc]init];
    editEmailArr=[[NSMutableArray alloc]init];
    editPhoneArr=[[NSMutableArray alloc]init];
    editStateArr=[[NSMutableArray alloc]init];
    editZipArr=[[NSMutableArray alloc]init];
    
    contactSourceArray=[[NSMutableArray alloc]init];
    
    [self  getDatas:from_count to:to_count];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"receiptentCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
    
    
      [picker1 addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pickerTapped:)] ];
    
    
    
    // Do any additional setup after loading the view from its nib.
}

-(void)pickerTapped:(UIGestureRecognizer *)gestureRecognizer
{
    //(@"TABBED");
    
     //- (NSInteger)selectedRowInComponent:(NSInteger)component
    
    int valInt=[picker1 selectedRowInComponent:0];
    
     //(@"TABBED:%d",valInt);
    
    
    [_editcountrybtn setTitle:[NSString stringWithFormat:@"    %@", countryArray[valInt]] forState:UIControlStateNormal];
    
    countrystr=countryArray[valInt];
}

-(IBAction)referesh
{
    
    progressAlert = [[UIAlertView alloc] initWithTitle: @"Refereshing..." message: @"Please wait..." delegate: self cancelButtonTitle: nil otherButtonTitles: nil];
    
    Activity = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(90.0f, 40.0f, 100.0f, 100.0f)];
    
    
    
    [progressAlert addSubview:Activity];
    
    [Activity startAnimating];
    
    [progressAlert show];
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/getFBcontacts",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post1 = [ASIFormDataRequest requestWithURL:url];
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    
    //[request_post setValidatesSecureCertificate:NO];
    [request_post1 setPostValue:user forKey:@"userID"];
    
    [request_post1 setPostValue:[Config get_token] forKey:@"apikey"];
    
    
    // [request_post setTimeOutSeconds:30];
    
    
    [request_post1 setCompletionBlock:^{
        NSString *responseString = [request_post1 responseString];
        NSMutableData *results1 = [responseString JSONValue];
        
        
        //(@"ABCC:%@",responseString);
        
        
       
        
        [self viewDidLoad];
        
        [Activity stopAnimating];
        [progressAlert dismissWithClickedButtonIndex:0 animated:YES];
        
        
    }];
    [request_post1 setFailedBlock:^{
        NSError *error = [request_post1 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1 startAsynchronous];
}


-(void)viewWillAppear:(BOOL)animated
{
    [self.collectionView.infiniteScrollingView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
    [self.collectionView addInfiniteScrollingWithActionHandler:^{
        ////(@"Infinitescroll");
        
        //referesh_count=referesh_count+1;
        // [self getDatas:referesh_count];
        
        
        from_count=from_count+30;
        
        [self  getDatas:from_count to:to_count];
        
        
        @try {
            int64_t delayInSeconds = 10.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                //[self.dataSource addObjectsFromArray:tmp];
                [self.collectionView performBatchUpdates:^{
                    //[self.collectionView insertItemsAtIndexPaths:(NSArray*)indexPaths];
                    
                    ////(@"UPDAtES");
                    
                } completion:nil];
                [self.collectionView.infiniteScrollingView stopAnimating];
            });
        }
        @catch (NSException *exception) {
            
        }
      
        
       
        
        
    }];
    
    [super viewWillAppear:animated];
    /*
     [collectionView addPullToRefreshWithActionHandler:^{
     
     
     referesh_count=referesh_count+1;
     [self getDatas:referesh_count];
     
     } position:SVPullToRefreshPositionBottom];
     */
}


-(void)getDatas:(int)get_from_count to:(int)get_to_count
{
    
    
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/contacts/retrieveUserContacts",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post1234 = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    NSString *access=[check objectForKey:@"ACCESS_TOKEN"];
    
    
    ////(@"referesh_count_str:%d",get_from_count);
    ////(@"referesh_count_str:%d",get_to_count);
    
    
    
    NSString *referesh_count_from_str=[NSString stringWithFormat:@"%d",get_from_count];
    NSString *referesh_count_to_str=[NSString stringWithFormat:@"%d",get_to_count];
    
    NSString *cardID=[check objectForKey:@"CARD_ID"];
    
    [request_post1234 setPostValue:user forKey:@"userID"];
    [request_post1234 setPostValue:@"30" forKey:@"item_count"];
    [request_post1234 setPostValue:referesh_count_from_str forKey:@"current_count"];
      [request_post1234 setPostValue:[Config get_token] forKey:@"apikey"];
    [request_post1234 setPostValue:cardID forKey:@"cardID"];
    
    
    [request_post1234 setTimeOutSeconds:30];
    
    
    [request_post1234 setCompletionBlock:^{
        NSString *responseString23 = [request_post1234 responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        
        // ////(@"RESULTS LOGIN:%@",results11);
        
        
        NSArray *temp= [results11 valueForKeyPath:@"contacts"];
        
        NSString *total_str=[results11 valueForKeyPath:@"header.total"];
        
        
        
        total_count=[total_str intValue];
        
        ////(@"TEMPARY CONTACTS:%d",total_count);
        
        
        
        
        //from_count=to_count;
        
        
        
        
        ////(@"FROM CONTACTS:%d",from_count);
        ////(@"TO CONTACTS:%d",to_count);
        
        for(NSDictionary *value in temp)
        {
            [imageThumpArray  addObject:[value valueForKey:@"friend_facebookproimg"]];
            [FnameArray  addObject:[value valueForKey:@"firstname"]];
            [LnameArray  addObject:[value valueForKey:@"lastname"]];
            [contactIDArray  addObject:[value valueForKey:@"id"]];
            
            [address_entered_arr  addObject:[value valueForKey:@"address_entered"]];
            
            
            
            [editZipArr  addObject:[value valueForKey:@"zipcode"]];
            [editStateArr  addObject:[value valueForKey:@"state"]];
            [editPhoneArr  addObject:[value valueForKey:@"phoneno"]];
            [editEmailArr  addObject:[value valueForKey:@"emailid"]];
            
            [editCountryArr  addObject:[value valueForKey:@"country"]];
            [editCityArr  addObject:[value valueForKey:@"city"]];
            [editAddress2Arr  addObject:[value valueForKey:@"address2"]];
            [editAddress1Arr  addObject:[value valueForKey:@"address1"]];
            
            [contactSourceArray  addObject:[value valueForKey:@"src_contact"]];
            
            // [address_entered_arr  addObject:[value valueForKey:@"address_entered"]];
            
            
            
            
            NSString *checked=[value valueForKey:@"checked_status"];
            
            if ([checked isEqualToString:@""])
            {
                [checkmark_array addObject:@"NO"];
            }
            else
            {
                [checkmark_array addObject:@"YES"];
            }
            
            [checked_status_arr  addObject:[value valueForKey:@"checked_status"]];
            
        }
        
        
        
        
        selectContactArray1=[[NSMutableArray alloc]init];
        
        ////////(@"CONTACT ID ARR:%@",contactIDArray);
        
        
        for (int i=0; i<[contactIDArray count]; i++)
        {
            NSString *markString=checkmark_array[i];
            
            if ([markString isEqualToString:@"YES"])
            {
                [selectContactArray1 addObject:contactIDArray[i]];
            }
            
        }
        
        
        // //////(@"Select :%@",selectContactArray1);
        
        
        if ([selectContactArray1 count]==0)
        {
            
            _continueBtn.enabled=NO;
            _continueBtn.titleLabel.textColor=[UIColor grayColor];
            
        }
        else
        {
            _continueBtn.enabled=YES;
            _continueBtn.titleLabel.textColor=[UIColor whiteColor];
        }
        
        
        //////(@"ADD:%d",[imageThumpArray count]);
        
        
        [collectionView reloadData];
        
        
        
        
    }];
    [request_post1234 setFailedBlock:^{
        NSError *error = [request_post1234 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1234 startAsynchronous];
}

- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex

{
    if (alertView==firstalert)
    {
        if (buttonIndex == 0)
        {
            //cancel clicked ...do your action
            
            NSUserDefaults *checkSta=[NSUserDefaults standardUserDefaults];
            
            [checkSta setObject:@"NO" forKey:@"FIRSTTIME"];
            
        }else if (buttonIndex == 1)
        {
            
            NSString *deviceStr = [UIDevice currentDevice].model;
            //////(@"device:%@",deviceStr);
            
            
            if (![deviceStr isEqualToString:@"iPod touch"])
            {
                {
                    //reset clicked
                    
                    ABAddressBookRef addressBook = ABAddressBookCreate();
                    CFArrayRef addressBookData = ABAddressBookCopyArrayOfAllPeople(addressBook);
                    
                    CFIndex count = CFArrayGetCount(addressBookData);
                    
                    NSMutableArray *firstNameArray = [NSMutableArray new];
                    NSMutableArray *lastNameArray = [NSMutableArray new];
                    NSMutableArray *emailArray = [NSMutableArray new];
                    NSMutableArray *mobileArray = [NSMutableArray new];
                    NSMutableArray *streetArr = [NSMutableArray new];
                    NSMutableArray *cityArr = [NSMutableArray new];
                    NSMutableArray *stateArr = [NSMutableArray new];
                    NSMutableArray *zipArr = [NSMutableArray new];
                    NSMutableArray *countryArr = [NSMutableArray new];
                    
                    
                    for (CFIndex idx = 0; idx < count; idx++)
                    {
                        ABRecordRef person = CFArrayGetValueAtIndex(addressBookData, idx);
                        
                        ABMultiValueRef st = ABRecordCopyValue(person, kABPersonAddressProperty);
                        
                        ABMultiValueRef st1 = ABRecordCopyValue(person, kABPersonPhoneProperty);
                        
                        
                        
                        NSString *firstName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
                        NSString *lastName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);
                        
                        // NSString *emailStr = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonEmailProperty);
                        
                        
                        NSString *mustPhone;
                        ABMultiValueRef phone = ABRecordCopyValue(person, kABPersonPhoneProperty);
                        
                        
                        
                        
                        NSString *phoneno = (__bridge_transfer NSString *)ABMultiValueCopyValueAtIndex(phone, 0);
                        
                        //////(@"person.homeEmail = %@ ", phoneno);
                        @try {
                            [mobileArray addObject:phoneno];
                            
                            mustPhone=phoneno;
                        }
                        @catch (NSException *exception)
                        {
                            NSString *Phone1 = (__bridge_transfer NSString *)ABMultiValueCopyValueAtIndex(phone, 1);
                            
                            @try
                            {
                                
                                [mobileArray addObject:Phone1];
                                mustPhone=Phone1;
                            }
                            @catch (NSException *exception)
                            {
                                [mobileArray addObject:@""];
                                mustPhone=@"";
                            }
                            
                        }
                        /*
                         if ([mustEmail isEqualToString:@""]&&[mustPhone isEqualToString:@""])
                         {
                         
                         [mobileArray removeObjectAtIndex: [mobileArray count]-1];
                         [emailArray removeObjectAtIndex: [emailArray count]-1];
                         }*/
                        if ([mustPhone isEqualToString:@""])
                        {
                            
                            [mobileArray removeObjectAtIndex: [mobileArray count]-1];
                            
                        }
                        else
                        {
                            
                            
                            
                            NSString *mustEmail;
                            
                            
                            
                            ABMultiValueRef emails = ABRecordCopyValue(person, kABPersonEmailProperty);
                            
                            
                            if (emails)
                            {
                                
                                
                                NSString *email = (__bridge_transfer NSString *)ABMultiValueCopyValueAtIndex(emails, 0);
                                
                                //////(@"person.homeEmail = %@ ", email);
                                @try {
                                    [emailArray addObject:email];
                                    
                                    mustEmail=email;
                                }
                                @catch (NSException *exception)
                                {
                                    NSString *email1 = (__bridge_transfer NSString *)ABMultiValueCopyValueAtIndex(emails, 1);
                                    
                                    @try
                                    {
                                        
                                        [emailArray addObject:email1];
                                        mustEmail=email1;
                                        
                                    }
                                    @catch (NSException *exception)
                                    {
                                        [emailArray addObject:@""];
                                        mustEmail=@"";
                                    }
                                    
                                }
                                
                                
                                
                                
                            }
                            
                            
                            
                            
                            
                            
                            
                            
                            if (firstName)
                            {
                                
                                
                                [firstNameArray addObject:firstName];
                                
                            }
                            else
                            {
                                [firstNameArray addObject:@""];
                            }
                            
                            if (lastName)
                            {
                                
                                [lastNameArray addObject:lastName];
                            }
                            else
                            {
                                
                                if ([firstName isEqualToString:@""]||[firstName length]==0)
                                {
                                    [lastNameArray addObject:@"Unknown"];
                                }
                                else
                                {
                                    
                                    [lastNameArray addObject:@""];
                                }
                            }
                            
                            
                            
                            
                            
                            
                            
                            
                            if (ABMultiValueGetCount(st) > 0)
                            {
                                
                                //////(@"ENTER");
                                
                                CFDictionaryRef dict = ABMultiValueCopyValueAtIndex(st, 0);
                                NSString *dictString = CFDictionaryGetValue(dict, kABPersonAddressStreetKey);
                                NSString *cityString = CFDictionaryGetValue(dict, kABPersonAddressCityKey);
                                NSString *stateString = CFDictionaryGetValue(dict, kABPersonAddressStateKey);
                                NSString *zipString = CFDictionaryGetValue(dict, kABPersonAddressZIPKey);
                                NSString *countryString = CFDictionaryGetValue(dict, kABPersonAddressCountryKey);
                                @try
                                {
                                    [streetArr addObject:dictString];
                                }
                                @catch (NSException *exception)
                                {
                                    [streetArr addObject:@""];
                                }
                                
                                @try
                                {
                                    [cityArr addObject:cityString];
                                }
                                @catch (NSException *exception)
                                {
                                    [cityArr addObject:@""];
                                }
                                
                                
                                @try
                                {
                                    [stateArr addObject:stateString];
                                }
                                @catch (NSException *exception)
                                {
                                    [stateArr addObject:@""];
                                }
                                
                                @try
                                {
                                    [countryArr addObject:countryString];
                                }
                                @catch (NSException *exception)
                                {
                                    [countryArr addObject:@""];
                                }
                                
                                @try
                                {
                                    [zipArr addObject:zipString];
                                }
                                @catch (NSException *exception)
                                {
                                    [zipArr addObject:@""];
                                }
                                
                                
                                
                            }
                            else
                            {
                                [streetArr addObject:@""];
                                [zipArr addObject:@""];
                                [countryArr addObject:@""];
                                [stateArr addObject:@""];
                                [cityArr addObject:@""];
                            }
                            
                        }
                        
                        
                    }
                    CFRelease(addressBook);
                    CFRelease(addressBookData);
                    
                    
                    
                    NSUserDefaults *checkSta=[NSUserDefaults standardUserDefaults];
                    
                    [checkSta setObject:@"NO" forKey:@"FIRSTTIME"];
                    
                    //////(@"Contacts Array:%@,%@,%@",firstNameArray,lastNameArray,streetArr);
                    
                    //////(@"Contacts Array:%@,%@,%@,%@,%@,%@",cityArr,stateArr,countryArr,zipArr,mobileArray,emailArray);
                    
                    
                    NSMutableString* firstNameStr = [NSMutableString stringWithCapacity:2000];
                    NSMutableString* lastNameStr = [NSMutableString stringWithCapacity:2000];
                    NSMutableString* mobileStr = [NSMutableString stringWithCapacity:2000];
                    NSMutableString* emailStr = [NSMutableString stringWithCapacity:2000];
                    NSMutableString* streetStr = [NSMutableString stringWithCapacity:2000];
                    NSMutableString* cityStr = [NSMutableString stringWithCapacity:2000];
                    NSMutableString* countryStr = [NSMutableString stringWithCapacity:2000];
                    NSMutableString* zipStr = [NSMutableString stringWithCapacity:2000];
                    NSMutableString* stateStr = [NSMutableString stringWithCapacity:2000];
                    
                    //(@"COUNTEY:%@",countryArr);
                    
                    for (int i=0; i<[firstNameArray count]; i++)
                    {
                        [firstNameStr appendString:[NSString stringWithFormat:@"%@|",firstNameArray[i]]];
                        [lastNameStr appendString:[NSString stringWithFormat:@"%@|",lastNameArray[i]]];
                        [mobileStr appendString:[NSString stringWithFormat:@"%@|",mobileArray[i]]];
                        [emailStr appendString:[NSString stringWithFormat:@"%@|",emailArray[i]]];
                        [streetStr appendString:[NSString stringWithFormat:@"%@|",streetArr[i]]];
                        [cityStr appendString:[NSString stringWithFormat:@"%@|",cityArr[i]]];
                        [countryStr appendString:[NSString stringWithFormat:@"%@|",countryArr[i]]];
                        [zipStr appendString:[NSString stringWithFormat:@"%@|",zipArr[i]]];
                        [stateStr appendString:[NSString stringWithFormat:@"%@|",stateArr[i]]];
                    }
                    
                    
                    @try {
                        [firstNameStr deleteCharactersInRange:NSMakeRange([firstNameStr length] - 1, 1)];
                    }
                    @catch (NSException *exception) {
                        
                    }
                    
                    @try {
                        
                        [lastNameStr deleteCharactersInRange:NSMakeRange([lastNameStr length] - 1, 1)];
                    }
                    @catch (NSException *exception) {
                        
                    }
                    
                    @try {
                        [mobileStr deleteCharactersInRange:NSMakeRange([mobileStr length] - 1, 1)];
                    }
                    @catch (NSException *exception) {
                        
                    }
                    
                    @try {
                        [emailStr deleteCharactersInRange:NSMakeRange([emailStr length] - 1, 1)];
                    }
                    @catch (NSException *exception) {
                        
                    }
                    @try {
                        [streetStr deleteCharactersInRange:NSMakeRange([streetStr length] - 1, 1)];
                    }
                    @catch (NSException *exception) {
                        
                    }
                    
                    @try {
                        
                    }
                    @catch (NSException *exception) {
                        [cityStr deleteCharactersInRange:NSMakeRange([cityStr length] - 1, 1)];
                    }
                    @try {
                        
                    }
                    @catch (NSException *exception)
                    {
                        
                        [countryStr deleteCharactersInRange:NSMakeRange([countryStr length] - 1, 1)];
                    }
                    
                    
                    @try {
                        [zipStr deleteCharactersInRange:NSMakeRange([zipStr length] - 1, 1)];
                    }
                    @catch (NSException *exception) {
                        
                    }
                    
                    @try {
                        
                        [stateStr deleteCharactersInRange:NSMakeRange([stateStr length] - 1, 1)];
                    }
                    @catch (NSException *exception)
                    {
                        
                    }
                    
                    
                    
                    
                    //////(@"firstNameStr:%@",firstNameStr);
                    //////(@"lastNameStr:%@",lastNameStr);
                    //////(@"mobileStr:%@",mobileStr);
                    //////(@"emailStr:%@",emailStr);
                    //////(@"streetStr:%@",streetStr);
                    //////(@"cityStr:%@",cityStr);
                    //////(@"countryStr:%@",countryStr);
                    //////(@"zipStr:%@",zipStr);
                    
                    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/contacts/phoneContactsImport",CONFIG_BASE_URL]];
                    
                    
                    
                    __block  ASIFormDataRequest *requestmethod = [ASIFormDataRequest requestWithURL:url];
                    
                    
                    
                    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
                    NSString *user=[check objectForKey:@"USERID"];
                    
                    
                    
                    [requestmethod setPostValue:user forKey:@"userID"];
                    [requestmethod setPostValue:firstNameStr forKey:@"firstNameStr"];
                    [requestmethod setPostValue:lastNameStr forKey:@"lastNameStr"];
                    [requestmethod setPostValue:mobileStr forKey:@"mobileStr"];
                    [requestmethod setPostValue:emailStr forKey:@"emailStr"];
                    [requestmethod setPostValue:streetStr forKey:@"streetStr"];
                    [requestmethod setPostValue:cityStr forKey:@"cityStr"];
                    [requestmethod setPostValue:@"" forKey:@"countryStr"];
                    [requestmethod setPostValue:zipStr forKey:@"zipStr"];
                    [requestmethod setPostValue:stateStr forKey:@"stateStr"];
                    [requestmethod setPostValue:@"http://www.socialnetgateapp.com/bundles/acmeregistration/images/no-user-image.png" forKey:@"friend_facebookproimg"];
                      [requestmethod setPostValue:[Config get_token] forKey:@"apikey"];
                    [requestmethod setTimeOutSeconds:30];
                    
                    
                    [requestmethod setCompletionBlock:^{
                        NSString *responseString23 = [requestmethod responseString];
                        NSMutableData *results11 = [responseString23 JSONValue];
                        
                        
                        
                        //////(@"TEMPARY:%@",responseString23);
                        
                        [self viewDidLoad];
                        
                        
                        
                    }];
                    [requestmethod setFailedBlock:^{
                        NSError *error = [requestmethod error];
                        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        
                        
                        
                        
                    }];
                    
                    [requestmethod startAsynchronous];
                    
                }
            }
            
            
            
            
        }
    }
    
    
}
-(void)getCountry
{
    countryArray=[[NSMutableArray alloc]init];
    
    NSMutableArray *countryAmount=[[NSMutableArray alloc]init];
    NSMutableArray *smilyAmount=[[NSMutableArray alloc]init];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/rules/getCountry",CONFIG_BASE_URL]];
   
     
    __block  ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    
    
      
   
    [request setPostValue:[Config get_token] forKey:@"apikey"];
    
    
    
    
    [request setCompletionBlock:^{
        // Use when fetching text data
        NSString *responseString = [request responseString];
        
        
        NSMutableData *results1 = [responseString JSONValue];
        
        // //////(@"HELLO:%@",results1);
        
        
        NSArray *temp= [results1 valueForKeyPath:@"response.countries"];
        
        
        //  //////(@"TEMPARY:%@",temp);
        
        for(NSDictionary *value in temp)
        {
            [countryArray  addObject:[value valueForKey:@"country"]];
            [smilyAmount addObject:[value valueForKey:@"smileycoins"]];
            [countryAmount addObject:[value valueForKey:@"netcoins"]];
            
        }
        
        [picker1 reloadAllComponents];
         [picker2 reloadAllComponents];
        
        NSUserDefaults *valueAdd=[NSUserDefaults standardUserDefaults];
        [valueAdd setObject:countryArray forKey:@"COUNTRY_ARRAY"];
        [valueAdd setObject:countryAmount forKey:@"COUNTRY_AMOUNT"];
        [valueAdd setObject:smilyAmount forKey:@"SMILY_AMOUNT"];
        
    }];
    [request setFailedBlock:^{
        NSError *error = [request error];
    }];
    [request startAsynchronous];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [imageThumpArray count];
}


- (Image_cell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELL" forIndexPath:indexPath];
    
    
    
    // cell.progress.hidden=YES;
    
    @try {
        NSString *temp=[imageThumpArray objectAtIndex:indexPath.row];
        [cell.image_view setImageWithURL:[NSURL URLWithString:temp]
                        placeholderImage:[UIImage imageNamed:@"vbigicon.png"]];
        
        
        
    }
    @catch (NSException *exception) {
        //////////////(@"CHJECK");
    }
    
    
    @try {
        cell.cell_title_Label.text=[NSString stringWithFormat:@"%@ %@",FnameArray[indexPath.row],LnameArray[indexPath.row]];
    }
    @catch (NSException *exception) {
        
    }
    
    
    
    NSString *new_string=[NSString stringWithFormat:@"%@ %@",FnameArray[indexPath.row],LnameArray[indexPath.row]];
    
    
    
    
    new_string = [new_string stringByReplacingOccurrencesOfString:@" "
                                                       withString:@""];
    
    //////(@"ENTER:%@",new_string);
    
    if ([new_string isEqualToString:@""])
    {
        ////(@"Hello:%@",editEmailArr[indexPath.row]);
        
        @try {
            
            NSArray  *testArray2 = [editEmailArr[indexPath.row] componentsSeparatedByString:@"@"];
            
            cell.cell_title_Label.text=testArray2[0];
        }
        @catch (NSException *exception) {
            cell.cell_title_Label.text=@"";
        }
        
        
    }
    
    int check_address_enter=[address_entered_arr[indexPath.row]intValue];
    
    if (check_address_enter==0)
    {
        cell.check_image_view.hidden=YES;
        cell.add_image_view.hidden=NO;
        
        //cell.check_image_view.image=[UIImage imageNamed:@"checkbox_checked.png"];
    }
    else
    {
        
        cell.check_image_view.hidden=NO;
        cell.add_image_view.hidden=YES;
        
        if ([checkmark_array[indexPath.row]isEqualToString:@"YES"])
        {
            cell.check_image_view.image=[UIImage imageNamed:@"checkbox_checked.png"];
        }
        else
        {
            cell.check_image_view.image=[UIImage imageNamed:@"checkbox_unchecked.png"];
        }
        
    }
    
    
    if ([contactSourceArray[indexPath.row]isEqualToString:@"web"])
    {
        cell.source_image_view.image=[UIImage imageNamed:@"con1.png"];
    }
    
    if ([contactSourceArray[indexPath.row]isEqualToString:@"facebook"])
    {
        cell.source_image_view.image=[UIImage imageNamed:@"fac.png"];
    }
    
    if ([contactSourceArray[indexPath.row]isEqualToString:@"twitter"])
    {
        cell.source_image_view.image=[UIImage imageNamed:@"twiter.png"];
    }
    if ([contactSourceArray[indexPath.row]isEqualToString:@"linkedin"])
    {
        cell.source_image_view.image=[UIImage imageNamed:@"in.png"];
    }
    if ([contactSourceArray[indexPath.row]isEqualToString:@"google"])
    {
        cell.source_image_view.image=[UIImage imageNamed:@"gog.png"];
    }
    if ([contactSourceArray[indexPath.row]isEqualToString:@"iphone"])
    {
        cell.source_image_view.image=[UIImage imageNamed:@"mob.png"];
    }
    //cell.image_view.image=[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@", image_path]];
    
    return cell;
}
-(IBAction)saveContact
{
    
    
    NSString *fname=_firstName.text;
    NSString *lname=_lastname.text;
    NSString *address1text=_address1.text;
    NSString *address2text=_address2.text;
    NSString *citystr=_city.text;
    NSString *statestr=_state.text;
    NSString *zipCodeStr=_zipCode.text;
    
    NSString *emailStr=_emailID.text;
    NSString *phoneStr=_phoneNo.text;
    
    
    
    //////(@"COUNTRY:%@",countrystr);
    
    if ([fname isEqualToString:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Enter the first name" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
    }
    else
    {
        if ([lname isEqualToString:@""])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Enter the last name" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alert show];
        }
        else
        {
            if ([address1text isEqualToString:@""])
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Enter the address" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                [alert show];
            }
            else
            {
                if ([citystr isEqualToString:@""])
                {
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Enter the City" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    
                    [alert show];
                    
                }
                else
                {
                    if ([statestr isEqualToString:@""])
                    {
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Enter the state" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        
                        [alert show];
                    }
                    else
                    {
                        if ([countrystr isEqualToString:@""])
                        {
                            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Select the Country" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                            
                            [alert show];
                        }
                        else
                        {
                            if ([countrystr isEqualToString:@""])
                            {
                                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Enter the zip code" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                
                                [alert show];
                            }
                            else
                            {
                                //////(@"SUCCESS");
                                
                                
                                if ([emailStr isEqualToString:@""])
                                {
                                    if ([phoneStr isEqualToString:@""])
                                    {
                                        [self addcontectFun];
                                    }
                                    else
                                    {
                                        NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
                                        
                                        // NSString *phoneRegex = @"^[\+(00)][0-9]{6,14}$";
                                        NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
                                        
                                        BOOL phoneValidates = [phoneTest evaluateWithObject:phoneStr];
                                        
                                        
                                        
                                        if (phoneValidates == YES){
                                            
                                            ////(@"Matched");
                                            
                                            [self addcontectFun];
                                        }
                                        else {
                                            ////(@"Not matched");
                                            
                                            
                                            UIAlertView *loginalert = [[UIAlertView alloc] initWithTitle:@"" message:@"Enter Valid Phone Number" delegate:self
                                                                                       cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                            
                                            [loginalert show];
                                            
                                        }
                                        
                                    }
                                    
                                    
                                }
                                else
                                {
                                    //////(@"SUCCESS");
                                    
                                    NSString* emailRegex = @"[A-Z0-9a-z._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}";
                                    NSPredicate* emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
                                    NSString *subjectString =emailStr;
                                    
                                    
                                    if((emailStr.length!=0) && ([emailTest evaluateWithObject:subjectString] != YES))
                                    {
                                        
                                        if ([emailTest evaluateWithObject:subjectString] != YES)
                                            
                                        {
                                            
                                            
                                            UIAlertView *loginalert = [[UIAlertView alloc] initWithTitle:@"Enter Valid Email ID" message:@"abc@example.com format" delegate:self
                                                                                       cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                            
                                            [loginalert show];
                                            
                                            
                                        }
                                        
                                        
                                    }
                                    
                                    else
                                    {
                                        
                                        
                                        
                                        [self addcontectFun];
                                        
                                    }
                                }
                                
                                
                                
                            }
                            
                        }
                    }
                }
            }
        }
    }
    
    
    
}


-(void)addcontectFun
{
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/contacts/createContact",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post1234 = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *userImage=[check objectForKey:@"USER_IMAGE"];
    NSString *user=[check objectForKey:@"USERID"];
    
    if ([userImage isEqualToString:@"NULL"])
    {
        userImage=@"http://www.socialnetgateapp.com/bundles/acmeregistration/images/no-user-image.png";
    }
    
    
    //////(@"USER IMAGE:%@",userImage);
    
    
    NSString *fname=_firstName.text;
    NSString *lname=_lastname.text;
    NSString *address1text=_address1.text;
    NSString *address2text=_address2.text;
    NSString *citystr=_city.text;
    NSString *statestr=_state.text;
    NSString *zipCodeStr=_zipCode.text;
      NSString *emailStr=_emailID.text;
    
    NSUserDefaults *img=[NSUserDefaults standardUserDefaults];
    
    NSString *urlstr=[img objectForKey:@""];
    
    
    [request_post1234 setPostValue:user forKey:@"snguserid"];
    [request_post1234 setPostValue:userImage forKey:@"friend_facebookproimg"];
    [request_post1234 setPostValue:fname forKey:@"firstname"];
    [request_post1234 setPostValue:lname forKey:@"lastname"];
    [request_post1234 setPostValue:address1text forKey:@"address1"];
    [request_post1234 setPostValue:address2text forKey:@"address2"];
    [request_post1234 setPostValue:citystr forKey:@"city"];
    [request_post1234 setPostValue:statestr forKey:@"state"];
    [request_post1234 setPostValue:countrystr forKey:@"country"];
    [request_post1234 setPostValue:zipCodeStr forKey:@"zipcode"];
    [request_post1234 setPostValue:_emailID.text forKey:@"emailid"];
    [request_post1234 setPostValue:_phoneNo.text forKey:@"phoneno"];
    [request_post1234 setPostValue:@"1" forKey:@"address_entered"];
      [request_post1234 setPostValue:[Config get_token] forKey:@"apikey"];
    
    
    [request_post1234 setTimeOutSeconds:30];
    
    
    [request_post1234 setCompletionBlock:^{
        NSString *responseString23 = [request_post1234 responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        
        //////(@"RESULTS LOGIN:%@",results11);
        
        
        NSString *des=[results11 valueForKeyPath:@"header.description"];
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:des delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
        
        [_firstName resignFirstResponder];
        [_lastname resignFirstResponder];
        [_address2 resignFirstResponder];
        [_address1 resignFirstResponder];
        [_city resignFirstResponder];
        [_state resignFirstResponder];
        [_zipCode resignFirstResponder];
        
        _firstName.text=@"";
        _lastname.text=@"";
        _address2.text=@"";
        _address1.text=@"";
        _city.text=@"";
        _state.text=@"";
        _zipCode.text=@"";
        _emailID.text=@"";
        _phoneNo.text=@"";
        
        [countrybtn setTitle:@"  United States" forState:UIControlStateNormal];
        _contactAddView.hidden=YES;
        [self viewDidLoad];
        
    }];
    [request_post1234 setFailedBlock:^{
        NSError *error = [request_post1234 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1234 startAsynchronous];
}

-(IBAction)cancelContact
{
    _contactAddView.hidden=YES;
    
    [_firstName resignFirstResponder];
    [_lastname resignFirstResponder];
    [_address2 resignFirstResponder];
    [_address1 resignFirstResponder];
    [_city resignFirstResponder];
    [_state resignFirstResponder];
    [_zipCode resignFirstResponder];
    
    _firstName.text=@"";
    _lastname.text=@"";
    _address2.text=@"";
    _address1.text=@"";
    _city.text=@"";
    _state.text=@"";
    _zipCode.text=@"";
    
    [countrybtn setTitle:@"  United States" forState:UIControlStateNormal];
}

-(IBAction)editBackButton
{
    _editView.hidden=YES;
}
-(IBAction)editSaveButton
{
    
    
    
    
    if ([_editaddress1.text isEqualToString:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Enter the address" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
    }
    else
    {
        if ([_editcity.text isEqualToString:@""])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Enter the city" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alert show];
            
        }
        else
        {
            if ([_editstate.text isEqualToString:@""])
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Enter the state" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                [alert show];
            }
            else
            {
                if ([_editcountrybtn.titleLabel.text isEqualToString:@""]||[_editcountrybtn.titleLabel.text isEqualToString:@"    United States"])
                {
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Select the Country" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    
                    [alert show];
                }
                else
                {
                    if ([_editzipCode.text isEqualToString:@""])
                    {
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Enter the zip code" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        
                        [alert show];
                    }
                    else
                    {
                        if ([_editcountrybtn.titleLabel.text isEqualToString:@""]||[_editcountrybtn.titleLabel.text isEqualToString:@"    United States"]||[_editcountrybtn.titleLabel.text isEqualToString:@"United States"])
                        {
                            if ([_editemailID.text isEqualToString:@""])
                            {
                                
                                if ([_editphoneNo.text isEqualToString:@""])
                                {
                                    [self addcontectFun1];
                                }
                                else
                                {
                                    NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
                                    
                                    // NSString *phoneRegex = @"^[\+(00)][0-9]{6,14}$";
                                    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
                                    
                                    BOOL phoneValidates = [phoneTest evaluateWithObject:_editphoneNo.text];
                                    
                                    
                                    
                                    if (phoneValidates == YES){
                                        
                                        ////(@"Matched");
                                        
                                        [self addcontectFun1];
                                    }
                                    else {
                                        ////(@"Not matched");
                                        
                                        
                                        UIAlertView *loginalert = [[UIAlertView alloc] initWithTitle:@"" message:@"Enter Valid Phone Number" delegate:self
                                                                                   cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                        
                                        [loginalert show];
                                        
                                    }
                                    
                                }
                                
                                
                            }
                            else
                            {
                                //////(@"SUCCESS");
                                
                                
                                NSString* emailRegex = @"[A-Z0-9a-z._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}";
                                NSPredicate* emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
                                NSString *subjectString =_editemailID.text;
                                
                                
                                if((_editemailID.text.length!=0) && ([emailTest evaluateWithObject:subjectString] != YES))
                                {
                                    
                                    if ([emailTest evaluateWithObject:subjectString] != YES)
                                        
                                    {
                                        
                                        
                                        UIAlertView *loginalert = [[UIAlertView alloc] initWithTitle:@"Enter Valid Email ID" message:@"abc@example.com format" delegate:self
                                                                                   cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                        
                                        [loginalert show];
                                        
                                        
                                    }
                                    
                                    
                                }
                                
                                else
                                {
                                    
                                    
                                    
                                    [self addcontectFun1];
                                    
                                }
                            }
                        }
                        else
                        {
                            //////(@"SUCCESS");
                            
                            
                            
                            if ([_editemailID.text isEqualToString:@""])
                            {
                                
                                if ([_editphoneNo.text isEqualToString:@""])
                                {
                                    [self addcontectFun1];
                                }
                                else
                                {
                                    NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
                                    
                                    // NSString *phoneRegex = @"^[\+(00)][0-9]{6,14}$";
                                    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
                                    
                                    BOOL phoneValidates = [phoneTest evaluateWithObject:_editphoneNo.text];
                                    
                                    
                                    
                                    if (phoneValidates == YES){
                                        
                                        ////(@"Matched");
                                        
                                        [self addcontectFun1];
                                    }
                                    else {
                                        ////(@"Not matched");
                                        
                                        
                                        UIAlertView *loginalert = [[UIAlertView alloc] initWithTitle:@"" message:@"Enter Valid Phone Number" delegate:self
                                                                                   cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                        
                                        [loginalert show];
                                        
                                    }
                                    
                                }
                                
                                
                            }
                            else
                            {
                                //////(@"SUCCESS");
                                
                                
                                NSString* emailRegex = @"[A-Z0-9a-z._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}";
                                NSPredicate* emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
                                NSString *subjectString =_editemailID.text;
                                
                                
                                if((_editemailID.text.length!=0) && ([emailTest evaluateWithObject:subjectString] != YES))
                                {
                                    
                                    if ([emailTest evaluateWithObject:subjectString] != YES)
                                        
                                    {
                                        
                                        
                                        UIAlertView *loginalert = [[UIAlertView alloc] initWithTitle:@"Enter Valid Email ID" message:@"abc@example.com format" delegate:self
                                                                                   cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                        
                                        [loginalert show];
                                        
                                        
                                    }
                                    
                                    
                                }
                                
                                else
                                {
                                    
                                    
                                    
                                    [self addcontectFun1];
                                    
                                }
                            }
                            
                            
                            
                        }
                    }
                    
                }
            }
        }
    }
    
    
    
    
    
}
-(void)addcontectFun1
{
    
    
    
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/contacts/updateContact",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post1234 = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *userImage=[check objectForKey:@"USER_IMAGE"];
    NSString *user=[check objectForKey:@"USERID"];
    
    if ([userImage isEqualToString:@"NULL"])
    {
        userImage=@"http://www.socialnetgateapp.com/bundles/acmeregistration/images/no-user-image.png";
    }
    
    
    //////(@"USER IMAGE:%@",userImage);
    
    
    
    [request_post1234 setPostValue:selectedContactID forKey:@"id"];
    [request_post1234 setPostValue:_editaddress1.text forKey:@"address1"];
    [request_post1234 setPostValue:_editaddress2.text forKey:@"address2"];
    [request_post1234 setPostValue:_editcity.text forKey:@"city"];
      [request_post1234 setPostValue:userImage forKey:@"friend_facebookproimg"];
    [request_post1234 setPostValue:_editstate.text forKey:@"state"];
    [request_post1234 setPostValue:_editcountrybtn.titleLabel.text forKey:@"country"];
      [request_post1234 setPostValue:[Config get_token] forKey:@"apikey"];
    [request_post1234 setPostValue:@"1" forKey:@"address_entered"];
    [request_post1234 setPostValue:_editzipCode.text forKey:@"zipcode"];
    [request_post1234 setPostValue:_editemailID.text forKey:@"emailid"];
    [request_post1234 setPostValue:_editphoneNo.text forKey:@"phoneno"];
    
    
    
    [request_post1234 setTimeOutSeconds:30];
    
    
    [request_post1234 setCompletionBlock:^{
        NSString *responseString23 = [request_post1234 responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        
        ////(@"RESULTS LOGIN:%@",results11);
        
        
        NSString *des=[results11 valueForKeyPath:@"header.description"];
        
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:des delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
        
        
        [_editcountrybtn setTitle:@"  United States" forState:UIControlStateNormal];
        _contactAddView.hidden=YES;
        
        _contactAddView.hidden=YES;
        
        _address1.text=@"";
        _address2.text=@"";
        _city.text=@"";
        _state.text=@"";
        _zipCode.text=@"";
        [_editcountrybtn setTitle:@"  United States" forState:UIControlStateNormal];
        
        
        [_address1 resignFirstResponder];
        [_address2 resignFirstResponder];
        [_city resignFirstResponder];
        [_state resignFirstResponder];
        [_zipCode resignFirstResponder];
        
        
        [self viewDidLoad];
        
    }];
    [request_post1234 setFailedBlock:^{
        NSError *error = [request_post1234 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1234 startAsynchronous];
}

- (IBAction)editcountryButton:(id)sender
{
    [_editaddress1 resignFirstResponder];
    [_editaddress2 resignFirstResponder];
    [_editcity resignFirstResponder];
    [_editemailID resignFirstResponder];
    [_editphoneNo resignFirstResponder];
    [_editstate resignFirstResponder];
    [_editzipCode resignFirstResponder];
    
    _pickerBack.hidden=NO;
    
    
    /*
    
    LeveyPopListView *lplv = [[LeveyPopListView alloc] initWithTitle:@"Select country" options:countryArray handler:^(NSInteger anIndex)
                              {
                                  //////(@"%@",[NSString stringWithFormat:@"You have CLICKED %@", countryArray[anIndex]]);
                                  
                                  [_editcountrybtn setTitle:[NSString stringWithFormat:@"    %@", countryArray[anIndex]] forState:UIControlStateNormal];
                                  
                                  countrystr=countryArray[anIndex];
                                  
                              }];
    //    lplv.delegate = self;
    [lplv showInView:self.view animated:YES];
     */
}


- (IBAction)countryButton:(id)sender
{
    
    
    [_firstName resignFirstResponder];
    [_lastname resignFirstResponder];
    [_address2 resignFirstResponder];
    [_address1 resignFirstResponder];
    [_city resignFirstResponder];
    [_state resignFirstResponder];
    [_zipCode resignFirstResponder];
    
    
     _pickerBack2.hidden=NO;
    
    /*
    LeveyPopListView *lplv = [[LeveyPopListView alloc] initWithTitle:@"Select country" options:countryArray handler:^(NSInteger anIndex)
                              {
                                  //////(@"%@",[NSString stringWithFormat:@"You have CLICKED %@", countryArray[anIndex]]);
                                  
                                  [countrybtn setTitle:[NSString stringWithFormat:@"    %@", countryArray[anIndex]] forState:UIControlStateNormal];
                                  
                                  countrystr=countryArray[anIndex];
                                  
                              }];
    //    lplv.delegate = self;
    [lplv showInView:self.view animated:YES];
     */
}

#pragma mark - LeveyPopListView delegates
- (void)leveyPopListView:(LeveyPopListView *)popListView didSelectedIndex:(NSInteger)anIndex
{
    //////(@"%@",[NSString stringWithFormat:@"You have selected %@", countryArray[anIndex]]);
}
- (void)leveyPopListViewDidCancel
{
    //////(@"CANCELLED");
}

-(IBAction)addContactButton
{
    
    [_imageButton setImage:nil forState:UIControlStateNormal];
    [_imageButton setTitle:@"Select Image" forState:UIControlStateNormal];
    
    _contactAddView.hidden=NO;
    
}


- (IBAction)segmentDidChange:(id)sender
{
    
    _detailView.hidden=YES;
    _back_view.hidden=NO;
    
    [self updateSelectedSegmentLabel];
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    int check_address_enter=[address_entered_arr[indexPath.row]intValue];
    
    if (check_address_enter==0)
    {
        cell.check_image_view.hidden=YES;
        
        cell.add_image_view.hidden=NO;
        
        _editView.hidden=NO;
        
        selectedContactID=contactIDArray[indexPath.row];
        
        
        _editaddress1.text=editAddress1Arr[indexPath.row];
        _editaddress2.text=editAddress2Arr[indexPath.row];
        _editcity.text=editCityArr[indexPath.row];
        
        NSString *cou_str=editCountryArr[indexPath.row];
        
        if ([cou_str isEqualToString:@""]||cou_str==[NSNull null])
        {
            cou_str=@"United States";
        }
        
        
        [_editcountrybtn setTitle:[NSString stringWithFormat:@"  %@",cou_str] forState:UIControlStateNormal];
        _editemailID.text=editEmailArr[indexPath.row];
        _editphoneNo.text=editPhoneArr[indexPath.row];
        _editstate.text=editStateArr[indexPath.row];
        _editzipCode.text=editZipArr[indexPath.row];
        
        NSUserDefaults *imgtemp=[NSUserDefaults standardUserDefaults];
        
        [imgtemp setObject:imageThumpArray[indexPath.row] forKey:@"USER_IMAGE"];
        
        NSString *temp=[imageThumpArray objectAtIndex:indexPath.row];
        
        
       // imageURL=[imageThumpArray objectAtIndex:indexPath.row];
        
        UIImageView *tempImg=[[UIImageView alloc]init];
        [tempImg setImageWithURL:[NSURL URLWithString:temp]
                placeholderImage:[UIImage imageNamed:@"img_mt.png"]];
        
        [_imageButton1 setImage:tempImg.image forState:UIControlStateNormal];
        [_imageButton1 setTitle:nil forState:UIControlStateNormal];

    }
    else
    {
        
        cell.add_image_view.hidden=YES;
        
        //////(@"Select %d",indexPath.row);
        
        
        if ([checkmark_array[indexPath.row]isEqualToString:@"YES"])
        {
            [checkmark_array replaceObjectAtIndex:indexPath.row withObject:@"NO"];
            
            //////(@"CHECK MARK:%@",checkmark_array);
            
            
            NSString *contactID;
            
            
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/socialcards/updatecardcontacts",CONFIG_BASE_URL]];
            
            
            __block  ASIFormDataRequest *requestmethod = [ASIFormDataRequest requestWithURL:url];
            
            
            
            NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
            NSString *user=[check objectForKey:@"USERID"];
            NSString *cardID=[check objectForKey:@"CARD_ID"];
            NSString *imageID=[check objectForKey:@"IMAGE_ID"];
            //////(@"USER:%@",user);
            
            
            
            [requestmethod setPostValue:user forKey:@"userID"];
            [requestmethod setPostValue:cardID forKey:@"cardID"];
            [requestmethod setPostValue:contactIDArray[indexPath.row] forKey:@"contactid"];
            [requestmethod setPostValue:@"uncheck" forKey:@"contactStatus"];
              [requestmethod setPostValue:[Config get_token] forKey:@"apikey"];
            
            [requestmethod setTimeOutSeconds:30];
            
            
            [requestmethod setCompletionBlock:^{
                NSString *responseString23 = [requestmethod responseString];
                NSMutableData *results11 = [responseString23 JSONValue];
                
                
                
                
                ////(@"TEMPARY 11:%@",results11);
                
                //[self viewDidLoad];
                
            }];
            [requestmethod setFailedBlock:^{
                NSError *error = [requestmethod error];
                UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                    message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                          cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                
                
                
            }];
            
            [requestmethod startAsynchronous];
            
            
            
            
        }
        else
        {
            [checkmark_array replaceObjectAtIndex:indexPath.row withObject:@"YES"];
            
            //////(@"CHECK MARK:%@",checkmark_array);
            
            
            NSString *contactID;
            
            //http://apidev.socialnetgateapp.com/v1/socialcards/updatecardcontacts
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/socialcards/updatecardcontacts",CONFIG_BASE_URL]];
            
            
            __block  ASIFormDataRequest *requestmethod = [ASIFormDataRequest requestWithURL:url];
            
            
            
            NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
            NSString *user=[check objectForKey:@"USERID"];
            NSString *cardID=[check objectForKey:@"CARD_ID"];
            NSString *imageID=[check objectForKey:@"IMAGE_ID"];
            //////(@"USER:%@",user);
            
            
            
            [requestmethod setPostValue:user forKey:@"userID"];
            [requestmethod setPostValue:cardID forKey:@"cardID"];
            [requestmethod setPostValue:contactIDArray[indexPath.row] forKey:@"contactid"];
            [requestmethod setPostValue:@"check" forKey:@"contactStatus"];
              [requestmethod setPostValue:[Config get_token] forKey:@"apikey"];
            
            [requestmethod setTimeOutSeconds:30];
            
            
            [requestmethod setCompletionBlock:^{
                NSString *responseString23 = [requestmethod responseString];
                NSMutableData *results11 = [responseString23 JSONValue];
                
                
                
                ////(@"TEMPARY:%@",results11);
                
                //[self viewDidLoad];
                
            }];
            [requestmethod setFailedBlock:^{
                NSError *error = [requestmethod error];
                UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                    message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                          cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                
                
                
            }];
            
            [requestmethod startAsynchronous];
            
            
            
            
            
        }
        
        
        selectContactArray1=[[NSMutableArray alloc]init];
        
        //////(@"CONTACT ID ARR:%@",contactIDArray);
        
        
        for (int i=0; i<[contactIDArray count]; i++)
        {
            NSString *markString=checkmark_array[i];
            
            if ([markString isEqualToString:@"YES"])
            {
                [selectContactArray1 addObject:contactIDArray[i]];
            }
            
        }
        
        
        //////(@"Select :%@",selectContactArray1);
        
        
        if ([selectContactArray1 count]==0)
        {
            
            _continueBtn.enabled=NO;
            _continueBtn.titleLabel.textColor=[UIColor grayColor];
            
        }
        else
        {
            _continueBtn.enabled=YES;
            _continueBtn.titleLabel.textColor=[UIColor whiteColor];
        }
        
        
        // tag_contact=1;
        
        
        // [self nextViewButton1];
        
    }
    
    [collectionView reloadData];
    
    
    
    /*
     messageViewController *viewController=[[messageViewController alloc]initWithNibName:@"messageViewController" bundle:nil];
     [self presentViewController:viewController animated:NO completion:nil];
     */
    
    
}
-(IBAction)nextViewButton
{
    
    ////(@"CHECK:%@",checkmark_array);
    ////(@"CHECK:%@",contactIDArray);
    
    NSMutableArray *selectContactArray=[[NSMutableArray alloc]init];
    
    for (int i=0; i<[contactIDArray count]; i++)
    {
        NSString *markString=checkmark_array[i];
        
        if ([markString isEqualToString:@"YES"])
        {
            [selectContactArray addObject:contactIDArray[i]];
        }
        
    }
    
    
    //////(@"Select :%@",selectContactArray);
    
    
    if ([selectContactArray count]==0)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Select any of your friends." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
        
    }
    else
    {
        if ([selectContactArray count]>25)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Select Maximum 25 friends from this List." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alert show];
            
        }
        else
        {
            
            NSMutableString* message = [NSMutableString stringWithCapacity:30];
            
            
            
            for (int i=0; i<[selectContactArray count]; i++)
            {
                [message appendString:[NSString stringWithFormat:@"%@,",selectContactArray[i]]];
            }
            
            [message deleteCharactersInRange:NSMakeRange([message length] - 1, 1)];
            
            //////(@"MESSAGEGHGH:%@",message);
            
            
            NSString *tempISSUE=[NSString stringWithFormat:@"%@,%@",selectContactArray[0],message];
            
            
            //////(@"TEMP MESSAGE:%@",tempISSUE);
            
            
            
            // [self updateview:tempISSUE];
            
            [self updateview:message];
            
            
        }
        
    }
    
    /*
     
     
     */
}


-(void)updateview:(NSString *)getContact
{
    
    
    if (IS_IPHONE_5)
    {
        
        Listofreceiptions *viewController=[[Listofreceiptions alloc]initWithNibName:@"Listofreceiptions" bundle:nil];
        [self presentViewController:viewController animated:NO completion:nil];
        
    }
    else
    {
        
        Listofreceiptions *viewController=[[Listofreceiptions alloc]initWithNibName:@"Listofreceiptions~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:nil];
    }
    
    
    
    
    /*
     
     
     
     NSString *contactID;
     
     
     NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/socialcards/updateCard",CONFIG_BASE_URL]];
     
     
     __block  ASIFormDataRequest *requestmethod = [ASIFormDataRequest requestWithURL:url];
     
     
     
     NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
     NSString *user=[check objectForKey:@"USERID"];
     NSString *cardID=[check objectForKey:@"CARD_ID"];
     NSString *imageID=[check objectForKey:@"IMAGE_ID"];
     //////(@"USER:%@",user);
     
     
     
     [requestmethod setPostValue:user forKey:@"userID"];
     [requestmethod setPostValue:cardID forKey:@"id"];
     [requestmethod setPostValue:getContact forKey:@"contactid"];
     
     
     [requestmethod setTimeOutSeconds:30];
     
     
     [requestmethod setCompletionBlock:^{
     NSString *responseString23 = [requestmethod responseString];
     NSMutableData *results11 = [responseString23 JSONValue];
     
     NSString *cardID= [results11 valueForKeyPath:@"cardID"];
     
     //////(@"CARD_ID");
     
     NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
     [check setObject:cardID forKey:@"CARD_ID"];
     
     
     //////(@"TEMPARY:%@",results11);
     
     if (IS_IPHONE_5)
     {
     
     Listofreceiptions *viewController=[[Listofreceiptions alloc]initWithNibName:@"Listofreceiptions" bundle:nil];
     [self presentViewController:viewController animated:NO completion:nil];
     
     }
     else
     {
     
     Listofreceiptions *viewController=[[Listofreceiptions alloc]initWithNibName:@"Listofreceiptions~iPhone" bundle:nil];
     [self presentViewController:viewController animated:NO completion:nil];
     }
     
     
     }];
     [requestmethod setFailedBlock:^{
     NSError *error = [requestmethod error];
     UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
     message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
     cancelButtonTitle:@"OK" otherButtonTitles:nil];
     
     
     
     
     }];
     
     [requestmethod startAsynchronous];
     
     */
    
    
    
}


-(IBAction)nextViewButton1
{
    
    ////(@"CHECK:%@",checkmark_array);
    ////(@"CHECK:%@",contactIDArray);
    
    NSMutableArray *selectContactArray=[[NSMutableArray alloc]init];
    
    for (int i=0; i<[contactIDArray count]; i++)
    {
        NSString *markString=checkmark_array[i];
        
        if ([markString isEqualToString:@"YES"])
        {
            [selectContactArray addObject:contactIDArray[i]];
        }
        
    }
    
    
    //////(@"Select :%@",selectContactArray);
    
    
    if ([selectContactArray count]==0)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Select any of your friends." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
        
    }
    else
    {
        if ([selectContactArray count]>25)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Select Maximum 25 friends from this List." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alert show];
            
        }
        else
        {
            
            NSMutableString* message = [NSMutableString stringWithCapacity:30];
            
            
            
            for (int i=0; i<[selectContactArray count]; i++)
            {
                [message appendString:[NSString stringWithFormat:@"%@,",selectContactArray[i]]];
            }
            
            [message deleteCharactersInRange:NSMakeRange([message length] - 1, 1)];
            
            //////(@"MESSAGEGHGH:%@",message);
            
            
            NSString *tempISSUE=[NSString stringWithFormat:@"%@,%@",selectContactArray[0],message];
            
            
            //////(@"TEMP MESSAGE:%@",tempISSUE);
            
            
            
            // [self updateview:tempISSUE];
            
            [self updateview1:message];
            
            
        }
        
    }
    
    /*
     
     
     */
}


-(void)updateview1:(NSString *)getContact
{
    NSString *contactID;
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/socialcards/updateCard",CONFIG_BASE_URL]];
    
    
    __block  ASIFormDataRequest *requestmethod = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    NSString *cardID=[check objectForKey:@"CARD_ID"];
    NSString *imageID=[check objectForKey:@"IMAGE_ID"];
    //////(@"USER:%@",user);
    
    
    
    [requestmethod setPostValue:user forKey:@"userID"];
    [requestmethod setPostValue:cardID forKey:@"id"];
    [requestmethod setPostValue:getContact forKey:@"contactid"];
      [requestmethod setPostValue:[Config get_token] forKey:@"apikey"];
    
    [requestmethod setTimeOutSeconds:30];
    
    
    [requestmethod setCompletionBlock:^{
        NSString *responseString23 = [requestmethod responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        NSString *cardID= [results11 valueForKeyPath:@"cardID"];
        
        //////(@"CARD_ID");
        
        NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
        [check setObject:cardID forKey:@"CARD_ID"];
        
        
        //////(@"TEMPARY:%@",results11);
        
        //[self viewDidLoad];
        
    }];
    [requestmethod setFailedBlock:^{
        NSError *error = [requestmethod error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [requestmethod startAsynchronous];
    
    
    
    
    
}



-(IBAction)backButton
{
    
    
    NSUserDefaults *checkVal=[NSUserDefaults standardUserDefaults];
    
    
    NSString *getFrom=[checkVal objectForKey:@"VIEW_REFERENCE"];
    
    
    if ([getFrom isEqualToString:@"SAVED_CARDS"])
    {
        
        /*
         NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
         NSString *imageURL = [checkVal objectForKey:@"IMAGE_ID_REFERENCE"];
         
         NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
         UIImage *image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imageURL]]];
         
         NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"cropImage.png"]];
         if(image != NULL)
         {
         //Store the image in Document
         
         
         NSData *imageData = UIImagePNGRepresentation(image);
         [imageData writeToFile: imagePath atomically:YES];
         }
         
         
         */
        
        // getCardID=[checkVal objectForKey:@"CARD_ID_REFERENCE"];
        // getcardname=[checkVal objectForKey:@"CARD_NAME_REFERENCE"];
        
        
        ////////(@"TEMP:%@",temp);
        
        //NSUserDefaults *imgtemp=[NSUserDefaults standardUserDefaults];
        
        //[imgtemp setObject:temp forKey:@"EDIT_IMAGE"];
        
    }
    
    
    
    if ([comingView isEqualToString:@"SAVED_CARD"])
    {
        
        if (IS_IPHONE_5)
        {
            savedCards *viewController=[[savedCards alloc]initWithNibName:@"savedCards" bundle:nil];
            [self presentViewController:viewController animated:NO completion:^{
                ////////(@"SUCCESS");
            }
             ];
            
        }
        else
        {
            savedCards *viewController=[[savedCards alloc]initWithNibName:@"savedCards~iPhone" bundle:nil];
            [self presentViewController:viewController animated:NO completion:^{
                ////////(@"SUCCESS");
            }
             ];
            
        }
        
    }
    else
    {
        
        if (IS_IPHONE_5)
        {
            
            audioViewController *viewController=[[audioViewController alloc]initWithNibName:@"audioViewController" bundle:nil];
            [self presentViewController:viewController animated:NO completion:^{
                ////////(@"SUCCESS");
            }
             ];
        }
        else
        {
            
            audioViewController *viewController=[[audioViewController alloc]initWithNibName:@"audioViewController~iPhone" bundle:nil];
            [self presentViewController:viewController animated:NO completion:^{
                ////////(@"SUCCESS");
            }
             ];
        }
        
        
    }
}


- (void)updateSelectedSegmentLabel
{
    
    
    
    self.selectedSegmentLabel.font = [UIFont boldSystemFontOfSize:self.selectedSegmentLabel.font.pointSize];
    self.selectedSegmentLabel.text = [NSString stringWithFormat:@"%d", self.segmentedControl.selectedSegmentIndex];
    
    
    if (self.segmentedControl.selectedSegmentIndex==0)
    {
        
    }
    
    if (self.segmentedControl.selectedSegmentIndex==1)
    {
        
    }
    
    
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, .5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^(void)
                   {
                       self.selectedSegmentLabel.font = [UIFont systemFontOfSize:self.selectedSegmentLabel.font.pointSize];
                   });
}
-(IBAction)filterButtonClick
{
    NSArray *menuItems =
    @[
      
      [KxMenuItem menuItem:@"Filter Menu"
                     image:nil
                    target:nil
                    action:NULL],
      
      [KxMenuItem menuItem:@"All Deals"
                     image:[UIImage imageNamed:@"check_icon"]
                    target:self
                    action:@selector(pushMenuItem:)],
      
      [KxMenuItem menuItem:@"Populer"
                     image:[UIImage imageNamed:@"UIButton"]
                    target:self
                    action:@selector(pushMenuItem:)],
      
      [KxMenuItem menuItem:@"Nearest"
                     image:[UIImage imageNamed:@"UIButton"]
                    target:self
                    action:@selector(pushMenuItem:)],
      
      [KxMenuItem menuItem:@"Price"
                     image:[UIImage imageNamed:@"UIButton"]
                    target:self
                    action:@selector(pushMenuItem:)],
      
      [KxMenuItem menuItem:@"Selling"
                     image:[UIImage imageNamed:@"UIButton"]
                    target:self
                    action:@selector(pushMenuItem:)],
      ];
    
    KxMenuItem *first = menuItems[0];
    first.foreColor = [UIColor colorWithRed:47/255.0f green:112/255.0f blue:225/255.0f alpha:1.0];
    first.alignment = NSTextAlignmentCenter;
    
    [KxMenu showMenuInView:self.view
                  fromRect:CGRectMake(266, 75, 50, 50)
                 menuItems:menuItems];
}
- (void) pushMenuItem:(id)sender
{
    //////(@"%@", sender);
    NSString *string=[NSString stringWithFormat:@"%@",sender];
    ////////(@"HELLO:%@",string);
    
    
    
    
    
}

-(IBAction)browseImage
{
    
    [_firstName resignFirstResponder];
    [_lastname resignFirstResponder];
    [_address2 resignFirstResponder];
    [_address1 resignFirstResponder];
    [_city resignFirstResponder];
    [_state resignFirstResponder];
    [_zipCode resignFirstResponder];
    
    NSString *actionSheetTitle = @"Select your Picture"; //Action Sheet Title
    // NSString *destructiveTitle = @"Destructive Button"; //Action Sheet Button Titles
    NSString *other1 = @"Camera";
    NSString *other2 = @"Album";
    
    NSString *cancelTitle = @"Cancel";
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:actionSheetTitle
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:other1, other2,  nil];
    [actionSheet showInView:self.view];
    
    
}

-(IBAction)browseImage1
{
    
    [_firstName resignFirstResponder];
    [_lastname resignFirstResponder];
    [_address2 resignFirstResponder];
    [_address1 resignFirstResponder];
    [_city resignFirstResponder];
    [_state resignFirstResponder];
    [_zipCode resignFirstResponder];
    
    NSString *actionSheetTitle = @"Select your Picture"; //Action Sheet Title
    // NSString *destructiveTitle = @"Destructive Button"; //Action Sheet Button Titles
    NSString *other1 = @"Camera";
    NSString *other2 = @"Album";
    
    NSString *cancelTitle = @"Cancel";
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:actionSheetTitle
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:other1, other2,  nil];
    [actionSheet showInView:self.view];
    
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    if (buttonIndex==0)
    {
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            
            [self presentViewController:picker animated:YES completion:NULL];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"The device does not have camera" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            // [alert release];
        }
        
    }
    else  if (buttonIndex==1)
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:NULL];
        
    }
    
    
}


-(void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary*)info
{
    NSData *imagedata = UIImageJPEGRepresentation([info objectForKey:@"UIImagePickerControllerEditedImage" ],1);
    UIImage *image = [[UIImage alloc] initWithData:imagedata];
    
    
  //  image = [self imageWithImageSimple:image scaledToSize:CGSizeMake(240, 240)];
    
    
    float ori_height=image.size.height;
    float ori_width=image.size.width;
    
    NSLog(@"H:%f|W:%f",ori_height,ori_width);
    
    
    float height_ratio1=ori_height/4;
    float width_ratio1=ori_width/4;
    
    NSLog(@"HJ:%f|WJ:%f",height_ratio1,width_ratio1);
    
    
    
    image = [self imageWithImageSimple:image scaledToSize:CGSizeMake(width_ratio1, height_ratio1)];

    
    
    [_imageButton setImage:image forState:UIControlStateNormal];
    [_imageButton setTitle:nil forState:UIControlStateNormal];
    
    [_imageButton1 setImage:image forState:UIControlStateNormal];
    [_imageButton1 setTitle:nil forState:UIControlStateNormal];
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/files/postProImgfile",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
    
    
    //[request_post setFile:@"/Users/apple/Desktop/Free-Grammar-Ebook-Level-2.pdf" forKey:@"filename"];
    
    //UIImage * ourImage = [UIImage imageNamed:@"rajini-bday2.jpg"];
    NSData * ourImageData = UIImageJPEGRepresentation(image, 100);
    
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"HH:mm:ss zzz"];
    NSString *dateString = [dateFormat stringFromDate:date];
    
    //////(@"DATE STRING:%@",dateString);
    
    NSString *newString = [dateString stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *newString1 = [newString stringByReplacingOccurrencesOfString:@":" withString:@""];
    NSString *newString2 = [newString1 stringByReplacingOccurrencesOfString:@"GMT" withString:@""];
    NSString *newString3 = [newString2 stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    NSString *imageName=[NSString stringWithFormat:@"%@.jpg",newString3];
    
    
    
    [request_post addData:ourImageData withFileName:imageName andContentType:@"image/jpeg" forKey:@"file"];
    
    [request_post setTimeOutSeconds:60];
    
    
    [request_post setCompletionBlock:^{
        // Use when fetching text data
        NSString *responseString = [request_post responseString];
        
        
        //////(@"RES:%@",responseString);
        
        NSMutableData *results11 = [responseString JSONValue];
        
        
        NSLog(@"RESULTS LOGIN:%@",results11);
        
        
        NSString *temp= [results11 valueForKeyPath:@"photos.profile_img_url"];
        
        
        NSUserDefaults *imgtemp=[NSUserDefaults standardUserDefaults];
        
        [imgtemp setObject:temp forKey:@"USER_IMAGE"];
        
        
        
        
        if ([temp length]==0) {
            
            NSString *temp= [results11 valueForKeyPath:@"error.text"];
            
            
            NSUserDefaults *imgtemp=[NSUserDefaults standardUserDefaults];
            
            [imgtemp setObject:@"NULL" forKey:@"USER_IMAGE"];
            
            [_imageButton setImage:nil forState:UIControlStateNormal];
            [_imageButton setTitle:@"Select Image" forState:UIControlStateNormal];
            
        }
        
        // Use when fetching binary data
        // NSData *responseData = [request_post responseData];
    }];
    [request_post setFailedBlock:^{
        NSError *error = [request_post error];
        
        
        
        
    }];
    [request_post startAsynchronous];
    
    
    [picker dismissModalViewControllerAnimated:YES];
    
    
    
    
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissModalViewControllerAnimated:YES];
}


-(IBAction)meButton
{
    if (IS_IPHONE_5) {
        MeViewController *viewController=[[MeViewController alloc]initWithNibName:@"MeViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            ////////(@"SUCCESS");
        }
         ];
    }
    else
    {
        
        MeViewController *viewController=[[MeViewController alloc]initWithNibName:@"MeViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            ////////(@"SUCCESS");
        }
         ];
    }
    
}
-(IBAction)dealsButton
{
    dealsViewController *viewController=[[dealsViewController alloc]initWithNibName:@"dealsViewController" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        ////////(@"SUCCESS");
    }
     ];
}
-(IBAction)socialButton
{
    if (IS_IPHONE_5) {
        netCoinsViewController *viewController=[[netCoinsViewController alloc]initWithNibName:@"netCoinsViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            ////////(@"SUCCESS");
        }
         ];
    }
    else
    {
        netCoinsViewController *viewController=[[netCoinsViewController alloc]initWithNibName:@"netCoinsViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            ////////(@"SUCCESS");
        }
         ];
    }
    
}
-(IBAction)netCoinsButton
{
    if (IS_IPHONE_5) {
        socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            ////////(@"SUCCESS");
        }
         ];
    }
    else
    {
        socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            ////////(@"SUCCESS");
        }
         ];
    }
    
}
-(IBAction)myBusinessButton
{
    myBusinessViewController *viewController=[[myBusinessViewController alloc]initWithNibName:@"myBusinessViewController" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        ////////(@"SUCCESS");
    }
     ];
}


-(IBAction)searchClickButton
{
    [_searchTxtEield resignFirstResponder];
    
    
        
        NSString *getString=_searchTxtEield.text;
        
        
        if ([getString isEqualToString:@""])
        {
            [self viewDidLoad];
        }
        else
        {
            
            
            
            imageThumpArray=[[NSMutableArray alloc]init];
            FnameArray=[[NSMutableArray alloc]init];
            LnameArray=[[NSMutableArray alloc]init];
            checkmark_array=[[NSMutableArray alloc]init];
            contactIDArray=[[NSMutableArray alloc]init];
            checked_status_arr=[[NSMutableArray alloc]init];
            address_entered_arr=[[NSMutableArray alloc]init];
            
            
            editAddress1Arr=[[NSMutableArray alloc]init];
            editAddress2Arr=[[NSMutableArray alloc]init];
            editCityArr=[[NSMutableArray alloc]init];
            editCountryArr=[[NSMutableArray alloc]init];
            editEmailArr=[[NSMutableArray alloc]init];
            editPhoneArr=[[NSMutableArray alloc]init];
            editStateArr=[[NSMutableArray alloc]init];
            editZipArr=[[NSMutableArray alloc]init];
            
            contactSourceArray=[[NSMutableArray alloc]init];
            
            
            
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/contacts/searchContacts",CONFIG_BASE_URL]];
            
            __block  ASIFormDataRequest *request_post1234 = [ASIFormDataRequest requestWithURL:url];
            
            
            
            NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
            NSString *user=[check objectForKey:@"USERID"];
            NSString *access=[check objectForKey:@"ACCESS_TOKEN"];
            
            NSString *cardID=[check objectForKey:@"CARD_ID"];
            
            ////(@"USER:%@",cardID);
            
            
            
            
            [request_post1234 setPostValue:user forKey:@"snguserid"];
            [request_post1234 setPostValue:getString forKey:@"searchCriteriaText"];
            [request_post1234 setPostValue:@"1000" forKey:@"item_count"];
            [request_post1234 setPostValue:@"0" forKey:@"current_count"];
            [request_post1234 setPostValue:[Config get_token] forKey:@"apikey"];
            [request_post1234 setPostValue:cardID forKey:@"cardID"];
            
            [request_post1234 setTimeOutSeconds:30];
            
            [request_post1234 setCompletionBlock:^{
                NSString *responseString23 = [request_post1234 responseString];
                NSMutableData *results11 = [responseString23 JSONValue];
                
                
                ////(@"RESULTS LOGIN:%@",results11);
                
                
                NSArray *temp= [results11 valueForKeyPath:@"contacts"];
                
                
                //////(@"TEMPARY CONTACTS:%@",temp);
                
                for(NSDictionary *value in temp)
                {
                    [imageThumpArray  addObject:[value valueForKey:@"friend_facebookproimg"]];
                    [FnameArray  addObject:[value valueForKey:@"firstname"]];
                    [LnameArray  addObject:[value valueForKey:@"lastname"]];
                    [contactIDArray  addObject:[value valueForKey:@"id"]];
                    
                    [address_entered_arr  addObject:[value valueForKey:@"address_entered"]];
                    
                    
                    
                    [editZipArr  addObject:[value valueForKey:@"zipcode"]];
                    [editStateArr  addObject:[value valueForKey:@"state"]];
                    [editPhoneArr  addObject:[value valueForKey:@"phoneno"]];
                    [editEmailArr  addObject:[value valueForKey:@"emailid"]];
                    
                    [editCountryArr  addObject:[value valueForKey:@"country"]];
                    [editCityArr  addObject:[value valueForKey:@"city"]];
                    [editAddress2Arr  addObject:[value valueForKey:@"address2"]];
                    [editAddress1Arr  addObject:[value valueForKey:@"address1"]];
                    
                    [contactSourceArray  addObject:[value valueForKey:@"src_contact"]];
                    
                    // [address_entered_arr  addObject:[value valueForKey:@"address_entered"]];
                    
                    
                    
                    
                    NSString *checked=[value valueForKey:@"checked_status"];
                    
                    if ([checked isEqualToString:@""])
                    {
                        [checkmark_array addObject:@"NO"];
                    }
                    else
                    {
                        [checkmark_array addObject:@"YES"];
                    }
                    
                    [checked_status_arr  addObject:[value valueForKey:@"checked_status"]];
                    
                }
                
                /*
                 
                 //////(@"ARRAY:%@",contactIDArray);
                 
                 for (int i=0; i<[imageThumpArray count]; i++)
                 {
                 
                 [checkmark_array addObject:@"NO"];
                 
                 }
                 */
                
                
                selectContactArray1=[[NSMutableArray alloc]init];
                
                ////////(@"CONTACT ID ARR:%@",contactIDArray);
                
                
                for (int i=0; i<[contactIDArray count]; i++)
                {
                    NSString *markString=checkmark_array[i];
                    
                    if ([markString isEqualToString:@"YES"])
                    {
                        [selectContactArray1 addObject:contactIDArray[i]];
                    }
                    
                }
                
                
                // //////(@"Select :%@",selectContactArray1);
                
                
                if ([selectContactArray1 count]==0)
                {
                    
                    _continueBtn.enabled=NO;
                    _continueBtn.titleLabel.textColor=[UIColor grayColor];
                    
                }
                else
                {
                    _continueBtn.enabled=YES;
                    _continueBtn.titleLabel.textColor=[UIColor whiteColor];
                }
                
                
                //////(@"ADD:%d",[imageThumpArray count]);
                
                
                [collectionView reloadData];
                
                // [self getCountry];
                
                
            }];
            [request_post1234 setFailedBlock:^{
                NSError *error = [request_post1234 error];
                UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                    message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                          cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                
                
                
            }];
            
            [request_post1234 startAsynchronous];
            
                  }
    }


- (BOOL)textFieldShouldReturn: (UITextField *)textField
{
    
    [textField resignFirstResponder];
    
    if (textField==_searchTxtEield)
    {
        
        
        NSString *getString=_searchTxtEield.text;
        
        
        if ([getString isEqualToString:@""])
        {
            [self viewDidLoad];
        }
        else
        {
            
            
            
            imageThumpArray=[[NSMutableArray alloc]init];
            FnameArray=[[NSMutableArray alloc]init];
            LnameArray=[[NSMutableArray alloc]init];
            checkmark_array=[[NSMutableArray alloc]init];
            contactIDArray=[[NSMutableArray alloc]init];
            checked_status_arr=[[NSMutableArray alloc]init];
            address_entered_arr=[[NSMutableArray alloc]init];
            
            
            editAddress1Arr=[[NSMutableArray alloc]init];
            editAddress2Arr=[[NSMutableArray alloc]init];
            editCityArr=[[NSMutableArray alloc]init];
            editCountryArr=[[NSMutableArray alloc]init];
            editEmailArr=[[NSMutableArray alloc]init];
            editPhoneArr=[[NSMutableArray alloc]init];
            editStateArr=[[NSMutableArray alloc]init];
            editZipArr=[[NSMutableArray alloc]init];
            
            contactSourceArray=[[NSMutableArray alloc]init];
            
            
            
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/contacts/searchContacts",CONFIG_BASE_URL]];
            
            __block  ASIFormDataRequest *request_post1234 = [ASIFormDataRequest requestWithURL:url];
            
            
            
            NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
            NSString *user=[check objectForKey:@"USERID"];
            NSString *access=[check objectForKey:@"ACCESS_TOKEN"];
            
            NSString *cardID=[check objectForKey:@"CARD_ID"];
            
            ////(@"USER:%@",cardID);
            
            
            
            
            [request_post1234 setPostValue:user forKey:@"snguserid"];
            [request_post1234 setPostValue:getString forKey:@"searchCriteriaText"];
            [request_post1234 setPostValue:@"1000" forKey:@"item_count"];
            [request_post1234 setPostValue:@"0" forKey:@"current_count"];
              [request_post1234 setPostValue:[Config get_token] forKey:@"apikey"];
            [request_post1234 setPostValue:cardID forKey:@"cardID"];
            
            [request_post1234 setTimeOutSeconds:30];
            
            [request_post1234 setCompletionBlock:^{
                NSString *responseString23 = [request_post1234 responseString];
                NSMutableData *results11 = [responseString23 JSONValue];
                
                
                ////(@"RESULTS LOGIN:%@",results11);
                
                
                NSArray *temp= [results11 valueForKeyPath:@"contacts"];
                
                
                //////(@"TEMPARY CONTACTS:%@",temp);
                
                for(NSDictionary *value in temp)
                {
                    [imageThumpArray  addObject:[value valueForKey:@"friend_facebookproimg"]];
                    [FnameArray  addObject:[value valueForKey:@"firstname"]];
                    [LnameArray  addObject:[value valueForKey:@"lastname"]];
                    [contactIDArray  addObject:[value valueForKey:@"id"]];
                    
                    [address_entered_arr  addObject:[value valueForKey:@"address_entered"]];
                    
                    
                    
                    [editZipArr  addObject:[value valueForKey:@"zipcode"]];
                    [editStateArr  addObject:[value valueForKey:@"state"]];
                    [editPhoneArr  addObject:[value valueForKey:@"phoneno"]];
                    [editEmailArr  addObject:[value valueForKey:@"emailid"]];
                    
                    [editCountryArr  addObject:[value valueForKey:@"country"]];
                    [editCityArr  addObject:[value valueForKey:@"city"]];
                    [editAddress2Arr  addObject:[value valueForKey:@"address2"]];
                    [editAddress1Arr  addObject:[value valueForKey:@"address1"]];
                    
                    [contactSourceArray  addObject:[value valueForKey:@"src_contact"]];
                    
                    // [address_entered_arr  addObject:[value valueForKey:@"address_entered"]];
                    
                    
                    
                    
                    NSString *checked=[value valueForKey:@"checked_status"];
                    
                    if ([checked isEqualToString:@""])
                    {
                        [checkmark_array addObject:@"NO"];
                    }
                    else
                    {
                        [checkmark_array addObject:@"YES"];
                    }
                    
                    [checked_status_arr  addObject:[value valueForKey:@"checked_status"]];
                    
                }
                
                /*
                 
                 //////(@"ARRAY:%@",contactIDArray);
                 
                 for (int i=0; i<[imageThumpArray count]; i++)
                 {
                 
                 [checkmark_array addObject:@"NO"];
                 
                 }
                 */
                
                
                selectContactArray1=[[NSMutableArray alloc]init];
                
                ////////(@"CONTACT ID ARR:%@",contactIDArray);
                
                
                for (int i=0; i<[contactIDArray count]; i++)
                {
                    NSString *markString=checkmark_array[i];
                    
                    if ([markString isEqualToString:@"YES"])
                    {
                        [selectContactArray1 addObject:contactIDArray[i]];
                    }
                    
                }
                
                
                // //////(@"Select :%@",selectContactArray1);
                
                
                if ([selectContactArray1 count]==0)
                {
                    
                    _continueBtn.enabled=NO;
                    _continueBtn.titleLabel.textColor=[UIColor grayColor];
                    
                }
                else
                {
                    _continueBtn.enabled=YES;
                    _continueBtn.titleLabel.textColor=[UIColor whiteColor];
                }
                
                
                //////(@"ADD:%d",[imageThumpArray count]);
                
                
                [collectionView reloadData];
                
                // [self getCountry];
                
                
            }];
            [request_post1234 setFailedBlock:^{
                NSError *error = [request_post1234 error];
                UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                    message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                          cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                
                
                
            }];
            
            [request_post1234 startAsynchronous];
            
            /*
             imageThumpArray=[[NSMutableArray alloc]init];
             FnameArray=[[NSMutableArray alloc]init];
             LnameArray=[[NSMutableArray alloc]init];
             checkmark_array=[[NSMutableArray alloc]init];
             contactIDArray=[[NSMutableArray alloc]init];
             checked_status_arr=[[NSMutableArray alloc]init];
             address_entered_arr=[[NSMutableArray alloc]init];
             
             
             editAddress1Arr=[[NSMutableArray alloc]init];
             editAddress2Arr=[[NSMutableArray alloc]init];
             editCityArr=[[NSMutableArray alloc]init];
             editCountryArr=[[NSMutableArray alloc]init];
             editEmailArr=[[NSMutableArray alloc]init];
             editPhoneArr=[[NSMutableArray alloc]init];
             editStateArr=[[NSMutableArray alloc]init];
             editZipArr=[[NSMutableArray alloc]init];
             
             contactSourceArray=[[NSMutableArray alloc]init];
             
             
             
             
             NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/contacts/searchContacts",CONFIG_BASE_URL]];
             
             __block  ASIFormDataRequest *request_post1234 = [ASIFormDataRequest requestWithURL:url];
             
             
             
             NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
             NSString *user=[check objectForKey:@"USERID"];
             NSString *access=[check objectForKey:@"ACCESS_TOKEN"];
             
             NSString *cardID=[check objectForKey:@"CARD_ID"];
             
             ////(@"USER:%@",cardID);
             
             
             
             
             [request_post1234 setPostValue:user forKey:@"snguserid"];
             [request_post1234 setPostValue:getString forKey:@"searchCriteriaText"];
             [request_post1234 setPostValue:@"1000" forKey:@"item_count"];
             [request_post1234 setPostValue:@"0" forKey:@"current_count"];
             
             [request_post1234 setPostValue:cardID forKey:@"cardID"];
             
             [request_post1234 setTimeOutSeconds:30];
             
             
             [request_post1234 setCompletionBlock:^{
             NSString *responseString23 = [request_post1234 responseString];
             NSMutableData *results11 = [responseString23 JSONValue];
             
             
             ////////(@"RESULTS LOGIN:%@",results11);
             
             
             NSArray *temp= [results11 valueForKeyPath:@"contacts"];
             
             
             ////(@"TEMPARY:%@",temp);
             
             for(NSDictionary *value in temp)
             {
             [imageThumpArray  addObject:[value valueForKey:@"friend_facebookproimg"]];
             [FnameArray  addObject:[value valueForKey:@"firstname"]];
             [LnameArray  addObject:[value valueForKey:@"lastname"]];
             [contactIDArray  addObject:[value valueForKey:@"id"]];
             
             [address_entered_arr  addObject:[value valueForKey:@"address_entered"]];
             
             
             [contactSourceArray  addObject:[value valueForKey:@"src_contact"]];
             
             [editZipArr  addObject:[value valueForKey:@"zipcode"]];
             [editStateArr  addObject:[value valueForKey:@"state"]];
             [editPhoneArr  addObject:[value valueForKey:@"phoneno"]];
             [editEmailArr  addObject:[value valueForKey:@"emailid"]];
             
             [editCountryArr  addObject:[value valueForKey:@"country"]];
             [editCityArr  addObject:[value valueForKey:@"city"]];
             [editAddress2Arr  addObject:[value valueForKey:@"address2"]];
             [editAddress1Arr  addObject:[value valueForKey:@"address1"]];
             
             NSString *checked=[value valueForKey:@"checked_status"];
             
             if ([checked isEqualToString:@""])
             {
             [checkmark_array addObject:@"NO"];
             }
             else
             {
             [checkmark_array addObject:@"YES"];
             }
             
             [checked_status_arr  addObject:[value valueForKey:@"checked_status"]];
             
             }
             
             
             //////(@"ARRAY:%@",imageThumpArray);
             
             selectContactArray1=[[NSMutableArray alloc]init];
             
             ////////(@"CONTACT ID ARR:%@",contactIDArray);
             
             
             for (int i=0; i<[contactIDArray count]; i++)
             {
             NSString *markString=checkmark_array[i];
             
             if ([markString isEqualToString:@"YES"])
             {
             [selectContactArray1 addObject:contactIDArray[i]];
             }
             
             }
             
             
             // //////(@"Select :%@",selectContactArray1);
             
             
             if ([selectContactArray1 count]==0)
             {
             
             _continueBtn.enabled=NO;
             _continueBtn.titleLabel.textColor=[UIColor grayColor];
             
             }
             else
             {
             _continueBtn.enabled=YES;
             _continueBtn.titleLabel.textColor=[UIColor whiteColor];
             }
             
             
             //////(@"ADD:%d",[imageThumpArray count]);
             
             
             [collectionView reloadData];
             
             [self getCountry];
             
             
             }];
             [request_post1234 setFailedBlock:^{
             NSError *error = [request_post1234 error];
             UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
             message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
             cancelButtonTitle:@"OK" otherButtonTitles:nil];
             
             
             
             
             }];
             
             [request_post1234 startAsynchronous];
             
             */
        }
    }
    
    
    return YES;
    
    
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up

{
	
    int txtPosition = (textField.frame.origin.y - 200);
	
    const int movementDistance = (txtPosition < 0 ? 0 : txtPosition); // tweak as needed
	
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
	
    [UIView setAnimationBeginsFromCurrentState: YES];
	
    [UIView setAnimationDuration: movementDuration];
	
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
	
    [UIView commitAnimations];
	
}
- (void)textFieldDidBeginEditing:(UITextField *)textField

{
    _pickerBack.hidden=YES;
    _pickerBack2.hidden=YES;
    
    [self animateTextField: textField up: YES];
    
}


- (void)textFieldDidEndEditing:(UITextField *)textField

{
	
    [self animateTextField: textField up: NO];
   	
}

#ifdef IOS_OLDER_THAN_6
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return (toInterfaceOrientation == UIInterfaceOrientationMaskPortrait);
}
#endif
#ifdef IOS_NEWER_OR_EQUAL_TO_6
-(BOOL)shouldAutorotate {
    return YES;
}
- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
#endif

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
