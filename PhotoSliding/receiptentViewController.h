//
//  dealsViewController.h
//  PhotoSliding
//
//  Created by ephronsystems on 6/27/13.
//
//

#import <UIKit/UIKit.h>
#import "LeveyPopListView.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>



@interface receiptentViewController : GAITrackedViewController<UICollectionViewDelegate,UICollectionViewDataSource,LeveyPopListViewDelegate,UIAlertViewDelegate,UIActionSheetDelegate>
@property(retain,nonatomic)IBOutlet UICollectionView *collectionView;
@property (retain, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (weak, nonatomic) IBOutlet UILabel *selectedSegmentLabel;
@property (nonatomic,retain)IBOutlet UITableView *avilableTableView;
@property (retain, nonatomic) IBOutlet UITextField *searchTxtEield;
@property (weak, nonatomic) IBOutlet UIButton *filterButton;
@property (weak, nonatomic) IBOutlet UIView  *detailView;
@property (weak, nonatomic) IBOutlet UIView  *back_view;
@property (weak, nonatomic) IBOutlet UITextField *address1;
@property (weak, nonatomic) IBOutlet UITextField *address2;
@property (weak, nonatomic) IBOutlet UITextField *zipCode;
@property (weak, nonatomic) IBOutlet UITextField *firstName;
@property (weak, nonatomic) IBOutlet UITextField *lastname;
@property (weak, nonatomic) IBOutlet UITextField *city;
@property (weak, nonatomic) IBOutlet UITextField *state;
@property (weak, nonatomic) IBOutlet UIButton *countrybtn;
@property (weak, nonatomic) IBOutlet UIView  *contactAddView;
@property (weak, nonatomic) IBOutlet UITextField *emailID;
@property (weak, nonatomic) IBOutlet UITextField *phoneNo;
@property (weak, nonatomic) IBOutlet UIButton *continueBtn;

@property (weak, nonatomic) IBOutlet UIButton *imageButton;
-(IBAction)browseImage;

@property (weak, nonatomic) IBOutlet UIButton *imageButton1;
-(IBAction)browseImage1;
-(IBAction)referesh;

@property(nonatomic,retain)UIAlertView *firstalert;

@property (weak, nonatomic) IBOutlet UIView  *editView;

@property(nonatomic,retain)NSString *comingView;


@property (weak, nonatomic) IBOutlet UIButton  *profileButton;
-(IBAction)saveContact;
-(IBAction)cancelContact;
- (IBAction)countryButton:(id)sender;

- (IBAction)segmentDidChange:(id)sender;
-(IBAction)meButton;
-(IBAction)dealsButton;
-(IBAction)netCoinsButton;
-(IBAction)socialButton;
-(IBAction)myBusinessButton;
-(IBAction)filterButtonClick;
-(IBAction)dealbackButtonClick;
-(IBAction)settingsButton;
-(IBAction)backButton;
-(IBAction)importContacts;
-(IBAction)nextViewButton;
-(IBAction)searchClickButton;
-(IBAction)addContactButton;


-(IBAction)editBackButton;
-(IBAction)editSaveButton;


@property (weak, nonatomic) IBOutlet UIPickerView *picker1;

@property (weak, nonatomic) IBOutlet UIView *pickerBack;
-(IBAction)donePicker;


@property (weak, nonatomic) IBOutlet UIPickerView *picker2;
@property (weak, nonatomic) IBOutlet UIView *pickerBack2;
-(IBAction)donePicker2;




@property (weak, nonatomic) IBOutlet UITextField *editaddress1;
@property (weak, nonatomic) IBOutlet UITextField *editaddress2;
@property (weak, nonatomic) IBOutlet UITextField *editzipCode;

@property (weak, nonatomic) IBOutlet UITextField *editcity;
@property (weak, nonatomic) IBOutlet UITextField *editstate;
@property (weak, nonatomic) IBOutlet UIButton *editcountrybtn;

@property (weak, nonatomic) IBOutlet UITextField *editemailID;
@property (weak, nonatomic) IBOutlet UITextField *editphoneNo;

- (IBAction)editcountryButton:(id)sender;

@end
