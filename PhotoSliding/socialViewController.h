//
//  dealsViewController.h
//  PhotoSliding
//
//  Created by ephronsystems on 6/27/13.
//
//

#import <UIKit/UIKit.h>

@interface socialViewController : GAITrackedViewController<UIAlertViewDelegate>
@property (nonatomic,retain)IBOutlet UITableView *avilableTableView;
-(IBAction)meButton;
-(IBAction)dealsButton;
-(IBAction)netCoinsButton;
-(IBAction)socialButton;
-(IBAction)sendCardButton;
-(IBAction)viewsendCardButton;

-(IBAction)myBusinessButton;
-(IBAction)settingsButton;

-(IBAction)saveCard;

-(IBAction)receivedCard;

-(IBAction)sharedListCard;
-(IBAction)SlideShowCard;
-(IBAction)infoBtn;

@property (weak, nonatomic) IBOutlet UIButton  *profileButton;
@end
