//
//  MyCell.h
//  TestCollectionViewWithXIB
//
//  Created by Quy Sang Le on 2/3/13.
//  Copyright (c) 2013 Quy Sang Le. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface History : UICollectionViewCell
@property (retain, nonatomic) IBOutlet UITextView *cellLabel;
@property (retain, nonatomic) IBOutlet UILabel *cell_title_Label;
@property (retain, nonatomic) IBOutlet UILabel *cell_status;
@property (retain, nonatomic) IBOutlet UILabel *cell_date;
@property (retain, nonatomic) IBOutlet UILabel *cell_value;
@property (retain, nonatomic) IBOutlet UIImageView *image_view;
@property (nonatomic,retain)IBOutlet UIProgressView *progress;
@property(nonatomic,retain )IBOutlet UIButton *deletebtn;

@property (retain, nonatomic) IBOutlet UIImageView *check_image_view;
@property (weak, nonatomic) IBOutlet UIButton  *profileButton;
@end
