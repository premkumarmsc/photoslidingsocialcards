//
//  AppDelegate.m
//  PhotoSliding
//
//  Created by Neon Spark on 1/21/12.
//  Copyright (c) 2012 http://sugartin.info. All rights reserved.
//

#import "AppDelegate.h"
#import <GooglePlus/GooglePlus.h>
#import "ViewController.h"
#import "MeViewController.h"
#import "socialViewController.h"
@interface AppDelegate () <GPPDeepLinkDelegate>

@end
@implementation AppDelegate
/*static NSString * const kClientID =
@"239601502538-j48llatglueqmbbt2k0ibrdpbpo0tssi.apps.googleusercontent.com";
 */
static NSString * const kClientID =
@"197229363481-0rea01dfb9lghp9jb5gv40tvsi0n0rg2.apps.googleusercontent.com";


@synthesize window = _window;
@synthesize viewController = _viewController;

@synthesize meViewController = socialViewController;



- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    
//    NSString* InputPath = [[NSBundle mainBundle] pathForResource:@"Input"
//                                                          ofType:@"txt"];
//
//    NSData *myData = [[NSData alloc] initWithContentsOfFile:InputPath] ;
//    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/files/postfile",CONFIG_BASE_URL]];
//    
//    __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
//    
//    
//           
//       
//    [request_post setFile:InputPath forKey:@"uploadedfile"];
//    [request_post setPostValue:[Config get_token] forKey:@"apikey"];
//    
//    //  [request_post setPostValue:[Config get_token] forKey:@"apikey"];
//    [request_post setTimeOutSeconds:60];
//    
//    
//    [request_post setCompletionBlock:^{
//        // Use when fetching text data
//        NSString *responseString = [request_post responseString];
//        
//        
//        //(@"RES:%@",responseString);
//               // Use when fetching binary data
//        // NSData *responseData = [request_post responseData];
//    }];
//    [request_post setFailedBlock:^{
//        NSError *error = [request_post error];
//        
//        
//        
//        
//    }];
//    [request_post startAsynchronous];
//
//    
//    
    
    
    
    /*
    UIImage *img=[UIImage imageNamed:@"addnew1.png"];
    
    NSData *imageData = UIImagePNGRepresentation(img);
    
    NSString* newStr = [[NSString alloc] initWithData:imageData
                                              encoding:NSUTF8StringEncoding] ;
    
    NSLog(@"NEW:%@",imageData);
    */
    
    // Optional: automatically send uncaught exceptions to Google Analytics.
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
    [GAI sharedInstance].dispatchInterval = 50;
    // Optional: set debug to YES for extra debugging information.
    [GAI sharedInstance].debug = YES;
    // Create tracker instance.
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:GOOGLE_ANAL_KEY];
    
    
    
   
        // Use when fetching text data
    
    
    
    
    
    
    NSUserDefaults *checkSta=[NSUserDefaults standardUserDefaults];
    
    
    NSString *addressStatus=[checkSta objectForKey:@"FIRSTTIME"];
    
    ////(@"STS:%@",addressStatus);
    
    if ([addressStatus isEqualToString:@"NO"])
    {
        NSUserDefaults *checkSta=[NSUserDefaults standardUserDefaults];
        
        [checkSta setObject:@"NO" forKey:@"FIRSTTIME"];
    }
    else 
    {
        
       
        
    NSUserDefaults *checkSta=[NSUserDefaults standardUserDefaults];
    
    [checkSta setObject:@"YES" forKey:@"FIRSTTIME"];
    }
    
    
     [GPPSignIn sharedInstance].clientID = kClientID;
    
    
     NSString *kAppID =FB_KEY;
    
    
    NSUserDefaults *add=[NSUserDefaults standardUserDefaults];
    
    
   NSString *userID=[add objectForKey:@"USERID"];
    
    
    ////(@"USERRR:%@",userID);
    
    if ([userID isEqualToString:@""]||[userID length]==0)
    {
        if( IS_IPHONE_5 )
        {
            
            
            
            
            self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
            // Override point for customization after application launch.
            self.viewController = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
            self.window.rootViewController = self.viewController;
            [self.window makeKeyAndVisible];
        }
        else
        {
            
           
            
            
            self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
            // Override point for customization after application launch.
            self.viewController = [[ViewController alloc] initWithNibName:@"ViewController~iPhone" bundle:nil];
            self.window.rootViewController = self.viewController;
            [self.window makeKeyAndVisible];
        }
        
  
    
    
    }
    else
    {
        
        
        NSUserDefaults *chk=[NSUserDefaults standardUserDefaults];
        [chk setObject:@"NO" forKey:@"IS_FIRSTTIME"];
        
        if( IS_IPHONE_5 )
        {
            
         
            
            
            self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
            // Override point for customization after application launch.
            self.meViewController = [[socialViewController alloc] initWithNibName:@"socialViewController" bundle:nil];
            self.window.rootViewController = self.meViewController;
            [self.window makeKeyAndVisible];
        }
        else
        {
            
           
            
            
            self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
            // Override point for customization after application launch.
            self.meViewController = [[socialViewController alloc] initWithNibName:@"socialViewController~iPhone" bundle:nil];
            self.window.rootViewController = self.meViewController;
            [self.window makeKeyAndVisible];
        }

    
           
    }
    
    
    [GPPDeepLink setDelegate:self];
    [GPPDeepLink readDeepLinkAfterInstall];
    
    
    //(@"Registering for push notifications...");
    [[UIApplication sharedApplication]
     registerForRemoteNotificationTypes:
     (UIRemoteNotificationTypeAlert |
      UIRemoteNotificationTypeBadge |
      UIRemoteNotificationTypeSound)];
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    return YES;
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    return [FBSession.activeSession handleOpenURL:url];
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // FBSample logic
    // if the app is going away, we close the session object
    [FBSession.activeSession close];
}


/*
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [GPPURLHandler handleURL:url
                  sourceApplication:sourceApplication
                         annotation:annotation];
}
*/
#pragma mark - GPPDeepLinkDelegate

- (void)didReceiveDeepLink:(GPPDeepLink *)deepLink {
    // An example to handle the deep link data.
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"Deep-link Data"
                          message:[deepLink deepLinkID]
                          delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] ;
    [alert show];
}

- (void)applicationDidFinishLaunching:(UIApplication *)application
{
	
}




/*
 * --------------------------------------------------------------------------------------------------------------
 *  BEGIN APNS CODE
 * --------------------------------------------------------------------------------------------------------------
 */

/**
 * Fetch and Format Device Token and Register Important Information to Remote Server
 */


- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)devToken {
    
    ////(@"ENTER");
    // 6bc2db2c1bf274bcceb4a93fcb84fb9cd5b1946f587b16209c66e8842e4bbb12 // My
    // bed8143d8257c904504476f0d09244492ee877c026d5738a82b80df68207c13a // iPhone
    // 7f6bce35595dd819e4019d3a4a9c96a25db706dd3686905173eb87ff413cd1f3 //iPOd
    // openssl pkcs12 -in dev.p12 -out dev.pem -nodes
    
    // openssl pkcs12 -in adhoc.p12 -out adhoc.pem -nodes
	
#if !TARGET_IPHONE_SIMULATOR
    
	// Get Bundle Info for Remote Registration (handy if you have more than one app)
	NSString *appName = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"];
	NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
	
	// Check what Notifications the user has turned on.  We registered for all three, but they may have manually disabled some or all of them.
	NSUInteger rntypes = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
	
	// Set the defaults to disabled unless we find otherwise...
	NSString *pushBadge = (rntypes & UIRemoteNotificationTypeBadge) ? @"enabled" : @"disabled";
	NSString *pushAlert = (rntypes & UIRemoteNotificationTypeAlert) ? @"enabled" : @"disabled";
	NSString *pushSound = (rntypes & UIRemoteNotificationTypeSound) ? @"enabled" : @"disabled";
	
	// Get the users Device Model, Display Name, Unique ID, Token & Version Number
	UIDevice *dev = [UIDevice currentDevice];
	NSString *deviceUuid;
		
	NSString *deviceName = dev.name;
	NSString *deviceModel = dev.model;
	NSString *deviceSystemVersion = dev.systemVersion;
	
	// Prepare the Device Token for Registration (remove spaces and < >)
	NSString *deviceToken = [[[[devToken description]
                               stringByReplacingOccurrencesOfString:@"<"withString:@""]
                              stringByReplacingOccurrencesOfString:@">" withString:@""]
                             stringByReplacingOccurrencesOfString: @" " withString: @""];
	
    
    
    //(@"DEVICE TOKEN:%@",deviceToken);
    
    NSUserDefaults *userValue=[NSUserDefaults standardUserDefaults];
    
    [userValue setObject:deviceToken forKey:@"DEVICE_TOKEN"];
    
    
    /*
	// Build URL String for Registration
	// !!! CHANGE "www.mywebsite.com" TO YOUR WEBSITE. Leave out the http://
	// !!! SAMPLE: "secure.awesomeapp.com"
	NSString *host = @"www.mywebsite.com";
	
	// !!! CHANGE "/apns.php?" TO THE PATH TO WHERE apns.php IS INSTALLED
	// !!! ( MUST START WITH / AND END WITH ? ).
	// !!! SAMPLE: "/path/to/apns.php?"
	NSString *urlString = [NSString stringWithFormat:@"/apns.php?task=%@&appname=%@&appversion=%@&deviceuid=%@&devicetoken=%@&devicename=%@&devicemodel=%@&deviceversion=%@&pushbadge=%@&pushalert=%@&pushsound=%@", @"register", appName,appVersion, deviceUuid, deviceToken, deviceName, deviceModel, deviceSystemVersion, pushBadge, pushAlert, pushSound];
	
	// Register the Device Data
	// !!! CHANGE "http" TO "https" IF YOU ARE USING HTTPS PROTOCOL
	NSURL *url = [[NSURL alloc] initWithScheme:@"http" host:host path:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
	NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
	//////(@"Register URL: %@", url);
	//////(@"Return Data: %@", returnData);
     */
     
	
#endif
}


- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
	
#if !TARGET_IPHONE_SIMULATOR
	
	//(@"Error in registration. Error: %@", error);
	
#endif
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
	
#if !TARGET_IPHONE_SIMULATOR
    
    
    
    	//(@"remote notification: %@",[userInfo description]);
	NSDictionary *apsInfo = [userInfo objectForKey:@"aps"];
	
	NSString *alert = [apsInfo objectForKey:@"alert"];
	//(@"Received Push Alert: %@", alert);
	
	NSString *sound = [apsInfo objectForKey:@"sound"];
	//(@"Received Push Sound: %@", sound);
	AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
	
	NSString *badge = [apsInfo objectForKey:@"badge"];
	//(@"Received Push Badge: %@", badge);
	application.applicationIconBadgeNumber = [[apsInfo objectForKey:@"badge"] integerValue];
	
#endif
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
    
     [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
     [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    
     [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
     [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}



@end
