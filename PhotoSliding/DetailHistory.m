//
//  dealsViewController.m
//  PhotoSliding
//
//  Created by ephronsystems on 6/27/13.
//
//

#import "DetailHistory.h"
#import "receiptentCell.h"
#import "Image_cell.h"
#import "History.h"

@interface DetailHistory ()

{
    AVAudioRecorder *recorder;
    AVAudioPlayer *player;
}

@end

@implementation DetailHistory

@synthesize filterButton;
@synthesize collectionView;
@synthesize messageText,cropImage,senderImage;
@synthesize getCardID;
@synthesize getCreated,getImage,getMessage,getName,getUpdated,getStatus;
@synthesize collectionViewNew;
@synthesize getContact;
@synthesize getSmileyIDs;
@synthesize getAudioID;
@synthesize getViewName;
@synthesize getSenderPic;
@synthesize getCancelparam;
History *cell;

NSMutableArray *contactID;
NSMutableArray *currentStatus;
NSMutableArray *created_date;
NSMutableArray *idValue;

UIAlertView *alertExit;

NSMutableArray *senderID;
NSMutableArray *senderSNGID;
NSMutableArray *senderImageArr;
NSMutableArray *senderBirthdate;
NSMutableArray *senderFirstName;
NSMutableArray *senderLastName;
NSMutableArray *senderdate_created;
NSMutableArray *senderdate_updated;
-(IBAction)cancel
{
    ////(@"AUDIO:%@",audio);
    
    
    alertExit = [[UIAlertView alloc] initWithTitle:nil
                                           message:@"Are you sure you want to cancel this ordered SocialCard?" delegate:self cancelButtonTitle: @"No"
                                 otherButtonTitles:@"Yes", nil];
    
    //note above delegate property
    
    [alertExit show];
    
    
    
    
    
}


-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    //u need to change 0 to other value(,1,2,3) if u have more buttons.then u can check which button was pressed.
    
    if (alertView==alertExit)
    {
        if (buttonIndex == 0)
        {
            
            ////(@"0");
            
            
        }
        else
        {
            
            self.trackedViewName = @"Detail Cards Screen";
            
            
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/socialcards/cancelSocialCard",CONFIG_BASE_URL]];
            
            
            __block  ASIFormDataRequest *requestmethod = [ASIFormDataRequest requestWithURL:url];
            
            
            
            NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
            NSString *user=[check objectForKey:@"USERID"];
            NSString *cardID=[check objectForKey:@"CARD_ID"];
            
            NSMutableArray *smilyIDArr=[[NSMutableArray alloc]init];
            NSMutableArray *smilyName=[[NSMutableArray alloc]init];
            
            [requestmethod setPostValue:user forKey:@"userID"];
            [requestmethod setPostValue:cardID forKey:@"cardID"];
            [requestmethod setPostValue:[Config get_token] forKey:@"apikey"];
            [requestmethod setTimeOutSeconds:30];
            
            
            [requestmethod setCompletionBlock:^{
                NSString *responseString23 = [requestmethod responseString];
                NSMutableData *results11 = [responseString23 JSONValue];
                
                //(@"RESULTS:%@",results11);
                
                
                getCancelparam=@"no";
                
                [self viewDidLoad];
                
                
                
            }];
            [requestmethod setFailedBlock:^{
                NSError *error = [requestmethod error];
                UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                    message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                          cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                
                
                
            }];
            
            [requestmethod startAsynchronous];
                       
            
        }
    }
    else
    {
        
        
    }
    
}





- (IBAction)AudioPlay:(id)sender
{
    
    _playAudio.selected=_playAudio.selected?NO : YES;
    
    if (_playAudio.selected)
    {
        
        
                
                                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                
                NSString *audioString1 = [documentsDirectory stringByAppendingPathComponent:@"SendAudio"];
                
                NSString *audioString=[NSString stringWithFormat:@"%@.m4a",audioString1];
                
                
                NSUserDefaults *Confirmation=[NSUserDefaults standardUserDefaults];
                
                [Confirmation setObject:audioString forKey:@"AUDIO_STRING"];
                
                
                NSURL *url = [NSURL fileURLWithPath:audioString];
                
                
                
                NSError *error;
                audioPlayer = [[AVAudioPlayer alloc]
                               initWithContentsOfURL:url
                               error:&error];
                //audioPlayer.numberOfLoops=-1;
                [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
                [[AVAudioSession sharedInstance] setActive: YES error: nil];
                [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
                
                
                ////(@"TOTAL:%f",audioPlayer.duration);
                
                                
                if (error)
                {
                    ////(@"Error in audioPlayer: %@",
                        //  [error localizedDescription]);
                    
                    
                             
                    
                    
                    
                    
                    
                    
                    
                }
                else
                    
                {
                    audioPlayer.delegate = self;
                    
                   
                    
                    [audioPlayer prepareToPlay];
                }
                
            
                   
        
            audioPlayer.volume=1.5;
        
       
        [audioPlayer play];
    }

    else
    {
        ////(@"It is deselected");
        
        
                
        NSUserDefaults *Confirmation=[NSUserDefaults standardUserDefaults];
        
        [Confirmation setObject:@"YES" forKey:@"AUDIO_STOP"];
        
        
        [audioPlayer stop];
    }
}

-(void)audioPlayerDidFinishPlaying:
(AVAudioPlayer *)player successfully:(BOOL)flag
{
    _playAudio.selected=NO;
    ////(@"audioPlayerDidFinishPlaying");
  
}



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    _cancelButton.hidden=YES;
    
    
    if ([getViewName isEqualToString:@"SHARE"])
    {
        _scrollImageView.hidden=YES;
    }
    
    
    if ([getViewName isEqualToString:@"RECEIVED"])
    {
        _scrollImageView.hidden=YES;
        collectionViewNew.hidden=YES;
    }
    
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    
    NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:@"profileImage.jpg"];
    UIImage *profileIMG = [UIImage imageWithContentsOfFile:getImagePath];
    
    
  
    if ([getViewName isEqualToString:@"RECEIVED"])
    {
        _scrollImageView.hidden=YES;
        collectionViewNew.hidden=YES;
        
        
        //(@"GETSENDER:%@",getSenderPic);
        
        [senderImage setImageWithURL:[NSURL URLWithString:getSenderPic]
                  placeholderImage:[UIImage imageNamed:nil]];
        
        [_fromImageView1 setImageWithURL:[NSURL URLWithString:getSenderPic]
                    placeholderImage:[UIImage imageNamed:nil]];
    }
    else
    {
         senderImage.image=profileIMG;
        
        _fromImageView1.image=profileIMG;
    }

    
    if ([getSmileyIDs isEqualToString:@""]||[getSmileyIDs isEqualToString:@"0"]||[getSmileyIDs length]==0 )
    {
        
        
        //lines= [smiley_ids componentsSeparatedByString: @","];
        
        // ////(@"LINES:%@",[lines count]);
        
        
    }
    else
    {
        [self getSmilies:getSmileyIDs];
    }
    
     _playAudio.hidden=YES;
    
    if ([getAudioID isEqualToString:@""]||[getAudioID isEqualToString:@"0"]||[getAudioID length]==0 )
    {
        
        
        //lines= [smiley_ids componentsSeparatedByString: @","];
        
        // ////(@"LINES:%@",[lines count]);
        
        
       
        
    }
    else
    {
        
        [self getAudio:getAudioID];
    }

    
    
    _titleLabel.text=getName;
    messageText.text=getMessage;
    
    
    contactID=[[NSMutableArray alloc]init];
    currentStatus=[[NSMutableArray alloc]init];
    created_date=[[NSMutableArray alloc]init];
    idValue=[[NSMutableArray alloc]init];
        
    [cropImage setImageWithURL:[NSURL URLWithString:getImage]
                   placeholderImage:[UIImage imageNamed:@"bigicon.png"]];
    
    
    
    
    [self.view addSubview:_landscapeView];
    _landscapeView.hidden=YES;
    
  
    
    [_finalImageView1 setImageWithURL:[NSURL URLWithString:getImage]
                     placeholderImage:[UIImage imageNamed:@"bigicon.png"]];
    
   
    
    
    
   
    _messageText1.text=getMessage;
    
    
    
   
    _titleText1.text=getName;
    
    
    
    
   
    if (![getCancelparam isEqualToString:@"no"])
    {
        _cancelButton.hidden=NO;
        
        
    }
      

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
        
    [self getContects:getContact];
    
     [self.collectionViewNew registerNib:[UINib nibWithNibName:@"History" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
    
    
    pageControlBeingUsed = NO;
    
    NSArray *colors1 = [NSArray arrayWithObjects:_cardView1,_cardView2,_cardView3,_cardView4, nil];
    NSArray *colors = [NSArray arrayWithObjects:[UIColor redColor],[UIColor blueColor],[UIColor redColor],[UIColor blueColor], nil];
	for (int i = 0; i < colors.count; i++) {
		CGRect frame;
		frame.origin.x = self.scrollView.frame.size.width * i;
		frame.origin.y = 0;
		frame.size = self.scrollView.frame.size;
		
		UIView *subview = [[UIView alloc] initWithFrame:frame];
		//subview.backgroundColor = [colors objectAtIndex:i];
        [subview addSubview:[colors1 objectAtIndex:i]];
		[self.scrollView addSubview:subview];
		
	}
	
	self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * colors.count, self.scrollView.frame.size.height);
	
	self.pageControl.currentPage = 0;
	self.pageControl.numberOfPages = colors.count;
    self.scrollView.pagingEnabled = YES;
    
    
    pageControlBeingUsedLands = NO;
    
    NSArray *colors11 = [NSArray arrayWithObjects:_cardViewLands1,_cardViewLands2, nil];
    
    
    
	for (int i = 0; i < colors11.count; i++) {
		CGRect frame;
		frame.origin.x = self.scrollViewLands.frame.size.width * i;
		frame.origin.y = 0;
		frame.size = self.scrollViewLands.frame.size;
		
		UIView *subview = [[UIView alloc] initWithFrame:frame];
		//subview.backgroundColor = [colors objectAtIndex:i];
        [subview addSubview:[colors11 objectAtIndex:i]];
		[self.scrollViewLands addSubview:subview];
		
	}
	
	self.scrollViewLands.contentSize = CGSizeMake(self.scrollViewLands.frame.size.width * colors11.count, self.scrollViewLands.frame.size.height);
	
	self.pageControlLands.currentPage = 0;
    
    
	self.pageControlLands.numberOfPages = colors11.count;
    self.scrollViewLands.pagingEnabled = YES;
    
    
    
    
    

    
    
    _front_img.image=[UIImage imageNamed:@"sfrontact1.png"];
    _bage_img.image=[UIImage imageNamed:@"sback.png"];

   
}

-(BOOL)shouldAutorotate
{
    return YES;
}


-(NSInteger)supportedInterfaceOrientations{
    if (UIDeviceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation]))
    {
        //(@"PORTRAIT");
        
        _landscapeView.hidden=YES;
        
    }
    
    else
    {
        
        //(@"LANDSCAPE");
        
        _landscapeView.hidden=NO;
        
    }
    return UIInterfaceOrientationMaskAll;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)orientation
                                duration:(NSTimeInterval)duration
{
    if (UIDeviceOrientationIsPortrait(orientation))
    {
        
        //(@"PORT");
        
        _landscapeView.hidden=YES;
    }
    
    else
    {
        //(@"LAND");
        
        _landscapeView.hidden=NO;
        
    }
}


- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    
    
    if (sender==scrollViewLands)
        
    {
        
        
        if (!pageControlBeingUsedLands)
        {
            // Switch the indicator when more than 50% of the previous/next page is visible
            CGFloat pageWidth = self.scrollViewLands.frame.size.width;
            int page = floor((self.scrollViewLands.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
            self.pageControlLands.currentPage = page;
        }
        
    }
    else
    {
        
        // ////(@"CURRENT:%d",self.pageControl.currentPage);
        
        if (self.pageControl.currentPage==0)
        {
            //        [_b1 setTitle:@"A" forState:UIControlStateNormal];
            //        [_b2 setTitle:@"B" forState:UIControlStateNormal];
            //        [_b3 setTitle:@"B" forState:UIControlStateNormal];
            //        [_b4 setTitle:@"B" forState:UIControlStateNormal];
            
            _front_img.image=[UIImage imageNamed:@"sfrontact1.png"];
            _bage_img.image=[UIImage imageNamed:@"sback.png"];
            
        }
        
        if (self.pageControl.currentPage==1)
        {
            //        [_b1 setTitle:@"B" forState:UIControlStateNormal];
            //        [_b2 setTitle:@"A" forState:UIControlStateNormal];
            //        [_b3 setTitle:@"B" forState:UIControlStateNormal];
            //        [_b4 setTitle:@"B" forState:UIControlStateNormal];
            
            _front_img.image=[UIImage imageNamed:@"sfrontact2.png"];
            _bage_img.image=[UIImage imageNamed:@"sback.png"];
            
        }
        
        if (self.pageControl.currentPage==2)
        {
            //        [_b1 setTitle:@"B" forState:UIControlStateNormal];
            //        [_b2 setTitle:@"B" forState:UIControlStateNormal];
            //        [_b3 setTitle:@"A" forState:UIControlStateNormal];
            //        [_b4 setTitle:@"B" forState:UIControlStateNormal];
            
            _front_img.image=[UIImage imageNamed:@"sfront.png"];
            _bage_img.image=[UIImage imageNamed:@"sbackact1.png"];
            
            
        }
        
        if (self.pageControl.currentPage==3)
        {
            //        [_b1 setTitle:@"B" forState:UIControlStateNormal];
            //        [_b2 setTitle:@"B" forState:UIControlStateNormal];
            //        [_b3 setTitle:@"B" forState:UIControlStateNormal];
            //        [_b4 setTitle:@"A" forState:UIControlStateNormal];
            
            _front_img.image=[UIImage imageNamed:@"sfront.png"];
            _bage_img.image=[UIImage imageNamed:@"sbackact2.png"];
            
        }
        
        if (!pageControlBeingUsed)
        {
            // Switch the indicator when more than 50% of the previous/next page is visible
            CGFloat pageWidth = self.scrollView.frame.size.width;
            int page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
            self.pageControl.currentPage = page;
        }
        
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if (scrollView==scrollViewLands)
        
    {
        pageControlBeingUsedLands = NO;
    }
    else
    {
        pageControlBeingUsed = NO;
        
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView==scrollViewLands)
        
    {
        pageControlBeingUsedLands = NO;
    }
    else
    {
        pageControlBeingUsed = NO;
    }
}

- (IBAction)changePageLands {
	// Update the scroll view to the appropriate page
	CGRect frame;
	frame.origin.x = self.scrollViewLands.frame.size.width * self.pageControlLands.currentPage;
	frame.origin.y = 0;
	frame.size = self.scrollViewLands.frame.size;
	[self.scrollViewLands scrollRectToVisible:frame animated:YES];
	
	// Keep track of when scrolls happen in response to the page control
	// value changing. If we don't do this, a noticeable "flashing" occurs
	// as the the scroll delegate will temporarily switch back the page
	// number.
	pageControlBeingUsedLands = YES;
}






-(IBAction)b1_Click
{
    
    CGPoint bottomOffset = CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
    [self.scrollView setContentOffset:bottomOffset animated:YES];
    
    //
    //
    //    [_b1 setTitle:@"A" forState:UIControlStateNormal];
    //    [_b2 setTitle:@"B" forState:UIControlStateNormal];
    //    [_b3 setTitle:@"B" forState:UIControlStateNormal];
    //    [_b4 setTitle:@"B" forState:UIControlStateNormal];
    
}
-(IBAction)b2_Click
{
    
    CGPoint bottomOffset = CGPointMake(320, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
    [self.scrollView setContentOffset:bottomOffset animated:YES];
    
    //    [_b1 setTitle:@"B" forState:UIControlStateNormal];
    //    [_b2 setTitle:@"A" forState:UIControlStateNormal];
    //    [_b3 setTitle:@"B" forState:UIControlStateNormal];
    //    [_b4 setTitle:@"B" forState:UIControlStateNormal];
    
	
    
}

-(IBAction)b3_Click
{
    CGPoint bottomOffset = CGPointMake(640, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
    [self.scrollView setContentOffset:bottomOffset animated:YES];
    
    //
    //    [_b1 setTitle:@"B" forState:UIControlStateNormal];
    //    [_b2 setTitle:@"B" forState:UIControlStateNormal];
    //    [_b3 setTitle:@"A" forState:UIControlStateNormal];
    //    [_b4 setTitle:@"B" forState:UIControlStateNormal];
    
}
-(IBAction)b4_Click
{
    
    CGPoint bottomOffset = CGPointMake(960, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
    [self.scrollView setContentOffset:bottomOffset animated:YES];
    //
    //    [_b1 setTitle:@"B" forState:UIControlStateNormal];
    //    [_b2 setTitle:@"B" forState:UIControlStateNormal];
    //    [_b3 setTitle:@"B" forState:UIControlStateNormal];
    //    [_b4 setTitle:@"A" forState:UIControlStateNormal];
    
}



- (IBAction)changePage {
	// Update the scroll view to the appropriate page
	CGRect frame;
	frame.origin.x = self.scrollView.frame.size.width * self.pageControl.currentPage;
	frame.origin.y = 0;
	frame.size = self.scrollView.frame.size;
	[self.scrollView scrollRectToVisible:frame animated:YES];
	
	// Keep track of when scrolls happen in response to the page control
	// value changing. If we don't do this, a noticeable "flashing" occurs
	// as the the scroll delegate will temporarily switch back the page
	// number.
	pageControlBeingUsed = YES;
}



-(void)getAudio:(NSString *)audio
{
    ////(@"AUDIO:%@",audio);
    
    
        self.trackedViewName = @"Detail Cards Screen";
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/audios/retrieveCardAudios",CONFIG_BASE_URL]];
    
    
    __block  ASIFormDataRequest *requestmethod = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    NSString *cardID=[check objectForKey:@"CARD_ID"];
    
    NSMutableArray *smilyIDArr=[[NSMutableArray alloc]init];
    NSMutableArray *smilyName=[[NSMutableArray alloc]init];
    
    [requestmethod setPostValue:user forKey:@"userID"];
    [requestmethod setPostValue:audio forKey:@"audio_id"];
     [requestmethod setPostValue:[Config get_token] forKey:@"apikey"];
    [requestmethod setTimeOutSeconds:30];
    
    
    [requestmethod setCompletionBlock:^{
        NSString *responseString23 = [requestmethod responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        ////(@"RESULTS:%@",results11);
        
        
        NSArray *temp= [results11 valueForKeyPath:@"audio.audio_url"];
        
        
        
        ////(@"TEMP TEMP:%@",temp[0]);
        
    
        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSURL *url=[NSURL URLWithString:temp[0]];
        
        
        ////(@"Dowload Start");
        
        ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
        
        
        
        
        //  [collectionView reloadData];
        
        [request setShouldContinueWhenAppEntersBackground:YES];
        
        
        [request setCompletionBlock:^{
            // Use when fetching text data
            
            NSString *responseString = [request responseString];
            
            
            
            // Use when fetching binary data
            NSData *responseData = [request responseData];
            
            NSString *epub_name=[NSString stringWithFormat:@"SendAudio.m4a"];
            
            
            NSString *filePath = [documentsDirectory stringByAppendingPathComponent:epub_name];
            
            [responseData writeToFile:filePath atomically:YES];
            
            ////(@"Dowload Completes");
            
           _playAudio.hidden=NO;
            
            
        }];
        [request setFailedBlock:^
         {
             // NSError *error = [request error];
             
             
             UIAlertView *error_Alert=[[UIAlertView alloc]initWithTitle:@"Download Error" message:[NSString stringWithFormat:@"%@",[request error]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             
             [error_Alert show];
             
             
         }];
        [request startAsynchronous];
        
        
        
        
    }];
    [requestmethod setFailedBlock:^{
        NSError *error = [requestmethod error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [requestmethod startAsynchronous];
    
    
}

-(void)getSmilies:(NSString *)smiley
{
    ////(@"SMILEY:%@",smiley);
    
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/smiley/retrieveCardSmileys",CONFIG_BASE_URL]];
    
    
    __block  ASIFormDataRequest *requestmethod = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    NSString *cardID=[check objectForKey:@"CARD_ID"];
    
    NSMutableArray *smilyIDArr=[[NSMutableArray alloc]init];
    NSMutableArray *smilyName=[[NSMutableArray alloc]init];
    
    [requestmethod setPostValue:user forKey:@"userID"];
    [requestmethod setPostValue:smiley forKey:@"smiley_ids"];
     [requestmethod setPostValue:[Config get_token] forKey:@"apikey"];
    [requestmethod setTimeOutSeconds:30];
    
    
    [requestmethod setCompletionBlock:^{
        NSString *responseString23 = [requestmethod responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        ////(@"RESULTS:%@",results11);
        
        
        NSArray *temp= [results11 valueForKeyPath:@"smileys"];
        
        
        
        
        
        ////(@"TEMPARY:%@",temp);
        
        for(NSDictionary *value in temp)
        {
            [smilyIDArr  addObject:[value valueForKey:@"id"]];
            [smilyName  addObject:[value valueForKey:@"smiley_url"]];
            //[contactidArray  addObject:[value valueForKey:@"contactid"]];
            
        }
        
        
        
        if ([smilyName count]==1)
        {
            
            
            NSString *temp1=@"";
            [_sm1 setImageWithURL:[NSURL URLWithString:temp1]
                 placeholderImage:nil];
            [_sm11 setImageWithURL:[NSURL URLWithString:temp1]
                  placeholderImage:nil];
            
            NSString *temp2=@"";
            [_sm3 setImageWithURL:[NSURL URLWithString:temp2]
                 placeholderImage:nil];
            [_sm31 setImageWithURL:[NSURL URLWithString:temp2]
                  placeholderImage:nil];
            NSString *temp=[smilyName objectAtIndex:0];
            [_sm2 setImageWithURL:[NSURL URLWithString:temp]
                 placeholderImage:nil];
            [_sm21 setImageWithURL:[NSURL URLWithString:temp]
                  placeholderImage:nil];
        }
        
        if ([smilyName count]==2)
        {
            
            NSString *temp=[smilyName objectAtIndex:0];
            [_sm1 setImageWithURL:[NSURL URLWithString:temp]
                 placeholderImage:nil];
            [_sm11 setImageWithURL:[NSURL URLWithString:temp]
                  placeholderImage:nil];
            
            NSString *temp1=[smilyName objectAtIndex:1];
            [_sm2 setImageWithURL:[NSURL URLWithString:temp1]
                 placeholderImage:nil];
            [_sm21 setImageWithURL:[NSURL URLWithString:temp1]
                  placeholderImage:nil];
            NSString *temp2=@"";
            [_sm3 setImageWithURL:[NSURL URLWithString:temp2]
                 placeholderImage:nil];
            [_sm31 setImageWithURL:[NSURL URLWithString:temp2]
                  placeholderImage:nil];
            
        }
        
        if ([smilyName count]==3)
        {
            
            NSString *temp=[smilyName objectAtIndex:0];
            [_sm1 setImageWithURL:[NSURL URLWithString:temp]
                 placeholderImage:nil];
            [_sm11 setImageWithURL:[NSURL URLWithString:temp]
                  placeholderImage:nil];
            NSString *temp1=[smilyName objectAtIndex:1];
            [_sm2 setImageWithURL:[NSURL URLWithString:temp1]
                 placeholderImage:nil];
            [_sm21 setImageWithURL:[NSURL URLWithString:temp1]
                  placeholderImage:nil];
            
            NSString *temp2=[smilyName objectAtIndex:2];
            [_sm3 setImageWithURL:[NSURL URLWithString:temp2]
                 placeholderImage:nil];
            [_sm31 setImageWithURL:[NSURL URLWithString:temp2]
                  placeholderImage:nil];
        }
        
        
     
        
        
        
    }];
    [requestmethod setFailedBlock:^{
        NSError *error = [requestmethod error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [requestmethod startAsynchronous];
    
    
}





- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [senderFirstName count];
}


- (History *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELL" forIndexPath:indexPath];
    
    
    
    // cell.progress.hidden=YES;
    
    @try {
        NSString *temp=[senderImageArr objectAtIndex:indexPath.row];
        [cell.image_view setImageWithURL:[NSURL URLWithString:temp]
                        placeholderImage:[UIImage imageNamed:@"bigicon.png"]];
        
        
        
    }
    @catch (NSException *exception) {
        ////////////(@"CHJECK");
    }
    
    cell.cell_title_Label.text=[NSString stringWithFormat:@"%@",senderFirstName[indexPath.row]];
    
    
    NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc] init];
    
    [dateFormat1 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    
    NSString *date_updated=senderdate_updated[indexPath.row];
    ////(@"Original date:%@",date_updated);
        NSDate *todaydate = [dateFormat1 dateFromString:date_updated];// date with yyyy-MM-dd format
      // ////(@"DATE:%@",todaydate);
    
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"LLL d, yyyy HH:mm a"];
    NSString *dateString = [dateFormat stringFromDate:todaydate];
    
   // ////(@"DATE STRING:%@",dateString);
    
    
    cell.cell_date.text=[NSString stringWithFormat:@"%@",dateString];
    
    
    NSString *status_str=@"";
    
    if ([senderLastName[indexPath.row]isEqualToString:@"O"])
    {
        status_str=@"Order Received";
    }
    
    if ([senderLastName[indexPath.row]isEqualToString:@"S"])
    {
        status_str=@"Saved";
    }
    
    if ([senderLastName[indexPath.row]isEqualToString:@"C"])
    {
        status_str=@"Created";
    }
    
    if ([senderLastName[indexPath.row]isEqualToString:@"A"])
    {
        status_str=@"Approved";
    }
    
    if ([senderLastName[indexPath.row]isEqualToString:@"R"])
    {
        status_str=@"Rejected";
    }
    
    if ([senderLastName[indexPath.row]isEqualToString:@"P"])
    {
        status_str=@"Printed";
    }
    
    if ([senderLastName[indexPath.row]isEqualToString:@"M"])
    {
        status_str=@"Mailed";
    }
    
    if ([senderLastName[indexPath.row]isEqualToString:@"E"])
    {
        status_str=@"E-Mailed";
    }
    
    if ([senderLastName[indexPath.row]isEqualToString:@"D"])
    {
        status_str=@"Delivered";
    }
    
    if ([senderLastName[indexPath.row]isEqualToString:@"ED"])
    {
        status_str=@"Email Viewed";
    }
    if ([senderLastName[indexPath.row]isEqualToString:@"CAN"])
    {
        status_str=@"Canceled";
    }
    if ([senderLastName[indexPath.row]isEqualToString:@"P"])
    {
        status_str=@"Printed";
    }
    if ([senderLastName[indexPath.row]isEqualToString:@"DEL"])
    {
        status_str=@"Deleted";
    }

            cell.cell_status.text=[NSString stringWithFormat:@"Status:%@",status_str];
           
        //cell.image_view.image=[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@", image_path]];
    
    return cell;
}




-(void)getContects:(NSString *)contactIDs
{
    
    
   
    
    senderID=[[NSMutableArray alloc]init];
    senderSNGID=[[NSMutableArray alloc]init];
    senderImageArr=[[NSMutableArray alloc]init];
    senderBirthdate=[[NSMutableArray alloc]init];
    senderFirstName=[[NSMutableArray alloc]init];
    senderLastName=[[NSMutableArray alloc]init];
    senderdate_created=[[NSMutableArray alloc]init];
    senderdate_updated=[[NSMutableArray alloc]init];
    
    
    //http://apidev.socialnetgateapp.com/v1/socialcards/orderDetailsForMobile
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/socialcards/orderDetailsForMobile",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post1234 = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    
    //(@"GET:%@",getCardID);
    
    
    [request_post1234 setPostValue:user forKey:@"userID"];
    [request_post1234 setPostValue:getCardID forKey:@"cardID"];
     [request_post1234 setPostValue:[Config get_token] forKey:@"apikey"];
    
    [request_post1234 setTimeOutSeconds:30];
    
    
    [request_post1234 setCompletionBlock:^{
        NSString *responseString23 = [request_post1234 responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        
        //(@"RESULTS LOGIN:%@",results11);
        
        
        
        
        NSArray *temp= [results11 valueForKeyPath:@"response.socialcards"];
        
        
        ////(@"TEMPARY:%@",temp);
        
        
        @try {
            for(NSDictionary *value in temp)
            {
                
                
                [senderImageArr addObject:[value valueForKey:@"friend_facebookproimg"]];
                [senderFirstName addObject:[value valueForKey:@"recipient"]];
               [senderLastName addObject:[value valueForKey:@"current_status"]];
                [senderdate_updated addObject:[value valueForKey:@"created_date"]];
            }
        }
        @catch (NSException *exception)
        {
         
            ////(@"Exception");
        }
        
        
        
        
        
       
     [collectionViewNew reloadData];
     
        
        
    }];
    [request_post1234 setFailedBlock:^{
        NSError *error = [request_post1234 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1234 startAsynchronous];
}


-(IBAction)backButton
{
    
    if ([getViewName isEqualToString:@"SEND"])
    {
        if (IS_IPHONE_5) {
            sendCardViewController *viewController=[[sendCardViewController alloc]initWithNibName:@"sendCardViewController" bundle:nil];
            [self presentViewController:viewController animated:NO completion:^{
                //////(@"SUCCESS");
            }
             ];
        }
        else
        {
            sendCardViewController *viewController=[[sendCardViewController alloc]initWithNibName:@"sendCardViewController~iPhone" bundle:nil];
            [self presentViewController:viewController animated:NO completion:^{
                //////(@"SUCCESS");
            }
             ];
        }
    }
    
    if ([getViewName isEqualToString:@"SHARE"])
    {
        if (IS_IPHONE_5) {
            sharedCardViewController *viewController=[[sharedCardViewController alloc]initWithNibName:@"sharedCardViewController" bundle:nil];
            [self presentViewController:viewController animated:NO completion:^{
                //////(@"SUCCESS");
            }
             ];
        }
        else
        {
            sharedCardViewController *viewController=[[sharedCardViewController alloc]initWithNibName:@"sharedCardViewController~iPhone" bundle:nil];
            [self presentViewController:viewController animated:NO completion:^{
                //////(@"SUCCESS");
            }
             ];
        }

    }

    
    
    if ([getViewName isEqualToString:@"RECEIVED"])
    {
        if (IS_IPHONE_5) {
            receivedCardViewController *viewController=[[receivedCardViewController alloc]initWithNibName:@"receivedCardViewController" bundle:nil];
            [self presentViewController:viewController animated:NO completion:^{
                //////(@"SUCCESS");
            }
             ];
        }
        else
        {
            receivedCardViewController *viewController=[[receivedCardViewController alloc]initWithNibName:@"receivedCardViewController~iPhone" bundle:nil];
            [self presentViewController:viewController animated:NO completion:^{
                //////(@"SUCCESS");
            }
             ];
        }
    }


    
    
}
 - (void)handleSingleTap:(UIGestureRecognizer *)gestureRecognizer {
     
     [messageText resignFirstResponder];
 }

-(IBAction)continueButtonClick
{
    
    if (IS_IPHONE_5)
    {
        
        receiptentViewController *viewController=[[receiptentViewController alloc]initWithNibName:@"receiptentViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
    
    receiptentViewController *viewController=[[receiptentViewController alloc]initWithNibName:@"receiptentViewController~iPhone" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        //////(@"SUCCESS");
    }
     ];
    }
}

-(IBAction)meButton
{
    if (IS_IPHONE_5) {
        MeViewController *viewController=[[MeViewController alloc]initWithNibName:@"MeViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        
        MeViewController *viewController=[[MeViewController alloc]initWithNibName:@"MeViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }

}
-(IBAction)dealsButton
{
    dealsViewController *viewController=[[dealsViewController alloc]initWithNibName:@"dealsViewController" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        //////(@"SUCCESS");
    }
     ];
}
-(IBAction)socialButton
{
    if (IS_IPHONE_5) {
        netCoinsViewController *viewController=[[netCoinsViewController alloc]initWithNibName:@"netCoinsViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        netCoinsViewController *viewController=[[netCoinsViewController alloc]initWithNibName:@"netCoinsViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }

}
-(IBAction)netCoinsButton
{
    if (IS_IPHONE_5) {
        socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }

}
-(IBAction)myBusinessButton
{
    myBusinessViewController *viewController=[[myBusinessViewController alloc]initWithNibName:@"myBusinessViewController" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        //////(@"SUCCESS");
    }
     ];
}
- (BOOL)textFieldShouldReturn: (UITextView *)textField
{
  
    [textField resignFirstResponder];
    
    return YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
