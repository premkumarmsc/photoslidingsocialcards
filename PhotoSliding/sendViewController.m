//
//  dealsViewController.m
//  PhotoSliding
//
//  Created by ephronsystems on 6/27/13.
//
//

#import "sendViewController.h"
#import "receiptentCell.h"
#import "Image_cell.h"
#import "FinalCell.h"

@interface sendViewController ()

@end

@implementation sendViewController

@synthesize filterButton;
@synthesize collectionView;
@synthesize comeView;
@synthesize scroll;
@synthesize scroll_back_view;
@synthesize collectionViewFinal;
@synthesize redeem_view;
@synthesize scrollView,pageControl;
@synthesize scrollView1,pageControl1;
@synthesize scrollView2,pageControl2;
@synthesize scrollViewLands,pageControlLands;

FinalCell *cell;

NSString *card_name;
NSString *date_created;
NSString *date_updated;
NSString *cardid;
NSString *image_reference;
NSString *message_text;
NSString *saved_status;
NSString *smiley_ids;
NSString *thumbnail_url;
NSString *audioIds;
NSArray *firstName;
NSArray *lastname;
NSArray *address1;
NSArray *address2;
NSArray *city;
NSArray *state;
NSArray *country;
NSArray *toImage;
NSArray *zipcode;
NSArray *emailArr;
NSArray *phoneArr;
NSString *netCoins;
NSArray *toContactID;
int spend_coins;
int total_amount;
UIActionSheet *actionSheetGet;
UIActionSheet *actionSheetContinue;
NSMutableArray *countryArray;
int selected_index;

NSString *counteyString;
NSString *imageURL;
NSString *selectedContactID;
UIActivityIndicatorView *Activity;
UIAlertView *progressAlert;

//// Version Update ///

- (IBAction)editAddress
{
    //(@"EDIT");
    
    
    
    _edittitleText.text=[NSString stringWithFormat:@"%@ %@",firstName[selected_index],lastname[selected_index]];
    _editaddress1.text=[address1 objectAtIndex:selected_index];
     _editaddress2.text=[address2 objectAtIndex:selected_index];
     _editcityValue.text=[city objectAtIndex:selected_index];
     _editstate.text=[state objectAtIndex:selected_index];
     _editzipCodeValue.text=[zipcode objectAtIndex:selected_index];
    _editemailID.text=[emailArr objectAtIndex:selected_index];
     _editphoneNo.text=[phoneArr objectAtIndex:selected_index];
    counteyString=[country objectAtIndex:selected_index];
    [_editcountrybtn setTitle:[NSString stringWithFormat:@"   %@",[country objectAtIndex:selected_index]] forState:UIControlStateNormal];
    
    
    selectedContactID=[toContactID objectAtIndex:selected_index];
    
    
    NSString *temp=[toImage objectAtIndex:selected_index];
    
    
    imageURL=[toImage objectAtIndex:selected_index];
    
   // UIImageView *tempImg=[[UIImageView alloc]init];
    [_imageButton1 setImageWithURL:[NSURL URLWithString:temp]
                    placeholderImage:[UIImage imageNamed:@"img_mt.png"]];
    
    
    
    //[_imageButton setImage:tempImg.image forState:UIControlStateNormal];
   // [_imageButton setTitle:nil forState:UIControlStateNormal];
    
    
    _contactAddView.hidden=NO;
}

- (IBAction)back_contact
{
    _contactAddView.hidden=YES;
}
- (IBAction)save_contact
{
   
    
    //(@"STR:%@",_editaddress1.text);
    //(@"STR:%@",_editaddress2.text);
    //(@"STR:%@",_editcityValue.text);
    //(@"STR:%@",_editemailID.text);
    //(@"STR:%@",_editphoneNo.text);
    //(@"STR:%@",_editstate.text);
    //(@"STR:%@",_editzipCodeValue.text);
    //(@"STR:%@",counteyString);
     //(@"STR:%@",imageURL);
    
    
   
        
            if ([_editaddress1.text isEqualToString:@""])
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Enter the address" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                [alert show];
            }
            else
            {
                if ([_editcityValue.text isEqualToString:@""])
                {
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Enter the City" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    
                    [alert show];
                    
                }
                else
                {
                    if ([_editstate.text isEqualToString:@""])
                    {
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Enter the state" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        
                        [alert show];
                    }
                    else
                    {
                        if ([counteyString isEqualToString:@""])
                        {
                            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Select the Country" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                            
                            [alert show];
                        }
                        else
                        {
                            if ([counteyString isEqualToString:@""])
                            {
                                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Enter the zip code" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                
                                [alert show];
                            }
                            else
                            {
                                //////(@"SUCCESS");
                                
                                
                                if ([_editemailID.text isEqualToString:@""])
                                {
                                    if ([_editphoneNo.text isEqualToString:@""])
                                    {
                                        [self addcontectFun];
                                    }
                                    else
                                    {
                                        NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
                                        
                                        // NSString *phoneRegex = @"^[\+(00)][0-9]{6,14}$";
                                        NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
                                        
                                        BOOL phoneValidates = [phoneTest evaluateWithObject:_editphoneNo.text];
                                        
                                        
                                        
                                        if (phoneValidates == YES){
                                            
                                            ////(@"Matched");
                                            
                                            [self addcontectFun];
                                        }
                                        else {
                                            ////(@"Not matched");
                                            
                                            
                                            UIAlertView *loginalert = [[UIAlertView alloc] initWithTitle:@"" message:@"Enter Valid Phone Number" delegate:self
                                                                                       cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                            
                                            [loginalert show];
                                            
                                        }
                                        
                                    }
                                    
                                    
                                }
                                else
                                {
                                    //////(@"SUCCESS");
                                    
                                    NSString* emailRegex = @"[A-Z0-9a-z._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}";
                                    NSPredicate* emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
                                    NSString *subjectString =_editemailID.text;
                                    
                                    
                                    if((_editemailID.text.length!=0) && ([emailTest evaluateWithObject:subjectString] != YES))
                                    {
                                        
                                        if ([emailTest evaluateWithObject:subjectString] != YES)
                                            
                                        {
                                            
                                            
                                            UIAlertView *loginalert = [[UIAlertView alloc] initWithTitle:@"Enter Valid Email ID" message:@"abc@example.com format" delegate:self
                                                                                       cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                            
                                            [loginalert show];
                                            
                                            
                                        }
                                        
                                        
                                    }
                                    
                                    else
                                    {
                                        
                                        
                                        
                                        [self addcontectFun];
                                        
                                    }
                                }
                                
                                
                                
                            }
                            
                        }
                    }
                }
            }
        
    
    
    
    
   
    
}


-(void)addcontectFun
{
    
    
    
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/contacts/updateContact",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post1234 = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *userImage=[check objectForKey:@"USER_IMAGE"];
    NSString *user=[check objectForKey:@"USERID"];
    
    if ([userImage isEqualToString:@"NULL"])
    {
        userImage=@"http://dev.socialnetgateapp.com/bundles/acmeregistration/images/no-user-image.png";
    }
    
    
    //////(@"USER IMAGE:%@",userImage);
    
    
    
    [request_post1234 setPostValue:selectedContactID forKey:@"id"];
    [request_post1234 setPostValue:_editaddress1.text forKey:@"address1"];
    [request_post1234 setPostValue:_editaddress2.text forKey:@"address2"];
    [request_post1234 setPostValue:_editcityValue.text forKey:@"city"];
    [request_post1234 setPostValue:_editstate.text forKey:@"state"];
    [request_post1234 setPostValue:counteyString forKey:@"country"];
    [request_post1234 setPostValue:[Config get_token] forKey:@"apikey"];
    [request_post1234 setPostValue:@"1" forKey:@"address_entered"];
    [request_post1234 setPostValue:_editzipCodeValue.text forKey:@"zipcode"];
    [request_post1234 setPostValue:_editemailID.text forKey:@"emailid"];
    [request_post1234 setPostValue:_editphoneNo.text forKey:@"phoneno"];
     [request_post1234 setPostValue:imageURL forKey:@"friend_facebookproimg"];
    
    
    [request_post1234 setTimeOutSeconds:30];
    
    
    [request_post1234 setCompletionBlock:^{
        NSString *responseString23 = [request_post1234 responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        
        //(@"RESULTS LOGIN:%@",results11);
        
        
        NSString *des=[results11 valueForKeyPath:@"header.description"];
        
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:des delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
        
        
       
        
        _contactAddView.hidden=YES;
        
                
        
        if (IS_IPHONE_5)
        {
            
            Listofreceiptions *viewController=[[Listofreceiptions alloc]initWithNibName:@"Listofreceiptions" bundle:nil];
            [self presentViewController:viewController animated:NO completion:nil];
            
        }
        else
        {
            
            Listofreceiptions *viewController=[[Listofreceiptions alloc]initWithNibName:@"Listofreceiptions~iPhone" bundle:nil];
            [self presentViewController:viewController animated:NO completion:nil];
        }

        
        
      //  [self viewDidLoad];
        
    }];
    [request_post1234 setFailedBlock:^{
        NSError *error = [request_post1234 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1234 startAsynchronous];
}

-(IBAction)donePicker2
{
    
    _pickerBack2.hidden=YES;
}

- (IBAction)editcountryButton:(id)sender
{
  _pickerBack2.hidden=NO;
}

-(IBAction)browseImage
{
    
    [_editzipCodeValue resignFirstResponder];
    [_editstate resignFirstResponder];
    [_editphoneNo resignFirstResponder];
    [_editemailID resignFirstResponder];
    [_editcityValue resignFirstResponder];
    [_editaddress2 resignFirstResponder];
    [_editaddress1 resignFirstResponder];
    
    NSString *actionSheetTitle = @"Select your Picture"; //Action Sheet Title
    // NSString *destructiveTitle = @"Destructive Button"; //Action Sheet Button Titles
    NSString *other1 = @"Camera";
    NSString *other2 = @"Album";
    
    NSString *cancelTitle = @"Cancel";
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:actionSheetTitle
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:other1, other2,  nil];
    [actionSheet showInView:self.view];
    
    
}



-(void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary*)info
{
    NSData *imagedata = UIImageJPEGRepresentation([info objectForKey:@"UIImagePickerControllerEditedImage" ],1);
    UIImage *image = [[UIImage alloc] initWithData:imagedata];
    
    
    _savecontactbtn.enabled=NO;
    
    //image = [self imageWithImageSimple:image scaledToSize:CGSizeMake(240, 240)];
    
    float ori_height=image.size.height;
    float ori_width=image.size.width;
    
    NSLog(@"H:%f|W:%f",ori_height,ori_width);
    
    
    float height_ratio1=ori_height/4;
    float width_ratio1=ori_width/4;
    
    NSLog(@"HJ:%f|WJ:%f",height_ratio1,width_ratio1);
    
    
    
    image = [self imageWithImageSimple:image scaledToSize:CGSizeMake(width_ratio1, height_ratio1)];

    
    
    
    //[_imageButton setImage:image forState:UIControlStateNormal];
    //[_imageButton setTitle:nil forState:UIControlStateNormal];
    
  
    _imageButton1.image=image;
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/files/postProImgfile",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
    
    
    //[request_post setFile:@"/Users/apple/Desktop/Free-Grammar-Ebook-Level-2.pdf" forKey:@"filename"];
    
    //UIImage * ourImage = [UIImage imageNamed:@"rajini-bday2.jpg"];
    NSData * ourImageData = UIImageJPEGRepresentation(image, 100);
    
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"HH:mm:ss zzz"];
    NSString *dateString = [dateFormat stringFromDate:date];
    
    //////(@"DATE STRING:%@",dateString);
    
    NSString *newString = [dateString stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *newString1 = [newString stringByReplacingOccurrencesOfString:@":" withString:@""];
    NSString *newString2 = [newString1 stringByReplacingOccurrencesOfString:@"GMT" withString:@""];
    NSString *newString3 = [newString2 stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    NSString *imageName=[NSString stringWithFormat:@"%@.jpg",newString3];
    
    
    
    [request_post addData:ourImageData withFileName:imageName andContentType:@"image/jpeg" forKey:@"file"];
    
    [request_post setTimeOutSeconds:60];
    
    
    [request_post setCompletionBlock:^{
        // Use when fetching text data
        NSString *responseString = [request_post responseString];
        
        
        //////(@"RES:%@",responseString);
        
        NSMutableData *results11 = [responseString JSONValue];
        
        
        NSLog(@"RESULTS LOGIN:%@",results11);
        
        
        NSString *temp= [results11 valueForKeyPath:@"photos.profile_img_url"];
        
        
        imageURL=temp;
        
        NSUserDefaults *imgtemp=[NSUserDefaults standardUserDefaults];
        
        [imgtemp setObject:temp forKey:@"USER_IMAGE"];
        
         _savecontactbtn.enabled=YES;
        
        
        if ([temp length]==0) {
            
            NSString *temp= [results11 valueForKeyPath:@"error.text"];
            
            
            NSUserDefaults *imgtemp=[NSUserDefaults standardUserDefaults];
            
            [imgtemp setObject:@"NULL" forKey:@"USER_IMAGE"];
            
            [_imageButton setImage:nil forState:UIControlStateNormal];
            [_imageButton setTitle:@"Select Image" forState:UIControlStateNormal];
            
        }
        
        // Use when fetching binary data
        // NSData *responseData = [request_post responseData];
    }];
    [request_post setFailedBlock:^{
        NSError *error = [request_post error];
        
        
        
        
    }];
    [request_post startAsynchronous];
    
    
    [picker dismissModalViewControllerAnimated:YES];
    
    
    
    
}

-(UIImage *)imageWithImageSimple:(UIImage*)image scaledToSize:(CGSize)newSize
{
    
    UIGraphicsBeginImageContext(newSize);//根据当前大小创建一个基于位图图形的环境
    
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];//根据新的尺寸画出传过来的图片
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();//从当前环境当中得到重绘的图片
    
    UIGraphicsEndImageContext();//关闭当前环境
    
    return newImage;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissModalViewControllerAnimated:YES];
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    
    return 1;//Or return whatever as you intend
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
    
    return [countryArray count];//Or, return as suitable for you...normally we use array for dynamic
}
- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [NSString stringWithFormat:@"%@",countryArray[row]];//Or, your suitable title; like Choice-a, etc.
}
- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
            
        //Here, like the table view you can get the each section of each row if you've multiple sections
        //(@"Selected Color: %@.",countryArray[row]);
    counteyString=countryArray[row];
       [_editcountrybtn setTitle:[NSString stringWithFormat:@"    %@", countryArray[row]] forState:UIControlStateNormal];
        
        //countrystr=countryArray[row];
  
    
    
}












-(IBAction)continueButton
{
    
    
    if ([netCoins intValue]<total_amount)
    {
        
       
        
        
        NSString *actionSheetTitle = nil; //Action Sheet Title
        //NSString *destructiveTitle = @"Destructive Button"; //Action Sheet Button Titles
        NSString *other1 = @"Get NetCoins";
        NSString *other2 = @"Send by E-mail";
        NSString *other3 = @"Share this SocialCard";
        NSString *cancelTitle = @"Cancel";
        actionSheetContinue = [[UIActionSheet alloc]
                               initWithTitle:actionSheetTitle
                               delegate:self
                               cancelButtonTitle:cancelTitle
                               destructiveButtonTitle:nil
                               otherButtonTitles:other1, other2,other3, nil];
        [actionSheetContinue showInView:self.view];
    }
    else
    {
        
        
        NSString *actionSheetTitle = nil; //Action Sheet Title
        //NSString *destructiveTitle = @"Destructive Button"; //Action Sheet Button Titles
        NSString *other1 = @"Confirm";
        NSString *other2 = @"Send by E-mail";
        NSString *other3 = @"Share this SocialCard";
        NSString *cancelTitle = @"Cancel";
        actionSheetContinue = [[UIActionSheet alloc]
                               initWithTitle:actionSheetTitle
                               delegate:self
                               cancelButtonTitle:cancelTitle
                               destructiveButtonTitle:nil
                               otherButtonTitles:other1, other2,other3, nil];
        [actionSheetContinue showInView:self.view];
      
        
        
    }

    
    
    
   
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view addSubview:_landscapeView];
    _landscapeView.hidden=YES;
    
    
    [self.view addSubview:redeem_view];
    redeem_view.hidden=YES;
    [self.view addSubview:_contactAddView];
    _contactAddView.hidden=YES;
    selected_index=0;
    
    _pickerBack2.hidden=YES;
    
     self.trackedViewName = @"Card Confirmation Screen";
    
    [_allBackView addSubview:_socialCardView];
    [_allBackView addSubview:_emailView];
    [_allBackView addSubview:_shareview];
    
    
    
    _socialCardView.hidden=NO;
    _emailView.hidden=YES;
    _shareview.hidden=YES;
    
    CGFloat screenScale = [[UIScreen mainScreen] scale];
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    CGSize screenSize = CGSizeMake(screenBounds.size.width * screenScale, screenBounds.size.height * screenScale);
    if (screenSize.height==1136.000000)
    {
        scroll.frame = CGRectMake(0, 45, 320, 460);
        scroll.contentSize = CGSizeMake(320, 750);
        [scroll addSubview:scroll_back_view];
    }
    else
    {
        scroll.frame = CGRectMake(0, 45, 320, 365);
        scroll.contentSize = CGSizeMake(320, 750);
        [scroll addSubview:scroll_back_view];
    }

    
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    
    NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:@"profileImage.jpg"];
    UIImage *profileIMG = [UIImage imageWithContentsOfFile:getImagePath];
    
    NSUserDefaults *update=[NSUserDefaults standardUserDefaults];
    
     
    
    netCoins=[update objectForKey:@"TOTAL_COINS"];
    card_name=[update objectForKey:@"CARD_NAME"];
    date_created=[update objectForKey:@"CREATED"];
    date_updated=[update objectForKey:@"UPDATED"];
    cardid=[update objectForKey:@"CARD_ID"];
    image_reference=[update objectForKey:@"IMAGE"];
    message_text=[update objectForKey:@"MESSAGE"];
    saved_status=[update objectForKey:@"SAVED_STATUS"];
    smiley_ids=[update objectForKey:@"SMILIES"];
    thumbnail_url=[update objectForKey:@"THUMP"];
    audioIds=[update objectForKey:@"AUDIO"];
    
    //(@"TOTAL COINS:%@",cardid);
    
    
     ////(@"TOTAL image_reference:%@",image_reference);
    
     firstName=[update objectForKey:@"FIRST_NAME"];
    lastname=[update objectForKey:@"LAST_NAME"];
    address1=[update objectForKey:@"ADDRESS1"];
    address2=[update objectForKey:@"ADDRESS2"];
    city=[update objectForKey:@"CITY"];
    state=[update objectForKey:@"STATE"];
    country=[update objectForKey:@"COUNTRY"];
    zipcode=[update objectForKey:@"ZIP"];
    toImage=[update objectForKey:@"TO_IMAGE"];
    phoneArr=[update objectForKey:@"PHONES"];
    emailArr=[update objectForKey:@"EMAILS"];
    countryArray=[[NSMutableArray alloc]init];
    toContactID=[update objectForKey:@"TO_CONTACT_ID"];
    countryArray=[update objectForKey:@"COUNTRY_ARRAY"];
    
    
    NSArray *lines;
    
    if (![smiley_ids isEqualToString:@""])
    {
        
        
      lines= [smiley_ids componentsSeparatedByString: @","];
        
        @try {
              ////(@"LINES:%d",[lines count]);
             total_amount=[lines count];
        }
        @catch (NSException *exception) {
            total_amount=1;
        }
               
        
        
        
        [self getSmilies:smiley_ids];
    }
    else
    {
         total_amount=0;
    }
    
    _playAudio.hidden=YES;
    _playAudio1.hidden=YES;
    
    
    
    if ([audioIds isEqualToString:@""]||[audioIds isEqualToString:@"0"]||[audioIds length]==0 )
    {
        
        
        //lines= [smiley_ids componentsSeparatedByString: @","];
        
        // ////(@"LINES:%@",[lines count]);
        
        
        
        
    }
    else
    {
        
        [self getAudio:audioIds];
    }
     

    
    NSArray *countryAmount=[update objectForKey:@"COUNTRY_AMOUNT"];
     NSArray *countryNameArr=[update objectForKey:@"COUNTRY_ARRAY"];
     NSArray *smilyAmountArr=[update objectForKey:@"SMILY_AMOUNT"];
    
  
    
    for (int i=0;i<[country count]; i++)
    {
        NSString *countryName=country[i];
        
        for (int j=0; j<[countryNameArr count]; j++)
        {
            NSString *compare=countryNameArr[j];
            
            if ([countryName isEqualToString:compare])
            {
                
                int amount=[countryAmount[j]intValue];
                
                
                total_amount=total_amount+amount;
            }
            else
            {
                //int amount=10;
                
                
               //total_amount=amount;
                
               // //(@"TOTAL:%d",total_amount);
            }
            
        }
        
    }

    spend_coins=0;
    
  
    if ([audioIds isEqualToString:@""]||[audioIds isEqualToString:@"0"]||[audioIds length]==0 )
    {
        
        
        //lines= [smiley_ids componentsSeparatedByString: @","];
        
        // ////(@"LINES:%@",[lines count]);
        
         spend_coins=total_amount;
        ////(@"IF");
        
    }
    else
    {
        ////(@"ELSE");
        
        
         total_amount=total_amount+2;
    }

    
   
    
    
    
    ////(@"Total AMount:%d",total_amount);
    
   
    
   
    
    //[_profileButton setImage:profileIMG forState:UIControlStateNormal];
    
    
    _fromImageView.image=profileIMG;
    _fromImageView1.image=profileIMG;
    
    NSString *chek_str=message_text;
    
    NSString *newString = [[chek_str componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
    
    _messageText.text=newString;
     _messageText1.text=newString;
    
    
    
    _titleText.text=card_name;
    _titleText1.text=card_name;
    
    
    
    [_picker2 addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pickerTapped:)] ];
   
    
    [_toImageView setImageWithURL:[NSURL URLWithString:toImage[0]]
             placeholderImage:[UIImage imageNamed:@"bigicon.png"]];
    
    _nameText.text=[NSString stringWithFormat:@"%@ %@",firstName[0],lastname[0]];
    
    _ad1.text=[NSString stringWithFormat:@"%@ %@",address1[0],address2[0]];
    _ad2.text=[NSString stringWithFormat:@"%@,%@",city[0],state[0]];
    _ad3.text=[NSString stringWithFormat:@"%@",country[0]];
    _ad4.text=[NSString stringWithFormat:@"%@",zipcode[0]];
    
    _nameText1.text=[NSString stringWithFormat:@"%@ %@",firstName[0],lastname[0]];
    
    _ad11.text=[NSString stringWithFormat:@"%@ %@",address1[0],address2[0]];
    _ad21.text=[NSString stringWithFormat:@"%@,%@",city[0],state[0]];
    _ad31.text=[NSString stringWithFormat:@"%@",country[0]];
    _ad41.text=[NSString stringWithFormat:@"%@",zipcode[0]];
    
    
    //_addressText.text=[NSString stringWithFormat:@"%@ %@,\n%@,%@,\n%@,%@",address1[0],address2[0],city[0],state[0],country[0],zipcode[0]];

    if ([netCoins intValue]<0)
    {
        netCoins=@"0";
    }
    
    
    ////(@"R!");
    
    if ([netCoins intValue]<spend_coins)
    {
        _buyButton.hidden=NO;
        _sendButton.hidden=YES;
        NSString *label_for_display=[NSString stringWithFormat:@" NetCoins Balance: %@. Required NetCoins: %d.",netCoins,total_amount];
        _topLabel.text=label_for_display;
    }
    else
    {
       
        
        _buyButton.hidden=YES;
        _sendButton.hidden=NO;
        
        NSString *label_for_display=[NSString stringWithFormat:@" NetCoins Balance: %@. Required NetCoins: %d",netCoins,total_amount];
        _topLabel.text=label_for_display;
        
        //_topLabel.text=@"Need 10 Net coins. You have 100 Net coins";

        
    }
     
   
    [_finalImageView setImageWithURL:[NSURL URLWithString:image_reference]
                 placeholderImage:[UIImage imageNamed:@"bigicon.png"]];
    
    [_finalImageView1 setImageWithURL:[NSURL URLWithString:image_reference]
                    placeholderImage:[UIImage imageNamed:@"bigicon.png"]];
    
     [self.collectionViewFinal registerNib:[UINib nibWithNibName:@"FinalCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
    
    
     pageControlBeingUsed = NO;
    
    NSArray *colors1 = [NSArray arrayWithObjects:_cardView1,_cardView2,_cardView3,_cardView4, nil];
    NSArray *colors = [NSArray arrayWithObjects:[UIColor redColor], [UIColor greenColor], [UIColor blueColor],[UIColor blueColor], nil];
	for (int i = 0; i < colors.count; i++) {
		CGRect frame;
		frame.origin.x = self.scrollView.frame.size.width * i;
		frame.origin.y = 0;
		frame.size = self.scrollView.frame.size;
		
		UIView *subview = [[UIView alloc] initWithFrame:frame];
		//subview.backgroundColor = [colors objectAtIndex:i];
        [subview addSubview:[colors1 objectAtIndex:i]];
		[self.scrollView addSubview:subview];
		
	}
	
	self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * colors.count, self.scrollView.frame.size.height);
	
	self.pageControl.currentPage = 0;
    
    _front_img.image=[UIImage imageNamed:@"sfrontact1.png"];
     _bage_img.image=[UIImage imageNamed:@"sback.png"];
    

    
	self.pageControl.numberOfPages = colors.count;
    self.scrollView.pagingEnabled = YES;
    
    
    
    
    
    pageControlBeingUsedLands = NO;
    
    NSArray *colors11 = [NSArray arrayWithObjects:_cardViewLands1,_cardViewLands2, nil];
    
    
    NSArray *colors111 = [NSArray arrayWithObjects:[UIColor redColor], [UIColor greenColor], nil];
    
    
    
	for (int i = 0; i < colors11.count; i++) {
		CGRect frame;
		frame.origin.x = self.scrollViewLands.frame.size.width * i;
		frame.origin.y = 0;
		frame.size = self.scrollViewLands.frame.size;
		
		UIView *subview = [[UIView alloc] initWithFrame:frame];
		//subview.backgroundColor = [colors objectAtIndex:i];
        [subview addSubview:[colors11 objectAtIndex:i]];
		[self.scrollViewLands addSubview:subview];
		
	}
	
	self.scrollViewLands.contentSize = CGSizeMake(self.scrollViewLands.frame.size.width * colors11.count, self.scrollViewLands.frame.size.height);
	
	self.pageControlLands.currentPage = 0;
      
    
	self.pageControlLands.numberOfPages = colors11.count;
    self.scrollViewLands.pagingEnabled = YES;
    
       
    
}

-(void)pickerTapped:(UIGestureRecognizer *)gestureRecognizer
{
    //(@"TABBED");
    
    //(@"TABBED");
    
    //- (NSInteger)selectedRowInComponent:(NSInteger)component
    
    int valInt=[_picker2 selectedRowInComponent:0];
    
    //(@"TABBED:%d",valInt);
    

    
    
    //(@"Selected Color: %@.",countryArray[valInt]);
    counteyString=countryArray[valInt];
    [_editcountrybtn setTitle:[NSString stringWithFormat:@"    %@", countryArray[valInt]] forState:UIControlStateNormal];
}


-(IBAction)b1_Click
{
    
    CGPoint bottomOffset = CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
    [self.scrollView setContentOffset:bottomOffset animated:YES];
  
//
//    
//    [_b1 setTitle:@"A" forState:UIControlStateNormal];
//    [_b2 setTitle:@"B" forState:UIControlStateNormal];
//    [_b3 setTitle:@"B" forState:UIControlStateNormal];
//    [_b4 setTitle:@"B" forState:UIControlStateNormal];
   
}
-(IBAction)b2_Click
{
   
    CGPoint bottomOffset = CGPointMake(320, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
    [self.scrollView setContentOffset:bottomOffset animated:YES];
    
//    [_b1 setTitle:@"B" forState:UIControlStateNormal];
//    [_b2 setTitle:@"A" forState:UIControlStateNormal];
//    [_b3 setTitle:@"B" forState:UIControlStateNormal];
//    [_b4 setTitle:@"B" forState:UIControlStateNormal];
    
	
 
}
-(IBAction)b3_Click
{
    CGPoint bottomOffset = CGPointMake(640, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
    [self.scrollView setContentOffset:bottomOffset animated:YES];
    
//    
//    [_b1 setTitle:@"B" forState:UIControlStateNormal];
//    [_b2 setTitle:@"B" forState:UIControlStateNormal];
//    [_b3 setTitle:@"A" forState:UIControlStateNormal];
//    [_b4 setTitle:@"B" forState:UIControlStateNormal];
  
}
-(IBAction)b4_Click
{
   
    CGPoint bottomOffset = CGPointMake(960, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
    [self.scrollView setContentOffset:bottomOffset animated:YES];
//    
//    [_b1 setTitle:@"B" forState:UIControlStateNormal];
//    [_b2 setTitle:@"B" forState:UIControlStateNormal];
//    [_b3 setTitle:@"B" forState:UIControlStateNormal];
//    [_b4 setTitle:@"A" forState:UIControlStateNormal];
  
}

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    
    
    if (sender==scrollViewLands)
        
    {
        
      
        if (!pageControlBeingUsedLands)
        {
            // Switch the indicator when more than 50% of the previous/next page is visible
            CGFloat pageWidth = self.scrollViewLands.frame.size.width;
            int page = floor((self.scrollViewLands.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
            self.pageControlLands.currentPage = page;
        }

    }
    else
    {
    
   // ////(@"CURRENT:%d",self.pageControl.currentPage);
    
    if (self.pageControl.currentPage==0)
    {
//        [_b1 setTitle:@"A" forState:UIControlStateNormal];
//        [_b2 setTitle:@"B" forState:UIControlStateNormal];
//        [_b3 setTitle:@"B" forState:UIControlStateNormal];
//        [_b4 setTitle:@"B" forState:UIControlStateNormal];
        
        _front_img.image=[UIImage imageNamed:@"sfrontact1.png"];
        _bage_img.image=[UIImage imageNamed:@"sback.png"];

    }
    
    if (self.pageControl.currentPage==1)
    {
//        [_b1 setTitle:@"B" forState:UIControlStateNormal];
//        [_b2 setTitle:@"A" forState:UIControlStateNormal];
//        [_b3 setTitle:@"B" forState:UIControlStateNormal];
//        [_b4 setTitle:@"B" forState:UIControlStateNormal];
        
        _front_img.image=[UIImage imageNamed:@"sfrontact2.png"];
        _bage_img.image=[UIImage imageNamed:@"sback.png"];

    }
    
    if (self.pageControl.currentPage==2)
    {
//        [_b1 setTitle:@"B" forState:UIControlStateNormal];
//        [_b2 setTitle:@"B" forState:UIControlStateNormal];
//        [_b3 setTitle:@"A" forState:UIControlStateNormal];
//        [_b4 setTitle:@"B" forState:UIControlStateNormal];
        
        _front_img.image=[UIImage imageNamed:@"sfront.png"];
        _bage_img.image=[UIImage imageNamed:@"sbackact1.png"];

        
    }
    
    if (self.pageControl.currentPage==3)
    {
//        [_b1 setTitle:@"B" forState:UIControlStateNormal];
//        [_b2 setTitle:@"B" forState:UIControlStateNormal];
//        [_b3 setTitle:@"B" forState:UIControlStateNormal];
//        [_b4 setTitle:@"A" forState:UIControlStateNormal];
        
        _front_img.image=[UIImage imageNamed:@"sfront.png"];
        _bage_img.image=[UIImage imageNamed:@"sbackact2.png"];

    }
    
	if (!pageControlBeingUsed)
    {
		// Switch the indicator when more than 50% of the previous/next page is visible
		CGFloat pageWidth = self.scrollView.frame.size.width;
		int page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
		self.pageControl.currentPage = page;
	}
        
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if (scrollView==scrollViewLands)
        
    {
        pageControlBeingUsedLands = NO;
    }
    else
    {
	pageControlBeingUsed = NO;
        
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView==scrollViewLands)
        
    {
        pageControlBeingUsedLands = NO;
    }
    else
    {
	pageControlBeingUsed = NO;
    }
}

- (IBAction)changePage {
	// Update the scroll view to the appropriate page
	CGRect frame;
	frame.origin.x = self.scrollView.frame.size.width * self.pageControl.currentPage;
	frame.origin.y = 0;
	frame.size = self.scrollView.frame.size;
	[self.scrollView scrollRectToVisible:frame animated:YES];
	
	// Keep track of when scrolls happen in response to the page control
	// value changing. If we don't do this, a noticeable "flashing" occurs
	// as the the scroll delegate will temporarily switch back the page
	// number.
	pageControlBeingUsed = YES;
}


- (IBAction)changePageLands {
	// Update the scroll view to the appropriate page
	CGRect frame;
	frame.origin.x = self.scrollViewLands.frame.size.width * self.pageControlLands.currentPage;
	frame.origin.y = 0;
	frame.size = self.scrollViewLands.frame.size;
	[self.scrollViewLands scrollRectToVisible:frame animated:YES];
	
	// Keep track of when scrolls happen in response to the page control
	// value changing. If we don't do this, a noticeable "flashing" occurs
	// as the the scroll delegate will temporarily switch back the page
	// number.
	pageControlBeingUsedLands = YES;
}


- (IBAction)AudioPlay:(id)sender
{
    
    _playAudio.selected=_playAudio.selected?NO : YES;
     _playAudio1.selected=_playAudio1.selected?NO : YES;
    
    if (_playAudio.selected)
    {
        
        
        
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        NSString *audioString1 = [documentsDirectory stringByAppendingPathComponent:@"SendAudio"];
        
        NSString *audioString=[NSString stringWithFormat:@"%@.m4a",audioString1];
        
        
        NSUserDefaults *Confirmation=[NSUserDefaults standardUserDefaults];
        
        [Confirmation setObject:audioString forKey:@"AUDIO_STRING"];
        
        
        NSURL *url = [NSURL fileURLWithPath:audioString];
        
        
        
        NSError *error;
        audioPlayer = [[AVAudioPlayer alloc]
                       initWithContentsOfURL:url
                       error:&error];
        //audioPlayer.numberOfLoops=-1;
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
        [[AVAudioSession sharedInstance] setActive: YES error: nil];
        [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
        
        
        ////(@"TOTAL:%f",audioPlayer.duration);
        
        
        if (error)
        {
//            ////(@"Error in audioPlayer: %@",
//                  [error localizedDescription]);
            
            
            
            
            
            
            
            
            
            
        }
        else
            
        {
            audioPlayer.delegate = self;
            
            
            
            [audioPlayer prepareToPlay];
        }
        
        
        
        
        audioPlayer.volume=1.5;
        
        
        [audioPlayer play];
    }
    
    else
    {
        ////(@"It is deselected");
        
        
        
        NSUserDefaults *Confirmation=[NSUserDefaults standardUserDefaults];
        
        [Confirmation setObject:@"YES" forKey:@"AUDIO_STOP"];
        
        
        [audioPlayer stop];
    }
    
    if (_playAudio1.selected)
    {
        
        
        
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        NSString *audioString1 = [documentsDirectory stringByAppendingPathComponent:@"SendAudio"];
        
        NSString *audioString=[NSString stringWithFormat:@"%@.m4a",audioString1];
        
        
        NSUserDefaults *Confirmation=[NSUserDefaults standardUserDefaults];
        
        [Confirmation setObject:audioString forKey:@"AUDIO_STRING"];
        
        
        NSURL *url = [NSURL fileURLWithPath:audioString];
        
        
        
        NSError *error;
        audioPlayer = [[AVAudioPlayer alloc]
                       initWithContentsOfURL:url
                       error:&error];
        //audioPlayer.numberOfLoops=-1;
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
        [[AVAudioSession sharedInstance] setActive: YES error: nil];
        [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
        
        
        ////(@"TOTAL:%f",audioPlayer.duration);
        
        
        if (error)
        {
            //            ////(@"Error in audioPlayer: %@",
            //                  [error localizedDescription]);
            
            
            
            
            
            
            
            
            
            
        }
        else
            
        {
            audioPlayer.delegate = self;
            
            
            
            [audioPlayer prepareToPlay];
        }
        
        
        
        
        audioPlayer.volume=1.5;
        
        
        [audioPlayer play];
    }
    
    else
    {
        ////(@"It is deselected");
        
        
        
        NSUserDefaults *Confirmation=[NSUserDefaults standardUserDefaults];
        
        [Confirmation setObject:@"YES" forKey:@"AUDIO_STOP"];
        
        
        [audioPlayer stop];
    }
}

-(void)audioPlayerDidFinishPlaying:
(AVAudioPlayer *)player successfully:(BOOL)flag
{
    _playAudio.selected=NO;
    _playAudio1.selected=NO;
    ////(@"audioPlayerDidFinishPlaying");
    
}


-(void)getAudio:(NSString *)audio
{
    ////(@"AUDIO:%@",audio);
    
    
    
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/audios/retrieveCardAudios",CONFIG_BASE_URL]];
    
    
    __block  ASIFormDataRequest *requestmethod = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    NSString *cardID=[check objectForKey:@"CARD_ID"];
    
    NSMutableArray *smilyIDArr=[[NSMutableArray alloc]init];
    NSMutableArray *smilyName=[[NSMutableArray alloc]init];
    
    [requestmethod setPostValue:user forKey:@"userID"];
    [requestmethod setPostValue:audio forKey:@"audio_id"];
     [requestmethod setPostValue:[Config get_token] forKey:@"apikey"];
    [requestmethod setTimeOutSeconds:30];
    
    
    [requestmethod setCompletionBlock:^{
        NSString *responseString23 = [requestmethod responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        ////(@"RESULTS:%@",results11);
        
        
        NSArray *temp= [results11 valueForKeyPath:@"audio.audio_url"];
        
        
        
        ////(@"TEMP TEMP:%@",temp[0]);
        
        
        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSURL *url=[NSURL URLWithString:temp[0]];
        
        
        ////(@"Dowload Start");
        
        ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
        
        
        
        
        //  [collectionView reloadData];
        
        [request setShouldContinueWhenAppEntersBackground:YES];
        
        
        [request setCompletionBlock:^{
            // Use when fetching text data
            
            NSString *responseString = [request responseString];
            
            
            
            // Use when fetching binary data
            NSData *responseData = [request responseData];
            
            NSString *epub_name=[NSString stringWithFormat:@"SendAudio.m4a"];
            
            
            NSString *filePath = [documentsDirectory stringByAppendingPathComponent:epub_name];
            
            [responseData writeToFile:filePath atomically:YES];
            
            ////(@"Dowload Completes");
            
            _playAudio.hidden=NO;
            _playAudio1.hidden=NO;
            
            
        }];
        [request setFailedBlock:^
         {
             // NSError *error = [request error];
             
             
             UIAlertView *error_Alert=[[UIAlertView alloc]initWithTitle:@"Download Error" message:[NSString stringWithFormat:@"%@",[request error]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             
             [error_Alert show];
             
             
         }];
        [request startAsynchronous];
        
        
        
        
    }];
    [requestmethod setFailedBlock:^{
        NSError *error = [requestmethod error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [requestmethod startAsynchronous];
    
    
}


-(IBAction)redeemSubmit
{
  
        
        UIFont * titleFont = [UIFont fontWithName:@"System" size:20.0f];
        UIFont * messageFont = [UIFont fontWithName:@"System" size:12.0f];
        
        // Set default appearnce block for all WCAlertViews
        // Similar functionality to UIAppearnce Proxy
        
        [WCAlertView setDefaultCustomiaztonBlock:^(WCAlertView *alertView) {
            alertView.labelTextColor = [UIColor colorWithRed:0.11f green:0.08f blue:0.39f alpha:1.00f];
            alertView.labelShadowColor = [UIColor whiteColor];
            
            UIColor *topGradient = [UIColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:1.0f];
            UIColor *middleGradient = [UIColor colorWithRed:0.93f green:0.94f blue:0.96f alpha:1.0f];
            UIColor *bottomGradient = [UIColor colorWithRed:0.89f green:0.89f blue:0.92f alpha:1.00f];
            alertView.gradientColors = @[topGradient,middleGradient,bottomGradient];
            
            alertView.outerFrameColor = [UIColor colorWithRed:250.0f/255.0f green:250.0f/255.0f blue:250.0f/255.0f alpha:1.0f];
            
            alertView.buttonTextColor = [UIColor colorWithRed:0.11f green:0.08f blue:0.39f alpha:1.00f];
            alertView.buttonShadowColor = [UIColor whiteColor];
            
            alertView.titleFont = titleFont;
            alertView.messageFont = messageFont;
        }];
        
        
        
        if ([_redeemTextField.text isEqualToString:@""])
        {
            
            [WCAlertView showAlertWithTitle:@"Information" message:@"NetCoins gift card number is empty." customizationBlock:^(WCAlertView *alertView) {
                
                // You can also set different appearance for this alert using customization block
                
                alertView.style = WCAlertViewStyleBlackHatched;
                //        alertView.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
            } completionBlock:^(NSUInteger buttonIndex, WCAlertView *alertView) {
                if (buttonIndex == alertView.cancelButtonIndex) {
                    ////(@"Cancel");
                } else {
                    ////(@"Ok");
                    [_redeemTextField resignFirstResponder];
                }
            } cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        }
        else
        {
            
            if ([_redeemTextField.text length]<16)
            {
                [WCAlertView showAlertWithTitle:@"Information" message:@"16 Digit Gift Card Number Is Invalid" customizationBlock:^(WCAlertView *alertView) {
                    
                    // You can also set different appearance for this alert using customization block
                    
                    alertView.style = WCAlertViewStyleBlackHatched;
                    //        alertView.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
                } completionBlock:^(NSUInteger buttonIndex, WCAlertView *alertView) {
                    if (buttonIndex == alertView.cancelButtonIndex) {
                        ////(@"Cancel");
                    } else {
                        ////(@"Ok");
                    }
                } cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                
            }
            else
            {
                
                NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/redeems/processGiftCardNumber",CONFIG_BASE_URL]];
                
                __block  ASIFormDataRequest *request_post1 = [ASIFormDataRequest requestWithURL:url];
                
                
                NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
                NSString *user=[check objectForKey:@"USERID"];
                
                //[request_post setValidatesSecureCertificate:NO];
                [request_post1 setPostValue:user forKey:@"userID"];
                [request_post1 setPostValue:_redeemTextField.text forKey:@"giftcard_no"];
                 [request_post1 setPostValue:[Config get_token] forKey:@"apikey"];
                
                [request_post1 setCompletionBlock:^{
                    NSString *responseString = [request_post1 responseString];
                    NSMutableData *results1 = [responseString JSONValue];
                    
                    
                    
                    
                    
                    ////(@"HELLO:%@",results1);
                    
                    
                    NSString *error=[results1 valueForKeyPath:@"error.description"];
                    
                    NSString *success=[results1 valueForKeyPath:@"header.description"];
                    
                    
                    ////(@"HEADER:%@",success);
                    ////(@"HEADER:%@",error);
                    
                    if ([success length]==0) {
                        ////(@"FAILED");
                        
                        [WCAlertView showAlertWithTitle:@"Information" message:error customizationBlock:^(WCAlertView *alertView)
                         {
                             
                             // You can also set different appearance for this alert using customization block
                             
                             alertView.style = WCAlertViewStyleBlackHatched;
                             //        alertView.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
                         } completionBlock:^(NSUInteger buttonIndex, WCAlertView *alertView) {
                             if (buttonIndex == alertView.cancelButtonIndex) {
                                 ////(@"Cancel");
                             } else {
                                 
                                 [_redeemTextField resignFirstResponder];
                                 _redeemTextField.text=@"";
                                 ////(@"Ok");
                             }
                         } cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                        
                    }
                    else
                    {
                        [WCAlertView showAlertWithTitle:@"Information" message:success customizationBlock:^(WCAlertView *alertView)
                         {
                             
                             // You can also set different appearance for this alert using customization block
                             
                             alertView.style = WCAlertViewStyleBlackHatched;
                             //        alertView.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
                         } completionBlock:^(NSUInteger buttonIndex, WCAlertView *alertView) {
                             if (buttonIndex == alertView.cancelButtonIndex) {
                                 ////(@"Cancel");
                             } else {
                                 
                                 [_redeemTextField resignFirstResponder];
                                 _redeemTextField.text=@"";
                                 ////(@"Ok");
                             }
                         } cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                        
                    }
                    
                    
                    [self getNetCoins];
                    
                    //  [self getInfluenceLevel:total_scores];
                    
                    
                }];
                [request_post1 setFailedBlock:^{
                    NSError *error = [request_post1 error];
                    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                        message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                              cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    
                    
                    
                    
                }];
                
                [request_post1 startAsynchronous];
                
                
                
                
                
                
                
                
                
                
                
                
            }
        }
        
        
        
        
    }


-(IBAction)redeemBack
{
    
    _redeemTextField.text=@"";
    redeem_view.hidden=YES;
    
    [_redeemTextField resignFirstResponder];
}

-(void)getSmilies:(NSString *)smiley
{
    ////(@"SMILEY:%@",smiley);
    
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/smiley/retrieveCardSmileys",CONFIG_BASE_URL]];
    
    
    __block  ASIFormDataRequest *requestmethod = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    NSString *cardID=[check objectForKey:@"CARD_ID"];
    
    NSMutableArray *smilyIDArr=[[NSMutableArray alloc]init];
    NSMutableArray *smilyName=[[NSMutableArray alloc]init];
    
    [requestmethod setPostValue:user forKey:@"userID"];
    [requestmethod setPostValue:smiley forKey:@"smiley_ids"];
     [requestmethod setPostValue:[Config get_token] forKey:@"apikey"];
    [requestmethod setTimeOutSeconds:30];
    
    
    [requestmethod setCompletionBlock:^{
        NSString *responseString23 = [requestmethod responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        ////(@"RESULTS:%@",results11);
        
        
        NSArray *temp= [results11 valueForKeyPath:@"smileys"];
        
        
    
        
        
        ////(@"TEMPARY:%@",temp);
        
        for(NSDictionary *value in temp)
        {
            [smilyIDArr  addObject:[value valueForKey:@"id"]];
            [smilyName  addObject:[value valueForKey:@"smiley_url"]];
            //[contactidArray  addObject:[value valueForKey:@"contactid"]];
            
        }

    
        if ([smilyName count]==1)
        {
            
            
            NSString *temp1=@"";
            [_sm1 setImageWithURL:[NSURL URLWithString:temp1]
                 placeholderImage:nil];
            [_sm11 setImageWithURL:[NSURL URLWithString:temp1]
                 placeholderImage:nil];
            
            NSString *temp2=@"";
            [_sm3 setImageWithURL:[NSURL URLWithString:temp2]
                 placeholderImage:nil];
            [_sm31 setImageWithURL:[NSURL URLWithString:temp2]
                 placeholderImage:nil];
            NSString *temp=[smilyName objectAtIndex:0];
            [_sm2 setImageWithURL:[NSURL URLWithString:temp]
                 placeholderImage:nil];
            [_sm21 setImageWithURL:[NSURL URLWithString:temp]
                 placeholderImage:nil];
        }
        
        if ([smilyName count]==2)
        {
            
            NSString *temp=[smilyName objectAtIndex:0];
            [_sm1 setImageWithURL:[NSURL URLWithString:temp]
                 placeholderImage:nil];
            [_sm11 setImageWithURL:[NSURL URLWithString:temp]
                 placeholderImage:nil];

            NSString *temp1=[smilyName objectAtIndex:1];
            [_sm2 setImageWithURL:[NSURL URLWithString:temp1]
                 placeholderImage:nil];
            [_sm21 setImageWithURL:[NSURL URLWithString:temp1]
                 placeholderImage:nil];
            NSString *temp2=@"";
            [_sm3 setImageWithURL:[NSURL URLWithString:temp2]
                 placeholderImage:nil];
            [_sm31 setImageWithURL:[NSURL URLWithString:temp2]
                 placeholderImage:nil];
            
        }
        
        if ([smilyName count]==3)
        {
            
            NSString *temp=[smilyName objectAtIndex:0];
            [_sm1 setImageWithURL:[NSURL URLWithString:temp]
                 placeholderImage:nil];
            [_sm11 setImageWithURL:[NSURL URLWithString:temp]
                 placeholderImage:nil];
            NSString *temp1=[smilyName objectAtIndex:1];
            [_sm2 setImageWithURL:[NSURL URLWithString:temp1]
                 placeholderImage:nil];
            [_sm21 setImageWithURL:[NSURL URLWithString:temp1]
                 placeholderImage:nil];

            NSString *temp2=[smilyName objectAtIndex:2];
            [_sm3 setImageWithURL:[NSURL URLWithString:temp2]
                 placeholderImage:nil];
            [_sm31 setImageWithURL:[NSURL URLWithString:temp2]
                 placeholderImage:nil];
        }
        
        
        NSUserDefaults *valueCheck=[NSUserDefaults standardUserDefaults];

        NSArray *countryAmount=[valueCheck objectForKey:@"COUNTRY_AMOUNT"];
        NSArray *countryNameArr=[valueCheck objectForKey:@"COUNTRY_ARRAY"];
        NSArray *smilyAmountArr=[valueCheck objectForKey:@"SMILY_AMOUNT"];
        
        
        ////(@"SMILY AMOUNT:%@",smilyAmountArr);
        
        
        int smiley_value=0;
        
        for (int i=0;i<[country count]; i++)
        {
            NSString *countryName=country[i];
            
            for (int j=0; j<[countryNameArr count]; j++)
            {
                NSString *compare=countryNameArr[j];
                
                if ([countryName isEqualToString:compare])
                {
                    
                    int amount=[smilyAmountArr[j]intValue];
                    
                    
                    smiley_value=smiley_value+amount;
                }
                
            }
            
        }
        
        
        
        int check_smiley=[country count]*smiley_value;
        
        
        
        
        
        spend_coins=total_amount+check_smiley;
        
        ////(@"Total AMount:%d",total_amount);
        
        
        
        //[_profileButton setImage:profileIMG forState:UIControlStateNormal];
        
         NSString *newString = [[message_text componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
       
        _messageText.text=newString;
         _messageText1.text=newString;
        _titleText.text=card_name;
        _titleText1.text=card_name;
        
        
        
        
        
        
       // [_toImageView setImageWithURL:[NSURL URLWithString:toImage[0]]
                    // placeholderImage:nil];
        
        _nameText.text=[NSString stringWithFormat:@"%@ %@",firstName[0],lastname[0]];
        
        
        _ad1.text=[NSString stringWithFormat:@"%@ %@",address1[0],address2[0]];
         _ad2.text=[NSString stringWithFormat:@"%@,%@",city[0],state[0]];
        _ad3.text=[NSString stringWithFormat:@"%@",country[0]];
        _ad4.text=[NSString stringWithFormat:@"%@",zipcode[0]];
        
        
        _nameText1.text=[NSString stringWithFormat:@"%@ %@",firstName[0],lastname[0]];
        
        
        _ad11.text=[NSString stringWithFormat:@"%@ %@",address1[0],address2[0]];
        _ad21.text=[NSString stringWithFormat:@"%@,%@",city[0],state[0]];
        _ad31.text=[NSString stringWithFormat:@"%@",country[0]];
        _ad41.text=[NSString stringWithFormat:@"%@",zipcode[0]];

        
       /*
        NetCoins Balance: %@. Required NetCoins: %@
        
        */
        
        
        /*
        
        if ([netCoins intValue]<spend_coins)
        {
            _buyButton.hidden=NO;
            _sendButton.hidden=YES;
            NSString *label_for_display=[NSString stringWithFormat:@" NetCoins Balance: %@. Required NetCoins: %d.",netCoins,spend_coins];
            _topLabel.text=label_for_display;
        }
        else
        {
            
            
            _buyButton.hidden=YES;
            _sendButton.hidden=NO;
            
            NSString *label_for_display=[NSString stringWithFormat:@" NetCoins Balance: %@. Required NetCoins: %d",netCoins,spend_coins];
            _topLabel.text=label_for_display;
            
            //_topLabel.text=@"Need 10 Net coins. You have 100 Net coins";
            
            
        }

        */
        
        
    }];
    [requestmethod setFailedBlock:^{
        NSError *error = [requestmethod error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [requestmethod startAsynchronous];
    
    
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{

    
    if (actionSheet==actionSheetGet)
    {
        
        if (buttonIndex==0)
        {
            ////(@"IND1");
            
            if (IS_IPHONE_5)
            {
                buyViewController *viewController=[[buyViewController alloc]initWithNibName:@"buyViewController" bundle:nil];
                
                viewController.comingString=@"SOCIAL";
                
                [self presentViewController:viewController animated:NO completion:^{
                    //////(@"SUCCESS");
                }
                 ];
            }
            else
            {
                buyViewController *viewController=[[buyViewController alloc]initWithNibName:@"buyViewController~iPhone" bundle:nil];
                
                viewController.comingString=@"SOCIAL";
                
                [self presentViewController:viewController animated:NO completion:^{
                    //////(@"SUCCESS");
                }
                 ];
            }
            
            
            
        }
        if (buttonIndex==1)
        {
            ////(@"IND2");
            
            redeem_view.hidden=NO;
        }
    }
    
            
    
    else if(actionSheet==actionSheetContinue)
    {
        
        if (buttonIndex==0)
        {
            ////(@"AS1");
            
            
            if ([netCoins intValue]<spend_coins)
            {
               
               [self buy];
            }
            else
            {
                
                 [self sendButtonClick];
                
               
                               
                
            }

                      
            
        }
        if (buttonIndex==1)
        {
            ////(@"AS1");
            
            if (IS_IPHONE_5) {
                emailViewController *viewController=[[emailViewController alloc]initWithNibName:@"emailViewController" bundle:nil];
                [self presentViewController:viewController animated:NO completion:^{
                    //////(@"SUCCESS");
                }
                 ];
            }
            else
            {
                emailViewController *viewController=[[emailViewController alloc]initWithNibName:@"emailViewController~iPhone" bundle:nil];
                [self presentViewController:viewController animated:NO completion:^{
                    //////(@"SUCCESS");
                }
                 ];
            }
            
            
        }
        
        if (buttonIndex==2)
        {
            ////(@"AS3");
            
            
            if (IS_IPHONE_5) {
                shareViewController *viewController=[[shareViewController alloc]initWithNibName:@"shareViewController" bundle:nil];
                [self presentViewController:viewController animated:NO completion:^{
                    //////(@"SUCCESS");
                }
                 ];
            }
            else
            {
                shareViewController *viewController=[[shareViewController alloc]initWithNibName:@"shareViewController~iPhone" bundle:nil];
                [self presentViewController:viewController animated:NO completion:^{
                    //////(@"SUCCESS");
                }
                 ];
            }

            
        }
        
       
    }
    else 
    {
        if (buttonIndex==0)
        {
            if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
            {
                UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                picker.delegate = self;
                picker.allowsEditing = YES;
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                
                [self presentViewController:picker animated:YES completion:NULL];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"The device does not have camera" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
                // [alert release];
            }
            
        }
        else  if (buttonIndex==1)
        {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            
            [self presentViewController:picker animated:YES completion:NULL];
            
        }
    }

}

/*
#ifdef IOS_OLDER_THAN_6
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return (toInterfaceOrientation == UIInterfaceOrientationMaskPortrait);
}
#endif
#ifdef IOS_NEWER_OR_EQUAL_TO_6
-(BOOL)shouldAutorotate
{
    return YES;
}
- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
#endif
*/

-(BOOL)shouldAutorotate
{
    if (_contactAddView.hidden)
        return YES;
    else
        return NO;
}


-(NSInteger)supportedInterfaceOrientations{
    if (UIDeviceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation]))
    {
        //(@"PORTRAIT");
        
        _landscapeView.hidden=YES;
        
    }
    
    else
    {
       
         //(@"LANDSCAPE");
        
        
        
         _landscapeView.hidden=NO;
        
    }
    return UIInterfaceOrientationMaskAll;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)orientation
                                duration:(NSTimeInterval)duration
{
    if (UIDeviceOrientationIsPortrait(orientation))
    {
        
        //(@"PORT");
        
         _landscapeView.hidden=YES;
    }
    
    else
    {
              //(@"LAND");
        
         _landscapeView.hidden=NO;
        
    }
}




- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [firstName count];
}


- (FinalCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELL" forIndexPath:indexPath];
    
    
    
    // cell.progress.hidden=YES;
    
    @try {
        NSString *temp=[toImage objectAtIndex:indexPath.row];
        [cell.image_view setImageWithURL:[NSURL URLWithString:temp]
                        placeholderImage:[UIImage imageNamed:@"bigicon.png"]];
        
        
        
    }
    @catch (NSException *exception) {
        ////////////(@"CHJECK");
    }
    
    cell.cell_title_Label.text=[NSString stringWithFormat:@"%@ %@",firstName[indexPath.row],lastname[indexPath.row]];
    
    
    if (selected_index==indexPath.row)
    {
        
        cell.cell_title_Label.textColor=[UIColor orangeColor];
        
        cell.frameImage.image=[UIImage imageNamed:@"frm.png"];
        
    }
    else
    {
        
         cell.frameImage.image=[UIImage imageNamed:@"frmact.png"];
        
        cell.cell_title_Label.textColor=[UIColor blackColor];
    }
       
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    ////(@"Select %d",indexPath.row);
    
    
    selected_index=indexPath.row;

    [_toImageView setImageWithURL:[NSURL URLWithString:toImage[indexPath.row]]
                 placeholderImage:[UIImage imageNamed:@"bigicon.png"]];
    
    _nameText.text=[NSString stringWithFormat:@"%@ %@",firstName[indexPath.row],lastname[indexPath.row]];
    
    
    _ad1.text=[NSString stringWithFormat:@"%@ %@",address1[indexPath.row],address2[indexPath.row]];
    _ad2.text=[NSString stringWithFormat:@"%@,%@",city[indexPath.row],state[indexPath.row]];
    _ad3.text=[NSString stringWithFormat:@"%@,%@",country[indexPath.row],zipcode[indexPath.row]];
    
    _nameText1.text=[NSString stringWithFormat:@"%@ %@",firstName[indexPath.row],lastname[indexPath.row]];
    
    
    _ad11.text=[NSString stringWithFormat:@"%@ %@",address1[indexPath.row],address2[indexPath.row]];
    _ad21.text=[NSString stringWithFormat:@"%@,%@",city[indexPath.row],state[indexPath.row]];
    _ad31.text=[NSString stringWithFormat:@"%@,%@",country[indexPath.row],zipcode[indexPath.row]];
    
    CGPoint bottomOffset = CGPointMake(960, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
    [self.scrollView setContentOffset:bottomOffset animated:YES];
    
    [collectionViewFinal reloadData];
   
    
}
-(IBAction)settingsButton
{
    ViewController *viewController=[[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        //////(@"SUCCESS");
    }
     ];
}

-(IBAction)buy
{
    
    
    
    NSString *actionSheetTitle = @"Get NetCoins"; //Action Sheet Title
    //NSString *destructiveTitle = @"Destructive Button"; //Action Sheet Button Titles
    NSString *other1 = @"Buy";
    NSString *other2 = @"Redeem";
       NSString *cancelTitle = @"Cancel";
   actionSheetGet = [[UIActionSheet alloc]
                                  initWithTitle:actionSheetTitle
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:other1, other2, nil];
    [actionSheetGet showInView:self.view];
    
    
    /*
    buyViewController *viewController=[[buyViewController alloc]initWithNibName:@"buyViewController" bundle:nil];
    
    viewController.comingString=@"SOCIAL";
    
    [self presentViewController:viewController animated:NO completion:^{
        //////(@"SUCCESS");
    }
     ];
     */
}

-(IBAction)sendButtonClick
{
    
    
    progressAlert = [[UIAlertView alloc] initWithTitle: @"Ordering..." message: @"Please wait..." delegate: self cancelButtonTitle: nil otherButtonTitles: nil];
    
    Activity = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(90.0f, 40.0f, 100.0f, 100.0f)];
    
    
    
    [progressAlert addSubview:Activity];
    
    [Activity startAnimating];
    
    [progressAlert show];
  
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/socialcards/updateCard",CONFIG_BASE_URL]];
    
    //NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/photos/retrieveFacebookPhotos",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *requestmethod = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    
      NSString *contact=[check objectForKey:@"CONTACT_ID"];
    
    ////(@"USER:%@",contact);
    
    NSArray *lines = [contact componentsSeparatedByString:@","];
    
    /*
    NSMutableString* message = [NSMutableString stringWithCapacity:30];
    
    
    
    for (int i=0; i<[lines count]; i++)
    {
        
        if (i==0) {
            
        }
        else
        {
        [message appendString:[NSString stringWithFormat:@"%@,",lines[i]]];
        }
    }
    
    [message deleteCharactersInRange:NSMakeRange([message length] - 1, 1)];
    
    ////(@"MESSAGEGHGH:%@",message);
  */
    
    
    
    [requestmethod setPostValue:user forKey:@"userID"];
    [requestmethod setPostValue:cardid forKey:@"id"];
    [requestmethod setPostValue:card_name forKey:@"card_name"];
    [requestmethod setPostValue:message_text forKey:@"message_text"];
     [requestmethod setPostValue:@"1" forKey:@"saved_status"];
    [requestmethod setPostValue:@"pdf" forKey:@"card_status"];
    //[requestmethod setPostValue:message forKey:@"contactid"];
     [requestmethod setPostValue:[Config get_token] forKey:@"apikey"];
    [requestmethod setTimeOutSeconds:30];
    
    
    [requestmethod setCompletionBlock:^{
        NSString *responseString23 = [requestmethod responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        
        
        NSString *cardID= [results11 valueForKeyPath:@"cardID"];
        
               
        NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
        [check setObject:cardID forKey:@"CARD_ID"];
        
        //(@"TEMPARY XXX:%@",results11);
        
        
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/socialcards/socialCardConfirmation",CONFIG_BASE_URL]];
        
        //NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/photos/retrieveFacebookPhotos",CONFIG_BASE_URL]];
        
        __block  ASIFormDataRequest *requestmethod = [ASIFormDataRequest requestWithURL:url];
        
        
        
        NSUserDefaults *checkValue=[NSUserDefaults standardUserDefaults];
        NSString *userID=[check objectForKey:@"USERID"];
        
        //(@"cardid:%@",cardid);
        
        
        
        [requestmethod setPostValue:cardid forKey:@"cardID"];
        [requestmethod setPostValue:userID forKey:@"userID"];
        [requestmethod setPostValue:[Config get_token] forKey:@"apikey"];
        
        [requestmethod setTimeOutSeconds:30];
        
        
        [requestmethod setCompletionBlock:^{
            NSString *responseString23 = [requestmethod responseString];
            NSMutableData *results11 = [responseString23 JSONValue];
            
            
            
            NSString *cardID= [results11 valueForKeyPath:@"response.cardID"];
            
                       
            NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
            [check setObject:cardID forKey:@"CARD_ID"];
            
            //(@"TEMPARY SSS:%@",results11);
            
            
            [self createNetCoinCredit:nil];
            
           
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@""
                                                                message:[NSString stringWithFormat:@"SocialCard ordered Successfully"] delegate:self
                                                      cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alertView show];
            
            [Activity stopAnimating];
            [progressAlert dismissWithClickedButtonIndex:0 animated:YES];
            
            
            if (IS_IPHONE_5) {
                socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController" bundle:nil];
                [self presentViewController:viewController animated:NO completion:^{
                    //////(@"SUCCESS");
                }
                 ];
            }
            else
            {
                socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController~iPhone" bundle:nil];
                [self presentViewController:viewController animated:NO completion:^{
                    //////(@"SUCCESS");
                }
                 ];
            }

            
        }];
        [requestmethod setFailedBlock:^{
            NSError *error = [requestmethod error];
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                      cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            
            
            
        }];
        
        [requestmethod startAsynchronous];
        
        
        
        
    }];
    [requestmethod setFailedBlock:^{
        NSError *error = [requestmethod error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [requestmethod startAsynchronous];
    
    
    
    

 
}

-(IBAction)createNetCoinCredit:(id)sender
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/netcoins/createNetCoinCredit",CONFIG_BASE_URL]];
    
    
    
    __block  ASIFormDataRequest *requestmethod = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    NSString *userCard=[check objectForKey:@"CARD_ID"];
    
  
    
    //(@"USER:%@",userCard);
    
    
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"HH:mm:ss zzz"];
    NSString *dateString = [dateFormat stringFromDate:date];
    
    //(@"DATE STRING:%@",dateString);
    
    
    
    NSString *tag_string=[NSString stringWithFormat:@"<a href=\"/socialcards/%@/historydetails\" class=\"fancybox fancybox.ajax\" style=\"color:#FF8D00;\">%@</a>",userCard,userCard];
    
    NSString *description=[NSString stringWithFormat:@"Spent %d NetCoins for %@ SocialCard",total_amount,tag_string];
    
    NSString *currency=[NSString stringWithFormat:@"%d",total_amount];
    
   // NSString *descriptionString=[NSString stringWithFormat:@"Bought %@",description];
    
    //(@"HELLO:%@",description);
    
    [requestmethod setPostValue:user forKey:@"snguserid"];
    [requestmethod setPostValue:dateString forKey:@"trx_datetime"];
    [requestmethod setPostValue:@"S" forKey:@"type"];
    [requestmethod setPostValue:currency forKey:@"number_of_coins"];
     [requestmethod setPostValue:[Config get_token] forKey:@"apikey"];
    [requestmethod setPostValue:@"" forKey:@"expiry_date"];
    [requestmethod setPostValue:description forKey:@"description"];
     [requestmethod setPostValue:[Config get_token] forKey:@"apikey"];
    [requestmethod setPostValue:userCard forKey:@"cardID"];
    [requestmethod setTimeOutSeconds:30];
    
    
    [requestmethod setCompletionBlock:^{
        NSString *responseString23 = [requestmethod responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        
        //(@"TEMPARY SUCCESS:%@",results11);
        
        
        // NSString *message=[results11 valueForKeyPath:@"header.statuscode"];
        
        //if ([message intValue]==1)
        //{
        //[self createNetCoinCredit];
        // }
        
       [self getNetCoins];
        
        
        
        // [self dismissViewControllerAnimated:YES completion:nil];
        
    }];
    [requestmethod setFailedBlock:^{
        NSError *error = [requestmethod error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [requestmethod startAsynchronous];
    
    
    
    

}

-(void)getNetCoins
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/getUserNetCoins",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post1 = [ASIFormDataRequest requestWithURL:url];
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    
    //[request_post setValidatesSecureCertificate:NO];
    [request_post1 setPostValue:user forKey:@"userID"];
    
     [request_post1 setPostValue:[Config get_token] forKey:@"apikey"];
    
    [request_post1 setCompletionBlock:^{
        NSString *responseString = [request_post1 responseString];
        NSMutableData *results1 = [responseString JSONValue];
        
        
        
        
        
        NSString *netCoins=[results1 valueForKeyPath:@"response.totalNetCoins"];
        
        
        ////(@"influenceLevelStr:%@",netCoins);
        
        NSString *netCoinsStr=[NSString stringWithFormat:@"You have a total of %@ NetCoin(s)",netCoins];
        
        ////(@"TOTAL:%@",netCoinsStr);
        
        
        NSUserDefaults *update=[NSUserDefaults standardUserDefaults];
        
        [update setObject:netCoins forKey:@"TOTAL_COINS"];
               //_totalNetCoinslabel.text=netCoinsStr;
        
        //  [self getInfluenceLevel:total_scores];
        
               
        
        
    }];
    [request_post1 setFailedBlock:^{
        NSError *error = [request_post1 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1 startAsynchronous];
    
    
}

-(IBAction)backButton
{
    if (IS_IPHONE_5)
    {
        
        receiptentViewController *viewController=[[receiptentViewController alloc]initWithNibName:@"receiptentViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:nil];
        
    }
    else
    {
        
        receiptentViewController *viewController=[[receiptentViewController alloc]initWithNibName:@"receiptentViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:nil];
    }

    
}
/*

*/

-(IBAction)meButton
{
    if (IS_IPHONE_5) {
        MeViewController *viewController=[[MeViewController alloc]initWithNibName:@"MeViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        
        MeViewController *viewController=[[MeViewController alloc]initWithNibName:@"MeViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }

}
-(IBAction)dealsButton
{
    dealsViewController *viewController=[[dealsViewController alloc]initWithNibName:@"dealsViewController" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        //////(@"SUCCESS");
    }
     ];
}
-(IBAction)socialButton
{
    if (IS_IPHONE_5) {
        netCoinsViewController *viewController=[[netCoinsViewController alloc]initWithNibName:@"netCoinsViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        netCoinsViewController *viewController=[[netCoinsViewController alloc]initWithNibName:@"netCoinsViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }

}
-(IBAction)netCoinsButton
{
    if (IS_IPHONE_5) {
        socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }

}
-(IBAction)myBusinessButton
{
    myBusinessViewController *viewController=[[myBusinessViewController alloc]initWithNibName:@"myBusinessViewController" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        //////(@"SUCCESS");
    }
     ];
}
- (BOOL)textFieldShouldReturn: (UITextField *)textField
{
  
    [textField resignFirstResponder];
    
    return YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void) animateTextField: (UITextField*) textField up: (BOOL) up

{
	
    int txtPosition = (textField.frame.origin.y - 200);
	
    const int movementDistance = (txtPosition < 0 ? 0 : txtPosition); // tweak as needed
	
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
	
    [UIView setAnimationBeginsFromCurrentState: YES];
	
    [UIView setAnimationDuration: movementDuration];
	
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
	
    [UIView commitAnimations];
	
}
- (void)textFieldDidBeginEditing:(UITextField *)textField

{
    
  
    _pickerBack2.hidden=YES;
    [self animateTextField: textField up: YES];
    
}


- (void)textFieldDidEndEditing:(UITextField *)textField

{
	
    [self animateTextField: textField up: NO];
   	
}



@end
