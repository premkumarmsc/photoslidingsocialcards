//
//  dealsViewController.h
//  PhotoSliding
//
//  Created by ephronsystems on 6/27/13.
//
//

#import <UIKit/UIKit.h>

@interface netCoinsViewController : GAITrackedViewController<UICollectionViewDelegate,UICollectionViewDataSource,UIAlertViewDelegate>
@property(retain,nonatomic)IBOutlet UICollectionView *collectionView;
@property (retain, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (weak, nonatomic) IBOutlet UILabel *selectedSegmentLabel;
@property (nonatomic,retain)IBOutlet UITableView *avilableTableView;
@property (retain, nonatomic) IBOutlet UITextField *searchTxtEield;
@property (weak, nonatomic) IBOutlet UIButton *filterButton;
@property (retain, nonatomic) IBOutlet UITextField *cardTxtEield;
@property (retain, nonatomic) IBOutlet UITextField *monthTxtEield;
@property (retain, nonatomic) IBOutlet UITextField *yearTxtEield;
@property (retain, nonatomic) IBOutlet UITextField *cvvTxtEield;
@property (retain, nonatomic) IBOutlet UITextField *reademTxtEield;

@property (weak, nonatomic) IBOutlet UIView  *detailView;
@property (weak, nonatomic) IBOutlet UIView  *back_view;
@property (weak, nonatomic) IBOutlet UIView  *buyView;
@property (weak, nonatomic) IBOutlet UIView  *accountView;


@property (weak, nonatomic) IBOutlet UIButton  *profileButton;

@property (weak, nonatomic) IBOutlet UILabel  *totalNetCoinslabel;


- (IBAction)segmentDidChange:(id)sender;
-(IBAction)meButton;
-(IBAction)dealsButton;
-(IBAction)netCoinsButton;
-(IBAction)socialButton;
-(IBAction)myBusinessButton;
-(IBAction)filterButtonClick;
-(IBAction)dealbackButtonClick;
-(IBAction)settingsButton;
-(IBAction)shareButtonClick;
-(IBAction)inviteButtonClick;

-(IBAction)infoBtn;

-(IBAction)buyButtonClick;
-(IBAction)redeamButtonClick;

@property (weak, nonatomic) IBOutlet UIButton *button1;
@property (weak, nonatomic) IBOutlet UIButton *button2;
@property (weak, nonatomic) IBOutlet UIButton *button3;
@property (weak, nonatomic) IBOutlet UIButton *button4;

@property (weak, nonatomic) IBOutlet UILabel *lab1;
@property (weak, nonatomic) IBOutlet UILabel *lab2;
@property (weak, nonatomic) IBOutlet UILabel *lab3;
@property (weak, nonatomic) IBOutlet UILabel *lab4;

-(IBAction)button1Click;
-(IBAction)button2Click;
-(IBAction)button3Click;
-(IBAction)button4Click;


@end
