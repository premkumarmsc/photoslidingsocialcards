//
//  dealsViewController.h
//  PhotoSliding
//
//  Created by ephronsystems on 6/27/13.
//
//

#import <UIKit/UIKit.h>

@interface InvitenetCoinsViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>
@property(retain,nonatomic)IBOutlet UICollectionView *collectionView;
@property (retain, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (weak, nonatomic) IBOutlet UILabel *selectedSegmentLabel;
@property (nonatomic,retain)IBOutlet UITableView *avilableTableView;
@property (retain, nonatomic) IBOutlet UITextField *searchTxtEield;
@property (weak, nonatomic) IBOutlet UIButton *filterButton;
@property (retain, nonatomic) IBOutlet UITextField *cardTxtEield;
@property (retain, nonatomic) IBOutlet UITextField *monthTxtEield;
@property (retain, nonatomic) IBOutlet UITextField *yearTxtEield;
@property (retain, nonatomic) IBOutlet UITextField *cvvTxtEield;

@property (weak, nonatomic) IBOutlet UIView  *detailView;
@property (weak, nonatomic) IBOutlet UIView  *back_view;
@property (weak, nonatomic) IBOutlet UIView  *buyView;
@property (weak, nonatomic) IBOutlet UIView  *accountView;

- (IBAction)segmentDidChange:(id)sender;
-(IBAction)meButton;
-(IBAction)dealsButton;
-(IBAction)netCoinsButton;
-(IBAction)socialButton;
-(IBAction)myBusinessButton;
-(IBAction)filterButtonClick;
-(IBAction)dealbackButtonClick;
-(IBAction)settingsButton;
-(IBAction)shareButtonClick;
-(IBAction)inviteButtonClick;

@end
