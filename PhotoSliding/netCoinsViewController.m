//
//  dealsViewController.m
//  PhotoSliding
//
//  Created by ephronsystems on 6/27/13.
//
//

#import "netCoinsViewController.h"
#import "LibraryCell.h"
#import "Image_cell.h"
#import "AccountCell.h"
#import "SDSegmentedControl.h"
#import "ASIFormDataRequest.h"

@interface netCoinsViewController ()

@end

@implementation netCoinsViewController

@synthesize filterButton;
@synthesize collectionView;


NSMutableArray *hisDateArray;
NSMutableArray *hisDesArray;
NSMutableArray *netCoinsArray;

NSString *but1Title;
NSString *but2Title;
NSString *but3Title;
NSString *but4Title;
NSMutableArray *priceArray;
int buttonTag;
UIAlertView *  alertExit;
Image_cell *cell;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)infoBtn
{
    if (IS_IPHONE_5)
    {
        infoViewController *viewController=[[infoViewController alloc]initWithNibName:@"infoViewController" bundle:nil];
        [self presentViewController:viewController animated:YES completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
    infoViewController *viewController=[[infoViewController alloc]initWithNibName:@"infoViewController~iPhone" bundle:nil];
    [self presentViewController:viewController animated:YES completion:^{
        //////(@"SUCCESS");
    }
     ];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
     self.trackedViewName = @"NetCoins Screen";
    
    SDSegmentView *segmenteViewAppearance = SDSegmentView.appearance;
    [segmenteViewAppearance setTitleColor:UIColor.grayColor forState:UIControlStateNormal];
    [segmenteViewAppearance setTitleColor:UIColor.whiteColor forState:UIControlStateSelected];
    [segmenteViewAppearance setTitleColor:UIColor.orangeColor forState:UIControlStateDisabled];
    
    
    SDStainView *stainViewAppearance = SDStainView.appearance;
    stainViewAppearance.backgroundColor = [UIColor colorWithRed:245/255.0 green:127/255.0 blue:0/255.0 alpha:1];
    stainViewAppearance.shadowColor = [UIColor colorWithRed:245/255.0 green:127/255.0 blue:0/255.0 alpha:1];
    stainViewAppearance.shadowBlur = 5;
  
    /*
    SDStainView *stainViewAppearance = SDStainView.appearance;
    stainViewAppearance.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"footercoin_act.png"]];
    stainViewAppearance.shadowColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"footercoin_act.png"]];
    stainViewAppearance.shadowBlur = 5;
     */
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
      
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    
    NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:@"profileImage.jpg"];
    UIImage *profileIMG = [UIImage imageWithContentsOfFile:getImagePath];
    
    
    [_profileButton setImage:profileIMG forState:UIControlStateNormal];
    
    
    
    [_back_view addSubview:_detailView];
    [_back_view addSubview:_buyView];
    [_back_view addSubview:_accountView];
    
    
    _detailView.hidden=YES;
    _buyView.hidden=NO;
    _accountView.hidden=YES;
    
    
    buttonTag=1;
    
    [_button1 setImage:[UIImage imageNamed:@"checkbox_checked.png"] forState:UIControlStateNormal];
     [_button2 setImage:[UIImage imageNamed:@"checkbox_unchecked.png"] forState:UIControlStateNormal];
     [_button3 setImage:[UIImage imageNamed:@"checkbox_unchecked.png"] forState:UIControlStateNormal];
     [_button4 setImage:[UIImage imageNamed:@"checkbox_unchecked.png"] forState:UIControlStateNormal];
    
    
    [self getNetCoins];
    [self getUpdateCoins];
    
    
}
-(IBAction)button1Click
{
    buttonTag=1;
    
    [_button1 setImage:[UIImage imageNamed:@"checkbox_checked.png"] forState:UIControlStateNormal];
    [_button2 setImage:[UIImage imageNamed:@"checkbox_unchecked.png"] forState:UIControlStateNormal];
    [_button3 setImage:[UIImage imageNamed:@"checkbox_unchecked.png"] forState:UIControlStateNormal];
    [_button4 setImage:[UIImage imageNamed:@"checkbox_unchecked.png"] forState:UIControlStateNormal];

}
-(IBAction)button2Click
{
    buttonTag=2;
    
    [_button1 setImage:[UIImage imageNamed:@"checkbox_unchecked.png"] forState:UIControlStateNormal];
    [_button2 setImage:[UIImage imageNamed:@"checkbox_checked.png"] forState:UIControlStateNormal];
    [_button3 setImage:[UIImage imageNamed:@"checkbox_unchecked.png"] forState:UIControlStateNormal];
    [_button4 setImage:[UIImage imageNamed:@"checkbox_unchecked.png"] forState:UIControlStateNormal];

}
-(IBAction)button3Click
{
    buttonTag=3;
    
    [_button1 setImage:[UIImage imageNamed:@"checkbox_unchecked.png"] forState:UIControlStateNormal];
    [_button2 setImage:[UIImage imageNamed:@"checkbox_unchecked.png"] forState:UIControlStateNormal];
    [_button3 setImage:[UIImage imageNamed:@"checkbox_checked.png"] forState:UIControlStateNormal];
    [_button4 setImage:[UIImage imageNamed:@"checkbox_unchecked.png"] forState:UIControlStateNormal];

}
-(IBAction)button4Click
{
    buttonTag=4;
    
    [_button1 setImage:[UIImage imageNamed:@"checkbox_unchecked.png"] forState:UIControlStateNormal];
    [_button2 setImage:[UIImage imageNamed:@"checkbox_unchecked.png"] forState:UIControlStateNormal];
    [_button3 setImage:[UIImage imageNamed:@"checkbox_unchecked.png"] forState:UIControlStateNormal];
    [_button4 setImage:[UIImage imageNamed:@"checkbox_checked.png"] forState:UIControlStateNormal];

}
-(void)getUpdateCoins
{
    NSMutableArray *netIDArray=[[NSMutableArray alloc]init];
    NSMutableArray *netEachArray=[[NSMutableArray alloc]init];
    netCoinsArray=[[NSMutableArray alloc]init];
    priceArray=[[NSMutableArray alloc]init];
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/rules/getNetCoinPaymentConfig",CONFIG_BASE_URL]];
    
    
    
    __block  ASIFormDataRequest *requestmethod = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    NSString *cardID=[check objectForKey:@"CARD_ID"];
    
    //(@"cardID:%@",cardID);
    
    
    
    [requestmethod setPostValue:[Config get_token] forKey:@"apikey"];
    [requestmethod setTimeOutSeconds:30];
    
    
    [requestmethod setCompletionBlock:^{
        // Use when fetching text data
        NSString *responseString = [requestmethod responseString];
        
        
        NSMutableData *results1 = [responseString JSONValue];
        
        //(@"HELLO:%@",results1);
        
        
        NSArray *temp= [results1 valueForKeyPath:@"response.netCoinConfig"];
        
        
        //(@"TEMPARY:%@",temp);
        
        for(NSDictionary *value in temp)
        {
            [netIDArray  addObject:[value valueForKey:@"id"]];
            [netEachArray  addObject:[value valueForKey:@"each"]];
           
            
            
            NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc]init];
            NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init] ;
            [formatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
            [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
            [formatter setGroupingSeparator:@""];
            [formatter setDecimalSeparator:@"."];
            
            // Decimal values read from any db are always written with no grouping separator and a comma for decimal.
            
            
            NSString *net_coin=[value valueForKey:@"netcoins"];
            
            NSNumber *numberFromString = [formatter numberFromString:net_coin];
            
            [formatter setGroupingSeparator:@","]; // Whatever you want here
            [formatter setDecimalSeparator:@","]; // Whatever you want here
            
            NSString *finalValue = [formatter stringFromNumber:numberFromString];
            
            //(@"%@",finalValue); // Print 12 000,54
            
             [netCoinsArray  addObject:finalValue];
            
            
            [priceArray  addObject:[value valueForKey:@"price"]];
            
        }
        
        
        
        if ([netEachArray[0]isEqualToString:@""])
        {
            but1Title=[NSString stringWithFormat:@"%@ NetCoin - $%@",netCoinsArray[0],priceArray[0]];
        }
        else
        {
            but1Title=[NSString stringWithFormat:@"%@ NetCoins - $%@ ($%@ each)",netCoinsArray[0],priceArray[0],netEachArray[0]];
        }
        
        
        if ([netEachArray[1]isEqualToString:@""])
        {
            but2Title=[NSString stringWithFormat:@"%@ NetCoin - $%@",netCoinsArray[1],priceArray[1]];
        }
        else
        {
            but2Title=[NSString stringWithFormat:@"%@ NetCoins - $%@ ($%@ each)",netCoinsArray[1],priceArray[1],netEachArray[1]];
        }
        
        if ([netEachArray[2]isEqualToString:@""])
        {
            but3Title=[NSString stringWithFormat:@"%@ NetCoin - $%@",netCoinsArray[2],priceArray[2]];
        }
        else
        {
            but3Title=[NSString stringWithFormat:@"%@ NetCoins - $%@ ($%@ each)",netCoinsArray[2],priceArray[2],netEachArray[2]];
        }
        
        if ([netEachArray[3]isEqualToString:@""])
        {
            but4Title=[NSString stringWithFormat:@"%@ NetCoin - $%@",netCoinsArray[3],priceArray[3]];
        }
        else
        {
            but4Title=[NSString stringWithFormat:@"%@ NetCoins - $%@ ($%@ each)",netCoinsArray[3],priceArray[3],netEachArray[3]];
        }
        
        
        //(@"1:%@",but1Title);
        //(@"2:%@",but2Title);
        //(@"3:%@",but3Title);
        //(@"4:%@",but4Title);
        
        _lab1.text=but1Title;
        _lab2.text=but2Title;
        _lab3.text=but3Title;
        _lab4.text=but4Title;
        
        
    }];
    [requestmethod setFailedBlock:^{
        NSError *error = [requestmethod error];
    }];
    [requestmethod startAsynchronous];
}
#ifdef IOS_OLDER_THAN_6
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return (toInterfaceOrientation == UIInterfaceOrientationMaskPortrait);
}
#endif
#ifdef IOS_NEWER_OR_EQUAL_TO_6
-(BOOL)shouldAutorotate {
    return YES;
}
- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
#endif
-(void)getNetCoins
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/getUserNetCoins",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post1 = [ASIFormDataRequest requestWithURL:url];
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    
    //[request_post setValidatesSecureCertificate:NO];
    [request_post1 setPostValue:user forKey:@"userID"];
    [request_post1 setPostValue:[Config get_token] forKey:@"apikey"];
    
    
    [request_post1 setCompletionBlock:^{
        NSString *responseString = [request_post1 responseString];
        NSMutableData *results1 = [responseString JSONValue];
        
        
       
        
        
        NSString *netCoins=[results1 valueForKeyPath:@"response.totalNetCoins"];
        
        
        NSUserDefaults *value=[NSUserDefaults standardUserDefaults];
        
        [value setObject:netCoins forKey:@"TOTAL_COINS"];
        
       
         //(@"influenceLevelStr:%@",netCoins);
        
        NSString *netCoinsStr=[NSString stringWithFormat:@"You have a total of %@ NetCoin(s)",netCoins];
       
        
        _totalNetCoinslabel.text=netCoinsStr;
        
        //  [self getInfluenceLevel:total_scores];
        
        
    }];
    [request_post1 setFailedBlock:^{
        NSError *error = [request_post1 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1 startAsynchronous];
    
    
}


- (IBAction)segmentDidChange:(id)sender
{
    
    [_reademTxtEield resignFirstResponder];
    
    [self updateSelectedSegmentLabel];
}


- (void)updateSelectedSegmentLabel
{
    
   
    
    self.selectedSegmentLabel.font = [UIFont boldSystemFontOfSize:self.selectedSegmentLabel.font.pointSize];
    self.selectedSegmentLabel.text = [NSString stringWithFormat:@"%d", self.segmentedControl.selectedSegmentIndex];
    
    
    if (self.segmentedControl.selectedSegmentIndex==0)
    {
        _detailView.hidden=YES;
        _buyView.hidden=NO;
        _accountView.hidden=YES;
        
        [self getAmountDetails];
    }
    
    if (self.segmentedControl.selectedSegmentIndex==1)
    {
        _detailView.hidden=NO;
        _buyView.hidden=YES;
        _accountView.hidden=YES;
    }
    if (self.segmentedControl.selectedSegmentIndex==2)
    {
        _detailView.hidden=YES;
        _buyView.hidden=YES;
        _accountView.hidden=NO;
        @try
        {
            [self history];
        }
        @catch (NSException *exception) {
            //(@"CATCH");
        }
        
        
    }
  
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, .5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^(void)
                   {
                       self.selectedSegmentLabel.font = [UIFont systemFontOfSize:self.selectedSegmentLabel.font.pointSize];
                   });
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    
    return [hisDesArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    AccountCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        
        NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"AccountCell" owner:nil options:nil];
        
        for (UIView *view in views)
        {
            if([view isKindOfClass:[UITableViewCell class]])
            {
                cell = (AccountCell*)view;
                //cell.img=@"date.png";
                
            }
        }
    }
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    @try {
        
    }
    @catch (NSException *exception) {
        
    }
  
    
    
    @try {
        cell.title.text=hisDesArray[indexPath.row];
        NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc] init];
        
        [dateFormat1 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        
        NSString *date_updated=hisDateArray[indexPath.row];
        
        NSDate *todaydate = [dateFormat1 dateFromString:date_updated];
        
        [dateFormat1 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]] ;
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
        [dateFormat setDateFormat:@"LLL d, yyyy HH:mm a"];
        NSString *dateString = [dateFormat stringFromDate:todaydate];
        
        
        
        cell.details.text=dateString;
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
    
   
    
    
   
    
    return cell;
    
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    return 65;
    
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */




-(IBAction)filterButtonClick
{
    NSArray *menuItems =
    @[
      
      [KxMenuItem menuItem:@"Filter Menu"
                     image:nil
                    target:nil
                    action:NULL],
      
      [KxMenuItem menuItem:@"All Deals"
                     image:[UIImage imageNamed:@"check_icon"]
                    target:self
                    action:@selector(pushMenuItem:)],
      
      [KxMenuItem menuItem:@"Populer"
                     image:[UIImage imageNamed:@"UIButton"]
                    target:self
                    action:@selector(pushMenuItem:)],
      
      [KxMenuItem menuItem:@"Nearest"
                     image:[UIImage imageNamed:@"UIButton"]
                    target:self
                    action:@selector(pushMenuItem:)],
      
      [KxMenuItem menuItem:@"Price"
                     image:[UIImage imageNamed:@"UIButton"]
                    target:self
                    action:@selector(pushMenuItem:)],
      
      [KxMenuItem menuItem:@"Selling"
                     image:[UIImage imageNamed:@"UIButton"]
                    target:self
                    action:@selector(pushMenuItem:)],
      ];
    
    KxMenuItem *first = menuItems[0];
    first.foreColor = [UIColor colorWithRed:47/255.0f green:112/255.0f blue:225/255.0f alpha:1.0];
    first.alignment = NSTextAlignmentCenter;
    
    [KxMenu showMenuInView:self.view
                  fromRect:CGRectMake(266, 75, 50, 50)
                 menuItems:menuItems];
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    //u need to change 0 to other value(,1,2,3) if u have more buttons.then u can check which button was pressed.
    
    if (alertView==alertExit)
    {
        if (buttonIndex == 0)
        {
            
            //(@"0");
            
            
        }
        else
        {
           // [self fbDidLogout];
            
            [FBSession.activeSession closeAndClearTokenInformation];
            
            NSUserDefaults *add=[NSUserDefaults standardUserDefaults];
            
            //(@"DEVICE TOKEN 123:%@",[add objectForKey:@"DEVICE_TOKEN"]);
            
            NSString *device_token=[add objectForKey:@"DEVICE_TOKEN"];
            
            if([device_token length]==0)
            {
                device_token=@"";
                
                //(@"DEVICE TOKEN 789:%@",device_token);
            }
            else
            {
                
                //(@"DEVICE TOKEN 789:%@",device_token);
                
                NSString *user=[add objectForKey:@"USERID"];
                
                //[request_post setValidatesSecureCertificate:NO];
                
                
                
                NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/pushnotification/updatedevicetoken",CONFIG_BASE_URL]];
                
                __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
                
                
                [request_post setValidatesSecureCertificate:NO];
                [request_post setPostValue:user forKey:@"userID"];
                [request_post setPostValue:device_token forKey:@"deviceToken"];
                [request_post setPostValue:[Config get_token] forKey:@"apikey"];
                [request_post setPostValue:@"logout" forKey:@"userStatus"];
                
                
                [request_post setTimeOutSeconds:30];
                
                
                [request_post setCompletionBlock:^{
                    NSString *responseString = [request_post responseString];
                    NSMutableData *results1 = [responseString JSONValue];
                    
                    
                    //(@"RESULTS:%@",results1);
                    
                    
                    
                    
                    
                    
                    
                }];
                [request_post setFailedBlock:^{
                    NSError *error = [request_post error];
                    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                        message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                              cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    
                    
                    
                    
                }];
                
                [request_post startAsynchronous];
                
                
            }

            
            
            
            NSUserDefaults *add1=[NSUserDefaults standardUserDefaults];
            
            [add1 removeObjectForKey:@"USERID"];
            
            // [add setObject:@"" forKey:@"USERID"];
            
            
            
            if (IS_IPHONE_5)
            {
                ViewController *viewController=[[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
                [self presentViewController:viewController animated:NO completion:^{
                    //////(@"SUCCESS");
                }
                 ];
            }
            else
            {
                ViewController *viewController=[[ViewController alloc]initWithNibName:@"ViewController~iPhone" bundle:nil];
                [self presentViewController:viewController animated:NO completion:^{
                    //////(@"SUCCESS");
                }
                 ];
            }
            
            
            
        }
    }
    else
    {
        
        
    }
    
}

-(IBAction)settingsButton
{
    
    
    alertExit = [[UIAlertView alloc] initWithTitle:@"Logout"
                                           message:@"Do you want to really exit?" delegate:self cancelButtonTitle: @"No"
                                 otherButtonTitles:@"Yes", nil];
    
    //note above delegate property
    
    [alertExit show];
    
    
    
}
- (void)fbDidLogout
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"FBAccessTokenKey"];
    [defaults removeObjectForKey:@"FBExpirationDateKey"];
    
    [defaults synchronize];
    
    NSHTTPCookie *cookie;
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (cookie in [storage cookies]) {
        [storage deleteCookie:cookie];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}


-(IBAction)buyButtonClick
{
    
    NSString *sendString;
    
    int tahValue;
    
    if (buttonTag==1)
    {
        sendString=but1Title;
        tahValue=0;
    }
    
    if (buttonTag==2)
    {
        sendString=but2Title;
         tahValue=1;
    }
    
    if (buttonTag==3)
    {
        sendString=but3Title;
        
         tahValue=2;
    }
    
    if (buttonTag==4)
    {
        sendString=but4Title;
         tahValue=3;
    }
    
    //(@"SEND STRINg:%@",sendString);
    
    
    NSString *amount=priceArray[tahValue];
    
    NSString *coins=netCoinsArray[tahValue];
    
    
    PaymentViewController *viewController=[[PaymentViewController alloc]initWithNibName:@"PaymentViewController" bundle:nil];
    
    viewController.productString=sendString;
    viewController.amountString=amount;
    viewController.currencyString=coins;
    
    [self presentViewController:viewController animated:YES completion:^{
        ////(@"SUCCESS");
    }
     ];
    
    /*
    UIFont * titleFont = [UIFont fontWithName:@"System" size:20.0f];
    UIFont * messageFont = [UIFont fontWithName:@"System" size:12.0f];
    
    // Set default appearnce block for all WCAlertViews
    // Similar functionality to UIAppearnce Proxy
    
    [WCAlertView setDefaultCustomiaztonBlock:^(WCAlertView *alertView) {
        alertView.labelTextColor = [UIColor colorWithRed:0.11f green:0.08f blue:0.39f alpha:1.00f];
        alertView.labelShadowColor = [UIColor whiteColor];
        
        UIColor *topGradient = [UIColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:1.0f];
        UIColor *middleGradient = [UIColor colorWithRed:0.93f green:0.94f blue:0.96f alpha:1.0f];
        UIColor *bottomGradient = [UIColor colorWithRed:0.89f green:0.89f blue:0.92f alpha:1.00f];
        alertView.gradientColors = @[topGradient,middleGradient,bottomGradient];
        
        alertView.outerFrameColor = [UIColor colorWithRed:250.0f/255.0f green:250.0f/255.0f blue:250.0f/255.0f alpha:1.0f];
        
        alertView.buttonTextColor = [UIColor colorWithRed:0.11f green:0.08f blue:0.39f alpha:1.00f];
        alertView.buttonShadowColor = [UIColor whiteColor];
        
        alertView.titleFont = titleFont;
        alertView.messageFont = messageFont;
    }];
    
    
    [WCAlertView showAlertWithTitle:@"Information" message:@"Thank you for Purchasing" customizationBlock:^(WCAlertView *alertView) {
        
        // You can also set different appearance for this alert using customization block
        
        alertView.style = WCAlertViewStyleBlackHatched;
        //        alertView.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
    } completionBlock:^(NSUInteger buttonIndex, WCAlertView *alertView) {
        if (buttonIndex == alertView.cancelButtonIndex) {
            //(@"Cancel");
        } else {
            //(@"Ok");
        }
    } cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
     
     */

}
-(IBAction)redeamButtonClick
{
    
    
    UIFont * titleFont = [UIFont fontWithName:@"System" size:20.0f];
    UIFont * messageFont = [UIFont fontWithName:@"System" size:12.0f];
    
    // Set default appearnce block for all WCAlertViews
    // Similar functionality to UIAppearnce Proxy
    
    [WCAlertView setDefaultCustomiaztonBlock:^(WCAlertView *alertView) {
        alertView.labelTextColor = [UIColor colorWithRed:0.11f green:0.08f blue:0.39f alpha:1.00f];
        alertView.labelShadowColor = [UIColor whiteColor];
        
        UIColor *topGradient = [UIColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:1.0f];
        UIColor *middleGradient = [UIColor colorWithRed:0.93f green:0.94f blue:0.96f alpha:1.0f];
        UIColor *bottomGradient = [UIColor colorWithRed:0.89f green:0.89f blue:0.92f alpha:1.00f];
        alertView.gradientColors = @[topGradient,middleGradient,bottomGradient];
        
        alertView.outerFrameColor = [UIColor colorWithRed:250.0f/255.0f green:250.0f/255.0f blue:250.0f/255.0f alpha:1.0f];
        
        alertView.buttonTextColor = [UIColor colorWithRed:0.11f green:0.08f blue:0.39f alpha:1.00f];
        alertView.buttonShadowColor = [UIColor whiteColor];
        
        alertView.titleFont = titleFont;
        alertView.messageFont = messageFont;
    }];
    

    
    if ([_reademTxtEield.text isEqualToString:@""])
    {
           
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Information!"
                                                            message:[NSString stringWithFormat:@"NetCoins gift card number is empty."] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];

        
        
    }
    else
    {
        
        if ([_reademTxtEield.text length]<16)
        {
                  
            
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Information!"
                                                                message:[NSString stringWithFormat:@"16 Digit Gift Card Number Is Invalid"] delegate:self
                                                      cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];


        }
        else
        {
        
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/redeems/processGiftCardNumber",CONFIG_BASE_URL]];
            
            __block  ASIFormDataRequest *request_post1 = [ASIFormDataRequest requestWithURL:url];
            
            
            NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
            NSString *user=[check objectForKey:@"USERID"];
            
            //[request_post setValidatesSecureCertificate:NO];
            [request_post1 setPostValue:user forKey:@"userID"];
            [request_post1 setPostValue:_reademTxtEield.text forKey:@"giftcard_no"];
             [request_post1 setPostValue:[Config get_token] forKey:@"apikey"];
            
            [request_post1 setCompletionBlock:^{
                NSString *responseString = [request_post1 responseString];
                NSMutableData *results1 = [responseString JSONValue];
                
                
                
                _reademTxtEield.text=@"";
                
                //(@"HELLO:%@",results1);
                
                
                NSString *error=[results1 valueForKeyPath:@"error.description"];
                
                NSString *success=[results1 valueForKeyPath:@"header.description"];
                
                
                //(@"HEADER:%@",success);
                 //(@"HEADER:%@",error);
                
                if ([success length]==0) {
                    //(@"FAILED");
                    
                    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                        message:[NSString stringWithFormat:@"%@",error] delegate:self
                                                              cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alertView show];


                }
                else
                {
                    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Information!"
                                                                        message:[NSString stringWithFormat:@"%@",success] delegate:self
                                                              cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alertView show];
                }
                
               
                [self getNetCoins];
                
                //  [self getInfluenceLevel:total_scores];
                
                
            }];
            [request_post1 setFailedBlock:^{
                NSError *error = [request_post1 error];
                UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                    message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                          cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                
                
                
            }];
            
            [request_post1 startAsynchronous];

            
            
            
            
                   
            
            
            
            
            
            
        }
    }
    
        
   

}

-(void)getAmountDetails
{
    
}

-(void)history
{
    
    hisDateArray=[[NSMutableArray alloc]init];
    hisDesArray=[[NSMutableArray alloc]init];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/netcoins/retrieveNetCoinCredit",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post1 = [ASIFormDataRequest requestWithURL:url];
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    
    //[request_post setValidatesSecureCertificate:NO];
    [request_post1 setPostValue:user forKey:@"userID"];
    [request_post1 setPostValue:[Config get_token] forKey:@"apikey"];
    
    
    [request_post1 setCompletionBlock:^{
        NSString *responseString = [request_post1 responseString];
        NSMutableData *results1 = [responseString JSONValue];
        
        
        
        
        
        //(@"RESULTS:%@",results1);
        
        
        
        
        NSArray *temp= [results1 valueForKeyPath:@"netcoins"];
        
        
        //(@"TEMPARY:%@",temp);
        
        for(NSDictionary *value in temp)
        {
            [hisDateArray  addObject:[value valueForKey:@"created_date"]];
            [hisDesArray  addObject:[value valueForKey:@"histrytext"]];
        }
        
        
        //(@"ARRAY:%@",hisDesArray);
        
        [_avilableTableView reloadData];
         

        
        
               
        
    }];
    [request_post1 setFailedBlock:^{
        NSError *error = [request_post1 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1 startAsynchronous];
}

- (void) pushMenuItem:(id)sender
{
    //(@"%@", sender);
    NSString *string=[NSString stringWithFormat:@"%@",sender];
    //(@"HELLO:%@",string);
    
    
    
    
    
}

-(IBAction)shareButtonClick
{
    sharenetCoinsViewController *viewController=[[sharenetCoinsViewController alloc]initWithNibName:@"sharenetCoinsViewController" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        ////(@"SUCCESS");
    }
     ];
 
}
-(IBAction)inviteButtonClick
{
    InvitenetCoinsViewController *viewController=[[InvitenetCoinsViewController alloc]initWithNibName:@"InvitenetCoinsViewController" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        ////(@"SUCCESS");
    }
     ];
}


-(IBAction)meButton
{
    if (IS_IPHONE_5) {
        MeViewController *viewController=[[MeViewController alloc]initWithNibName:@"MeViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            ////(@"SUCCESS");
        }
         ];
    }
    else
    {
        
        MeViewController *viewController=[[MeViewController alloc]initWithNibName:@"MeViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            ////(@"SUCCESS");
        }
         ];
    }

}
-(IBAction)dealsButton
{
    dealsViewController *viewController=[[dealsViewController alloc]initWithNibName:@"dealsViewController" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        ////(@"SUCCESS");
    }
     ];
}




-(IBAction)socialButton
{
    if (IS_IPHONE_5) {
        netCoinsViewController *viewController=[[netCoinsViewController alloc]initWithNibName:@"netCoinsViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            ////(@"SUCCESS");
        }
         ];
    }
    else
    {
        netCoinsViewController *viewController=[[netCoinsViewController alloc]initWithNibName:@"netCoinsViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            ////(@"SUCCESS");
        }
         ];
    }

}
-(IBAction)netCoinsButton
{
    if (IS_IPHONE_5) {
        socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            ////(@"SUCCESS");
        }
         ];
    }
    else
    {
        socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            ////(@"SUCCESS");
        }
         ];
    }

}
-(IBAction)myBusinessButton
{
    myBusinessViewController *viewController=[[myBusinessViewController alloc]initWithNibName:@"myBusinessViewController" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        ////(@"SUCCESS");
    }
     ];
}
- (BOOL)textFieldShouldReturn: (UITextField *)textField
{
  
    [textField resignFirstResponder];
    
    return YES;
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up

{
	
    int txtPosition = (textField.frame.origin.y - 100);
	
    const int movementDistance = (txtPosition < 0 ? 0 : txtPosition); // tweak as needed
	
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
	
    [UIView setAnimationBeginsFromCurrentState: YES];
	
    [UIView setAnimationDuration: movementDuration];
	
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
	
    [UIView commitAnimations];
	
}
- (void)textFieldDidBeginEditing:(UITextField *)textField

{
    
    
    [self animateTextField: textField up: YES];
    
}


- (void)textFieldDidEndEditing:(UITextField *)textField

{
	
    [self animateTextField: textField up: NO];
   	
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
