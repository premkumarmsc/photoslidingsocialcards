//
//  dealsViewController.h
//  PhotoSliding
//
//  Created by ephronsystems on 6/27/13.
//
//

#import <UIKit/UIKit.h>

@interface sendViewController : GAITrackedViewController<UICollectionViewDelegate,UICollectionViewDataSource,UIActionSheetDelegate,AVAudioRecorderDelegate, AVAudioPlayerDelegate>
{
    AVAudioPlayer *audioPlayer;
    
    UIScrollView* scrollView;
	UIPageControl* pageControl;
	
	BOOL pageControlBeingUsed;
    
    UIScrollView* scrollView1;
	UIPageControl* pageControl1;
	
	BOOL pageControlBeingUsed1;
    
    UIScrollView* scrollView2;
	UIPageControl* pageControl2;
	
	BOOL pageControlBeingUsed2;
    
    
    BOOL pageControlBeingUsedLands;
    UIScrollView* scrollViewLands;
	UIPageControl* pageControlLands;
    
    UIImageView *newImh1;
    
}
@property (weak, nonatomic) IBOutlet UIImageView *imageButton1;
@property (weak, nonatomic) IBOutlet UIView *landscapeView;




@property (weak, nonatomic) IBOutlet UITextField *editaddress1;
@property (weak, nonatomic) IBOutlet UITextField *editaddress2;
@property (weak, nonatomic) IBOutlet UITextField *editzipCodeValue;

-(IBAction)browseImage;
@property (weak, nonatomic) IBOutlet UITextField *editcityValue;
@property (weak, nonatomic) IBOutlet UITextField *editstate;
@property (weak, nonatomic) IBOutlet UIButton *editcountrybtn;
@property (weak, nonatomic) IBOutlet UIButton *savecontactbtn;
@property (weak, nonatomic) IBOutlet UITextField *editemailID;
@property (weak, nonatomic) IBOutlet UITextField *editphoneNo;

- (IBAction)editcountryButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *imageButton;

@property (weak, nonatomic) IBOutlet UIPickerView *picker2;
@property (weak, nonatomic) IBOutlet UIView *pickerBack2;
-(IBAction)donePicker2;

- (void)setupScrollView:(UIScrollView*)scrMain ;
@property (nonatomic, retain) IBOutlet UIScrollView* scrollView;
@property (nonatomic, retain) IBOutlet UIPageControl* pageControl;

- (IBAction)changePage;
- (IBAction)editAddress;
- (IBAction)back_contact;
- (IBAction)save_contact;
@property (retain, nonatomic) IBOutlet UILabel *edittitleText;

@property (nonatomic, retain) IBOutlet UIScrollView* scrollView1;
@property (nonatomic, retain) IBOutlet UIPageControl* pageControl1;

- (IBAction)changePage1;

@property (nonatomic, retain) IBOutlet UIScrollView* scrollView2;
@property (nonatomic, retain) IBOutlet UIPageControl* pageControl2;
@property (weak, nonatomic) IBOutlet UIView  *contactAddView;
- (IBAction)changePage2;


- (void)setupScrollView1:(UIScrollView*)scrMain ;
@property (nonatomic, retain) IBOutlet UIScrollView* scrollViewLands;
@property (nonatomic, retain) IBOutlet UIPageControl* pageControlLands;

- (IBAction)changePageLands;



@property (strong, nonatomic) IBOutlet UIButton *playAudio;
- (IBAction)AudioPlay:(id)sender;
@property(retain,nonatomic)IBOutlet UICollectionView *collectionView;
@property (retain, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (weak, nonatomic) IBOutlet UILabel *selectedSegmentLabel;
@property (nonatomic,retain)IBOutlet UITableView *avilableTableView;
@property (retain, nonatomic) IBOutlet UITextField *searchTxtEield;
@property (weak, nonatomic) IBOutlet UIButton *filterButton;
@property (weak, nonatomic) IBOutlet UIView  *detailView;
@property (weak, nonatomic) IBOutlet UIView  *redeem_view;
@property (weak, nonatomic) IBOutlet UIButton  *profileButton;



@property (weak, nonatomic) IBOutlet UIImageView *finalImageView;
@property (weak, nonatomic) IBOutlet UIImageView *fromImageView;
@property (weak, nonatomic) IBOutlet UIImageView *toImageView;
@property (retain, nonatomic) IBOutlet UITextView *messageText;
@property (retain, nonatomic) IBOutlet UILabel *titleText;

@property (retain, nonatomic) IBOutlet UILabel *nameText;
@property (retain, nonatomic) IBOutlet UITextView *addressText;
@property(retain,nonatomic)IBOutlet UICollectionView *collectionViewFinal;
@property(nonatomic,retain)IBOutlet UIView *scroll_back_view;
@property (weak, nonatomic) IBOutlet UIImageView *sm1;
@property (weak, nonatomic) IBOutlet UIImageView *sm2;
@property (weak, nonatomic) IBOutlet UIImageView *sm3;

@property (retain, nonatomic) IBOutlet UILabel *ad1;
@property (retain, nonatomic) IBOutlet UILabel *ad2;
@property (retain, nonatomic) IBOutlet UILabel *ad3;
@property (retain, nonatomic) IBOutlet UILabel *ad4;

-(IBAction)redeemSubmit;
-(IBAction)redeemBack;
@property (retain, nonatomic) IBOutlet UITextField *redeemTextField;


@property (weak, nonatomic) IBOutlet UIImageView *front_img;
@property (weak, nonatomic) IBOutlet UIImageView *bage_img;




@property (retain, nonatomic) IBOutlet UILabel *titleText1;
@property (retain, nonatomic) IBOutlet UITextView *messageText1;
@property (weak, nonatomic) IBOutlet UIImageView *fromImageView1;
@property (weak, nonatomic) IBOutlet UIImageView *finalImageView1;
@property (weak, nonatomic) IBOutlet UIImageView *sm11;
@property (weak, nonatomic) IBOutlet UIImageView *sm21;
@property (weak, nonatomic) IBOutlet UIImageView *sm31;
@property (strong, nonatomic) IBOutlet UIButton *playAudio1;
@property (retain, nonatomic) IBOutlet UILabel *nameText1;
@property (retain, nonatomic) IBOutlet UILabel *ad11;
@property (retain, nonatomic) IBOutlet UILabel *ad21;
@property (retain, nonatomic) IBOutlet UILabel *ad31;
@property (retain, nonatomic) IBOutlet UILabel *ad41;


- (IBAction)segmentDidChange:(id)sender;
-(IBAction)meButton;
-(IBAction)dealsButton;
-(IBAction)netCoinsButton;
-(IBAction)socialButton;
-(IBAction)myBusinessButton;
-(IBAction)filterButtonClick;
-(IBAction)dealbackButtonClick;
-(IBAction)settingsButton;
-(IBAction)sendButtonClick;
-(IBAction)backButton;

-(IBAction)continueButton;

-(IBAction)b1_Click;
-(IBAction)b2_Click;
-(IBAction)b3_Click;
-(IBAction)b4_Click;



@property (weak, nonatomic) IBOutlet UIView  *mainBackView;
@property (weak, nonatomic) IBOutlet UIView  *cardView1;
@property (weak, nonatomic) IBOutlet UIView  *cardView2;
@property (weak, nonatomic) IBOutlet UIView  *cardView3;
@property (weak, nonatomic) IBOutlet UIView  *cardView4;


@property (weak, nonatomic) IBOutlet UIView  *cardViewLands1;
@property (weak, nonatomic) IBOutlet UIView  *cardViewLands2;

@property (weak, nonatomic) IBOutlet UIView  *allBackView;
@property (weak, nonatomic) IBOutlet UIView  *socialCardView;
@property (weak, nonatomic) IBOutlet UIView  *emailView;
@property (weak, nonatomic) IBOutlet UIView  *shareview;

@property (weak, nonatomic) IBOutlet UIButton *b1;
@property (weak, nonatomic) IBOutlet UIButton *b2;
@property (weak, nonatomic) IBOutlet UIButton *b3;
@property (weak, nonatomic) IBOutlet UIButton *b4;

@property(nonatomic,retain)IBOutlet UIScrollView *scroll;

@property (weak, nonatomic) NSString *comeView;
-(IBAction)buy;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UIButton *buyButton;
@property (weak, nonatomic) IBOutlet UILabel *topLabel;

@end
