//
//  dealsViewController.m
//  PhotoSliding
//
//  Created by ephronsystems on 6/27/13.
//
//

#import "receivedCardViewController.h"
#import "LibraryCell.h"
#import "DetailHistory.h"

@interface receivedCardViewController ()

@end

@implementation receivedCardViewController
@synthesize avilableTableView;

NSMutableArray *nameArray;
NSMutableArray *statusArray;

NSMutableArray *cardIDArray;
NSMutableArray *snguseridArray;
NSMutableArray *contactidArray;
NSMutableArray *card_nameArray;
NSMutableArray *thumbnail_urlArray;
NSMutableArray *image_referenceArray;
NSMutableArray *message_textArray;
NSMutableArray *date_createdArray;
NSMutableArray *date_updatedArray;
NSMutableArray *saved_statusArray;
NSMutableArray *smileyIDArr;
NSMutableArray *audioIDArr;

NSMutableArray *senderPicArray;

NSMutableArray *emailFlagArr;
NSMutableArray *encryptedCardIDArr;



NSMutableArray *receiptentsArray;
NSMutableArray *statussArray;

-(IBAction)backCard
{
    
    if (IS_IPHONE_5)
    {
        socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            ////////(@"SUCCESS");
        }
         ];
        
    }
    else
    {
        socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            ////////(@"SUCCESS");
        }
         ];
        
    }

}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
     self.trackedViewName = @"ReceivedCard Screen";
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    
    NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:@"profileImage.jpg"];
    UIImage *profileIMG = [UIImage imageWithContentsOfFile:getImagePath];
    
    
    [_profileButton setImage:profileIMG forState:UIControlStateNormal];

    
    nameArray=[[NSMutableArray alloc]initWithObjects:@"Ganesh",@"Ramesh",@"Murthy",@"Vinith", nil];
    statusArray=[[NSMutableArray alloc]initWithObjects:@"Ordered",@"Waiting",@"Mailed",@"Delivered", nil];
    
    
    cardIDArray=[[NSMutableArray alloc]init];
     snguseridArray=[[NSMutableArray alloc]init];
     contactidArray=[[NSMutableArray alloc]init];
     card_nameArray=[[NSMutableArray alloc]init];
     thumbnail_urlArray=[[NSMutableArray alloc]init];
     image_referenceArray=[[NSMutableArray alloc]init];
     message_textArray=[[NSMutableArray alloc]init];
    date_createdArray=[[NSMutableArray alloc]init];
    date_updatedArray=[[NSMutableArray alloc]init];
    saved_statusArray=[[NSMutableArray alloc]init];
    smileyIDArr=[[NSMutableArray alloc]init];
    receiptentsArray=[[NSMutableArray alloc]init];
    statussArray=[[NSMutableArray alloc]init];
    audioIDArr=[[NSMutableArray alloc]init];
    senderPicArray=[[NSMutableArray alloc]init];
    emailFlagArr=[[NSMutableArray alloc]init];
    encryptedCardIDArr=[[NSMutableArray alloc]init];
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/socialcards/receivedCardsnew",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post1234 = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
  
    
    [request_post1234 setPostValue:user forKey:@"userID"];
     [request_post1234 setPostValue:[Config get_token] forKey:@"apikey"];
   // [request_post1234 setPostValue:@"" forKey:@"card_status"];
    
    
    [request_post1234 setTimeOutSeconds:30];
    
    
    [request_post1234 setCompletionBlock:^{
        NSString *responseString23 = [request_post1234 responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        
        //(@"RESULTS LOGIN:%@",results11);
        
        
        NSArray *temp= [results11 valueForKeyPath:@"socialcards"];
        
        
        ////(@"TEMPARY RECEIVED:%@",temp);
        
        for(NSDictionary *value in temp)
        {
            [cardIDArray  addObject:[value valueForKey:@"cardid"]];
            [snguseridArray  addObject:[value valueForKey:@"sender_userid"]];
            [contactidArray  addObject:[value valueForKey:@"contactid"]];
            [card_nameArray  addObject:[value valueForKey:@"card_name"]];
            [thumbnail_urlArray  addObject:[value valueForKey:@"cardimg"]];
            [image_referenceArray  addObject:[value valueForKey:@"cardimg"]];//
            [message_textArray  addObject:[value valueForKey:@"message_text"]];
            [date_createdArray  addObject:[value valueForKey:@"date_created"]];
            [date_updatedArray  addObject:[value valueForKey:@"date_created"]];
            [saved_statusArray  addObject:[value valueForKey:@"current_status"]];
             [receiptentsArray  addObject:[value valueForKey:@"sender_name"]];
             [smileyIDArr  addObject:[value valueForKey:@"smiley_ids"]];
             [audioIDArr  addObject:[value valueForKey:@"audio_id"]];
             [senderPicArray  addObject:[value valueForKey:@"fbsender_pic"]];
            
            [emailFlagArr  addObject:[value valueForKey:@"email_flag"]];
            //[encryptedCardIDArr  addObject:[value valueForKey:@"encrypt_cardid"]];
        }
        
        
        //(@"ARRAY:%@",senderPicArray);
        
        [avilableTableView reloadData];
        
        
    }];
    [request_post1234 setFailedBlock:^{
        NSError *error = [request_post1234 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1234 startAsynchronous];
    
    
    [self getNetCoins];
    
    
    // Do any additional setup after loading the view from its nib.
}
#ifdef IOS_OLDER_THAN_6
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return (toInterfaceOrientation == UIInterfaceOrientationMaskPortrait);
}
#endif
#ifdef IOS_NEWER_OR_EQUAL_TO_6
-(BOOL)shouldAutorotate {
    return YES;
}
- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
#endif

-(IBAction)saveCard
{
    
    if (IS_IPHONE_5)
    {
        socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            ////////(@"SUCCESS");
        }
         ];
        
    }
    else
    {
        socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            ////////(@"SUCCESS");
        }
         ];
        
    }

}


-(void)getNetCoins
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/getUserNetCoins",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post1 = [ASIFormDataRequest requestWithURL:url];
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    
    //[request_post setValidatesSecureCertificate:NO];
    [request_post1 setPostValue:user forKey:@"userID"];
     [request_post1 setPostValue:[Config get_token] forKey:@"apikey"];
    
    
    [request_post1 setCompletionBlock:^{
        NSString *responseString = [request_post1 responseString];
        NSMutableData *results1 = [responseString JSONValue];
        
        
        
        
        
        NSString *netCoins=[results1 valueForKeyPath:@"response.totalNetCoins"];
        
        
        NSUserDefaults *value=[NSUserDefaults standardUserDefaults];
        
        [value setObject:netCoins forKey:@"TOTAL_COINS"];
        
        
        //////(@"influenceLevelStr:%@",netCoins);
        
        NSString *netCoinsStr=[NSString stringWithFormat:@"You have a total of %@ NetCoin(s)",netCoins];
        
        
       // _totalNetCoinslabel.text=netCoinsStr;
        
        //  [self getInfluenceLevel:total_scores];
        
        
    }];
    [request_post1 setFailedBlock:^{
        NSError *error = [request_post1 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1 startAsynchronous];
    
    
}



-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    
    return [card_nameArray count];
}
-(IBAction)settingsButton
{
    ViewController *viewController=[[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        ////////(@"SUCCESS");
    }
     ];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    LibraryCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        
        NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"LibraryCell" owner:nil options:nil];
        
        for (UIView *view in views)
        {
            if([view isKindOfClass:[UITableViewCell class]])
            {
                cell = (LibraryCell*)view;
                //cell.img=@"date.png";
                
            }
        }
    }
    
    cell.selectionStyle=UITableViewCellSelectionStyleGray;
    
    
    
//    if ([statusArray[indexPath.row] isEqualToString:@"Delivered"])
//    {
//         cell.details.textColor=[UIColor greenColor];
//    }
//    
//    
//    if ([statusArray[indexPath.row] isEqualToString:@"Waiting"])
//    {
//        cell.details.textColor=[UIColor blackColor];
//    }
//    
//    if ([statusArray[indexPath.row] isEqualToString:@"Mailed"])
//    {
//        cell.details.textColor=[UIColor blueColor];
//    }
    
    
    cell.title.text=[NSString stringWithFormat:@" %@",card_nameArray[indexPath.row]];
    
    
     NSString *newString = [[message_textArray[indexPath.row] componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
    
    
    cell.details.text=[NSString stringWithFormat:@" %@",newString];
    
    
    
       
    
     cell.name.text=receiptentsArray[indexPath.row];
    
    
   
    
    NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc] init];
    
    //[dateFormat1 setDateFormat:@"yyyy-MM-dd"];
    
    [dateFormat1 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    
    NSString *date_updated=date_updatedArray[indexPath.row];
    
   // NSString *new=[date_updated replaceO]
    
    
    //////(@"Original date:%@",date_updated);
    
       
    NSDate *todaydate = [dateFormat1 dateFromString:date_updated];// date with yyyy-MM-dd format
    
   
    
    //////(@"DATE:%@",todaydate);
    
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    
    
    
    [dateFormat setDateFormat:@"LLL d, yyyy HH:mm a"];
    NSString *dateString = [dateFormat stringFromDate:todaydate];
    
    //////(@"DATE STRING:%@",dateString);
    
    
    
    cell.date.text=dateString;
    
    
    NSString *temp=[thumbnail_urlArray objectAtIndex:indexPath.row];
    [cell.img setImageWithURL:[NSURL URLWithString:temp]
             placeholderImage:[UIImage imageNamed:@"bigicon.png"]];

    
    // [cell.img setImageWithURL:[NSURL URLWithString:searchImageArray[indexPath.row]]
    // placeholderImage:[UIImage imageNamed:@"temp.jpg"]];
    
    
    // cell.img.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@",searchImageArray[indexPath.row]]];
    // cell.title.text=searchStringArray[indexPath.row];
    // cell.category.text=searchmetadataArray[indexPath.row];
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    return cell;
    
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
       
    
    if (IS_IPHONE_5) {
        
    
    
    DetailHistory *detail=[[DetailHistory alloc]initWithNibName:@"DetailHistory" bundle:nil];
    
    //////(@"CCC:%@",card_nameArray[indexPath.row]);
    //////(@"CCC:%@",message_textArray[indexPath.row]);
    //////(@"CCC:%@",image_referenceArray[indexPath.row]);
    //////(@"CCC:%@",cardIDArray[indexPath.row]);
    //////(@"CCC:%@",date_createdArray[indexPath.row]);
    //////(@"CCC:%@",date_updatedArray[indexPath.row]);
    //////(@"CCC:%@",saved_statusArray[indexPath.row]);
    //////(@"CCC:%@",contactidArray[indexPath.row]);
    //////(@"CCC:%@",audioIDArr[indexPath.row]);
    
    detail.getName=card_nameArray[indexPath.row];
    detail.getMessage=message_textArray[indexPath.row];
    detail.getImage=image_referenceArray[indexPath.row];
    detail.getCardID=cardIDArray[indexPath.row];
    detail.getCreated=date_createdArray[indexPath.row];
    detail.getUpdated=date_updatedArray[indexPath.row];
    detail.getStatus=saved_statusArray[indexPath.row];
    detail.getContact=contactidArray[indexPath.row];
    detail.getSmileyIDs=smileyIDArr[indexPath.row];
         detail.getAudioID=audioIDArr[indexPath.row];
        detail.getCancelparam=@"no";
         detail.getViewName=@"RECEIVED";
        detail.getSenderPic=senderPicArray[indexPath.row];
    
    [self presentViewController:detail animated:NO completion:nil];
    }
        else
        {
            
            
            DetailHistory *detail=[[DetailHistory alloc]initWithNibName:@"DetailHistory~iPhone" bundle:nil];
            
            //////(@"CCC:%@",card_nameArray[indexPath.row]);
            //////(@"CCC:%@",message_textArray[indexPath.row]);
            //////(@"CCC:%@",image_referenceArray[indexPath.row]);
            //////(@"CCC:%@",cardIDArray[indexPath.row]);
            //////(@"CCC:%@",date_createdArray[indexPath.row]);
            //////(@"CCC:%@",date_updatedArray[indexPath.row]);
            //////(@"CCC:%@",saved_statusArray[indexPath.row]);
            //////(@"CCC:%@",contactidArray[indexPath.row]);
            detail.getName=card_nameArray[indexPath.row];
            detail.getMessage=message_textArray[indexPath.row];
            detail.getImage=image_referenceArray[indexPath.row];
            detail.getCardID=cardIDArray[indexPath.row];
            detail.getCreated=date_createdArray[indexPath.row];
            detail.getUpdated=date_updatedArray[indexPath.row];
            detail.getStatus=saved_statusArray[indexPath.row];
            detail.getContact=contactidArray[indexPath.row];
            detail.getSmileyIDs=smileyIDArr[indexPath.row];
            detail.getAudioID=audioIDArr[indexPath.row];
            detail.getCancelparam=@"no";
            detail.getViewName=@"RECEIVED";
            detail.getSenderPic=senderPicArray[indexPath.row];
            
            [self presentViewController:detail animated:NO completion:nil];
        }
           
        
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    return 94;
    
}

-(IBAction)sendCardButton
{
    NSUserDefaults *storeImageValues=[NSUserDefaults standardUserDefaults];
     [storeImageValues setObject:@"" forKey:@"AUDIO_REFERENCE"];
    
    [storeImageValues setObject:@"NEW_CARDS" forKey:@"VIEW_REFERENCE"];
    
    if (IS_IPHONE_5)
    {
        albumViewController *viewController=[[albumViewController alloc]initWithNibName:@"albumViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            ////////(@"SUCCESS");
        }
         ];
    }
    else
    {
    
    albumViewController *viewController=[[albumViewController alloc]initWithNibName:@"albumViewController~iPhone" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        ////////(@"SUCCESS");
    }
     ];
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)meButton
{
    if (IS_IPHONE_5) {
        MeViewController *viewController=[[MeViewController alloc]initWithNibName:@"MeViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            ////////(@"SUCCESS");
        }
         ];
    }
    else
    {
        
        MeViewController *viewController=[[MeViewController alloc]initWithNibName:@"MeViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            ////////(@"SUCCESS");
        }
         ];
    }

}
-(IBAction)dealsButton
{
    dealsViewController *viewController=[[dealsViewController alloc]initWithNibName:@"dealsViewController" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        ////////(@"SUCCESS");
    }
     ];
}
-(IBAction)socialButton
{
    if (IS_IPHONE_5) {
        netCoinsViewController *viewController=[[netCoinsViewController alloc]initWithNibName:@"netCoinsViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            ////////(@"SUCCESS");
        }
         ];
    }
    else
    {
        netCoinsViewController *viewController=[[netCoinsViewController alloc]initWithNibName:@"netCoinsViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            ////////(@"SUCCESS");
        }
         ];
    }

}
-(IBAction)netCoinsButton
{
    if (IS_IPHONE_5) {
        socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            ////////(@"SUCCESS");
        }
         ];
    }
    else
    {
        socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            ////////(@"SUCCESS");
        }
         ];
    }

}
-(IBAction)myBusinessButton
{
    myBusinessViewController *viewController=[[myBusinessViewController alloc]initWithNibName:@"myBusinessViewController" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        ////////(@"SUCCESS");
    }
     ];
}


@end
