//
//  dealsViewController.h
//  PhotoSliding
//
//  Created by ephronsystems on 6/27/13.
//
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
@interface audioViewController : GAITrackedViewController<AVAudioRecorderDelegate, AVAudioPlayerDelegate,UIAlertViewDelegate>
{
    int currentIndex;
    int prevIndex;
    
    AVAudioPlayer *audioPlayer;
}


@property (strong, nonatomic) IBOutlet UIButton *playAudio;
- (IBAction)Sound_List:(id)sender;
- (IBAction)Alarm:(id)sender;
- (IBAction)Sleep:(id)sender;
- (IBAction)Upgrade:(id)sender;
- (IBAction)adjustVolume:(id)sender;
- (IBAction)prevAudioPlay:(id)sender;
- (IBAction)nextAudioPlay:(id)sender;
- (IBAction)AudioPlay:(id)sender;
@property(nonatomic)int currentIndex;
@property (strong, nonatomic) IBOutlet UISlider *volumControl;
@property (strong, nonatomic) IBOutlet UIButton *prevAudio;
@property (strong, nonatomic) IBOutlet UIButton *nextAudio;

@property (strong, nonatomic) IBOutlet UILabel *maxLabelrec;

@property (strong, nonatomic) IBOutlet UILabel *recDis;

@property (strong, nonatomic) IBOutlet UILabel *playTimeLabel;

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

@property (strong, nonatomic) IBOutlet UILabel *currentLabelrec;

@property(nonatomic,retain)IBOutlet UIView *recordView;

@property(nonatomic,retain)IBOutlet UIImageView *recordImageView;

@property(nonatomic,retain)IBOutlet UIImageView *playImageView;

@property(nonatomic,retain)IBOutlet UIView *longPressView;

- (IBAction)playList:(id)sender;

@property (nonatomic,retain)IBOutlet UITableView *avilableTableView;
-(IBAction)meButton;
-(IBAction)dealsButton;
-(IBAction)netCoinsButton;
-(IBAction)socialButton;
-(IBAction)sendCardButton;
-(IBAction)myBusinessButton;
-(IBAction)settingsButton;

-(IBAction)back:(id)sender;

-(IBAction)continue_btn:(id)sender;
-(IBAction)skip_btn:(id)sender;

-(IBAction)saveCard;

@property (weak, nonatomic) IBOutlet UIButton  *profileButton;
@property (weak, nonatomic) IBOutlet UIButton  *continueButton;
@property (weak, nonatomic) IBOutlet UIButton  *skipButton;

//// AUDIO RECORDING

@property (weak, nonatomic) IBOutlet UIButton *recordPauseButton;
@property (weak, nonatomic) IBOutlet UIButton *stopButton;
@property (weak, nonatomic) IBOutlet UIButton *playButton;

@property (weak, nonatomic) IBOutlet UILabel *titleLength;
@property (weak, nonatomic) IBOutlet UILabel *textLength;
@property (weak, nonatomic) IBOutlet UILabel *labelButton;

- (IBAction)recordPauseTapped:(id)sender;
- (IBAction)stopTapped:(id)sender;
- (IBAction)playTapped:(id)sender;

///


@property(nonatomic,retain)IBOutlet UIView *back_view_bottom;
@property(nonatomic,retain)IBOutlet UIView *recordScreen;
@property(nonatomic,retain)IBOutlet UIView *listScreen;

- (IBAction)recordList:(id)sender;

- (IBAction)attach:(id)sender;

- (IBAction)delete_btn:(id)sender;

- (IBAction)newRecord:(id)sender;

@property(nonatomic,retain)IBOutlet UIActivityIndicatorView *activityRecord;
@property(nonatomic,retain)IBOutlet UIActivityIndicatorView *activityPlaylist;



@property (weak, nonatomic) IBOutlet UIButton *attachButton;
@property (weak, nonatomic) IBOutlet UIButton *attachButtonNew;
@property (weak, nonatomic) IBOutlet UIButton *removeButtonName;

@property (weak, nonatomic) IBOutlet UIButton *ListScreenButton;


- (IBAction)attachRec:(id)sender;

- (IBAction)newRecordRec:(id)sender;



@end
