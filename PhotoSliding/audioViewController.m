//
//  dealsViewController.m
//  PhotoSliding
//
//  Created by ephronsystems on 6/27/13.
//
//

#import "audioViewController.h"
#import "audio.h"
#import "DetailHistory.h"

@interface audioViewController ()

{
    AVAudioRecorder *recorder;
    AVAudioPlayer *player;
}



@end

@implementation audioViewController
@synthesize avilableTableView;

@synthesize stopButton, playButton, recordPauseButton;
NSTimer *sliderUpdateTimer;
int time_value;
int check_stop;


int checkAudioIndex;



NSMutableArray *nameArray;
NSMutableArray *statusArray;

NSMutableArray *cardIDArray;
NSMutableArray *snguseridArray;
NSMutableArray *contactidArray;
NSMutableArray *card_nameArray;
NSMutableArray *thumbnail_urlArray;
NSMutableArray *image_referenceArray;
NSMutableArray *message_textArray;
NSMutableArray *date_createdArray;
NSMutableArray *date_updatedArray;
NSMutableArray *saved_statusArray;
NSMutableArray *smileyIDArr;


NSMutableArray *audioURLArray;
NSMutableArray *audioIDArr;

NSMutableArray *receiptentsArray;
NSMutableArray *statussArray;

NSMutableArray *audioFilesArray;


int bool_value;


- (IBAction)recordList:(id)sender
{
    
    
    bool_value=0;
    
    if (audioPlayer)
    {
        [audioPlayer stop];
        
        audioPlayer = nil;
    } else
    {
        // what you already have goes here
    }
    
    
    
    [self stop_audio_playing];
    
    
    _attachButton.hidden=YES;
    _attachButtonNew.hidden=YES;
    
    _recordScreen.hidden=NO;
    _listScreen.hidden=YES;
    
    _maxLabelrec.hidden=NO;
    _currentLabelrec.hidden=NO;
    _recDis.text=@"";
    _volumControl.hidden=NO;
    _playImageView.hidden=NO;
    
    
    _nextAudio.hidden=YES;
    _prevAudio.hidden=YES;
    
    _nextAudio.enabled=NO;
    _prevAudio.enabled=NO;
    
    
    _titleLabel.text=@"";
    _playAudio.enabled=NO;
    
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* foofile = [documentsPath stringByAppendingPathComponent:@"SNGAudio.m4a"];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:foofile];
    
    
    if (fileExists)
    {
        
        _playAudio.enabled=YES;
        _titleLabel.text=@"New Record";
    }

    
    
    //_nextAudio.enabled=YES;
    //_prevAudio.enabled=YES;
    
    checkAudioIndex=0;
    
}
- (IBAction)playList:(id)sender
{
    
    if (audioPlayer)
    {
        [audioPlayer stop];
        
        audioPlayer = nil;
    } else
    {
        // what you already have goes here
    }
    
    
    
    [self stop_audio_playing];
    
    _recordScreen.hidden=YES;
    _listScreen.hidden=NO;
    
    _nextAudio.hidden=NO;
    _prevAudio.hidden=NO;
    
    
    //_titleLabel.text=@"";
    _playAudio.enabled=YES;
    //_nextAudio.enabled=YES;
    //_prevAudio.enabled=YES;
    
    if ([audioIDArr count]!=1)
    {
        _playAudio.enabled=YES;
        _nextAudio.enabled=YES;
        _prevAudio.enabled=YES;
    }
    
    ////(@"ASAS:%@",audioFilesArray);
    
    checkAudioIndex=1;
    
}

- (IBAction)attach:(id)sender
{
    _activityPlaylist.hidden=NO;
    [_activityPlaylist startAnimating];
    
    ////(@"Current Index:%d",currentIndex);
    
    _continueButton.hidden=NO;
    _skipButton.hidden=YES;
    
    
    if (audioPlayer)
    {
        [audioPlayer stop];
        
        audioPlayer = nil;
    } else
    {
        // what you already have goes here
    }
    
    
    
    
    
    
    
    [self stop_audio_playing];
    
    
    
    NSString *temp1= audioIDArr[currentIndex];
    
    
    ////(@"AudioID:%@",temp1);
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/socialcards/updateCard",CONFIG_BASE_URL]];
    
    
    
    __block  ASIFormDataRequest *requestmethod = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    NSString *cardID=[check objectForKey:@"CARD_ID"];
    
    ////(@"cardID:%@",cardID);
    
    
    
    [requestmethod setPostValue:user forKey:@"userID"];
    [requestmethod setPostValue:cardID forKey:@"id"];
     [requestmethod setPostValue:[Config get_token] forKey:@"apikey"];
    [requestmethod setPostValue:temp1 forKey:@"audio_id"];
    
    [requestmethod setTimeOutSeconds:30];
    
    
    [requestmethod setCompletionBlock:^{
        NSString *responseString23 = [requestmethod responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        NSString *cardID= [results11 valueForKeyPath:@"cardID"];
        
        ////(@"CARD_ID");
        
        NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
        [check setObject:cardID forKey:@"CARD_ID"];
        
        
              
        
        
        [check setObject:temp1 forKey:@"AUDIO_REFERENCE"];
        
       
        
        ////(@"TEMPARY:%@",results11);
        
        
        _activityPlaylist.hidden=YES;
        [_activityPlaylist stopAnimating];
        
        
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Success!"
                                                            message:[NSString stringWithFormat:@"Audio is successfully attached to the SocialCard"] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alertView show];
        _removeButtonName.hidden=NO;
        
        _skipButton.hidden=YES;
        _continueButton.hidden=NO;
        
        [self retriveAudios];
        
    }];
    [requestmethod setFailedBlock:^{
        NSError *error = [requestmethod error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        _activityPlaylist.hidden=YES;
        [_activityPlaylist stopAnimating];
        
    }];
    
    [requestmethod startAsynchronous];

    
    
}

- (IBAction)attachRec:(id)sender
{
   
    
    _activityRecord.hidden=NO;
    [_activityRecord startAnimating];
    
    _continueButton.hidden=NO;
    _skipButton.hidden=YES;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *getVideoPathNew = [documentsDirectory stringByAppendingPathComponent:@"SNGAudio.m4a"];
    
    
    
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/files/postfile",CONFIG_BASE_URL]];
    
    
    __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
    
    
    [request_post setFile:getVideoPathNew forKey:@"uploadedfile"];
    
    
    
    
    
     [request_post setPostValue:[Config get_token] forKey:@"apikey"];
    
    
    [request_post setShowAccurateProgress: YES];
    
    // UPLOAD APP ENTER BACKGROUND //
    [request_post setShouldContinueWhenAppEntersBackground:YES];
    
    
    //[request_post setUploadProgressDelegate:progressBar];
    
    
    
    
    
    [request_post setCompletionBlock:^{
        // Use when fetching text data
        NSString *responseString = [request_post responseString];
        
        
        
        
        ////(@"RES:%@",responseString);
        
        NSMutableData *results11 = [responseString JSONValue];
        
        
        //////(@"RESULTS LOGIN:%@",results11);
        
        
        NSString *temp= [results11 valueForKeyPath:@"photo_url"];
        
        
        ////(@"AUDIO URL:%@",temp);
        
        
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/audios/createAudioSticker",CONFIG_BASE_URL]];
        
        
        
        __block  ASIFormDataRequest *requestmethod = [ASIFormDataRequest requestWithURL:url];
        
        
        
        NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
        NSString *user=[check objectForKey:@"USERID"];
     
        ////(@"USER:%@",user);
        
        ////(@"TEMP:%@",temp);
        
        [requestmethod setPostValue:user forKey:@"userID"];
        [requestmethod setPostValue:temp forKey:@"audioURL"];
        [requestmethod setPostValue:@"iphone" forKey:@"source"];
         [requestmethod setPostValue:[Config get_token] forKey:@"apikey"];
        
        [requestmethod setTimeOutSeconds:30];
        
        
        [requestmethod setCompletionBlock:^{
            NSString *responseString23 = [requestmethod responseString];
            NSMutableData *results11 = [responseString23 JSONValue];
            
            
            
            ////(@"TEMPARY AUDIO:%@",results11);
            
            NSString *temp1= [results11 valueForKeyPath:@"audioID"];
            
             ////(@"AudioURL:%@",temp);
            ////(@"AudioID:%@",temp1);
            
                      
            
            
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/socialcards/updateCard",CONFIG_BASE_URL]];
            
            
            
            __block  ASIFormDataRequest *requestmethod = [ASIFormDataRequest requestWithURL:url];
            
            
            
            NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
            NSString *user=[check objectForKey:@"USERID"];
            NSString *cardID=[check objectForKey:@"CARD_ID"];
           
            ////(@"USER:%@",user);
            
            
            @try {
                
                [requestmethod setPostValue:user forKey:@"userID"];
                [requestmethod setPostValue:cardID forKey:@"id"];
                [requestmethod setPostValue:[Config get_token] forKey:@"apikey"];
                [requestmethod setPostValue:temp1 forKey:@"audio_id"];
            }
            @catch (NSException *exception) {
                
            }
           
            
            [requestmethod setTimeOutSeconds:30];
            
            
            [requestmethod setCompletionBlock:^{
                NSString *responseString23 = [requestmethod responseString];
                NSMutableData *results11 = [responseString23 JSONValue];
                
                NSString *cardID= [results11 valueForKeyPath:@"cardID"];
                
                ////(@"CARD_ID");
                
                NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
                [check setObject:cardID forKey:@"CARD_ID"];
                
                
              
                [check setObject:temp1 forKey:@"AUDIO_REFERENCE"];
                
               
                
                
                
                ////(@"TEMPARY:%@",results11);
                
                
                _activityRecord.hidden=YES;
                [_activityRecord stopAnimating];
                
                
                [self afterConversion:temp];
                
                
                UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Success!"
                                                                    message:[NSString stringWithFormat:@"Audio is successfully attached to the SocialCard"] delegate:self
                                                          cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                [alertView show];

                _removeButtonName.hidden=NO;
                _skipButton.hidden=YES;
                _continueButton.hidden=NO;
                
                
               [self retriveAudios];
            
            }];
            [requestmethod setFailedBlock:^{
                NSError *error = [requestmethod error];
                UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                    message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                          cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                
                _activityRecord.hidden=YES;
                
                
                
                [_activityRecord stopAnimating];
                
                
            }];
            
            [requestmethod startAsynchronous];
            
            
            // audioURL=temp1;
            
        }];
        [requestmethod setFailedBlock:^{
            NSError *error = [requestmethod error];
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                      cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            
            _activityRecord.hidden=YES;
            
           
            
            [_activityRecord stopAnimating];
            
        }];
        
        [requestmethod startAsynchronous];
        
        
        
        
        
        ////(@"Audio URL:%@",temp);;
        
        
    }];
    [request_post setFailedBlock:^{
        NSError *error = [request_post error];
        
        
        //text.text=[NSString stringWithFormat:@"%@",error];
        
    }];
    [request_post startAsynchronous];
    
}

-(void)afterConversion:(NSString *)getURL
{
    ////(@"GET URL ELEMENT:%@",getURL);
    
    
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/setAudioprocess",CONFIG_BASE_URL]];
    
    
    
    __block  ASIFormDataRequest *requestmethod = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    NSString *cardID=[check objectForKey:@"CARD_ID"];
    
    ////(@"USER:%@",user);
    
    
    
    [requestmethod setPostValue:user forKey:@"userID"];
    [requestmethod setPostValue:[Config get_token] forKey:@"apikey"];
    [requestmethod setPostValue:getURL forKey:@"fileSource"];
    
    [requestmethod setTimeOutSeconds:30];
    
    
    [requestmethod setCompletionBlock:^{
        NSString *responseString23 = [requestmethod responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
              
        
        ////(@"TEMPARY FINAL:%@",responseString23);
        
        
              
       
        
    }];
    [requestmethod setFailedBlock:^{
        NSError *error = [requestmethod error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        _activityRecord.hidden=YES;
        
        
        
       // [_activityRecord stopAnimating];
        
        
    }];
    
    [requestmethod startAsynchronous];
    
     
}

- (IBAction)newRecordRec:(id)sender
{
    
    checkAudioIndex=0;
    
    if (audioPlayer)
    {
        [audioPlayer stop];
        
        audioPlayer = nil;
    } else
    {
        // what you already have goes here
    }
    
    
    
    [self stop_audio_playing];
    
    _titleLabel.text=@"";
    _playAudio.enabled=NO;
    _nextAudio.enabled=NO;
    _prevAudio.enabled=NO;
    
    _recordScreen.hidden=NO;
    _listScreen.hidden=YES;
    
    _attachButton.hidden=YES;
    _attachButtonNew.hidden=YES;
    
    
    _volumControl.hidden=NO;
    _playImageView.hidden=NO;
    _currentLabelrec.hidden=NO;
    _maxLabelrec.hidden=NO;
    
    
    _nextAudio.hidden=YES;
    _prevAudio.hidden=YES;
    
    _nextAudio.enabled=NO;
    _prevAudio.enabled=NO;
    
   // _nextAudio.hidden=YES;
   // _prevAudio.hidden=YES;
}



- (IBAction)delete_btn:(id)sender
{
    
    _removeButtonName.hidden=YES;
    _activityPlaylist.hidden=NO;
    [_activityPlaylist startAnimating];
    
   
    
    _continueButton.hidden=NO;
    _skipButton.hidden=YES;
    
    
    if (audioPlayer)
    {
        [audioPlayer stop];
        
        audioPlayer = nil;
    } else
    {
        // what you already have goes here
    }
    
    
    
    
    
    
    
    [self stop_audio_playing];
    
    
    
    NSString *temp1= audioIDArr[currentIndex];
    
    
    ////(@"AudioID:%@",temp1);
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/socialcards/updateCard",CONFIG_BASE_URL]];
    
    
    
    __block  ASIFormDataRequest *requestmethod = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    NSString *cardID=[check objectForKey:@"CARD_ID"];
    
    ////(@"cardID:%@",cardID);
    
    
    
    [requestmethod setPostValue:user forKey:@"userID"];
    [requestmethod setPostValue:cardID forKey:@"id"];
     [requestmethod setPostValue:[Config get_token] forKey:@"apikey"];
    [requestmethod setPostValue:@"" forKey:@"audio_id"];
    
    [requestmethod setTimeOutSeconds:30];
    
    
    [requestmethod setCompletionBlock:^{
        NSString *responseString23 = [requestmethod responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        NSString *cardID= [results11 valueForKeyPath:@"cardID"];
        
        ////(@"CARD_ID");
        
        NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
        [check setObject:cardID forKey:@"CARD_ID"];
        
        
        
        
        
        [check setObject:@"" forKey:@"AUDIO_REFERENCE"];
        
        
        
        ////(@"TEMPARY:%@",results11);
        
        
        _activityPlaylist.hidden=YES;
        [_activityPlaylist stopAnimating];
        
        
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Success!"
                                                            message:[NSString stringWithFormat:@"Audio successfully removed from your card"] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alertView show];
        
        
        _skipButton.hidden=NO;
        _continueButton.hidden=YES;
        
        
        //[self retriveAudios];
        
    }];
    [requestmethod setFailedBlock:^{
        NSError *error = [requestmethod error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        _activityPlaylist.hidden=YES;
        [_activityPlaylist stopAnimating];
        
    }];
    
    [requestmethod startAsynchronous];

}

- (IBAction)newRecord:(id)sender
{
    
    checkAudioIndex=0;
    
    _recordScreen.hidden=NO;
    _listScreen.hidden=YES;
    
    _maxLabelrec.hidden=NO;
    _currentLabelrec.hidden=NO;
    _recDis.text=@"";
    _volumControl.hidden=NO;
    _playImageView.hidden=NO;
    
    
    _titleLabel.text=@"";
    _playAudio.enabled=NO;
    _nextAudio.enabled=NO;
    _prevAudio.enabled=NO;
    
    _recordScreen.hidden=NO;
    _listScreen.hidden=YES;
    
    _attachButton.hidden=YES;
    _attachButtonNew.hidden=YES;
    
    //_nextAudio.hidden=YES;
    //_prevAudio.hidden=YES;
    
    _nextAudio.hidden=YES;
    _prevAudio.hidden=YES;
    
    _nextAudio.enabled=NO;
    _prevAudio.enabled=NO;
    
    if (audioPlayer)
    {
        [audioPlayer stop];
        
        audioPlayer = nil;
    } else
    {
        // what you already have goes here
    }
    
    
    
    [self stop_audio_playing];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
     self.trackedViewName = @"Audio Screen";
    
    _activityPlaylist.hidden=YES;
    _activityRecord.hidden=YES;
    
    
    checkAudioIndex=0;
    
    //_recordView.hidden=NO;
   // avilableTableView.hidden=YES;
    
     [_back_view_bottom addSubview:_recordScreen];
     [_back_view_bottom addSubview:_listScreen];
    
   
        
    
    currentIndex=0;
    
    if (audioPlayer)
    {
        [audioPlayer stop];
        
        audioPlayer = nil;
    } else
    {
        // what you already have goes here
    }
    
    
    
    [self stop_audio_playing];
    
    
    
    // Disable Stop/Play button when application launches
    [stopButton setEnabled:NO];
    [playButton setEnabled:NO];
    
    playButton.hidden=YES;
    check_stop=0;
    
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(buttonLongPressed:)];
    longPress.minimumPressDuration =.5;
    [_longPressView addGestureRecognizer:longPress];
    

    
    _titleLabel.text=@"";
    _playAudio.enabled=NO;
    _nextAudio.enabled=NO;
    _prevAudio.enabled=NO;
    
    _recordScreen.hidden=NO;
    _listScreen.hidden=YES;
    
    _attachButton.hidden=YES;
    _attachButtonNew.hidden=YES;
    
    //_nextAudio.hidden=YES;
    //_prevAudio.hidden=YES;
    
    
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* foofile = [documentsPath stringByAppendingPathComponent:@"SNGAudio.m4a"];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:foofile];
    
    
    if (fileExists)
    {
        
       _playAudio.enabled=YES;
    }
    
    
    NSUserDefaults *checkVal=[NSUserDefaults standardUserDefaults];
    
    
    NSString *getFrom=[checkVal objectForKey:@"VIEW_REFERENCE"];
    
   
    NSString *getAudioID;
   
    _removeButtonName.hidden=YES;
    _skipButton.hidden=NO;
    _continueButton.hidden=YES;
    
    if ([getFrom isEqualToString:@"SAVED_CARDS"])
    {
         getAudioID=[checkVal objectForKey:@"AUDIO_REFERENCE"];
        
        
        @try
        {
            
            
            
            if ([getAudioID isEqualToString:@""])
            {
                //[self recordList:nil];
            }
            else
            {
                [self playList:nil];
                
                _skipButton.hidden=YES;
                _continueButton.hidden=NO;
                _removeButtonName.hidden=NO;
            }
        }
        @catch (NSException *exception)
        {
            [self playList:nil];
            
            _skipButton.hidden=YES;
            _continueButton.hidden=NO;
            _removeButtonName.hidden=NO;
        }
       
        
      
        
    }
    
    
    
    [self retriveAudios];
    
    _nextAudio.hidden=YES;
    _prevAudio.hidden=YES;
    
    // Set the audio file
    
    
    
    
    // Do any additional setup after loading the view from its nib.
}

-(void)retriveAudios
{
    card_nameArray=[[NSMutableArray alloc]init];
    date_createdArray=[[NSMutableArray alloc]init];
    audioURLArray=[[NSMutableArray alloc]init];
    audioIDArr=[[NSMutableArray alloc]init];
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/audios/retrieveUserAudios",CONFIG_BASE_URL]];
    
    
    
    __block  ASIFormDataRequest *requestmethod21 = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    
    
    ////(@"USER:%@",user);
    
    
    
    [requestmethod21 setPostValue:user forKey:@"userID"];
    
     [requestmethod21 setPostValue:[Config get_token] forKey:@"apikey"];
    [requestmethod21 setTimeOutSeconds:30];
    
    
    [requestmethod21 setCompletionBlock:^{
        NSString *responseString23 = [requestmethod21 responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        
        
        
        ////(@"TEMPARY RESPONSE:%@",results11);
        
        
        NSArray *temp= [results11 valueForKeyPath:@"audio"];
        
        
        ////(@"TEMPARY:%@",temp);
        
        for(NSDictionary *value in temp)
        {
            [audioIDArr  addObject:[value valueForKey:@"id"]];
            [audioURLArray  addObject:[value valueForKey:@"audio_url"]];
            [card_nameArray  addObject:[value valueForKey:@"created_date"]];
            //[contactidArray  addObject:[value valueForKey:@"contactid"]];
            
        }
        
        
        ////(@"Audio ID:%@",audioIDArr);
        ////(@"Audio ID:%@",card_nameArray);
        ////(@"Audio ID:%@",audioURLArray);
        
        
        audioFilesArray=[[NSMutableArray alloc]init];
        
        @try
        {
            
            _ListScreenButton.enabled=YES;
            
            for (int i=0; i<[audioIDArr count]; i++)
            {
                
                
                [audioFilesArray addObject:@"NILL"];
                
            }
            
            
            
            
            [avilableTableView reloadData];
            
            
            if ([audioFilesArray[checkAudioIndex]isEqualToString:@"NILL"])
            {
                
                
                
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *imageURL = audioURLArray[checkAudioIndex];
                
                ////(@"IMAGE URL:%@",audioURLArray[checkAudioIndex]);
                
                
                NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
                
                
                NSString *date_str1=card_nameArray [checkAudioIndex];
                
                
                NSString *date_str=[date_str1 stringByReplacingOccurrencesOfString:@" " withString:@""];
                
                
                
                NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                NSString *epub_name=[NSString stringWithFormat:@"%@.m4a",date_str];
                NSString* foofile = [documentsPath stringByAppendingPathComponent:epub_name];
                BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:foofile];
                
                
                if (fileExists)
                {
                    ////(@"EXISTS");
                    
                    [audioFilesArray replaceObjectAtIndex:checkAudioIndex withObject:date_str];
                    
                    // [self AudioPlay:self];
                    
                }
                else
                {
                    
                    
                    
                    
                    
                    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                    NSURL *url=[NSURL URLWithString:imageURL];
                    
                    
                    ////(@"Dowload Start");
                    
                    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
                    
                    
                    
                    
                    //  [collectionView reloadData];
                    
                    [request setShouldContinueWhenAppEntersBackground:YES];
                    
                    
                    [request setCompletionBlock:^{
                        // Use when fetching text data
                        
                        NSString *responseString = [request responseString];
                        
                        
                        
                        // Use when fetching binary data
                        NSData *responseData = [request responseData];
                        
                        NSString *epub_name=[NSString stringWithFormat:@"%@.m4a",date_str];
                        
                        
                        NSString *filePath = [documentsDirectory stringByAppendingPathComponent:epub_name];
                        
                        [responseData writeToFile:filePath atomically:YES];
                        
                        ////(@"Dowload Completes");
                        
                        
                        [audioFilesArray replaceObjectAtIndex:checkAudioIndex withObject:date_str];
                        
                        // [self AudioPlay:self];
                        
                        ////(@"AUDIO:%@",audioFilesArray);
                        
                        
                    }];
                    [request setFailedBlock:^
                     {
                         // NSError *error = [request error];
                         
                         
                         UIAlertView *error_Alert=[[UIAlertView alloc]initWithTitle:@"Download Error" message:[NSString stringWithFormat:@"%@",[request error]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                         
                         [error_Alert show];
                         
                         
                     }];
                    [request startAsynchronous];
                    
                    
                    
                }
                
                
            }
            else
            {
                //[self AudioPlay:self];
            }

        }
        @catch (NSException *exception) {
            _ListScreenButton.enabled=NO;
        }
        
       
        
        
        
        
        
        
    }];
    [requestmethod21 setFailedBlock:^{
        NSError *error = [requestmethod21 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [requestmethod21 startAsynchronous];
    
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)stop_audio_playing
{
    _playAudio.selected=NO;
    
    NSUserDefaults *time=[NSUserDefaults standardUserDefaults];
    [time setFloat:audioPlayer.currentTime forKey:@"CURRENT_TIME"];
    
    [audioPlayer stop];
    
}
- (IBAction)adjustVolume:(id)sender {
    
    audioPlayer.volume=_volumControl.value;
    [[NSUserDefaults standardUserDefaults] setFloat:_volumControl.value forKey:@"valumValue"];
    
    ////(@"Vloume controller value : %f", [[NSUserDefaults standardUserDefaults] floatForKey:@"valumValue"]);
    
}
- (IBAction)AudioPlay:(id)sender
{
    
    
    
    
    if (currentIndex==0)
        [_prevAudio setEnabled:NO];
    else
        [_prevAudio setEnabled:YES];
    
    if (currentIndex == card_nameArray.count-1)
        [_nextAudio setEnabled:NO];
    else
        [_nextAudio setEnabled:YES];
    
    if (_playAudio.selected) {
        
    }
    
    ////(@"Audio Index:%d",checkAudioIndex);
    
    
    _playAudio.selected=_playAudio.selected?NO : YES;
    
    if (_playAudio.selected)
    {
        
        if (prevIndex!=currentIndex || !audioPlayer)
        {
            
            
            if (checkAudioIndex==1)
            {
                
                
                
                [avilableTableView reloadData];
                
                NSIndexPath *newPath = [NSIndexPath indexPathForRow:currentIndex inSection:0];
                
                NSIndexPath *oldPath =0;
                
                if (currentIndex!=0) {
                    oldPath = [NSIndexPath indexPathForRow:currentIndex-1 inSection:0];
                }
                
                
                
                
                [avilableTableView moveRowAtIndexPath:oldPath toIndexPath:newPath];
                
                
                NSUserDefaults *add_array=[NSUserDefaults standardUserDefaults];
                
                NSString *cur=[NSString stringWithFormat:@"%d",currentIndex];
                
                [add_array setObject:cur forKey:@"AUDIO_INDEX"];
                
                
                NSString *audio_value=[audioFilesArray objectAtIndex:currentIndex];
                
                //_title_label.text=[title_array objectAtIndex:currentIndex];
                
                
                if ([card_nameArray count]==0)
                {
                    _titleLabel.text=@"";
                }
                else
                {
                    _titleLabel.text=[NSString stringWithFormat:@"Record %d",currentIndex+1];
                }
                
                
                NSTimer * myTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                                     target:self
                                                                   selector:@selector(updateTimeLeft)
                                                                   userInfo:nil
                                                                    repeats:YES];
                
                
                //NSString* audioString = [[NSBundle mainBundle] pathForResource:audio_value ofType:@"wav"];
                
                
                
                
                
                
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                
                NSString *audioString1 = [documentsDirectory stringByAppendingPathComponent:audio_value];
                
                NSString *audioString=[NSString stringWithFormat:@"%@.m4a",audioString1];
                
                
                NSUserDefaults *Confirmation=[NSUserDefaults standardUserDefaults];
                
                [Confirmation setObject:audioString forKey:@"AUDIO_STRING"];
                
                
                NSURL *url = [NSURL fileURLWithPath:audioString];
                
                
                
                NSError *error;
                audioPlayer = [[AVAudioPlayer alloc]
                               initWithContentsOfURL:url
                               error:&error];
                //audioPlayer.numberOfLoops=-1;
                [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
                [[AVAudioSession sharedInstance] setActive: YES error: nil];
                [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
                
                
                ////(@"TOTAL:%f",audioPlayer.duration);
                
                
                _maxLabelrec.hidden=YES;
                _currentLabelrec.hidden=YES;
                _recDis.text=@"";
                _volumControl.hidden=YES;
                _playImageView.hidden=YES;
                
                int result = (int)floorf(audioPlayer.duration);
                
                
                if (result>=12) {
                    _playTimeLabel.text=[NSString stringWithFormat:@"00:00/00:12"];
                    
                    //_volumControl.maximumValue = 12.00;
                }
                else
                {
                    _playTimeLabel.text=[NSString stringWithFormat:@"00:00/00:%d",result];
                    
                    //_volumControl.maximumValue = audioPlayer.duration;
                }
                
                
                
                if (error)
                {
                    
                    /*
                    ////(@"Error in audioPlayer: %@",
                          [error localizedDescription]);
                    UIAlertView *error_Alert=[[UIAlertView alloc]initWithTitle:@"Audio Codec Error" message:[NSString stringWithFormat:@"Audio codec  "] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    
                    [error_Alert show];
                    
                    if (bool_value==0)
                    {
                        
                        if (audioPlayer)
                        {
                            [audioPlayer stop];
                            
                            audioPlayer = nil;
                        } else
                        {
                            // what you already have goes here
                        }
                        
                        
                        
                        [self stop_audio_playing];
                        
                        
                        if ([audioFilesArray[currentIndex]isEqualToString:@"NILL"])
                        {
                            
                            
                            
                            
                            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                            NSString *imageURL = audioURLArray[currentIndex];
                            
                            ////(@"IMAGE URL:%@",audioURLArray[currentIndex]);
                            
                            
                            NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
                            
                            
                            NSString *date_str1=card_nameArray [currentIndex];
                            
                            
                            NSString *date_str=[date_str1 stringByReplacingOccurrencesOfString:@" " withString:@""];
                            
                            
                            
                            NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                            NSString *epub_name=[NSString stringWithFormat:@"%@.m4a",date_str];
                            NSString* foofile = [documentsPath stringByAppendingPathComponent:epub_name];
                            BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:foofile];
                            
                            
                            if (fileExists)
                            {
                                ////(@"EXISTS");
                                
                                [audioFilesArray replaceObjectAtIndex:currentIndex withObject:date_str];
                                
                                [self AudioPlay:self];
                                
                                 bool_value=1;
                                
                            }
                            else
                            {
                                
                                
                                
                                
                                
                                NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                                NSURL *url=[NSURL URLWithString:imageURL];
                                
                                
                                ////(@"Dowload Start");
                                
                                ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
                                
                                
                                
                                
                                //  [collectionView reloadData];
                                
                                [request setShouldContinueWhenAppEntersBackground:YES];
                                
                                
                                [request setCompletionBlock:^{
                                    // Use when fetching text data
                                    
                                    NSString *responseString = [request responseString];
                                    
                                    
                                    
                                    // Use when fetching binary data
                                    NSData *responseData = [request responseData];
                                    
                                    NSString *epub_name=[NSString stringWithFormat:@"%@.m4a",date_str];
                                    
                                    
                                    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:epub_name];
                                    
                                    [responseData writeToFile:filePath atomically:YES];
                                    
                                    ////(@"Dowload Completes");
                                    
                                    
                                    [audioFilesArray replaceObjectAtIndex:currentIndex withObject:date_str];
                                    
                                    [self AudioPlay:self];
                                    
                                    ////(@"AUDIO:%@",audioFilesArray);
                                     bool_value=1;
                                    
                                }];
                                [request setFailedBlock:^
                                 {
                                     // NSError *error = [request error];
                                     
                                     
                                     UIAlertView *error_Alert=[[UIAlertView alloc]initWithTitle:@"Download Error" message:[NSString stringWithFormat:@"%@",[request error]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                     
                                     [error_Alert show];
                                     
                                     
                                 }];
                                [request startAsynchronous];
                                
                                
                                
                            }
                            
                            
                        }
                        else
                        {
                            [self AudioPlay:self];
                            
                            
                            bool_value=1;
                            
                            
                            
                            
                        }
                        
                    }
                    else
                    {
                        UIAlertView *error_Alert=[[UIAlertView alloc]initWithTitle:@"Audio Codec Error" message:[NSString stringWithFormat:@"Audio codec  "] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        
                        [error_Alert show];

                    }
                    */
                    
                    
                    
                   
                    
                    
                    
                } else
                
                {
                    audioPlayer.delegate = self;
                    
                    bool_value=0;
                    
                    [audioPlayer prepareToPlay];
                }

            }
            else
            {
                
                 _titleLabel.text=@"New Record";
                                       
                
                NSTimer * myTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                                     target:self
                                                                   selector:@selector(updateTimeLeft)
                                                                   userInfo:nil
                                                                    repeats:YES];
                
                
                //NSString* audioString = [[NSBundle mainBundle] pathForResource:audio_value ofType:@"wav"];
                
                
              
                
                
                
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                
                NSString *audioString = [documentsDirectory stringByAppendingPathComponent:@"SNGAudio.m4a"];
                
                
                NSUserDefaults *Confirmation=[NSUserDefaults standardUserDefaults];
                
                [Confirmation setObject:audioString forKey:@"AUDIO_STRING"];
                
                
                NSURL *url = [NSURL fileURLWithPath:audioString];
                
                
                
                NSError *error;
                audioPlayer = [[AVAudioPlayer alloc]
                               initWithContentsOfURL:url
                               error:&error];
                //audioPlayer.numberOfLoops=-1;
                [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
                [[AVAudioSession sharedInstance] setActive: YES error: nil];
                [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
                
                
                ////(@"TOTAL:%f",audioPlayer.duration);
                
                
                _maxLabelrec.hidden=YES;
                _currentLabelrec.hidden=YES;
                _recDis.text=@"";
                _volumControl.hidden=YES;
                _playImageView.hidden=YES;
                
                _attachButton.hidden=NO;
                _attachButtonNew.hidden=NO;
                
                
                _nextAudio.hidden=YES;
                _prevAudio.hidden=YES;
                
                _nextAudio.enabled=NO;
                _prevAudio.enabled=NO;
                
                
                int result = (int)floorf(audioPlayer.duration);
                
                if (result>=12) {
                    _playTimeLabel.text=[NSString stringWithFormat:@"00:00/00:12"];
                    
                    //_volumControl.maximumValue = 12.00;
                }
                else
                {
                    _playTimeLabel.text=[NSString stringWithFormat:@"00:00/00:%d",result];
                    
                    //_volumControl.maximumValue = audioPlayer.duration;
                }
                
                
                
                if (error)
                {
                    ////(@"Error in audioPlayer: %@",
                       //   [error localizedDescription]);
                } else {
                    audioPlayer.delegate = self;
                    [audioPlayer prepareToPlay];
                }

            }
            
            
           
        }
        
        if ([[NSUserDefaults standardUserDefaults] floatForKey:@"valumValue"]) {
            audioPlayer.volume=[[NSUserDefaults standardUserDefaults] floatForKey:@"valumValue"];
        }else
            audioPlayer.volume=1.5;
        
        NSUserDefaults *Confirmation=[NSUserDefaults standardUserDefaults];
        [Confirmation setObject:@"NO" forKey:@"AUDIO_STOP"];
        [audioPlayer play];
    }
    else
    {
        ////(@"It is deselected");
        
        
        if (checkAudioIndex==0)
        {
            _nextAudio.enabled=NO;
            _prevAudio.enabled=NO;
        }
        else
        {
            //_nextAudio.enabled=YES;
            //_prevAudio.enabled=YES;
        }
        
        
        NSUserDefaults *Confirmation=[NSUserDefaults standardUserDefaults];
        
        [Confirmation setObject:@"YES" forKey:@"AUDIO_STOP"];
        
        
        [audioPlayer stop];
    }
    
    
}

- (void)updateTimeLeft {
    NSTimeInterval timeLeft = player.duration - player.currentTime;
    
    
    int result = (int)floorf(audioPlayer.duration);
     int result1 = (int)floorf(audioPlayer.currentTime);
    
    if (result>=12) {
        result=12;
        
        
        
    }
    
    
    NSInteger milliseconds1 = (result1 * 1000);
    
    
    if (result1>result)
    {
        
    }
    else
    {
        
        
        _playTimeLabel.text=[NSString stringWithFormat:@"00:%d/00:%d",result1,result];
           
         //[_volumControl setValue:audioPlayer.currentTime animated:YES];
        
    }
    
    
    // update your UI with timeLeft
    
   // ////(@"TIME LEFT:%@",[NSString stringWithFormat:@"%f seconds left", timeLeft]);
    
   // self.timeLeftLabel.text = [NSString stringWithFormat:@"%f seconds left", timeLeft];
}




-(void)fireMethod
{
    
    time_value=time_value+1;
    
    
    ////(@"fireMethod:%d",time_value);
    
    int display_timeValue=12-time_value;
    
    if (check_stop==0) {
        _labelButton.text=[NSString stringWithFormat:@"Don't Release finger for %d seconds",display_timeValue];
        
        _currentLabelrec.text=[NSString stringWithFormat:@"00:%d",time_value];
        
        _recDis.text=@"Recording...";
        
        _recordImageView.image=[UIImage imageNamed:@"record_act.png"];
        
        if (time_value>=12)
        {
             _currentLabelrec.text=[NSString stringWithFormat:@"00:%d",12];
            
            _recDis.text=@"";
        }
        
        _maxLabelrec.text=[NSString stringWithFormat:@"00:%d",12];
        
        [_volumControl setValue:time_value animated:YES];
    }
    else
    {
        _labelButton.text=[NSString stringWithFormat:@"0"];
        
        _recDis.text=@"";
        
         _recordImageView.image=[UIImage imageNamed:@"record.png"];
    }
    
    if (time_value==13)
    {
        
        [sliderUpdateTimer invalidate];
        
        
        time_value=0;
        check_stop=1;
        
        [recorder stop];
        playButton.hidden=NO;
        
        _labelButton.text=[NSString stringWithFormat:@""];
        
        AVAudioSession *audioSession = [AVAudioSession sharedInstance];
        [audioSession setActive:NO error:nil];
        
       
        
        /*
        
        Dataware *dd=[[Dataware alloc]initDataware];
        
        NSString *sstr=@"";
        
        sstr=@"INSERT INTO tb_sng(name,date) VALUES(?,?)";
        
        //NSString *book_name=@"test";
        
        NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
        
        
        NSString *getName=[check objectForKey:@"NAME_RECORD"];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"MM-dd-yyyy hh:mm:ss:a"];
        NSString *strDate = [dateFormatter stringFromDate:[NSDate date]];
        
        
        sqlite3_stmt *stmt=[dd OpenSQL:[sstr UTF8String]];
        if (stmt!=nil)
        {
            sqlite3_bind_text(stmt,1,[getName UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt,2, [strDate UTF8String],-1 ,SQLITE_TRANSIENT);
            sqlite3_step(stmt);
        }
        sqlite3_finalize(stmt);
        */
        
        
        playButton.hidden=NO;
        
        _titleLabel.text=@"";
        _playAudio.enabled=YES;
        _nextAudio.enabled=YES;
        _prevAudio.enabled=YES;
        
        _recordScreen.hidden=NO;
        _listScreen.hidden=YES;
        
        _attachButton.hidden=NO;
        _attachButtonNew.hidden=NO;
        
        _volumControl.hidden=YES;
        _playImageView.hidden=YES;
        _currentLabelrec.hidden=YES;
        _maxLabelrec.hidden=YES;
        
        _nextAudio.hidden=YES;
        _prevAudio.hidden=YES;
        
        _nextAudio.enabled=NO;
        _prevAudio.enabled=NO;

        

         [_volumControl setValue:0 animated:YES];
        _currentLabelrec.text=@"00:00";
        
        //[self viewDidLoad];
        
        
    }
}


- (IBAction)tapDetected:(UIGestureRecognizer *)sender {
	// Code to respond to gesture here
    
    ////(@"START");
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)recordPauseTapped:(id)sender
{
    // Stop the audio player before recording
    if (player.playing)
    {
        [player stop];
    }
    
    if (!recorder.recording)
    {
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setActive:YES error:nil];
        
        // Start recording
        [recorder record];
        [recordPauseButton setTitle:@"Pause" forState:UIControlStateNormal];
        
    } else
    {
        
        // Pause recording
        [recorder pause];
        [recordPauseButton setTitle:@"Record" forState:UIControlStateNormal];
    }
    
    [stopButton setEnabled:YES];
    [playButton setEnabled:NO];
}

- (IBAction)stopTapped:(id)sender {
    [recorder stop];
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setActive:NO error:nil];
}
-(IBAction)keyboardButton
{
  //  [messageText resignFirstResponder];
}
- (IBAction)playTapped:(id)sender
{
    if (!recorder.recording)
    {
        player = [[AVAudioPlayer alloc] initWithContentsOfURL:recorder.url error:nil];
        [player setDelegate:self];
        [player play];
        // [playButton setTitle:@"Stop" forState:UIControlStateNormal];
        
    }
    
    
}

#pragma mark - AVAudioRecorderDelegate

- (void) audioRecorderDidFinishRecording:(AVAudioRecorder *)avrecorder successfully:(BOOL)flag
{
    [recordPauseButton setTitle:@"Record" forState:UIControlStateNormal];
    [stopButton setEnabled:NO];
    [playButton setEnabled:YES];
}

#pragma mark - AVAudioPlayerDelegate

-(void)audioPlayerDidFinishPlaying:
(AVAudioPlayer *)player successfully:(BOOL)flag
{
    _playAudio.selected=NO;
    ////(@"audioPlayerDidFinishPlaying");
    
    //_maxLabelrec.hidden=NO;
   // _currentLabelrec.hidden=NO;
   // _recDis.text=@"";
    
    
   // [self AudioPlay:nil];
}

- (IBAction)nextAudioPlay:(id)sender {
    
    
     bool_value=0;
    
    prevIndex=currentIndex;
    currentIndex++;
    
    ////(@"soundList.count : %d == %d currentIndex",card_nameArray.count,currentIndex);
    
    
    ////(@"CHECK:%d",checkAudioIndex);
    
    
    if (currentIndex==card_nameArray.count)
    {
        //currentIndex=soundList.count-1;
        //PREM ADD
        currentIndex=0;
        _playAudio.selected=NO;
       // _bg_image.image=[UIImage imageNamed:[audio_array objectAtIndex:currentIndex]];
        
       // _title_label.text=[audio_array objectAtIndex:currentIndex];
        
        
        ////(@"ENTER HEL");
        
        
        if ([audioFilesArray[currentIndex]isEqualToString:@"NILL"])
        {
            
            
            
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *imageURL = audioURLArray[currentIndex];
            
            ////(@"IMAGE URL:%@",audioURLArray[currentIndex]);
            
            
            NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
            
            
            NSString *date_str1=card_nameArray [currentIndex];
            
            
            NSString *date_str=[date_str1 stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            
            
            NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            NSString *epub_name=[NSString stringWithFormat:@"%@.m4a",date_str];
            NSString* foofile = [documentsPath stringByAppendingPathComponent:epub_name];
            BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:foofile];
            
            
            if (fileExists)
            {
                ////(@"EXISTS");
                
                [audioFilesArray replaceObjectAtIndex:currentIndex withObject:date_str];
                
                [self AudioPlay:self];
                
            }
            else
            {
                
                
                
                
                
                NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                NSURL *url=[NSURL URLWithString:imageURL];
                
                
                ////(@"Dowload Start");
                
                ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
                
                
                
                
                //  [collectionView reloadData];
                
                [request setShouldContinueWhenAppEntersBackground:YES];
                
                
                [request setCompletionBlock:^{
                    // Use when fetching text data
                    
                    NSString *responseString = [request responseString];
                    
                    
                    
                    // Use when fetching binary data
                    NSData *responseData = [request responseData];
                    
                    NSString *epub_name=[NSString stringWithFormat:@"%@.m4a",date_str];
                    
                    
                    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:epub_name];
                    
                    [responseData writeToFile:filePath atomically:YES];
                    
                    ////(@"Dowload Completes");
                    
                    
                    [audioFilesArray replaceObjectAtIndex:currentIndex withObject:date_str];
                    
                     [self AudioPlay:self];
                    
                    ////(@"AUDIO:%@",audioFilesArray);
                    
                    
                }];
                [request setFailedBlock:^
                 {
                     // NSError *error = [request error];
                     
                     
                     UIAlertView *error_Alert=[[UIAlertView alloc]initWithTitle:@"Download Error" message:[NSString stringWithFormat:@"%@",[request error]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                     
                     [error_Alert show];
                     
                     
                 }];
                [request startAsynchronous];
                
                
                
            }
            
            
        }
        else
        {
            [self AudioPlay:self];
        }
        
        
        //[self AudioPlay:self];
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:currentIndex inSection:0];
        [avilableTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];

        
        
    }else
    {
        _playAudio.selected=NO;
       // _bg_image.image=[UIImage imageNamed:[audio_array objectAtIndex:currentIndex]];
        
       // _title_label.text=[audio_array objectAtIndex:currentIndex];
        //[self AudioPlay:self];
        
         ////(@"ENTER ELSE");
        
        if ([audioFilesArray[currentIndex]isEqualToString:@"NILL"])
        {
            
            
            
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *imageURL = audioURLArray[currentIndex];
            
            ////(@"IMAGE URL:%@",audioURLArray[currentIndex]);
            
            
            NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
            
            
            NSString *date_str1=card_nameArray [currentIndex];
            
            
            NSString *date_str=[date_str1 stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            
            
            NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            NSString *epub_name=[NSString stringWithFormat:@"%@.m4a",date_str];
            NSString* foofile = [documentsPath stringByAppendingPathComponent:epub_name];
            BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:foofile];
            
            
            if (fileExists)
            {
                ////(@"EXISTS");
                
                [audioFilesArray replaceObjectAtIndex:currentIndex withObject:date_str];
                
                [self AudioPlay:self];
                
            }
            else
            {
                
                
                
                
                
                NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                NSURL *url=[NSURL URLWithString:imageURL];
                
                
                ////(@"Dowload Start");
                
                
                _nextAudio.enabled=NO;
                _prevAudio.enabled=NO;
                
                ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
                
                
                
                
                //  [collectionView reloadData];
                
                [request setShouldContinueWhenAppEntersBackground:YES];
                
                
                [request setCompletionBlock:^{
                    // Use when fetching text data
                    
                    NSString *responseString = [request responseString];
                    
                    
                    
                    // Use when fetching binary data
                    NSData *responseData = [request responseData];
                    
                    NSString *epub_name=[NSString stringWithFormat:@"%@.m4a",date_str];
                    
                    
                    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:epub_name];
                    
                    [responseData writeToFile:filePath atomically:YES];
                    
                    ////(@"Dowload Completes");
                    
                    _nextAudio.enabled=YES;
                    _prevAudio.enabled=YES;
                    
                    
                    [audioFilesArray replaceObjectAtIndex:currentIndex withObject:date_str];
                    
                    [self AudioPlay:self];
                    
                    ////(@"AUDIO:%@",audioFilesArray);
                    
                    
                }];
                [request setFailedBlock:^
                 {
                     // NSError *error = [request error];
                     
                     _nextAudio.enabled=YES;
                     _prevAudio.enabled=YES;
                     
                     UIAlertView *error_Alert=[[UIAlertView alloc]initWithTitle:@"Download Error" message:[NSString stringWithFormat:@"%@",[request error]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                     
                     [error_Alert show];
                     
                     
                 }];
                [request startAsynchronous];
                
                
                
            }
            
            
        }
        else
        {
            [self AudioPlay:self];
        }
        
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:currentIndex inSection:0];
        [avilableTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
    
}

- (IBAction)prevAudioPlay:(id)sender {
    
    
     bool_value=0;
    
    prevIndex=currentIndex;
    currentIndex--;
    
    ////(@"soundList.count : %d == %d currentIndex",card_nameArray.count,currentIndex);
    
    
    if (currentIndex<0)
    {
        currentIndex=0;
        
    }
    else
    {
        
        _playAudio.selected=NO;
               
       // [self AudioPlay:self];
        
        
        if ([audioFilesArray[currentIndex]isEqualToString:@"NILL"])
        {
            
            
            
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *imageURL = audioURLArray[currentIndex];
            
            ////(@"IMAGE URL:%@",audioURLArray[currentIndex]);
            
            
            NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
            
            
            NSString *date_str1=card_nameArray [currentIndex];
            
            
            NSString *date_str=[date_str1 stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            
            
            NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            NSString *epub_name=[NSString stringWithFormat:@"%@.m4a",date_str];
            NSString* foofile = [documentsPath stringByAppendingPathComponent:epub_name];
            BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:foofile];
            
            
            if (fileExists)
            {
                ////(@"EXISTS");
                
                [audioFilesArray replaceObjectAtIndex:currentIndex withObject:date_str];
                
                [self AudioPlay:self];
                
            }
            else
            {
                
                
                
                
                
                NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                NSURL *url=[NSURL URLWithString:imageURL];
                
                
                ////(@"Dowload Start");
                
                ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
                
                
                
                
                //  [collectionView reloadData];
                
                [request setShouldContinueWhenAppEntersBackground:YES];
                
                
                [request setCompletionBlock:^{
                    // Use when fetching text data
                    
                    NSString *responseString = [request responseString];
                    
                    
                    
                    // Use when fetching binary data
                    NSData *responseData = [request responseData];
                    
                    NSString *epub_name=[NSString stringWithFormat:@"%@.m4a",date_str];
                    
                    
                    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:epub_name];
                    
                    [responseData writeToFile:filePath atomically:YES];
                    
                    ////(@"Dowload Completes");
                    
                    @try
                    {
                        [audioFilesArray replaceObjectAtIndex:currentIndex withObject:date_str];
                        
                        [self AudioPlay:self];
                    }
                    @catch (NSException *exception) {
                        
                    }
                    
                   
                    
                    ////(@"AUDIO:%@",audioFilesArray);
                    
                    
                }];
                [request setFailedBlock:^
                 {
                     // NSError *error = [request error];
                     
                     
                     UIAlertView *error_Alert=[[UIAlertView alloc]initWithTitle:@"Download Error" message:[NSString stringWithFormat:@"%@",[request error]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                     
                     [error_Alert show];
                     
                     
                 }];
                [request startAsynchronous];
                
                
                
            }
            
            
        }
        else
        {
            [self AudioPlay:self];
        }
        
        
    }
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:currentIndex inSection:0];
    [avilableTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}







-(void)audioPlayerDecodeErrorDidOccur:
(AVAudioPlayer *)player error:(NSError *)error
{
    ////(@"audioPlayerDecodeErrorDidOccur");
}
-(void)audioPlayerBeginInterruption:(AVAudioPlayer *)player
{
    ////(@"audioPlayerBeginInterruption");
}
-(void)audioPlayerEndInterruption:(AVAudioPlayer *)player
{
    ////(@"audioPlayerEndInterruption");
}





-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
             /*
            
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            
            NSString *getVideoPathNew = [documentsDirectory stringByAppendingPathComponent:@"MyAudioMemo.m4a"];
            
            
            
            
            
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://api.socialnetgateapp.com/v1/files/postfile"]];
            
            
            __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
            
            
            [request_post setFile:getVideoPathNew forKey:@"uploadedfile"];
            
            
            
            [request_post setShowAccurateProgress: YES];
            
            // UPLOAD APP ENTER BACKGROUND //
            [request_post setShouldContinueWhenAppEntersBackground:YES];
            
            
            //[request_post setUploadProgressDelegate:progressBar];
            
            
            
            
            
            [request_post setCompletionBlock:^{
                // Use when fetching text data
                NSString *responseString = [request_post responseString];
                
                
                
                
                ////(@"RES:%@",responseString);
                
                NSMutableData *results11 = [responseString JSONValue];
                
                
                //////(@"RESULTS LOGIN:%@",results11);
                
                
                NSString *temp= [results11 valueForKeyPath:@"photo_url"];
                
                
                
                NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/audios/createAudioSticker",CONFIG_BASE_URL]];
                
                
                
                __block  ASIFormDataRequest *requestmethod = [ASIFormDataRequest requestWithURL:url];
                
                
                
                NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
                NSString *user=[check objectForKey:@"USERID"];
                NSString *cardID=[check objectForKey:@"CARD_ID"];
                NSString *imageID=[check objectForKey:@"IMAGE_ID"];
                NSString *imageSource=[check objectForKey:@"EDIT_IMAGE"];
                ////(@"USER:%@",user);
                
                
                
                [requestmethod setPostValue:user forKey:@"userID"];
                [requestmethod setPostValue:temp forKey:@"audioURL"];
                [requestmethod setPostValue:@"iphone" forKey:@"source"];
                
                
                [requestmethod setTimeOutSeconds:30];
                
                
                [requestmethod setCompletionBlock:^{
                    NSString *responseString23 = [requestmethod responseString];
                    NSMutableData *results11 = [responseString23 JSONValue];
                    
                    
                    
                    ////(@"TEMPARY:%@",results11);
                    
                    NSString *temp1= [results11 valueForKeyPath:@"audioID"];
                    
                    
                   // audioURL=temp1;
                    
                }];
                [requestmethod setFailedBlock:^{
                    NSError *error = [requestmethod error];
                    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                        message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                              cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    
                    
                    
                    
                }];
                
                [requestmethod startAsynchronous];
                
                
                
                
                
                ////(@"Audio URL:%@",temp);;
                
                
            }];
            [request_post setFailedBlock:^{
                NSError *error = [request_post error];
                
                
                //text.text=[NSString stringWithFormat:@"%@",error];
                
            }];
            [request_post startAsynchronous];
              
              */
        }
        




-(IBAction)saveCard
{
    
    if (IS_IPHONE_5)
    {
        savedCards *viewController=[[savedCards alloc]initWithNibName:@"savedCards" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
        
    }
    else
    {
        savedCards *viewController=[[savedCards alloc]initWithNibName:@"savedCards~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
        
    }

}

-(void)buttonLongPressed:(UILongPressGestureRecognizer *)sender
{
    
   NSString *name_record=@"";
    
    if(sender.state == UIGestureRecognizerStateBegan)
    {
        
        /*
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"buffer.png"]];
        imageView.frame = CGRectMake(30,20,252,10);//starts big
        [_recordScreen addSubview:imageView];
        //[UIView beginAnimations:@"Progress Bar Animation" Context:nil];
        
        [UIView beginAnimations:@"Progress Bar Animation" context:nil];
        
        [UIView setAnimationDuration:12]; //whatever time you want
        imageView.frame = CGRectMake(30,20,0,10);//ends small
        [UIView commitAnimations];
        
        */
        
        _attachButton.hidden=YES;
        _attachButtonNew.hidden=YES;
        
        _volumControl.hidden=NO;
        _playImageView.hidden=NO;
        _currentLabelrec.hidden=NO;
        _maxLabelrec.hidden=NO;
        
        NSDate *date = [NSDate date];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
        [dateFormat setDateFormat:@"dd:MM:yyyy_HH:mm:ss"];
        NSString *dateString = [dateFormat stringFromDate:date];
        
        ////(@"DATE STRING:%@",dateString);
        
        NSString *newString = [dateString stringByReplacingOccurrencesOfString:@" " withString:@""];
        // NSString *newString1 = [newString stringByReplacingOccurrencesOfString:@":" withString:@""];
        NSString *newString2 = [newString stringByReplacingOccurrencesOfString:@"GMT" withString:@""];
        NSString *newString3 = [newString2 stringByReplacingOccurrencesOfString:@"+" withString:@""];
        
        NSString *imageName=[NSString stringWithFormat:@"%@",newString3];
        
        
        //name_record=[NSString stringWithFormat:@"SNGAudio_%@.m4a",imageName];
        
        name_record=[NSString stringWithFormat:@"SNGAudio.m4a"];
        
        NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
        [check setObject:name_record forKey:@"NAME_RECORD"];
        
        
        NSArray *pathComponents = [NSArray arrayWithObjects:
                                   [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],name_record,
                                   nil];
        NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
        
        // Setup audio session
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
        
        // Define the recorder setting
        NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
        
        [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
        [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
        [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
        
        // Initiate and prepare the recorder
        recorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSetting error:nil];
        recorder.delegate = self;
        recorder.meteringEnabled = YES;
        [recorder prepareToRecord];
        
        _maxLabelrec.hidden=NO;
        _currentLabelrec.hidden=NO;
        _recDis.text=@"Recording...";
        _volumControl.hidden=NO;
        _playImageView.hidden=NO;
        
         //_volumControl.maximumValue = 12.00;
        
        
        ////(@"START");
        playButton.hidden=YES;
        
        AVAudioSession *session1 = [AVAudioSession sharedInstance];
        [session setActive:YES error:nil];
        
        // Start recording
        [recorder record];
        [recordPauseButton setTitle:@"Recording...." forState:UIControlStateNormal];
        
        
        _recordImageView.image=[UIImage imageNamed:@"record_act.png"];
        check_stop=0;
        
        
        time_value=0;
        sliderUpdateTimer= [NSTimer    scheduledTimerWithTimeInterval:1.0    target:self    selector:@selector(fireMethod)    userInfo:nil repeats:YES];
        
    }
    
    if (sender.state==UIGestureRecognizerStateEnded)
    {
        ////(@"ENDED");
         _recordImageView.image=[UIImage imageNamed:@"record.png"];
        _labelButton.text=nil;
        
        [recorder stop];
        check_stop=1;
        [sliderUpdateTimer invalidate];
        
        playButton.hidden=YES;
        time_value=0;
        _labelButton.text=[NSString stringWithFormat:@""];
        
        AVAudioSession *audioSession = [AVAudioSession sharedInstance];
        [audioSession setActive:NO error:nil];
        
        /*
        Dataware *dd=[[Dataware alloc]initDataware];
        
        NSString *sstr=@"";
        
        sstr=@"INSERT INTO tb_sng(name,date) VALUES(?,?)";
        
        //NSString *book_name=@"test";
        
        NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
       
        
        NSString *getName=[check objectForKey:@"NAME_RECORD"];
     
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"MM-dd-yyyy hh:mm:ss:a"];
        NSString *strDate = [dateFormatter stringFromDate:[NSDate date]];
        
        
        sqlite3_stmt *stmt=[dd OpenSQL:[sstr UTF8String]];
        if (stmt!=nil)
        {
            sqlite3_bind_text(stmt,1,[getName UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt,2, [strDate UTF8String],-1 ,SQLITE_TRANSIENT);
                       sqlite3_step(stmt);
        }
        sqlite3_finalize(stmt);
        */
        
        
       

        
        
        playButton.hidden=NO;
        
        
        _titleLabel.text=@"";
        _playAudio.enabled=YES;
        _nextAudio.enabled=YES;
        _prevAudio.enabled=YES;
        
        _recordScreen.hidden=NO;
        _listScreen.hidden=YES;
        
        _attachButton.hidden=NO;
        _attachButtonNew.hidden=NO;
        
        _volumControl.hidden=YES;
        _playImageView.hidden=YES;
        _currentLabelrec.hidden=YES;
        _maxLabelrec.hidden=YES;
        
        
        _nextAudio.hidden=YES;
        _prevAudio.hidden=YES;
        
        _nextAudio.enabled=NO;
        _prevAudio.enabled=NO;

        
        
        [_volumControl setValue:0 animated:YES];
        _currentLabelrec.text=@"00:00";
        
        //[self viewDidLoad];
        
        
        
        /*
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Done"
                                                        message: @"Record Interupted"
                                                       delegate: nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        */
        
        
    }
    
    if (sender.state==UIGestureRecognizerStateRecognized)
    {
        ////(@"REGOGNIZED");
        
        _recDis.text=@"";
    }
    
    
}


-(IBAction)back:(id)sender
{
    
    NSUserDefaults *checkVal=[NSUserDefaults standardUserDefaults];
    
    
    NSString *getFrom=[checkVal objectForKey:@"VIEW_REFERENCE"];
    
    
    if ([getFrom isEqualToString:@"SAVED_CARDS"])
    {
        
        /*
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *imageURL = [checkVal objectForKey:@"IMAGE_ID_REFERENCE"];
        
        NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
        UIImage *image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imageURL]]];
        
        NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"cropImage.png"]];
         
        if(image != NULL)
        {
            //Store the image in Document
            
            
            NSData *imageData = UIImagePNGRepresentation(image);
            [imageData writeToFile: imagePath atomically:YES];
        }
        */
        if (IS_IPHONE_5) {
            messageViewController *viewController=[[messageViewController alloc]initWithNibName:@"messageViewController" bundle:nil];
            [self presentViewController:viewController animated:NO completion:^{
                //////(@"SUCCESS");
            }
             ];
        }
        else
        {
            
            messageViewController *viewController=[[messageViewController alloc]initWithNibName:@"messageViewController~iPhone" bundle:nil];
            [self presentViewController:viewController animated:NO completion:^{
                //////(@"SUCCESS");
            }
             ];
        }
        
    }
    else
    {
        if (IS_IPHONE_5) {
            messageViewController *viewController=[[messageViewController alloc]initWithNibName:@"messageViewController" bundle:nil];
            [self presentViewController:viewController animated:NO completion:^{
                //////(@"SUCCESS");
            }
             ];
        }
        else
        {
            
            messageViewController *viewController=[[messageViewController alloc]initWithNibName:@"messageViewController~iPhone" bundle:nil];
            [self presentViewController:viewController animated:NO completion:^{
                //////(@"SUCCESS");
            }
             ];
        }

    }
}

-(IBAction)continue_btn:(id)sender
{
    if (IS_IPHONE_5)
    {
        
        receiptentViewController *viewController=[[receiptentViewController alloc]initWithNibName:@"receiptentViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        
        receiptentViewController *viewController=[[receiptentViewController alloc]initWithNibName:@"receiptentViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }

}
-(IBAction)skip_btn:(id)sender
{
    if (IS_IPHONE_5)
    {
        
        receiptentViewController *viewController=[[receiptentViewController alloc]initWithNibName:@"receiptentViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        
        receiptentViewController *viewController=[[receiptentViewController alloc]initWithNibName:@"receiptentViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }

}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    
    return [audioIDArr count];
}
-(IBAction)settingsButton
{
    ViewController *viewController=[[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        //////(@"SUCCESS");
    }
     ];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    audio * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        
        NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"audio" owner:nil options:nil];
        
        for (UIView *view in views)
        {
            if([view isKindOfClass:[UITableViewCell class]])
            {
                cell = (audio*)view;
                //cell.img=@"date.png";
                
            }
        }
    }
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    
    
//    if ([statusArray[indexPath.row] isEqualToString:@"Delivered"])
//    {
//         cell.details.textColor=[UIColor greenColor];
//    }
//    
//    
//    if ([statusArray[indexPath.row] isEqualToString:@"Waiting"])
//    {
//        cell.details.textColor=[UIColor blackColor];
//    }
//    
//    if ([statusArray[indexPath.row] isEqualToString:@"Mailed"])
//    {
//        cell.details.textColor=[UIColor blueColor];
//    }
    
    
    NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc] init];
    
    [dateFormat1 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    
    NSString *date_updated=card_nameArray[indexPath.row];
    //////(@"Original date:%@",date_updated);
    NSDate *todaydate = [dateFormat1 dateFromString:date_updated];// date with yyyy-MM-dd format
    
    [dateFormat1 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]] ;
    
   // ////(@"DATE:%@",todaydate);
    
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"LLL d, yyyy HH:mm a"];
    NSString *dateString = [dateFormat stringFromDate:todaydate];
    
    //////(@"DATE STRING:%@",dateString);
    
    
    
   
    
    
    cell.name.text=[NSString stringWithFormat:@" Record %d      %@",indexPath.row+1,dateString];
    
    if (currentIndex==indexPath.row)
    {
       // cell.name.textColor=[UIColor blackColor];
        
        cell.img.image=[UIImage imageNamed:@"listact.png"];
        
    }
    else
    {
        cell.img.image=[UIImage imageNamed:@"list.png"];
    }

    
    // [cell.img setImageWithURL:[NSURL URLWithString:searchImageArray[indexPath.row]]
    // placeholderImage:[UIImage imageNamed:@"temp.jpg"]];
    
    
    // cell.img.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@",searchImageArray[indexPath.row]]];
    // cell.title.text=searchStringArray[indexPath.row];
    // cell.category.text=searchmetadataArray[indexPath.row];
    
    //cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    return cell;
    
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (audioPlayer)
    {
        [audioPlayer stop];
        
        audioPlayer = nil;
    } else
    {
        // what you already have goes here
    }
    
    
    
    [self stop_audio_playing];
    
    currentIndex=indexPath.row;
    
    [avilableTableView reloadData];
    
    
    
    
    if ([audioFilesArray[indexPath.row]isEqualToString:@"NILL"])
    {
        
        
        @try {
            
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *imageURL = audioURLArray[indexPath.row];
            
            ////(@"IMAGE URL:%@",audioURLArray[indexPath.row]);
            
            
            NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
            
            
            NSString *date_str1=card_nameArray [indexPath.row];
            
            
            NSString *date_str=[date_str1 stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            
            
            NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            NSString *epub_name=[NSString stringWithFormat:@"%@.m4a",date_str];
            NSString* foofile = [documentsPath stringByAppendingPathComponent:epub_name];
            BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:foofile];
            
            
            if (fileExists)
            {
                ////(@"EXISTS");
                
                [audioFilesArray replaceObjectAtIndex:indexPath.row withObject:date_str];
                
                [self AudioPlay:self];
                
            }
            else
            {
                
                
                
                
                
                NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                NSURL *url=[NSURL URLWithString:imageURL];
                
                _nextAudio.enabled=NO;
                _prevAudio.enabled=NO;
                ////(@"Dowload Start");
                
                ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
                
                
                
                
                //  [collectionView reloadData];
                
                [request setShouldContinueWhenAppEntersBackground:YES];
                
                
                [request setCompletionBlock:^{
                    // Use when fetching text data
                    
                    NSString *responseString = [request responseString];
                    
                    
                    
                    // Use when fetching binary data
                    NSData *responseData = [request responseData];
                    
                    NSString *epub_name=[NSString stringWithFormat:@"%@.m4a",date_str];
                    
                    
                    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:epub_name];
                    
                    [responseData writeToFile:filePath atomically:YES];
                    
                    ////(@"Dowload Completes");
                    
                    _nextAudio.enabled=YES;
                    _prevAudio.enabled=YES;
                    [audioFilesArray replaceObjectAtIndex:indexPath.row withObject:date_str];
                    
                    [self AudioPlay:self];
                    
                    ////(@"AUDIO:%@",audioFilesArray);
                    
                    
                }];
                [request setFailedBlock:^
                 {
                     // NSError *error = [request error];
                     
                     _nextAudio.enabled=YES;
                     _prevAudio.enabled=YES;
                     UIAlertView *error_Alert=[[UIAlertView alloc]initWithTitle:@"Download Error" message:[NSString stringWithFormat:@"%@",[request error]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                     
                     [error_Alert show];
                     
                     
                 }];
                [request startAsynchronous];
                
                
                
            }
        }
        @catch (NSException *exception)
        {
            ////(@"Catched");
        }
     
     
        
 
    }
    else
    {
        @try {
               [self AudioPlay:self];
        }
        @catch (NSException *exception)
        {
            ////(@"CATCHED");
        }
             
    }
    
   
    
    
   
    
   
    
    //NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:@"savedImage.jpg"];
    
    
    //[self AudioPlay:self];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    return 24;
    
}

-(IBAction)sendCardButton
{
    if (IS_IPHONE_5)
    {
        albumViewController *viewController=[[albumViewController alloc]initWithNibName:@"albumViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
    
    albumViewController *viewController=[[albumViewController alloc]initWithNibName:@"albumViewController~iPhone" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        //////(@"SUCCESS");
    }
     ];
        
    }
}


-(IBAction)meButton
{
    if (IS_IPHONE_5) {
        MeViewController *viewController=[[MeViewController alloc]initWithNibName:@"MeViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        
        MeViewController *viewController=[[MeViewController alloc]initWithNibName:@"MeViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }

}
-(IBAction)dealsButton
{
    dealsViewController *viewController=[[dealsViewController alloc]initWithNibName:@"dealsViewController" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        //////(@"SUCCESS");
    }
     ];
}
-(IBAction)socialButton
{
    if (IS_IPHONE_5) {
        netCoinsViewController *viewController=[[netCoinsViewController alloc]initWithNibName:@"netCoinsViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        netCoinsViewController *viewController=[[netCoinsViewController alloc]initWithNibName:@"netCoinsViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }

}
-(IBAction)netCoinsButton
{
    if (IS_IPHONE_5) {
        socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }

}
-(IBAction)myBusinessButton
{
    myBusinessViewController *viewController=[[myBusinessViewController alloc]initWithNibName:@"myBusinessViewController" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        //////(@"SUCCESS");
    }
     ];
}
#ifdef IOS_OLDER_THAN_6
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return (toInterfaceOrientation == UIInterfaceOrientationMaskPortrait);
}
#endif
#ifdef IOS_NEWER_OR_EQUAL_TO_6
-(BOOL)shouldAutorotate {
    return YES;
}
- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
#endif

@end
