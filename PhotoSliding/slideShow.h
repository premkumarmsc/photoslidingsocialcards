//
//  dealsViewController.h
//  PhotoSliding
//
//  Created by ephronsystems on 6/27/13.
//
//

#import <UIKit/UIKit.h>

@interface slideShow : GAITrackedViewController<UICollectionViewDelegate,UICollectionViewDataSource,AVAudioRecorderDelegate, AVAudioPlayerDelegate>
{
     AVAudioPlayer *audioPlayer;
    
    
    UIScrollView* scrollView;
	UIPageControl* pageControl;
	
	BOOL pageControlBeingUsed;
}


- (void)setupScrollView:(UIScrollView*)scrMain ;
@property (nonatomic, retain) IBOutlet UIScrollView* scrollView;
@property (nonatomic, retain) IBOutlet UIPageControl* pageControl;

- (IBAction)changePage;


@property (weak, nonatomic) IBOutlet UIImageView *front_img;
@property (weak, nonatomic) IBOutlet UIImageView *bage_img;

@property (retain, nonatomic) IBOutlet UIButton *fb_btn;
@property (retain, nonatomic) IBOutlet UIButton *tw_btn;
@property (retain, nonatomic) IBOutlet UIButton *li_btn;

-(IBAction)facebookSubmit;
-(IBAction)linkdinSubmit;
-(IBAction)twitterSubmit;
-(IBAction)b1_Click;
-(IBAction)b2_Click;
-(IBAction)b3_Click;
-(IBAction)b4_Click;



@property (strong, nonatomic) IBOutlet UIButton *playAudio;
- (IBAction)AudioPlay:(id)sender;
@property(retain,nonatomic)IBOutlet UICollectionView *collectionView;
@property (retain, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (weak, nonatomic) IBOutlet UILabel *selectedSegmentLabel;
@property (nonatomic,retain)IBOutlet UITableView *avilableTableView;
@property (retain, nonatomic) IBOutlet UITextField *searchTxtEield;
@property (weak, nonatomic) IBOutlet UIButton *filterButton;
@property (weak, nonatomic) IBOutlet UIView  *detailView;
@property (weak, nonatomic) IBOutlet UIView  *back_view;
@property (retain, nonatomic) IBOutlet UITextField *titleTxtEield;
@property (weak, nonatomic) IBOutlet UITextView *messageText;
@property (weak, nonatomic) IBOutlet UIImageView *cropImage;
@property (weak, nonatomic) IBOutlet UIImageView *senderImage;
@property (retain, nonatomic) IBOutlet UILabel *titleLabel;
@property(retain,nonatomic)IBOutlet UICollectionView *collectionViewNew;
@property (retain, nonatomic)NSString *getCardID;
@property (retain, nonatomic)NSString *getImage;
@property (retain, nonatomic)NSString *getMessage;
@property (retain, nonatomic)NSString *getName;
@property (retain, nonatomic)NSString *getCreated;
@property (retain, nonatomic)NSString *getUpdated;
@property (retain, nonatomic)NSString *getStatus;
@property (retain, nonatomic)NSString *getContact;
@property (retain, nonatomic)NSString *getSmileyIDs;
@property (retain, nonatomic)NSString *getAudioID;
@property (weak, nonatomic) IBOutlet UIView  *cardView1;
@property (weak, nonatomic) IBOutlet UIView  *cardView2;
@property (weak, nonatomic) IBOutlet UIView  *cardView3;
@property (weak, nonatomic) IBOutlet UIView  *cardView4;

@property (weak, nonatomic) IBOutlet UIView  *bottomView;
@property (weak, nonatomic) IBOutlet UIView  *topView;


@property (weak, nonatomic) IBOutlet UILabel *listLabel;

@property (weak, nonatomic) IBOutlet UIView  *backView;


@property (weak, nonatomic) IBOutlet UIView  *slidebackView;

@property (retain, nonatomic)NSString *getViewName;

@property (weak, nonatomic) IBOutlet UIButton  *profileButton;


@property (weak, nonatomic) IBOutlet UIImageView *scrollImageView;

@property (weak, nonatomic) IBOutlet UIImageView *sm1;
@property (weak, nonatomic) IBOutlet UIImageView *sm2;
@property (weak, nonatomic) IBOutlet UIImageView *sm3;


-(IBAction)backButton;
-(IBAction)settingsButton;
- (IBAction)segmentDidChange:(id)sender;
-(IBAction)meButton;
-(IBAction)dealsButton;
-(IBAction)netCoinsButton;
-(IBAction)socialButton;
-(IBAction)myBusinessButton;
-(IBAction)continueButtonClick;
-(IBAction)dealbackButtonClick;
@end
