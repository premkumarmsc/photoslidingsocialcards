//
//  dealsViewController.m
//  PhotoSliding
//
//  Created by ephronsystems on 6/27/13.
//
//

#import "dealsViewController.h"
#import "LibraryCell.h"

@interface dealsViewController ()

@end

@implementation dealsViewController
@synthesize avilableTableView;
@synthesize filterButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _detailView.frame=CGRectMake(0, 100, 320, 385);
    [self.view addSubview:_detailView];
    
    _detailView.hidden=YES;
    
    // Do any additional setup after loading the view from its nib.
}

- (IBAction)segmentDidChange:(id)sender
{
    
    _detailView.hidden=YES;
    _back_view.hidden=NO;
    
    [self updateSelectedSegmentLabel];
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
           LibraryCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            
            NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"LibraryCell" owner:nil options:nil];
            
            for (UIView *view in views)
            {
                if([view isKindOfClass:[UITableViewCell class]])
                {
                    cell = (LibraryCell*)view;
                    //cell.img=@"date.png";
                    
                }
            }
        }
        
        cell.selectionStyle=UITableViewCellSelectionStyleGray;
    
    
    if (self.segmentedControl.selectedSegmentIndex==0)
    {
        cell.reademBtn.hidden=YES;
        cell.climedBtn.hidden=NO;
        
    }
    else
    {
        cell.reademBtn.hidden=NO;
        cell.climedBtn.hidden=YES;
    }
        
       // [cell.img setImageWithURL:[NSURL URLWithString:searchImageArray[indexPath.row]]
                // placeholderImage:[UIImage imageNamed:@"temp.jpg"]];
        
        
        // cell.img.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@",searchImageArray[indexPath.row]]];
       // cell.title.text=searchStringArray[indexPath.row];
       // cell.category.text=searchmetadataArray[indexPath.row];
        return cell;
      
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
   
        return 154;
   
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ////(@"Select %d",indexPath.row);
    
    _detailView.hidden=NO;
    _back_view.hidden=YES;
    
}
-(IBAction)dealbackButtonClick
{
    _detailView.hidden=YES;
    _back_view.hidden=NO;
    
    [avilableTableView reloadData];
}

- (void)updateSelectedSegmentLabel
{
    
   
    
    self.selectedSegmentLabel.font = [UIFont boldSystemFontOfSize:self.selectedSegmentLabel.font.pointSize];
    self.selectedSegmentLabel.text = [NSString stringWithFormat:@"%d", self.segmentedControl.selectedSegmentIndex];
    
    
    if (self.segmentedControl.selectedSegmentIndex==0)
    {
        
          }
    
    if (self.segmentedControl.selectedSegmentIndex==1)
    {
        
          }
    
    [avilableTableView reloadData];
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, .5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^(void)
                   {
                       self.selectedSegmentLabel.font = [UIFont systemFontOfSize:self.selectedSegmentLabel.font.pointSize];
                   });
}
-(IBAction)filterButtonClick
{
    NSArray *menuItems =
    @[
      
      [KxMenuItem menuItem:@"Filter Menu"
                     image:nil
                    target:nil
                    action:NULL],
      
      [KxMenuItem menuItem:@"All Deals"
                     image:[UIImage imageNamed:@"check_icon"]
                    target:self
                    action:@selector(pushMenuItem:)],
      
      [KxMenuItem menuItem:@"Populer"
                     image:[UIImage imageNamed:@"UIButton"]
                    target:self
                    action:@selector(pushMenuItem:)],
      
      [KxMenuItem menuItem:@"Nearest"
                     image:[UIImage imageNamed:@"UIButton"]
                    target:self
                    action:@selector(pushMenuItem:)],
      
      [KxMenuItem menuItem:@"Price"
                     image:[UIImage imageNamed:@"UIButton"]
                    target:self
                    action:@selector(pushMenuItem:)],
      
      [KxMenuItem menuItem:@"Selling"
                     image:[UIImage imageNamed:@"UIButton"]
                    target:self
                    action:@selector(pushMenuItem:)],
      ];
    
    KxMenuItem *first = menuItems[0];
    first.foreColor = [UIColor colorWithRed:47/255.0f green:112/255.0f blue:225/255.0f alpha:1.0];
    first.alignment = NSTextAlignmentCenter;
    
    [KxMenu showMenuInView:self.view
                  fromRect:CGRectMake(266, 75, 50, 50)
                 menuItems:menuItems];
}
- (void) pushMenuItem:(id)sender
{
    ////(@"%@", sender);
    NSString *string=[NSString stringWithFormat:@"%@",sender];
    ////(@"HELLO:%@",string);
    
    
    
    
    
}
-(IBAction)meButton
{
    if (IS_IPHONE_5) {
        MeViewController *viewController=[[MeViewController alloc]initWithNibName:@"MeViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        
        MeViewController *viewController=[[MeViewController alloc]initWithNibName:@"MeViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }

}
-(IBAction)dealsButton
{
    dealsViewController *viewController=[[dealsViewController alloc]initWithNibName:@"dealsViewController" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        //////(@"SUCCESS");
    }
     ];
}
-(IBAction)socialButton
{
    if (IS_IPHONE_5) {
        netCoinsViewController *viewController=[[netCoinsViewController alloc]initWithNibName:@"netCoinsViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        netCoinsViewController *viewController=[[netCoinsViewController alloc]initWithNibName:@"netCoinsViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }

}
-(IBAction)netCoinsButton
{
    socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        //////(@"SUCCESS");
    }
     ];
}
-(IBAction)myBusinessButton
{
    myBusinessViewController *viewController=[[myBusinessViewController alloc]initWithNibName:@"myBusinessViewController" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        //////(@"SUCCESS");
    }
     ];
}
- (BOOL)textFieldShouldReturn: (UITextField *)textField
{
  
    [textField resignFirstResponder];
    
    return YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
