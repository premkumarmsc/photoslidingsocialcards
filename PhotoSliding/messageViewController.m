//
//  dealsViewController.m
//  PhotoSliding
//
//  Created by ephronsystems on 6/27/13.
//
//

#import "messageViewController.h"
#import "receiptentCell.h"
#import "Image_cell.h"
#import "SmilyCell.h"

@interface messageViewController ()
{
    AVAudioRecorder *recorder;
    AVAudioPlayer *player;
}


@end

@implementation messageViewController

@synthesize filterButton;
@synthesize collectionView;
@synthesize messageText,cropImage,senderImage;
@synthesize imageURL;
@synthesize comingFrom;
@synthesize collectionViewSmily;
receiptentCell *cell;
SmilyCell *cell1;
@synthesize stopButton, playButton, recordPauseButton;
NSTimer *sliderUpdateTimer;
int time_value;
int check_stop;

NSString *audioURL;

UIAlertView *alertSoundFinish;


NSMutableArray *smilyIDArr;
NSMutableArray *smilyName;
NSMutableArray *smilyCheck;
NSMutableArray* checked_status_arr;
NSMutableArray *selectContactArray1;
NSMutableArray *selectImageArray;
NSMutableString* selectedSmily;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _titleTxtEield.autocapitalizationType = UITextAutocapitalizationTypeWords;
    
        self.trackedViewName = @"Compose Message Screen";
   
    
    _back_textView.hidden=YES;
    
    
    smilyCheck=[[NSMutableArray alloc]init];
     smilyIDArr=[[NSMutableArray alloc]init];
     smilyName=[[NSMutableArray alloc]init];
    checked_status_arr=[[NSMutableArray alloc]init];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(messageMethod:)
     name:UITextFieldTextDidChangeNotification
     object:messageText];
    
     selectContactArray1=[[NSMutableArray alloc]init];
     selectImageArray=[[NSMutableArray alloc]init];
    
      audioURL=@"";
    
    
    
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:@"cropImage.png"];
    UIImage *img = [UIImage imageWithContentsOfFile:getImagePath];
    
    
    
    
    
    NSURL *url1 = [NSURL URLWithString:[NSString stringWithFormat:@"%@/smiley/smileysList",CONFIG_BASE_URL]];
    
    
    
    __block  ASIFormDataRequest *requestmethod = [ASIFormDataRequest requestWithURL:url1];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
   
 
    NSString *cardID=[check objectForKey:@"CARD_ID"];
    
    
    [requestmethod setPostValue:user forKey:@"userID"];
    [requestmethod setPostValue:cardID forKey:@"cardID"];
     [requestmethod setPostValue:[Config get_token] forKey:@"apikey"];
    
    [requestmethod setTimeOutSeconds:30];
    
    
    [requestmethod setCompletionBlock:^{
        NSString *responseString23 = [requestmethod responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
               
       // ////(@"TEMPARY:%@",results11);
        
        
        NSArray *temp= [results11 valueForKeyPath:@"Response.smileys"];
        
        
        ////(@"TEMPARY xyz:%@",temp);
        
        for(NSDictionary *value in temp)
        {
            [smilyIDArr  addObject:[value valueForKey:@"id"]];
            [smilyName  addObject:[value valueForKey:@"smiley_url"]];
            
            
            NSString *checked=[value valueForKey:@"checked_status"];
            
            if ([checked isEqualToString:@""])
            {
                 [smilyCheck addObject:@"NO"];
            }
            else
            {
                [smilyCheck addObject:@"YES"];
            }
            
            [checked_status_arr  addObject:[value valueForKey:@"checked_status"]];
            
            
            ////(@"Sm:%@",smilyName);
            
        }

        
        selectContactArray1=[[NSMutableArray alloc]init];
        selectImageArray=[[NSMutableArray alloc]init];
        
        for (int i=0; i<[smilyCheck count]; i++)
        {
            NSString *check=smilyCheck[i];
            
            if ([check isEqualToString:@"YES"])
            {
                
                [selectContactArray1 addObject:smilyIDArr[i]];
                [selectImageArray addObject:smilyName[i]];
                
            }
        }

        ////(@"Selected Contact :%@",selectContactArray1);
        
        if ([selectContactArray1 count]==1)
        {
            
            
            NSString *temp1=@"";
            [_sm1 setImageWithURL:[NSURL URLWithString:temp1]
                 placeholderImage:nil];
            
            
            NSString *temp2=@"";
            [_sm3 setImageWithURL:[NSURL URLWithString:temp2]
                 placeholderImage:nil];
            
            NSString *temp=[selectImageArray objectAtIndex:0];
            [_sm2 setImageWithURL:[NSURL URLWithString:temp]
                 placeholderImage:nil];
            
        }
        
        if ([selectContactArray1 count]==2)
        {
            
            NSString *temp=[selectImageArray objectAtIndex:0];
            [_sm1 setImageWithURL:[NSURL URLWithString:temp]
                 placeholderImage:nil];
            
            NSString *temp1=[selectImageArray objectAtIndex:1];
            [_sm2 setImageWithURL:[NSURL URLWithString:temp1]
                 placeholderImage:nil];
            
            NSString *temp2=@"";
            [_sm3 setImageWithURL:[NSURL URLWithString:temp2]
                 placeholderImage:nil];
            
            
        }
        
        if ([selectContactArray1 count]==3)
        {
            
            NSString *temp=[selectImageArray objectAtIndex:0];
            [_sm1 setImageWithURL:[NSURL URLWithString:temp]
                 placeholderImage:nil];
            
            NSString *temp1=[selectImageArray objectAtIndex:1];
            [_sm2 setImageWithURL:[NSURL URLWithString:temp1]
                 placeholderImage:nil];
            
            NSString *temp2=[selectImageArray objectAtIndex:2];
            [_sm3 setImageWithURL:[NSURL URLWithString:temp2]
                 placeholderImage:nil];
        }
        
        
        
        selectedSmily = [NSMutableString stringWithCapacity:30];
        
        ////(@"MESSAGEG");
        
        @try {
            
            for (int i=0; i<[selectContactArray1 count]; i++)
            {
                [selectedSmily appendString:[NSString stringWithFormat:@"%@,",selectContactArray1[i]]];
            }
            
            [selectedSmily deleteCharactersInRange:NSMakeRange([selectedSmily length] - 1, 1)];
        }
        @catch (NSException *exception)
        {
            ////(@"CATCHED");
            _sm2.image=[UIImage imageNamed:@""];
        }
        
        
        
        
        ////(@"MESSAGEGHGH:%@",selectedSmily);

       
        /*
        for (int i=0; i<[smilyIDArr count]; i++)
        {
            
           
            
        }
        */
        
        [collectionViewSmily reloadData];
        
     
        
    }];
    [requestmethod setFailedBlock:^{
        NSError *error = [requestmethod error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [requestmethod startAsynchronous];
    
    
    
    
    
    if ([imageURL isEqualToString:@"NULL"])
    {
        _cropImageGetView.image=img;
    }
    else
    {

        
         _cropImageGetView.image=img;
    }
    
   

  
    UIGraphicsBeginImageContextWithOptions(_cropImageGet.bounds.size, _cropImageGet.opaque, 0.0);
    [_cropImageGet.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img_cropped_view = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
  
    
    NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:@"newCropImage.png"];
    // UIImage *image = img.image; // imageView is my image from camera
    
    
    
    NSData *imageData = UIImagePNGRepresentation(_cropImageGetView.image);
    [imageData writeToFile:savedImagePath atomically:NO];


    NSString *getImagePathNew = [documentsDirectory stringByAppendingPathComponent:@"newCropImage.png"];
    UIImage *imgNew = [UIImage imageWithContentsOfFile:getImagePathNew];

    cropImage.image=imgNew;
    
       
    
    
    
    // Disable Stop/Play button when application launches
    [stopButton setEnabled:NO];
    [playButton setEnabled:NO];
    
    playButton.hidden=YES;
    check_stop=0;
    
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(buttonLongPressed:)];
    longPress.minimumPressDuration =.5;
    [_longPress addGestureRecognizer:longPress];
    
    
    
    
    
    
    //[Button2 addGestureRecognizer:longPress];
    
    
    // Set the audio file
    NSArray *pathComponents = [NSArray arrayWithObjects:
                               [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                               @"MyAudioMemo.m4a",
                               nil];
    NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
    
    // Setup audio session
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    
    // Define the recorder setting
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
    
    // Initiate and prepare the recorder
    recorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSetting error:nil];
    recorder.delegate = self;
    recorder.meteringEnabled = YES;
    [recorder prepareToRecord];
    
    
    
    NSUserDefaults *checkVal=[NSUserDefaults standardUserDefaults];
    
    
    NSString *getFrom=[checkVal objectForKey:@"VIEW_REFERENCE"];
    
    NSString *getCardID;
    NSString *getcardname;
    NSString *getImage;
    NSString *getMessage;
    NSString *getSmileyID;
    NSString *getContactIDS;
    
    
    if ([getFrom isEqualToString:@"SAVED_CARDS"])
    {
        getCardID=[checkVal objectForKey:@"CARD_ID_REFERENCE"];
        getcardname=[checkVal objectForKey:@"CARD_NAME_REFERENCE"];
        getImage=[checkVal objectForKey:@"IMAGE_ID_REFERENCE"];
        getMessage=[checkVal objectForKey:@"MESSAGE_REFERENCE"];
        getSmileyID=[checkVal objectForKey:@"SMILEY_REFERENCE"];
        getContactIDS=[checkVal objectForKey:@"CONTACT_ID_REFERENCE"];
        
        
        
        NSString *temp= getImage;
        _titleTxtEield.text=getcardname;
         messageText.text=getMessage;
        _temp_textView.text=getMessage;
       
        if ([getImage isEqualToString:@"NULL"])
        {
            ////(@"ENTER HERE");
        }
        else
        {
            
            ////(@" ENTER ELSE:%@",comingFrom);
            
            if ([comingFrom isEqualToString:@"CROP"])
            {
                NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/files/postfile",CONFIG_BASE_URL]];
                
                __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
                
                
                //[request_post setFile:@"/Users/apple/Desktop/Free-Grammar-Ebook-Level-2.pdf" forKey:@"filename"];
                
                //UIImage * ourImage = [UIImage imageNamed:@"rajini-bday2.jpg"];
                NSData * ourImageData = UIImageJPEGRepresentation(imgNew, 100);
                
                NSDate *date = [NSDate date];
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
                [dateFormat setDateFormat:@"HH:mm:ss zzz"];
                NSString *dateString = [dateFormat stringFromDate:date];
                
                ////(@"DATE STRING:%@",dateString);
                
                NSString *newString = [dateString stringByReplacingOccurrencesOfString:@" " withString:@""];
                NSString *newString1 = [newString stringByReplacingOccurrencesOfString:@":" withString:@""];
                NSString *newString2 = [newString1 stringByReplacingOccurrencesOfString:@"GMT" withString:@""];
                NSString *newString3 = [newString2 stringByReplacingOccurrencesOfString:@"+" withString:@""];
                
                NSString *imageName=[NSString stringWithFormat:@"%@.jpg",newString3];
                
                
                
                [request_post addData:ourImageData withFileName:imageName andContentType:@"image/jpeg" forKey:@"uploadedfile"];
              //  [request_post setPostValue:[Config get_token] forKey:@"apikey"];
                [request_post setTimeOutSeconds:60];
                
                
                [request_post setCompletionBlock:^{
                    // Use when fetching text data
                    NSString *responseString = [request_post responseString];
                    
                    
                    ////(@"RES:%@",responseString);
                    
                    NSMutableData *results11 = [responseString JSONValue];
                    
                    
                    //////(@"RESULTS LOGIN:%@",results11);
                    
                    
                    NSString *temp= [results11 valueForKeyPath:@"photo_url"];
                    
                    
                    ////(@"TEMP LIST:%@",temp);
                    
                    NSUserDefaults *imgtemp=[NSUserDefaults standardUserDefaults];
                    
                    [imgtemp setObject:temp forKey:@"EDIT_IMAGE"];
                    
                    
                    
                    
                    if ([temp length]==0) {
                        
                        NSString *temp= [results11 valueForKeyPath:@"error.text"];
                        
                        
                        NSUserDefaults *imgtemp=[NSUserDefaults standardUserDefaults];
                        
                        [imgtemp setObject:@"NULL" forKey:@"EDIT_IMAGE"];
                        
                        
                        
                    }
                    
                    // Use when fetching binary data
                    // NSData *responseData = [request_post responseData];
                }];
                [request_post setFailedBlock:^{
                    NSError *error = [request_post error];
                    
                    
                    
                    
                }];
                [request_post startAsynchronous];
            }
            else
            {
                [cropImage setImageWithURL:[NSURL URLWithString:getImage]
                          placeholderImage:nil];
            }
            
        
           
            
        }

        
        
        ////(@"TEMP:%@",temp);
        
        NSUserDefaults *imgtemp=[NSUserDefaults standardUserDefaults];
        
        [imgtemp setObject:temp forKey:@"EDIT_IMAGE"];
        
        
        ////(@"SMILEY IDS:%@",getSmileyID);
        
        if (![getSmileyID isEqualToString:@""]) {
            
        
        
        NSArray *lines = [getSmileyID componentsSeparatedByString: @","];
        
       selectContactArray1=[[NSMutableArray alloc]init];
            
           // selectContactArray1=lines;
            
           // ////(@"SELECTED CONTACT:%@",selectContactArray1);
        
        }
        
               
        
    }
       else
       {
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/files/postfile",CONFIG_BASE_URL]];
        
        __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
        
        
        //[request_post setFile:@"/Users/apple/Desktop/Free-Grammar-Ebook-Level-2.pdf" forKey:@"filename"];
        
        //UIImage * ourImage = [UIImage imageNamed:@"rajini-bday2.jpg"];
        NSData * ourImageData = UIImageJPEGRepresentation(imgNew, 100);
        
        NSDate *date = [NSDate date];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
        [dateFormat setDateFormat:@"HH:mm:ss zzz"];
        NSString *dateString = [dateFormat stringFromDate:date];
        
        ////(@"DATE STRING:%@",dateString);
        
        NSString *newString = [dateString stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *newString1 = [newString stringByReplacingOccurrencesOfString:@":" withString:@""];
        NSString *newString2 = [newString1 stringByReplacingOccurrencesOfString:@"GMT" withString:@""];
        NSString *newString3 = [newString2 stringByReplacingOccurrencesOfString:@"+" withString:@""];
        
        NSString *imageName=[NSString stringWithFormat:@"%@.jpg",newString3];
        
        
        
        [request_post addData:ourImageData withFileName:imageName andContentType:@"image/jpeg" forKey:@"uploadedfile"];
       // [request_post setPostValue:[Config get_token] forKey:@"apikey"];
        [request_post setTimeOutSeconds:60];
        
        
        [request_post setCompletionBlock:^{
            // Use when fetching text data
            NSString *responseString = [request_post responseString];
            
            
            ////(@"RES 12:%@",responseString);
            
            NSMutableData *results11 = [responseString JSONValue];
            
            
            //////(@"RESULTS LOGIN:%@",results11);
            
            
            NSString *temp= [results11 valueForKeyPath:@"photo_url"];
            
            
            ////(@"TEMP 324:%@",temp);
            
            NSUserDefaults *imgtemp=[NSUserDefaults standardUserDefaults];
            
            [imgtemp setObject:temp forKey:@"EDIT_IMAGE"];
            
            
            
            
            if ([temp length]==0) {
                
                NSString *temp= [results11 valueForKeyPath:@"error.text"];
                
                
                NSUserDefaults *imgtemp=[NSUserDefaults standardUserDefaults];
                
                [imgtemp setObject:@"NULL" forKey:@"EDIT_IMAGE"];
                
                
                
            }
            
            // Use when fetching binary data
            // NSData *responseData = [request_post responseData];
        }];
        [request_post setFailedBlock:^{
            NSError *error = [request_post error];
            
            
            
            
        }];
        [request_post startAsynchronous];
       }
    
    
    
     [self.collectionViewSmily registerNib:[UINib nibWithNibName:@"SmilyCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
    
}

- (IBAction)messageDidChange:(id)sender
{
   
    
   
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
     ////(@"CHANGE");
    
    
    
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    
    int balance=20-newLength;
    
    
    
    
    if (balance==0)
    {
        
    }
    else
    {
         //////(@"BALANCE:%d",balance);
        
        _titleLength.text=[NSString stringWithFormat:@"%d",balance];
        
        if (balance==-1) {
            
             _titleLength.text=[NSString stringWithFormat:@"0"];
        }
        
    }
    
   
    
    
    return (newLength > 20) ? NO : YES;
}



- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSUInteger newLength = (textView.text.length - range.length) + text.length;
    
    
    int balance=200-newLength;
    
    
    
    
    if (balance==0)
    {
        
    }
    else
    {
       // ////(@"BALANCE:%d",balance);
        
        
        
        _textLength.text=[NSString stringWithFormat:@"* %d characters left",balance];
        
        if (balance==-1||balance<=0)
        {
            
            _textLength.text=[NSString stringWithFormat:@"* 0 characters left"];
        }
        
    }

    
    
    if(newLength <= 200)
    {
        return YES;
    }
    else
    {
        NSUInteger emptySpace = 200 - (textView.text.length - range.length);
        textView.text = [[[textView.text substringToIndex:range.location]
                          stringByAppendingString:[text substringToIndex:emptySpace]]
                         stringByAppendingString:[textView.text substringFromIndex:(range.location + range.length)]];
        return NO;
    }
}



- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    return [smilyIDArr count];
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if ([selectContactArray1 count]==3)
    {
        if ([smilyCheck[indexPath.row]isEqualToString:@"YES"])
        {
            [smilyCheck replaceObjectAtIndex:indexPath.row withObject:@"NO"];
            
            ////(@"CHECK MARK:%@",smilyCheck);
            
            selectContactArray1=[[NSMutableArray alloc]init];
            selectImageArray=[[NSMutableArray alloc]init];
            
            
            
            for (int i=0; i<[smilyCheck count]; i++)
            {
                NSString *check=smilyCheck[i];
                
                if ([check isEqualToString:@"YES"])
                {
                    
                    [selectContactArray1 addObject:smilyIDArr[i]];
                     [selectImageArray addObject:smilyName[i]];
                    
                }
            }

        }
        
    }
    else
    {
        
    
    
        if ([smilyCheck[indexPath.row]isEqualToString:@"YES"])
        {
            [smilyCheck replaceObjectAtIndex:indexPath.row withObject:@"NO"];
            
            ////(@"CHECK MARK:%@",smilyCheck);
        }
        else
        {
            [smilyCheck replaceObjectAtIndex:indexPath.row withObject:@"YES"];
            
            ////(@"CHECK MARK:%@",smilyCheck);
        }
    
    
    selectContactArray1=[[NSMutableArray alloc]init];
    selectImageArray=[[NSMutableArray alloc]init];
    
    for (int i=0; i<[smilyCheck count]; i++)
    {
        NSString *check=smilyCheck[i];
        
        if ([check isEqualToString:@"YES"])
        {
            
            [selectContactArray1 addObject:smilyIDArr[i]];
            [selectImageArray addObject:smilyName[i]];
            
        }
    }
  
    }
    
    ////(@"Selected Contact :%@",selectContactArray1);
    
    if ([selectContactArray1 count]==1)
    {
        
        
        NSString *temp1=@"";
        [_sm1 setImageWithURL:[NSURL URLWithString:temp1]
             placeholderImage:nil];
        
        
        NSString *temp2=@"";
        [_sm3 setImageWithURL:[NSURL URLWithString:temp2]
             placeholderImage:nil];
        
        NSString *temp=[selectImageArray objectAtIndex:0];
        [_sm2 setImageWithURL:[NSURL URLWithString:temp]
                         placeholderImage:nil];
    
    }
    
    if ([selectContactArray1 count]==2)
    {
        
        NSString *temp=[selectImageArray objectAtIndex:0];
        [_sm1 setImageWithURL:[NSURL URLWithString:temp]
             placeholderImage:nil];
        
        NSString *temp1=[selectImageArray objectAtIndex:1];
        [_sm2 setImageWithURL:[NSURL URLWithString:temp1]
             placeholderImage:nil];
        
        NSString *temp2=@"";
        [_sm3 setImageWithURL:[NSURL URLWithString:temp2]
             placeholderImage:nil];

        
    }
    
    if ([selectContactArray1 count]==3)
    {
        
        NSString *temp=[selectImageArray objectAtIndex:0];
        [_sm1 setImageWithURL:[NSURL URLWithString:temp]
             placeholderImage:nil];
        
        NSString *temp1=[selectImageArray objectAtIndex:1];
        [_sm2 setImageWithURL:[NSURL URLWithString:temp1]
             placeholderImage:nil];
        
        NSString *temp2=[selectImageArray objectAtIndex:2];
        [_sm3 setImageWithURL:[NSURL URLWithString:temp2]
             placeholderImage:nil];
    }

    
    
    selectedSmily = [NSMutableString stringWithCapacity:30];
    
     ////(@"MESSAGEG");
    
    @try {
        
        for (int i=0; i<[selectContactArray1 count]; i++)
        {
            [selectedSmily appendString:[NSString stringWithFormat:@"%@,",selectContactArray1[i]]];
        }
        
        [selectedSmily deleteCharactersInRange:NSMakeRange([selectedSmily length] - 1, 1)];
    }
    @catch (NSException *exception)
    {
        ////(@"CATCHED");
        _sm2.image=[UIImage imageNamed:@""];
    }
   
    
  
    
    ////(@"MESSAGEGHGH:%@",selectedSmily);

    
    
     [collectionViewSmily reloadData];
  

}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    ////(@"Height");
    
    
}
- (SmilyCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
           
        cell1 = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELL" forIndexPath:indexPath];
        
                       
        //cell1.image_view.image=[UIImage imageNamed:[NSString stringWithFormat:@"%d.png",indexPath.row]];
        
    NSString *temp=[smilyName objectAtIndex:indexPath.row];
    [cell1.image_view setImageWithURL:[NSURL URLWithString:temp]
             placeholderImage:nil];

    
    //////(@"SMILY:%@",smilyCheck[indexPath.row]);
    
    
    if ([smilyCheck[indexPath.row]isEqualToString:@"YES"])
    {
        cell1.frameImage.image=[UIImage imageNamed:@"select.png"];
    }
    else
    {
        cell1.frameImage.image=[UIImage imageNamed:@"unselect.png"];
    }

    
    
        return cell1;
        
        
        
    
    
}


-(void)buttonLongPressed:(UILongPressGestureRecognizer *)sender
{
    if(sender.state == UIGestureRecognizerStateBegan)
    {
        ////(@"START");
        playButton.hidden=YES;
        
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setActive:YES error:nil];
        
        // Start recording
        [recorder record];
        [recordPauseButton setTitle:@"Recording...." forState:UIControlStateNormal];
        check_stop=0;
        
        
        time_value=0;
        sliderUpdateTimer= [NSTimer    scheduledTimerWithTimeInterval:1.0    target:self    selector:@selector(fireMethod)    userInfo:nil repeats:YES];
        
    }
    
    if (sender.state==UIGestureRecognizerStateEnded)
    {
        ////(@"ENDED");
        
         _labelButton.text=nil;
        
        [recorder stop];
        check_stop=1;
        [sliderUpdateTimer invalidate];
        
         playButton.hidden=YES;
        time_value=0;
         _labelButton.text=[NSString stringWithFormat:@""];
        
        AVAudioSession *audioSession = [AVAudioSession sharedInstance];
        [audioSession setActive:NO error:nil];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Done"
                                                        message: @"Record Interupted"
                                                       delegate: nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        
        
    }
    
    if (sender.state==UIGestureRecognizerStateRecognized)
    {
        ////(@"REGOGNIZED");
    }
    
    
}

-(void)fireMethod
{
    
    time_value=time_value+1;
    
    ////(@"fireMethod:%d",time_value);
    
    int display_timeValue=12-time_value;
    
    if (check_stop==0) {
         _labelButton.text=[NSString stringWithFormat:@"Don't Release finger for %d seconds",display_timeValue];
    }
   else
   {
        _labelButton.text=[NSString stringWithFormat:@""];
   }
    
    if (time_value==13)
    {
        
        [sliderUpdateTimer invalidate];
        
        
        time_value=0;
        check_stop=1;
        
        [recorder stop];
        playButton.hidden=NO;
        
        _labelButton.text=[NSString stringWithFormat:@""];
        
        AVAudioSession *audioSession = [AVAudioSession sharedInstance];
        [audioSession setActive:NO error:nil];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Done"
                                                        message: @"Finish the recording!"
                                                       delegate: nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    }
}


- (IBAction)tapDetected:(UIGestureRecognizer *)sender {
	// Code to respond to gesture here
    
    ////(@"START");
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)recordPauseTapped:(id)sender
{
    // Stop the audio player before recording
    if (player.playing)
    {
        [player stop];
    }
    
    if (!recorder.recording)
    {
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setActive:YES error:nil];
        
        // Start recording
        [recorder record];
        [recordPauseButton setTitle:@"Pause" forState:UIControlStateNormal];
        
    } else
    {
        
        // Pause recording
        [recorder pause];
        [recordPauseButton setTitle:@"Record" forState:UIControlStateNormal];
    }
    
    [stopButton setEnabled:YES];
    [playButton setEnabled:NO];
}

- (IBAction)stopTapped:(id)sender {
    [recorder stop];
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setActive:NO error:nil];
}
-(IBAction)keyboardButton
{
    
    _back_textView.hidden=YES;
    
    messageText.text=_temp_textView.text;
    
    [messageText resignFirstResponder];
    [_temp_textView resignFirstResponder];
    
    _textLength1.text=_textLength.text;
    
  
     [_titleTxtEield resignFirstResponder];
}
- (IBAction)playTapped:(id)sender
{
    if (!recorder.recording)
    {
        player = [[AVAudioPlayer alloc] initWithContentsOfURL:recorder.url error:nil];
        [player setDelegate:self];
        [player play];
       // [playButton setTitle:@"Stop" forState:UIControlStateNormal];
        
    }
    

}

#pragma mark - AVAudioRecorderDelegate

- (void) audioRecorderDidFinishRecording:(AVAudioRecorder *)avrecorder successfully:(BOOL)flag{
    [recordPauseButton setTitle:@"Record" forState:UIControlStateNormal];
    [stopButton setEnabled:NO];
    [playButton setEnabled:YES];
}

#pragma mark - AVAudioPlayerDelegate

- (void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
   alertSoundFinish = [[UIAlertView alloc] initWithTitle: @"Finish playing the sound!"
                                                    message: @"Do you want to Add this sound to card?"
                                                   delegate: self
                                          cancelButtonTitle:@"No"
                                          otherButtonTitles:@"Yes",nil];
    [alertSoundFinish show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (alertView==alertSoundFinish)
    {
        
        if(buttonIndex == 0)
        {
            
            ////(@"ZERO");
            
            
        }
        else
        {
            ////(@"ONE");
            
            
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            
            NSString *getVideoPathNew = [documentsDirectory stringByAppendingPathComponent:@"MyAudioMemo.m4a"];
            
            
            
            
            
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/files/postfile",CONFIG_BASE_URL]];
            
            
            __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
            
            
            [request_post setFile:getVideoPathNew forKey:@"uploadedfile"];
           //  [request_post setPostValue:[Config get_token] forKey:@"apikey"];
            
            
            [request_post setShowAccurateProgress: YES];
            
            // UPLOAD APP ENTER BACKGROUND //
            [request_post setShouldContinueWhenAppEntersBackground:YES];
            
            
            //[request_post setUploadProgressDelegate:progressBar];
            
            
            
            
            
            [request_post setCompletionBlock:^{
                // Use when fetching text data
                NSString *responseString = [request_post responseString];
                
                
                
                
                ////(@"RES:%@",responseString);
                
                NSMutableData *results11 = [responseString JSONValue];
                
                
                //////(@"RESULTS LOGIN:%@",results11);
                
                
                NSString *temp= [results11 valueForKeyPath:@"photo_url"];
                
                
                
                NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/audios/createAudioSticker",CONFIG_BASE_URL]];
                
                
                
                __block  ASIFormDataRequest *requestmethod = [ASIFormDataRequest requestWithURL:url];
                
                
                
                NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
                NSString *user=[check objectForKey:@"USERID"];
                NSString *cardID=[check objectForKey:@"CARD_ID"];
                NSString *imageID=[check objectForKey:@"IMAGE_ID"];
                NSString *imageSource=[check objectForKey:@"EDIT_IMAGE"];
                ////(@"USER:%@",user);
                
                
                
                [requestmethod setPostValue:user forKey:@"userID"];
                [requestmethod setPostValue:temp forKey:@"audioURL"];
                [requestmethod setPostValue:@"iphone" forKey:@"source"];
                [requestmethod setPostValue:[Config get_token] forKey:@"apikey"];
                
                [requestmethod setTimeOutSeconds:30];
                
                
                [requestmethod setCompletionBlock:^{
                    NSString *responseString23 = [requestmethod responseString];
                    NSMutableData *results11 = [responseString23 JSONValue];
                   
                    
                    
                    ////(@"TEMPARY:%@",results11);
                    
                    NSString *temp1= [results11 valueForKeyPath:@"audioID"];
                    
                    
                    audioURL=temp1;
                                      
                }];
                [requestmethod setFailedBlock:^{
                    NSError *error = [requestmethod error];
                    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                        message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                              cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    
                    
                    
                    
                }];
                
                [requestmethod startAsynchronous];
                
                
                
                
                
                ////(@"Audio URL:%@",temp);;
                
                
            }];
            [request_post setFailedBlock:^{
                NSError *error = [request_post error];
                
                
                //text.text=[NSString stringWithFormat:@"%@",error];
                
            }];
            [request_post startAsynchronous];
        }

    }
    
}



-(IBAction)backButton
{
    
    
    NSUserDefaults *checkVal=[NSUserDefaults standardUserDefaults];
    
    
    NSString *getFrom=[checkVal objectForKey:@"VIEW_REFERENCE"];
    
  
    NSString *getImage;
    
    
    if (IS_IPHONE_5)
    {
        albumViewController *viewController1=[[albumViewController alloc]initWithNibName:@"albumViewController" bundle:nil];
        [self presentViewController:viewController1 animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        albumViewController *viewController1=[[albumViewController alloc]initWithNibName:@"albumViewController~iPhone" bundle:nil];
        [self presentViewController:viewController1 animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }

    
   
    /*
    
    if ([getFrom isEqualToString:@"SAVED_CARDS"])
    {
     
        getImage=[checkVal objectForKey:@"IMAGE_ID_REFERENCE"];
               
        if ([getImage isEqualToString:@"NULL"])
        {
            if (IS_IPHONE_5)
            {
                ViewController1 *viewController1=[[ViewController1 alloc]initWithNibName:@"ViewController_iPhone" bundle:nil];
                [self presentViewController:viewController1 animated:NO completion:^{
                    //////(@"SUCCESS");
                }
                 ];
            }
            else
            {
                ViewController1 *viewController1=[[ViewController1 alloc]initWithNibName:@"ViewController_iPhone~iPhone" bundle:nil];
                [self presentViewController:viewController1 animated:NO completion:^{
                    //////(@"SUCCESS");
                }
                 ];
            }

        }
        else
        {
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *imageURL = getImage;
            
            ////(@"IMAGE URL:%@",getImage);
            
            
            NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
            UIImage *image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imageURL]]];
            
            NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"savedImage.jpg"]];
            if(image != NULL)
            {
                //Store the image in Document
                
                
                NSData *imageData = UIImagePNGRepresentation(image);
                [imageData writeToFile: imagePath atomically:YES];
            }
            
            
            NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:@"savedImage.jpg"];
            UIImage *img = [UIImage imageWithContentsOfFile:getImagePath];
            
            if (IS_IPHONE_5)
            {
                ViewController1 *viewController1=[[ViewController1 alloc]initWithNibName:@"ViewController_iPhone" bundle:nil];
                [self presentViewController:viewController1 animated:NO completion:^{
                    //////(@"SUCCESS");
                }
                 ];
            }
            else
            {
                ViewController1 *viewController1=[[ViewController1 alloc]initWithNibName:@"ViewController_iPhone~iPhone" bundle:nil];
                [self presentViewController:viewController1 animated:NO completion:^{
                    //////(@"SUCCESS");
                }
                 ];
            }

        }
        
    }
    
    else
    {
        if (IS_IPHONE_5)
        {
            ViewController1 *viewController1=[[ViewController1 alloc]initWithNibName:@"ViewController_iPhone" bundle:nil];
            [self presentViewController:viewController1 animated:NO completion:^{
                //////(@"SUCCESS");
            }
             ];
        }
        else
        {
            ViewController1 *viewController1=[[ViewController1 alloc]initWithNibName:@"ViewController_iPhone~iPhone" bundle:nil];
            [self presentViewController:viewController1 animated:NO completion:^{
                //////(@"SUCCESS");
            }
             ];
        }
  
    }
    
    */
    
    
   
    
   
    
    
    
}
- (BOOL)textFieldShouldReturn: (UITextView *)textField
{
    
    [textField resignFirstResponder];
    
    return YES;
}

-(IBAction)messageButton
{
    _back_textView.hidden=NO;
    
    _temp_textView.text=messageText.text;
     _textLength.text=_textLength1.text;
    
    [messageText resignFirstResponder];
    [_temp_textView becomeFirstResponder];
}

-(void) textViewDidBeginEditing:(UITextView *) observationComment
{
    ////(@"ENTER");
    
    
    _back_textView.hidden=NO;
    [messageText resignFirstResponder];
    [_temp_textView becomeFirstResponder];
    
}


-(void) textFieldDidBeginEditing:(UITextField *) observationComment
{
    ////(@"ENTER");
    
    
    _back_textView.hidden=YES;
    
    messageText.text=_temp_textView.text;
    _textLength1.text=_textLength.text;
    
    //[messageText resignFirstResponder];
    //[_temp_textView becomeFirstResponder];
    
}


 - (void)handleSingleTap:(UIGestureRecognizer *)gestureRecognizer {
     
     [messageText resignFirstResponder];
 }

-(IBAction)continueButtonClick
{
    
    
    ////(@"MESSAGEGHGH:%@",selectedSmily);
    
    ////(@"AUDIO URL:%@",audioURL);
    
    messageText.text=_temp_textView.text;
    
    
    NSString *smilyStr=@"";
    
    if ([selectedSmily length]==0) {
        
        smilyStr=@"";
    }
    else
    {
        smilyStr=selectedSmily;
    }
    
    
    ////(@"SMILY STR:%@",smilyStr);
    
        if ([messageText.text isEqualToString:@""])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Enter a message. Title is optional" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alert show];

        }
        else
        {
                       
            
           
            
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/socialcards/updateCard",CONFIG_BASE_URL]];
            
        
            
            __block  ASIFormDataRequest *requestmethod = [ASIFormDataRequest requestWithURL:url];
            
            
            
            NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
            NSString *user=[check objectForKey:@"USERID"];
             NSString *cardID=[check objectForKey:@"CARD_ID"];
             NSString *imageID=[check objectForKey:@"IMAGE_ID"];
              NSString *imageSource=[check objectForKey:@"EDIT_IMAGE"];
            ////(@"imageSource:%@",imageSource);
            
            
            
            [requestmethod setPostValue:user forKey:@"userID"];
            [requestmethod setPostValue:cardID forKey:@"id"];
            [requestmethod setPostValue:_titleTxtEield.text forKey:@"card_name"];
            [requestmethod setPostValue:messageText.text forKey:@"message_text"];
             [requestmethod setPostValue:imageID forKey:@"fbphoto_id"];
            [requestmethod setPostValue:imageSource forKey:@"image_reference"];
             [requestmethod setPostValue:imageSource forKey:@"thumbnail_url"];
             [requestmethod setPostValue:smilyStr forKey:@"smiley_ids"];
             [requestmethod setPostValue:[Config get_token] forKey:@"apikey"];
            [requestmethod setTimeOutSeconds:30];
            
            
            [requestmethod setCompletionBlock:^{
                NSString *responseString23 = [requestmethod responseString];
                NSMutableData *results11 = [responseString23 JSONValue];
                
                NSString *cardID= [results11 valueForKeyPath:@"cardID"];
                
                ////(@"CARD_ID");
                
               
                [check setObject:cardID forKey:@"CARD_ID"];
                
                              
                ////(@"TEMPARY: NEWX : %@",results11);
                
                
                
                NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
               
                
                
                [check setObject:cardID forKey:@"CARD_ID_REFERENCE"];
                [check setObject:imageSource forKey:@"IMAGE_ID_REFERENCE"];
                
                [check setObject:_titleTxtEield.text forKey:@"CARD_NAME_REFERENCE"];
                [check setObject:messageText.text forKey:@"MESSAGE_REFERENCE"];
                [check setObject:smilyStr forKey:@"SMILEY_REFERENCE"];
               
                
                [check setObject:@"SAVED_CARDS" forKey:@"VIEW_REFERENCE"];

                
                
                
                if (IS_IPHONE_5)
                {
                    
                    audioViewController *viewController=[[audioViewController alloc]initWithNibName:@"audioViewController" bundle:nil];
                    [self presentViewController:viewController animated:NO completion:^{
                        //////(@"SUCCESS");
                    }
                     ];
                }
                else
                {
                    
                    audioViewController *viewController=[[audioViewController alloc]initWithNibName:@"audioViewController~iPhone" bundle:nil];
                    [self presentViewController:viewController animated:NO completion:^{
                        //////(@"SUCCESS");
                    }
                     ];
                }
            }];
            [requestmethod setFailedBlock:^{
                NSError *error = [requestmethod error];
                UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                    message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                          cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                
                
                
            }];
            
            [requestmethod startAsynchronous];

        }
    
    
    
    
}

-(IBAction)meButton
{
    if (IS_IPHONE_5)
    {
        MeViewController *viewController=[[MeViewController alloc]initWithNibName:@"MeViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        
        MeViewController *viewController=[[MeViewController alloc]initWithNibName:@"MeViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }

}
-(IBAction)dealsButton
{
    dealsViewController *viewController=[[dealsViewController alloc]initWithNibName:@"dealsViewController" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        //////(@"SUCCESS");
    }
     ];
}

/*
 This photo  is now stored in Amazon S3 and can be Downloaded for PDF Processing.*/

-(IBAction)socialButton
{
    if (IS_IPHONE_5) {
        netCoinsViewController *viewController=[[netCoinsViewController alloc]initWithNibName:@"netCoinsViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        netCoinsViewController *viewController=[[netCoinsViewController alloc]initWithNibName:@"netCoinsViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }

}
-(IBAction)netCoinsButton
{
    if (IS_IPHONE_5) {
        socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }

}
-(IBAction)myBusinessButton
{
    myBusinessViewController *viewController=[[myBusinessViewController alloc]initWithNibName:@"myBusinessViewController" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        //////(@"SUCCESS");
    }
     ];
}

#ifdef IOS_OLDER_THAN_6
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return (toInterfaceOrientation == UIInterfaceOrientationMaskPortrait);
}
#endif
#ifdef IOS_NEWER_OR_EQUAL_TO_6
-(BOOL)shouldAutorotate {
    return YES;
}
- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
#endif

@end
