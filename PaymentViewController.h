//
//  ViewController.h
//  HelloStripes
//
//  Created by ephronsystems on 7/10/13.
//  Copyright (c) 2013 EphronSystems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "STPView.h"

@interface PaymentViewController : GAITrackedViewController<STPViewDelegate>
@property STPView* stripeView;

-(IBAction)save:(id)sender;

-(IBAction)cancel:(id)sender;

@property(nonatomic,retain)IBOutlet UIButton *buyButton;

@property(nonatomic,retain)IBOutlet UILabel *productLabel;

@property(nonatomic,retain)NSString *productString;


@property(nonatomic,retain)NSString *comingString;

@property(nonatomic,retain)NSString *amountString;
@property(nonatomic,retain)NSString *currencyString;

@end
