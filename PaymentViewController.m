//
//  ViewController.m
//  HelloStripes
//
//  Created by ephronsystems on 7/10/13.
//  Copyright (c) 2013 EphronSystems. All rights reserved.
//

#import "PaymentViewController.h"
#import "MBProgressHUD.h"

@interface PaymentViewController ()

@end

@implementation PaymentViewController
@synthesize buyButton;
@synthesize productLabel;
@synthesize productString;
@synthesize  amountString,currencyString;
@synthesize comingString;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
     buyButton.hidden=YES;
    
     self.trackedViewName = @"Payment Screen";
    
    productLabel.text=productString;
    
    ////(@"AMOUNT STRING:%@",amountString);
    
    
    
    self.stripeView = [[STPView alloc] initWithFrame:CGRectMake(15,130,290,55)
                                              andKey:STRIPE_KEY];
    self.stripeView.delegate = self;
    [self.view addSubview:self.stripeView];
    
	// Do any additional setup after loading the view, typically from a nib.
}
-(IBAction)cancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)save:(id)sender
{
    // Call 'createToken' when the save button is tapped
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    
    [self.stripeView createToken:^(STPToken *token, NSError *error)
     {
         
        
         
        if (error) {
            // Handle error
             [self handleError:error];
            
             [MBProgressHUD hideHUDForView:self.view animated:YES];
        } else {
            // Send off token to your server
             [self handleToken:token];
        }
    }];}


- (void)handleError:(NSError *)error
{
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error")
                                                      message:[error localizedDescription]
                                                     delegate:nil
                                            cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                            otherButtonTitles:nil];
    [message show];
    
     [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)handleToken:(STPToken *)token
{
    ////(@"Received token %@", token.tokenId);
    
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/redeems/stripeValidation",CONFIG_BASE_URL]];
    
    
    
    __block  ASIFormDataRequest *requestmethod = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
   
    ////(@"USER:%@",user);
    
    
    
    [requestmethod setPostValue:user forKey:@"userID"];
    [requestmethod setPostValue:token.tokenId forKey:@"stripeToken"];
    [requestmethod setPostValue:amountString forKey:@"amount"];
    [requestmethod setPostValue:@"usd" forKey:@"currency"];
     [requestmethod setPostValue:[Config get_token] forKey:@"apikey"];
    
    [requestmethod setTimeOutSeconds:30];
    
    
    [requestmethod setCompletionBlock:^{
        NSString *responseString23 = [requestmethod responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
                
        ////(@"TEMPARY:%@",results11);
        
        
        NSString *message=[results11 valueForKeyPath:@"response.stripe"];
        
        if ([message isEqualToString:@"Success"])
        {
            [self addPaymentInfo];
            
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Success!"
                                                                message:[NSString stringWithFormat:@"Bought NetCoin(s) Successfully"] delegate:self
                                                      cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alertView show];
        }
        else
        {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Payment failed!"
                                                                message:[NSString stringWithFormat:@"Check your Card Informations"] delegate:self
                                                      cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alertView show];
            
            
            if ([comingString isEqualToString:@"SOCIAL"])
            {
                
                if (IS_IPHONE_5) {
                    
                    sendViewController *viewController=[[sendViewController alloc]initWithNibName:@"sendViewController" bundle:nil];
                    [self presentViewController:viewController animated:NO completion:^{
                        //////(@"SUCCESS");
                    }
                     ];
                }
                else
                {
                    
                    sendViewController *viewController=[[sendViewController alloc]initWithNibName:@"sendViewController~iPhone" bundle:nil];
                    [self presentViewController:viewController animated:NO completion:^{
                        //////(@"SUCCESS");
                    }
                     ];
                }
                
            }
            else
            {
                
                if (IS_IPHONE_5) {
                    netCoinsViewController *viewController=[[netCoinsViewController alloc]initWithNibName:@"netCoinsViewController" bundle:nil];
                    [self presentViewController:viewController animated:NO completion:^{
                        //////(@"SUCCESS");
                    }
                     ];
                }
                else
                {
                    netCoinsViewController *viewController=[[netCoinsViewController alloc]initWithNibName:@"netCoinsViewController~iPhone" bundle:nil];
                    [self presentViewController:viewController animated:NO completion:^{
                        //////(@"SUCCESS");
                    }
                     ];
                }
                
            }
  
        }
        
        
        
        
        // [self dismissViewControllerAnimated:YES completion:nil];
              
    }];
    [requestmethod setFailedBlock:^{
        NSError *error = [requestmethod error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [requestmethod startAsynchronous];
     
        
   
    
}


- (void)addPaymentInfo
{
   
    
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/redeems/addPaymentInfo",CONFIG_BASE_URL]];
    
    
    
    __block  ASIFormDataRequest *requestmethod = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    
    ////(@"USER:%@",user);
    
    
    
    [requestmethod setPostValue:user forKey:@"userID"];
    [requestmethod setPostValue:currencyString forKey:@"package_type"];
    [requestmethod setPostValue:amountString forKey:@"paid_amount"];
    [requestmethod setPostValue:@"Success" forKey:@"payment_status"];
     [requestmethod setPostValue:[Config get_token] forKey:@"apikey"];
    
    [requestmethod setTimeOutSeconds:30];
    
    
    [requestmethod setCompletionBlock:^{
        NSString *responseString23 = [requestmethod responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        
        ////(@"TEMPARY:%@",results11);
        
        
        NSString *message=[results11 valueForKeyPath:@"header.statuscode"];
        
        if ([message intValue]==1)
        {
            [self createNetCoinCredit];
        }
        
        
        
        
        
        //[self dismissViewControllerAnimated:YES completion:nil];
        
    }];
    [requestmethod setFailedBlock:^{
        NSError *error = [requestmethod error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [requestmethod startAsynchronous];
    
    
   
    
    
}


- (void)createNetCoinCredit
{
    
    
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/netcoins/createNetCoinCredit",CONFIG_BASE_URL]];
    
    
    
    __block  ASIFormDataRequest *requestmethod = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    
    ////(@"USER:%@",user);
    
    
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"HH:mm:ss zzz"];
    NSString *dateString = [dateFormat stringFromDate:date];
    
    ////(@"DATE STRING:%@",dateString);
    
    
    
    

    NSString *description=[productString stringByReplacingOccurrencesOfString:@"-" withString:@"for"];
    
    NSString *descriptionString=[NSString stringWithFormat:@"Bought %@",description];
    
    
    [requestmethod setPostValue:user forKey:@"snguserid"];
    [requestmethod setPostValue:dateString forKey:@"trx_datetime"];
    [requestmethod setPostValue:@"P" forKey:@"type"];
    [requestmethod setPostValue:currencyString forKey:@"number_of_coins"];
     [requestmethod setPostValue:[Config get_token] forKey:@"apikey"];
    [requestmethod setPostValue:@"" forKey:@"expiry_date"];
    [requestmethod setPostValue:descriptionString forKey:@"description"];
    
    
    [requestmethod setTimeOutSeconds:30];
    
    
    [requestmethod setCompletionBlock:^{
        NSString *responseString23 = [requestmethod responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        
        ////(@"TEMPARY SUCCESS:%@",results11);
        
        
       // NSString *message=[results11 valueForKeyPath:@"header.statuscode"];
        
        //if ([message intValue]==1)
        //{
            //[self createNetCoinCredit];
       // }
        
        [self getNetCoins];
        
        
        
       // [self dismissViewControllerAnimated:YES completion:nil];
        
    }];
    [requestmethod setFailedBlock:^{
        NSError *error = [requestmethod error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [requestmethod startAsynchronous];
    
    
    
    
    
}

#ifdef IOS_OLDER_THAN_6
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return (toInterfaceOrientation == UIInterfaceOrientationMaskPortrait);
}
#endif
#ifdef IOS_NEWER_OR_EQUAL_TO_6
-(BOOL)shouldAutorotate {
    return YES;
}
- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
#endif

-(void)getNetCoins
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/getUserNetCoins",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post1 = [ASIFormDataRequest requestWithURL:url];
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    
    //[request_post setValidatesSecureCertificate:NO];
    [request_post1 setPostValue:user forKey:@"userID"];
     [request_post1 setPostValue:[Config get_token] forKey:@"apikey"];
    
    
    [request_post1 setCompletionBlock:^{
        NSString *responseString = [request_post1 responseString];
        NSMutableData *results1 = [responseString JSONValue];
        
        
        
        
        
        NSString *netCoins=[results1 valueForKeyPath:@"response.totalNetCoins"];
        
        
        ////(@"influenceLevelStr:%@",netCoins);
        
        NSString *netCoinsStr=[NSString stringWithFormat:@"You have a total of %@ NetCoin(s)",netCoins];
        
        ////(@"TOTAL:%@",netCoinsStr);
        
        NSUserDefaults *value=[NSUserDefaults standardUserDefaults];
        
        [value setObject:netCoins forKey:@"TOTAL_COINS"];
        
        
        //_totalNetCoinslabel.text=netCoinsStr;
        
        //  [self getInfluenceLevel:total_scores];
        
         [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        if ([comingString isEqualToString:@"SOCIAL"])
        {
            
            if (IS_IPHONE_5) {
                
                sendViewController *viewController=[[sendViewController alloc]initWithNibName:@"sendViewController" bundle:nil];
                [self presentViewController:viewController animated:NO completion:^{
                    //////(@"SUCCESS");
                }
                 ];
            }
            else
            {
                
                sendViewController *viewController=[[sendViewController alloc]initWithNibName:@"sendViewController~iPhone" bundle:nil];
                [self presentViewController:viewController animated:NO completion:^{
                    //////(@"SUCCESS");
                }
                 ];
            }

        }
        else
        {
            
            if (IS_IPHONE_5) {
                netCoinsViewController *viewController=[[netCoinsViewController alloc]initWithNibName:@"netCoinsViewController" bundle:nil];
                [self presentViewController:viewController animated:NO completion:^{
                    //////(@"SUCCESS");
                }
                 ];
            }
            else
            {
                netCoinsViewController *viewController=[[netCoinsViewController alloc]initWithNibName:@"netCoinsViewController~iPhone" bundle:nil];
                [self presentViewController:viewController animated:NO completion:^{
                    //////(@"SUCCESS");
                }
                 ];
            }

        }
        
       
        
        
        
    }];
    [request_post1 setFailedBlock:^{
        NSError *error = [request_post1 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
   }];
    
    [request_post1 startAsynchronous];
    
    
}


- (void)stripeView:(STPView *)view withCard:(PKCard *)card isValid:(BOOL)valid
{
    // Toggle navigation, for example
    // self.saveButton.enabled = valid;
    
    // self.navigationItem.rightBarButtonItem.enabled = valid;
    
    if (valid)
    {
        buyButton.hidden=NO;
    }
    
    
    
    
}

@end
