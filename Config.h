//
//  Config.h
//  PhotoSliding
//
//  Created by ephronsystems on 8/21/13.
//
//

#import <Foundation/Foundation.h>

@interface Config : NSObject
+(NSString *)get_token;
@end
