//
//  ViewController.m
//  PerfectImageCropperDemo
//
//  Created by Jin Huang on 5/29/13.
//  Copyright (c) 2013 Jin Huang. All rights reserved.
//

#import "ViewController1.h"
#import <QuartzCore/QuartzCore.h>
#import "ImageCropperView.h"

@interface ViewController1 ()

@property (nonatomic, retain) IBOutlet ImageCropperView *cropper;
@property (nonatomic, retain) IBOutlet UIImageView *result;
@property (nonatomic, retain) IBOutlet UIButton *btn;

@end

@implementation ViewController1
@synthesize cropper, result, btn;

UIAlertView *sampleAlert;

#ifdef IOS_OLDER_THAN_6
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return (toInterfaceOrientation == UIInterfaceOrientationMaskPortrait);
}
#endif
#ifdef IOS_NEWER_OR_EQUAL_TO_6
-(BOOL)shouldAutorotate {
    return YES;
}
- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
#endif


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    self.trackedViewName = @"Cropping Screen";
    
    ////(@"HEELLE");
    
    @try
    {
        // Do any additional setup after loading the view, typically from a nib.
       // cropper.layer.borderWidth = 2.0;
        //cropper.layer.borderColor = [UIColor colorWithRed:245/255.0 green:127/255.0 blue:0/255.0 alpha:1].CGColor;
        
       // cropper.layer.borderColor = [UIColor clearColor].CGColor;
        
        [cropper setup];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
        NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:@"savedImage.jpg"];
        UIImage *img = [UIImage imageWithContentsOfFile:getImagePath];
        
        cropper.image = img;
    }
    @catch (NSException *exception) {
        ////(@"EXC:%@",exception);
    }
   
    
	
    
    
    
    
    ////(@"After");
    
    [btn addTarget:self action:@selector(buttonClicked) forControlEvents:UIControlEventTouchUpInside];
}

- (void)buttonClicked
{
    [cropper finishCropping];
    //result.image = cropper.croppedImage;
    
    
    cropper.image=cropper.croppedImage;
    
  //  cropper.hidden = YES;
   // [btn setTitle:@"Back" forState:UIControlStateNormal];
   // [btn setTitle:@"Back" forState:UIControlStateHighlighted];
    
    
    UIImageView *resultView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 230, 305)];
    
    CGSize newSize = resultView.frame.size;
    
    UIImage *oldImage =cropper.croppedImage ;
	UIImage *newImage;
	
    
    //CGRect newFrame;
    //newFrame.size = CGSizeMake(115, 155);

    //newImage = [oldImage imageScaledToFitSize:newFrame.size];
    newImage = [oldImage imageScaledToFitSize:newSize];

    
    UIImageView *img=[[UIImageView alloc]initWithImage:newImage];
    
    
    ////(@"IMG:%@",img.image);
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:@"cropImage.png"];
   // UIImage *image = img.image; // imageView is my image from camera
    
    
    
    NSData *imageData = UIImagePNGRepresentation(newImage);
    [imageData writeToFile:savedImagePath atomically:NO];
    
    
    
     NSUserDefaults *storeImageValues=[NSUserDefaults standardUserDefaults];
    NSString *getFrom=[storeImageValues objectForKey:@"VIEW_REFERENCE"];
  
    
    ////(@"GET FROM:%@",getFrom);
    
    if ([getFrom isEqualToString:@"SAVED_CARDS"])
    {

        if (IS_IPHONE_5) {
            messageViewController *viewController=[[messageViewController alloc]initWithNibName:@"messageViewController" bundle:nil];
            
             viewController.comingFrom=@"CROP";
            
            [self presentViewController:viewController animated:NO completion:^{
                //////(@"SUCCESS");
            }
             ];
        }
        else
        {
            
            messageViewController *viewController=[[messageViewController alloc]initWithNibName:@"messageViewController~iPhone" bundle:nil];
            
             viewController.comingFrom=@"CROP";
            
            [self presentViewController:viewController animated:NO completion:^{
                //////(@"SUCCESS");
            }
             ];
        }

    }
    else
    {
    
    
    
    NSUserDefaults *storeImageValues=[NSUserDefaults standardUserDefaults];
    
    
    
    NSString *imageSource=[storeImageValues objectForKey:@"IMAGE_REFERENCE"];
    NSString *imageThump=[storeImageValues objectForKey:@"THUMPNAIL"];
    NSString *imageID=[storeImageValues objectForKey:@"IMAGE_ID"];
    
    ////(@"S:P%@",imageSource);
    ////(@"S:P%@",imageThump);
    ////(@"S:P%@",imageID);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/socialcards/createCard",CONFIG_BASE_URL]];
    
    //NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/photos/retrieveFacebookPhotos",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *requestmethod = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    
    ////(@"USER:%@",user);
        
        if ([imageSource length]==0||imageSource==[NSNull null])
        {
            imageSource=@"";
            
            ////(@"ENTER");
            
            NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            
                           NSMutableString *randomString = [NSMutableString stringWithCapacity: 10];
                
                for (int i=0; i<10; i++) {
                    [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random() % [letters length]]];
                }
                
                imageID=randomString;
           imageSource=@"http://dev.socialnetgateapp.com/bundles/acmeregistration/images/no-user-image.png";
        }
    
    
    
    [requestmethod setPostValue:user forKey:@"snguserid"];
    [requestmethod setPostValue:imageID forKey:@"fbphoto_id"];
    [requestmethod setPostValue:imageSource forKey:@"image_reference"];
    [requestmethod setPostValue:imageThump forKey:@"thumbnail_url"];
     [requestmethod setPostValue:[Config get_token] forKey:@"apikey"];
    [requestmethod setTimeOutSeconds:30];
    
    
    [requestmethod setCompletionBlock:^{
        NSString *responseString23 = [requestmethod responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        
        
        NSString *cardID= [results11 valueForKeyPath:@"cardID"];
        
        ////(@"CARD_ID:%@",cardID);
        
        NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
        [check setObject:cardID forKey:@"CARD_ID"];
        
         [check setObject:@"NULL" forKey:@"IMAGE_ID_REFERENCE"];
        
        
        
        ////(@"TEMPARY CARD:%@",results11);
        
        
        
        if (IS_IPHONE_5)
        {
            messageViewController *viewController=[[messageViewController alloc]initWithNibName:@"messageViewController" bundle:nil];
            
            
           
            
            [self presentViewController:viewController animated:NO completion:^{
                //////(@"SUCCESS");
            }
             ];
        }
        else
        {
            
            messageViewController *viewController=[[messageViewController alloc]initWithNibName:@"messageViewController~iPhone" bundle:nil];
            
            
            
            [self presentViewController:viewController animated:NO completion:^{
                //////(@"SUCCESS");
            }
             ];
        }

        
        
        
    }];
    [requestmethod setFailedBlock:^{
        NSError *error = [requestmethod error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [requestmethod startAsynchronous];
    
    
    }
    
    
    
    
    
    
    
    if ([btn.currentTitle isEqualToString:@"Crop"]) {
        [cropper finishCropping];
        //result.image = cropper.croppedImage;
        cropper.hidden = YES;
        [btn setTitle:@"Back" forState:UIControlStateNormal];
        [btn setTitle:@"Back" forState:UIControlStateHighlighted];
    }else
    {
        [cropper reset];
        cropper.hidden = NO;
       // [btn setTitle:@"Crop" forState:UIControlStateNormal];
       // [btn setTitle:@"Crop" forState:UIControlStateHighlighted];
        result.image = nil;
    }
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // the user clicked one of the OK/Cancel buttons
    
    if (alertView==sampleAlert)
    {
        if (buttonIndex == 0)
        {
            //just to show its working, i call another alert view
            
          
            
            
        }
        else
        {
            ////(@"SUCCESS");
            
            NSUserDefaults *img=[NSUserDefaults standardUserDefaults];
            
            NSString *img_temp=[img objectForKey:@"UPLOAD_IMG"];
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:img_temp]];
        }

    }
  }



-(IBAction)backButton
{
    if (IS_IPHONE_5)
    {
        albumViewController *viewController=[[albumViewController alloc]initWithNibName:@"albumViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        
        albumViewController *viewController=[[albumViewController alloc]initWithNibName:@"albumViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
        
    }

}

-(IBAction)meButton
{
    if (IS_IPHONE_5) {
        MeViewController *viewController=[[MeViewController alloc]initWithNibName:@"MeViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        
        MeViewController *viewController=[[MeViewController alloc]initWithNibName:@"MeViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }

}
-(IBAction)dealsButton
{
    dealsViewController *viewController=[[dealsViewController alloc]initWithNibName:@"dealsViewController" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        //////(@"SUCCESS");
    }
     ];
}
-(IBAction)socialButton
{
    if (IS_IPHONE_5) {
        netCoinsViewController *viewController=[[netCoinsViewController alloc]initWithNibName:@"netCoinsViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        netCoinsViewController *viewController=[[netCoinsViewController alloc]initWithNibName:@"netCoinsViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }

}
-(IBAction)netCoinsButton
{
    if (IS_IPHONE_5) {
        socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        socialViewController *viewController=[[socialViewController alloc]initWithNibName:@"socialViewController~iPhone" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }

}
-(IBAction)myBusinessButton
{
    myBusinessViewController *viewController=[[myBusinessViewController alloc]initWithNibName:@"myBusinessViewController" bundle:nil];
    [self presentViewController:viewController animated:NO completion:^{
        //////(@"SUCCESS");
    }
     ];
}
@end
